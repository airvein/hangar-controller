from abc import ABCMeta, abstractmethod

import paho.mqtt.client as mqtt

import settings


class MQTTClient(metaclass=ABCMeta):
    def __init__(self, client_id: str):
        """
        Create a MQTT client

        You must subscribe topic/topics in inheriting class __init__ method
        after super() call. To subscribe topic use self.client.subscribe method
        """
        self.client = mqtt.Client(client_id)
        self.client.on_message = self._on_message_callback
        self.client.connect(settings.MQTT_BROKER_ADDRESS,
                            settings.MQTT_BROKER_PORT)
        self.client.loop_start()

    def close_connection(self):
        """Close connection with broker"""
        self.client.loop_stop()
        self.client.disconnect()

    @abstractmethod
    def _on_message_callback(self, client, userdata, message):
        """Income message callback"""

    def _publish_message(self, message: str, topic: str, qos: int = 2):
        """
        Publish message on specific topic

        You can use it inside inheriting class methods to publish message on
        specific topic
        """
        self.client.publish(topic, message, qos)
