import queue
from concurrent.futures.thread import ThreadPoolExecutor
from threading import Thread
from typing import Tuple, Optional

import paho.mqtt.client as mqtt
from satella.coding.structures import Singleton

from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import \
    DriversDiagnostics
from hangar_controller.devices.drivers.hangar_drivers_mqtt_client import \
    HangarDriversMQTTClient
from hangar_controller.devices.drivers.messages.abstract_message_manager import \
    AbstractMessageManager
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_base_shift import \
    CargoBaseShift
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_base_shift_check_conditions import \
    CargoBaseShiftCheckConditions
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_close_window import \
    CargoCloseWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_plc_io import \
    CargoGetPLCIO
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_states import \
    CargoGetStates
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_weight import \
    CargoGetWeight
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_open_window import \
    CargoOpenWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_reset_controller_get_pin import \
    CargoResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_reset_controller_with_pin import \
    CargoResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_reset_plc import \
    CargoResetPLC
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_work_shift import \
    CargoWorkShift
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_work_shift_check_conditions import \
    CargoWorkShiftCheckConditions
from hangar_controller.devices.drivers.messages.commands.climate.get_parameters import \
    ClimateGetParameters
from hangar_controller.devices.drivers.messages.commands.climate.get_plc_io import \
    ClimateGetPLCIO
from hangar_controller.devices.drivers.messages.commands.climate.get_states import \
    ClimateGetStates
from hangar_controller.devices.drivers.messages.commands.climate.get_telemetry import \
    ClimateGetTelemetry
from hangar_controller.devices.drivers.messages.commands.climate.reset_controller_get_pin import \
    ClimateResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.climate.reset_controller_with_pin import \
    ClimateResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.climate.reset_plc import \
    ClimateResetPLC
from hangar_controller.devices.drivers.messages.commands.climate.set_ac import \
    SetAC
from hangar_controller.devices.drivers.messages.commands.climate.set_fan import \
    SetFan
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater import \
    SetHangarHeater
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater_hysteresis import \
    SetHangarHeaterHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater_temperature \
    import SetHangarHeaterTemperature
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_hysteresis import \
    SetHangarHumidityHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_setpoint \
    import SetHangarHumiditySetPoint
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater import \
    SetRoofHeater
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater_hysteresis \
    import SetRoofHeaterHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater_temperature import \
    SetRoofHeaterTemperature
from hangar_controller.devices.drivers.messages.commands.climate.set_type_climate_controller import \
    SetTypeClimateController
from hangar_controller.devices.drivers.messages.commands.commands_manager import \
    CommandsManager
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_chargers_number import \
    MasterChargerGetChargersNumber
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_plc_io import \
    MasterChargerGetPLCIO
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_states import \
    MasterChargerGetStates
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_telemetry import \
    MasterChargerGetTelemetry
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_reset_controller_get_pin import \
    MasterChargerResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_reset_controller_with_pin import \
    MasterChargerResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_reset_plc import \
    MasterChargerResetPLC
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_set_power import \
    MasterChargerSetPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_plc_io import \
    PMSGetPLCIO
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_states import \
    PMSGetStates
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_controller_get_pin import \
    PMSResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_controller_with_pin import \
    PMSResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_plc import \
    PMSResetPLC
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_air_compressor import \
    PMSSetAirCompressor
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_chargers_power import \
    PMSSetChargersPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_irlock_power import \
    PMSSetIRLockPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_lift_power import \
    PMSSetMotorsLiftPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_man_power import \
    PMSSetMotorsManPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_pos_power import \
    PMSSetMotorsPosPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_pressure import \
    PMSSetPressure
from hangar_controller.devices.drivers.messages.commands.roof.roof_close import \
    RoofClose
from hangar_controller.devices.drivers.messages.commands.roof.roof_get_plc_io import \
    RoofGetPLCIO
from hangar_controller.devices.drivers.messages.commands.roof.roof_get_states import \
    RoofGetStates
from hangar_controller.devices.drivers.messages.commands.roof.roof_open import \
    RoofOpen
from hangar_controller.devices.drivers.messages.commands.roof.roof_reset_controller_get_pin import \
    RoofResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.roof.roof_reset_controller_with_pin import \
    RoofResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.roof.roof_reset_plc import \
    RoofResetPLC
from hangar_controller.devices.drivers.messages.commands.roof.roof_stop import \
    RoofStop
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_pin import \
    UserPanelGetPin
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_states import \
    UserPanelGetStates
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_reset_controller_get_pin import \
    UserPanelResetControllerGetPin
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_reset_controller_with_pin import \
    UserPanelResetControllerWithPin
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_set_confirmation_view import \
    UserPanelSetConfirmationView
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_set_view import \
    UserPanelSetView
from hangar_controller.devices.drivers.messages.errors.error_manager import \
    ErrorManager
from hangar_controller.devices.drivers.messages.measurements.measurements_manager import \
    MeasurementsManager
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    pin_topic
from hangar_controller.devices.drivers.messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation
from hangar_controller.devices.drivers.messages.responses.response_manager import \
    ResponseManager
from hangar_controller.devices.drivers.messages.state_readers.state_read_manager import \
    StateReadManager
from hangar_controller.devices.drivers.messages.warnings.warning_manager import \
    WarningManager
from hangar_controller.devices.error_queues import ErrorQueue, EventLogQueue
from hangar_controller.devices.motors.controller.task_queue.task_queue import \
    TaskQueue
from hangar_controller.devices.motors.motors import Motors
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    ConfirmationViews


@Singleton
class DriversController(Thread):
    """
    Driver thread that sends messages to stm controllers.
    Driver is run automatically after starting motors.py
    """

    def __init__(self, error_queue: queue.Queue = None,
                 error_warning_log_queue: queue.Queue = None):
        super().__init__()
        self._error_queue = error_queue
        if error_queue is None:
            self._error_queue = ErrorQueue()
        self._error_warning_log_queue = error_warning_log_queue
        if error_warning_log_queue is None:
           self._error_warning_log_queue = EventLogQueue()

        motors = Motors()
        wobit_diagnostics_reader = motors.wobits.diagnostics_reader
        self._mqtt_client = HangarDriversMQTTClient()
        self.send_message = self._mqtt_client.send_message
        self.message_queue = self._mqtt_client.messages_queue
        self._task_queue = TaskQueue()
        self._diagnostics = DriversDiagnostics()
        self.executor = ThreadPoolExecutor()
        self._measurements_manager = MeasurementsManager(self._diagnostics)
        self._response_manager = ResponseManager(self.executor)
        self._commands_manager = CommandsManager(self._response_manager,
                                                 self.send_message,
                                                 self._error_queue)
        self._state_read_manager = StateReadManager(wobit_diagnostics_reader,
                                                    self.send_message)
        self._error_manager = ErrorManager(self._error_queue,
                                           self._error_warning_log_queue)
        self._warning_manager = WarningManager(self._error_warning_log_queue)
        self.stop_controller = False

    @property
    def diagnostics(self) -> DriversDiagnostics:
        """
        Property returning DriverDiagnostics class that contains all
        measurements sent from STM controllers. __repr__ from DriverDiagnostics
        is a string containing dictionary with format
        'topic': value
        where topic is mqtt topic that message was sent with
        :return: Driver Diagnostics class
        """
        return self._diagnostics

    def stop(self) -> None:
        """
        Stops drivers thread. Puts None on the queue to push it forwards.
        Stops all future responses that were waiting for response.
        """
        self.stop_controller = True
        self.message_queue.put(None)
        self._commands_manager.future_response_manager.stop()
        self._mqtt_client.close_connection()

    def _receive_message(self, message: mqtt.MQTTMessage) -> None:
        """
        This method is called after message arrives from STM controller.
        Message can either be:
        - a response to some command, that is registered in _response_manager
        - measurement, that is registered in _measurement_manager
        - error, that is registered in _error_manager
        - warning, that is registered in _warning_manager
        If message received is not registered, we ignore it.

        Note:
        Checking response needs to be executed before checking measurement
        because get_telemetry will register telemetry topics as responses,
        if we receive telemetry message as a measurement, command get_telemetry
        will raise Timeout Error

        Note2:
        After receiving an error from Drivers, error is put on the error_queue,
        which should be read and decided upon by higher instance

        :param message: message received from STM
        :return: None
        """
        managers = [self._response_manager, self._measurements_manager,
                    self._state_read_manager, self._error_manager,
                    self._warning_manager]

        for manager in managers:
            assert (isinstance(manager, AbstractMessageManager))

            if manager.is_topic_registered(message.topic):
                manager.receive(message)
                break

    def run(self) -> None:
        """
        Starting main thread. This method is already executed in motors.start()
        :return: None
        """
        while not self.stop_controller:
            message = self.message_queue.get()
            if message is None:
                continue

            self._receive_message(message)

    def start_heartbeat(self) -> None:
        """
        Starts heartbeat sender thread that sends heartbeat message to all STMs
        with specified frequency.

        :return: None
        """
        self._commands_manager.heartbeat_sender.start()

    def stop_heartbeat(self) -> None:
        """
        Stops heartbeat sender thread
        """
        self._commands_manager.heartbeat_sender.stop()

    # region climate
    def climate_get_parameters(self) -> CollectiveResponseEvaluation:
        """
        Get parameters from Climate STM controller

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = ClimateGetParameters()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_get_states(self) -> CollectiveResponseEvaluation:
        """
        Get states from Climate STM controller

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = ClimateGetStates()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_get_plc_io(self) -> CollectiveResponseEvaluation:
        """
        Get plc_io values from Climate STM controller

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = ClimateGetPLCIO()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_get_telemetry(self) -> CollectiveResponseEvaluation:
        """
        Get telemetry data from climate STM controller.

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = ClimateGetTelemetry()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_reset_plc(self):
        """
        Reset PLC without pin

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = ClimateResetPLC()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            ClimateResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(ClimateResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = ClimateResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_ac(self, enable: bool) -> CollectiveResponseEvaluation:
        """
        Set AC temperature to value specified

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        ClimateControlTimeouts.AC)
        :param enable: bool parameter: enable if True, disable if False
        :return: evaluation of responses
        """
        enable_command = SetAC.ENABLE if enable else SetAC.DISABLE
        command = SetAC(enable_command)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_fan(self, on: bool) -> CollectiveResponseEvaluation:
        """
        Set Fan to ON/OFF

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        ClimateControlTimeouts.FAN)
        :param on: bool ON if True, OFF if False
        :return: evaluation of responses
        """
        message = SetFan.ENABLE if on else SetFan.DISABLE
        command = SetFan(message)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_hangar_heater(self, on: bool) \
            -> CollectiveResponseEvaluation:
        """
        Set hangar heater ON/OFF

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        ClimateControlTimeouts.HANGAR_HEATER)
        :param on: bool ON if True, OFF if False
        :return: evaluation of responses
        """
        message = SetHangarHeater.ENABLE if on else SetHangarHeater.DISABLE
        command = SetHangarHeater(message)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_hangar_heater_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """
        Set hangar heater hysteresis to temperature value in Kelvins

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param temperature: temperature in Kelvin
        :return: evaluation of responses
        """
        command = SetHangarHeaterHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_hangar_heater_temperature(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """
        Sets hangar heater temperature set point

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param temperature: temperature in Kelvin
        :return: evaluation of responses
        """
        command = SetHangarHeaterTemperature(temperature)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_hangar_humidity_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """
        Sets hangar humidity hysteresis

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param temperature: temperature in Kelvin
        :return: evaluation of responses
        """
        command = SetHangarHumidityHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_hangar_humidity_set_point(self, humidity: float) \
            -> CollectiveResponseEvaluation:
        """
        Sets hangar humidifier set point

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param humidity: humidity set point in %
        :return: evaluation of responses
        """
        command = SetHangarHumiditySetPoint(humidity)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_roof_heater(self, on: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sets roof heater ON/OFF

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        ClimateControlTimeouts.ROOF_HEATER)
        :param on: bool parameter: ON if True, OFF if False
        :return: evaluation of responses
        """
        value = SetRoofHeater.ENABLE if on else SetRoofHeater.DISABLE
        command = SetRoofHeater(value)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_roof_heater_hysteresis(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """
        Sets roof heater hysteresis temperature

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param temperature: temperature in Kelvin
        :return: evaluation of responses
        """
        command = SetRoofHeaterHysteresis(temperature)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def climate_set_roof_heater_temperature(self, temperature: float) \
            -> CollectiveResponseEvaluation:
        """
        Sets roof heater temperature set point

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param temperature: temperature in Kelvin
        :return: evaluation of responses
        """
        command = SetRoofHeaterTemperature(temperature)
        response = self._commands_manager.send_command_and_wait(command
                                                                )
        return response

    def climate_set_type_climate_controller(self, auto: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sets type of climate controller AUTO/MANUAL

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :param auto: bool parameter AUTO if True, MANUAL if False
        :return: evaluation of responses
        """
        value = SetTypeClimateController.AUTO \
            if auto else SetTypeClimateController.MANUAL
        command = SetTypeClimateController(value)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
    # region roof

    def roof_get_states(self) -> CollectiveResponseEvaluation:
        """
        Reads states from STM Roof controller

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = RoofGetStates()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_get_plc_io(self) -> CollectiveResponseEvaluation:
        """
        Reads PLC inputs and outputs values

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = RoofGetPLCIO()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_stop(self) -> CollectiveResponseEvaluation:
        """
        Stops roof if it was opening or closing

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        RoofControlTimeouts.ROOF_STOP)
        :return: evaluation of responses
        """
        command = RoofStop()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_open(self) -> CollectiveResponseEvaluation:
        """
        Opens roof

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        RoofControlTimeouts.ROOF_OPEN)
        :return: evaluation of responses
        """
        command = RoofOpen()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_close(self) -> CollectiveResponseEvaluation:
        """
        Closes roof

        Lock time: max(stm_communication_settings.ACK_TIMEOUT,
                        RoofControlTimeouts.ROOF_CLOSE)
        :return: evaluation of responses
        """
        command = RoofClose()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_reset_plc(self):
        """
        Reset PLC without pin

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = RoofResetPLC()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def roof_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            RoofResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(RoofResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = RoofResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
    # region master_charger
    def master_charger_get_chargers_number(self) \
            -> Tuple[CollectiveResponseEvaluation, int]:
        """
        Get amount of chargers

        Lock time = stm_communication_settings.MULTIPLE_RESPONSE_TIMEOUT
        :return: Tuple of evaluation and amount of chargers
        """
        command = MasterChargerGetChargersNumber()
        response = self._commands_manager.send_command_and_wait(command)
        chargers_response = response.get_response_by_topic(
            MasterChargerGetChargersNumber.STATE_TOPIC)
        chargers_number = int(chargers_response.payload)

        return response, chargers_number

    def master_charger_set_charger_power(self, charger_id: str, enabled: bool) \
            -> CollectiveResponseEvaluation:
        """
        Send message to STM Master Charger specified by charger_id to
        set charger power ON/OFF

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        MasterChargerTimeouts.POWER)
        :param charger_id: string name of charger_id
        :param enabled: bool parameter ON if True, OFF if False
        :return: evaluation of responses
        """
        value = MasterChargerSetPower.ENABLED if enabled \
            else MasterChargerSetPower.DISABLED
        command = MasterChargerSetPower(charger_id, value)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def master_charger_get_states(self, charger_id: str) \
            -> CollectiveResponseEvaluation:
        """
        Reads states from STM Master Charger

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = MasterChargerGetStates(charger_id)
        states = self._commands_manager.send_command_and_wait(command)

        return states

    def master_charger_get_telemetry(self, charger_id: str) \
            -> CollectiveResponseEvaluation:
        """
        Read all telemetry from STM Master Charger

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :param: charger_id: string id of charger
        :return: evaluation of responses
        """
        command = MasterChargerGetTelemetry(charger_id)
        telemetry = self._commands_manager.send_command_and_wait(command)

        return telemetry

    def master_charger_get_plc_io(self) -> CollectiveResponseEvaluation:
        """
        Read PLC inputs and outputs from STM Master Charger

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = MasterChargerGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)

        return plc_io

    def master_charger_reset_plc(self):
        """
        Reset PLC without pin

        Lock time: stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = MasterChargerResetPLC()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def master_charger_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            MasterChargerResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(MasterChargerResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = MasterChargerResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
    # region cargo

    def cargo_open_window(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to open window

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.WINDOW_OPEN)
        :return: evaluation of responses
        """
        command = CargoOpenWindow()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_close_window(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to close window

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.WINDOW_CLOSE)
        :return: evaluation of responses
        """
        command = CargoCloseWindow()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_base_shift(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to set shift to base position

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_BASE)
        :return: evaluation of responses
        """
        command = CargoBaseShift()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_base_shift_check_conditions(self) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to set shift to base position. STM checks
        cargo weight and position before starting

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_BASE_CHECK_COND)
        :return: evaluation of responses
        """
        command = CargoBaseShiftCheckConditions()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_work_shift(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to set shift to work position

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_WORK)
        :return: evaluation of responses
        """
        command = CargoWorkShift()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_work_shift_check_conditions(self) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to set shift to work position. STM
        checks cargo weight and position before starting

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.SHIFT_WORK_CHECK_COND)
        :return: evaluation of responses
        """
        command = CargoWorkShiftCheckConditions()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_get_weight(self) -> Tuple[CollectiveResponseEvaluation,
                                        Optional[float]]:
        """
        Sends message to STM Cargo to get weight of cargo

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        CargoTimeouts.CARGO_WEIGHT)
        :return: evaluation of responses
        """
        command = CargoGetWeight()
        response = self._commands_manager.send_command_and_wait(command)
        weight_response = response.get_response_by_topic(
            CargoGetWeight.STATE_CHANGE_TOPIC)
        weight = weight_response.payload

        if not response.evaluation:
            return response, None

        return response, float(weight)

    def cargo_get_states(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to get states

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = CargoGetStates()
        states = self._commands_manager.send_command_and_wait(command)

        return states

    def cargo_get_plc_io(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Cargo to get plc inputs and outputs

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = CargoGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)

        return plc_io

    def cargo_reset_plc(self):
        """
        Reset PLC

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = CargoResetPLC()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def cargo_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            CargoResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(CargoResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = CargoResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
    # region power_management_system

    def pms_set_chargers_power(self, enable: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        chargers power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.CHARGERS_POWER)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetChargersPower.ENABLE if enable else PMSSetChargersPower.DISABLE
        command = PMSSetChargersPower(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_ir_lock_power(self, enable: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        ir lock power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.IR_LOCK_POWER)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetIRLockPower.ENABLE if enable else PMSSetIRLockPower.DISABLE
        command = PMSSetIRLockPower(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_motors_lift_power(self, enable: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        lift power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.IR_LOCK_POWER)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetMotorsLiftPower.ENABLE if enable \
            else PMSSetMotorsLiftPower.DISABLE
        command = PMSSetMotorsLiftPower(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_motors_man_power(self, enable: bool) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        manipulator motors power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.MOTORS_MAN_POWER)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetMotorsManPower.ENABLE if enable \
            else PMSSetMotorsManPower.DISABLE
        command = PMSSetMotorsManPower(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_motors_pos_power(self,
                                 enable: bool) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        positioning motors power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.MOTORS_POS_POWER)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetMotorsPosPower.ENABLE if enable \
            else PMSSetMotorsPosPower.DISABLE
        command = PMSSetMotorsPosPower(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_pressure(self, enable: bool) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        pressure power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.PRESSURE)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetPressure.ENABLE if enable else PMSSetPressure.DISABLE
        command = PMSSetPressure(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_set_air_compressor(self,
                               enable: bool) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to ENABLE/DISABLE
        pressure power

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        PMSTimeouts.AIR_COMPRESSOR)
        :param enable: bool parameter ENABLE if True, DISABLE if False
        :return: evaluation of responses
        """
        power = PMSSetAirCompressor.ENABLE if enable else PMSSetAirCompressor.DISABLE
        command = PMSSetAirCompressor(power)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_get_states(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to get states

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = PMSGetStates()
        states = self._commands_manager.send_command_and_wait(command)

        return states

    def pms_get_plc_io(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to get PLC inputs
        and outputs

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = PMSGetPLCIO()
        plc_io = self._commands_manager.send_command_and_wait(command)

        return plc_io

    def pms_reset_plc(self) -> CollectiveResponseEvaluation:
        """
        Sends message to STM Power Management System to reset PLC

        Lock time = stm_communication_settings.ACK_TIMEOUT
        :return: evaluation of responses
        """
        command = PMSResetPLC()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def pms_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            PMSResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(PMSResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = PMSResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
    # region user_panel

    def user_panel_get_pin(self) -> Tuple[CollectiveResponseEvaluation, str]:
        """
        Sends message to STM User Panel to change view of the
        screen and ask user for pin. This panel has a timeout

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        UserPanelTimeouts.AUTH_PIN)
        :return: evaluation of responses
        """
        command = UserPanelGetPin()
        response = self._commands_manager.send_command_and_wait(command)
        pin_response = response.get_response_by_topic(
            UserPanelGetPin.PIN_STATE)
        pin = pin_response.payload

        return response, pin

    def user_panel_set_confirmation_view(self, view: ConfirmationViews) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM User Panel to change view of the screen to
        specified one. View has a OK button that user needs to press in
        order to unlock this function.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        timeout_from_view)
        where timeout_from_view is one of timeouts based on the confirmation
        view that is set

        :param view: specific view to change. Select one from ConfirmationViews
        :return: evaluation of responses
        """
        command = UserPanelSetConfirmationView(view)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def user_panel_set_view(self, view: UserPanelViews) \
            -> CollectiveResponseEvaluation:
        """
        Sends message to STM User Panel to change view of the screen to
        specified one.

        Lock time = max(stm_communication_settings.ACK_TIMEOUT,
                        UserPanelTimeouts.VIEW)
        :param view: specific view to change. Select one from UserPanelViews
        :return: evaluation of responses
        """
        command = UserPanelSetView(view)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def user_panel_reset_controller(self) -> CollectiveResponseEvaluation:
        """
        Reset controller using pin request, then sending reset with pin received

        Lock time: stm_communication_settings.ACK_TIMEOUT +
                    stm_communication_settings.PIN_TIMEOUT
        :return: evaluation of responses
        """
        response = self._commands_manager.send_command_and_wait(
            UserPanelResetControllerGetPin())
        pin_response = response.get_response_by_topic(
            pin_topic(UserPanelResetControllerGetPin.TOPIC))

        if not pin_response.evaluation:
            return response

        pin = pin_response.payload
        command = UserPanelResetControllerWithPin(pin)
        response = self._commands_manager.send_command_and_wait(command)

        return response

    def user_panel_get_states(self):
        """
        Get current states (view) of user panel
        """
        command = UserPanelGetStates()
        response = self._commands_manager.send_command_and_wait(command)

        return response

    # endregion
