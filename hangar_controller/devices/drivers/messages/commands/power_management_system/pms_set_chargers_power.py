from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    state_change_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSControlCommands, \
    PMSTimeouts, PMSStates


class PMSSetChargersPower(AbstractCommand):
    ENABLE = EnableDisableResponse.ENABLE
    DISABLE = EnableDisableResponse.DISABLE

    TOPIC = command_topic(Controllers.STM_PMS,
                          PMSControlCommands.TRAFO)

    def __init__(self, value: Union[ENABLE, DISABLE]):
        responses = [
            create_ack_response(PMSSetChargersPower.TOPIC),
            Response(state_change_topic(Controllers.STM_PMS,
                                        PMSStates.TRAFO),
                     PMSTimeouts.CHARGERS_POWER, EnableDisableResponse(value))]

        super().__init__(call_topic(PMSSetChargersPower.TOPIC), responses,
                         value)
