from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSControlCommands, \
    PMSTimeouts, PMSStates


class PMSSetPressure(AbstractCommand):
    ENABLE = EnableDisableResponse.ENABLE
    DISABLE = EnableDisableResponse.DISABLE

    TOPIC = command_topic(Controllers.STM_PMS,
                          PMSControlCommands.PRESSURE)

    def __init__(self, value: Union[ENABLE, DISABLE]):
        ack_response = create_ack_response(PMSSetPressure.TOPIC)
        state_response = Response(
            state_change_topic(Controllers.STM_PMS, PMSStates.PRESSURE),
            PMSTimeouts.PRESSURE,
            EnableDisableResponse(value))

        responses = [ack_response, state_response]

        super().__init__(call_topic(PMSSetPressure.TOPIC), responses,
                         value)
