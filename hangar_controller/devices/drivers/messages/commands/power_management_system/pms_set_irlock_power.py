from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSControlCommands, \
    PMSTimeouts, PMSStates


class PMSSetIRLockPower(AbstractCommand):
    ENABLE = EnableDisableResponse.ENABLE
    DISABLE = EnableDisableResponse.DISABLE

    TOPIC = command_topic(Controllers.STM_PMS,
                          PMSControlCommands.IR_LOCK_POWER)

    def __init__(self, value: Union[ENABLE, DISABLE]):
        ack_response = create_ack_response(PMSSetIRLockPower.TOPIC)
        ir_lock_response = Response(
            state_change_topic(Controllers.STM_PMS, PMSStates.IR_LOCK_POWER),
            PMSTimeouts.IR_LOCK_POWER,
            EnableDisableResponse(value))

        responses = [ack_response, ir_lock_response]

        super().__init__(call_topic(PMSSetIRLockPower.TOPIC), responses, value)
