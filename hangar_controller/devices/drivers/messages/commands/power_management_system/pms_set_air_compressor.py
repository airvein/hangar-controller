from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    state_change_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSControlCommands, \
    PMSTimeouts, PMSStates


class PMSSetAirCompressor(AbstractCommand):
    ENABLE = EnableDisableResponse.ENABLE
    DISABLE = EnableDisableResponse.DISABLE

    TOPIC = command_topic(Controllers.STM_PMS,
                          PMSControlCommands.AIR_COMPRESSOR_POWER)

    def __init__(self, value: Union[ENABLE, DISABLE]):
        responses = [
            create_ack_response(PMSSetAirCompressor.TOPIC),
            Response(state_change_topic(Controllers.STM_PMS,
                                        PMSStates.AIR_COMPRESSOR_POWER),
                     PMSTimeouts.AIR_COMPRESSOR, EnableDisableResponse(value))]

        super().__init__(call_topic(PMSSetAirCompressor.TOPIC), responses,
                         value)
