from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.pin_response import PINResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage
from settingsd.drivers.stm_user_panel_settings import \
    UserPanelControlCommands, \
    UserPanelStates, UserPanelTimeouts, ConfirmationViews


class UserPanelGetPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_USER_PANEL,
                          UserPanelControlCommands.CARGO_PIN)
    VIEW_STATE = state_change_topic(Controllers.STM_USER_PANEL,
                                    UserPanelStates.VIEW)
    PIN_STATE = state_change_topic(Controllers.STM_USER_PANEL,
                                   UserPanelStates.CARGO_PIN)

    def __init__(self):
        ack_response = create_ack_response(UserPanelGetPin.TOPIC)
        view_change = Response(UserPanelGetPin.VIEW_STATE,
                               UserPanelTimeouts.VIEW,
                               ViewResponse(ConfirmationViews.CARGO_PIN))
        pin_response = Response(UserPanelGetPin.PIN_STATE,
                                UserPanelTimeouts.CARGO_PIN,
                                PINResponse())
        responses = [ack_response, view_change, pin_response]

        super().__init__(call_topic(UserPanelGetPin.TOPIC), responses,
                         CommandMessage.GET)
