from hangar_controller.devices.drivers.messages.commands.abstract_command import \
    AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.user_confirmation import \
    UserConfirmation
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from hangar_controller.devices.drivers.messages.responses.response import \
    Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage
from settingsd.drivers.stm_user_panel_settings import \
    UserPanelControlCommands, \
    UserPanelStates, ConfirmationViews, \
    get_confirmation_timeout, UserPanelTimeouts


class UserPanelSetConfirmationView(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_USER_PANEL,
                          UserPanelControlCommands.CONFIRMATION_VIEW)
    CONFIRMATION_STATE_CHANGE = state_change_topic(Controllers.STM_USER_PANEL,
                                                   UserPanelStates.USER_CONFIRMATION)
    # VIEW_STATE_CHANGE = state_change_topic(Controllers.STM_USER_PANEL,
    #                                        UserPanelStates.VIEW)

    def __init__(self, view: ConfirmationViews):
        ack_response = create_ack_response(UserPanelSetConfirmationView.TOPIC)
        # view change has been commented out, because when Home is timeout, we will set it again
        # but stm will net send view state change. Hence we can't wait for it to timeout
        # view_change = Response(UserPanelSetConfirmationView.VIEW_STATE_CHANGE,
        #                        UserPanelTimeouts.VIEW,
        #                        ViewResponse(view))
        timeout = get_confirmation_timeout(view)
        response = Response(UserPanelSetConfirmationView.CONFIRMATION_STATE_CHANGE,
                            timeout, UserConfirmation())
        responses = [ack_response, response]

        super().__init__(call_topic(UserPanelSetConfirmationView.TOPIC),
                         responses,
                         str(view))
