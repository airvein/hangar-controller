from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    response_topic, state_change_topic, call_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_user_panel_settings import \
    UserPanelControlCommands


class UserPanelGetStates(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_USER_PANEL,
                          CommandMessage.GET_STATES)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        state_change_topic(Controllers.STM_USER_PANEL,
                           UserPanelControlCommands.VIEW)
    ]
    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [
            Acknowledgment(),
            ViewResponse(None)
        ]

        responses = create_response_list(UserPanelGetStates.RESPONSE_TOPICS,
                                         UserPanelGetStates.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(UserPanelGetStates.TOPIC), responses,
                         CommandMessage.GET)
