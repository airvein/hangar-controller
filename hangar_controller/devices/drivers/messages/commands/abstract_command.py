import logging
from typing import List, Callable

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage
from hangar_controller.devices.drivers.messages.responses.response import \
    Response


class AbstractCommand(AbstractMessage):
    def __init__(self, topic: str, responses: List[Response], payload: str):
        super().__init__(topic)
        self.responses = responses
        self.payload = payload
        self.log_creation()

    def publish(self, send_message: Callable):
        send_message(self.topic, self.payload)
        logging.info(f'Publishing Command: TOPIC: {self.topic}, '
                     f'PAYLOAD: {self.payload}')

    def log_creation(self):
        logging.debug(f'Creating command with \nTOPIC: {self.topic}, \n'
                      f'RESPONSES: \n{self.responses} \n'
                      f'and PAYLOAD: {self.payload}')
