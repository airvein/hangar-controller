from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, state_change_topic, sub_controller_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_response \
    import BatteryResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class MasterChargerGetStates(AbstractCommand):
    COMMAND = command_topic(Controllers.STM_MASTER_CHARGER,
                            CommandMessage.GET_STATES)

    POWER_STATE = state_change_topic(Controllers.STM_MASTER_CHARGER,
                                     MasterChargerStates.POWER)
    BATTERY_STATE = state_change_topic(Controllers.STM_MASTER_CHARGER,
                                       MasterChargerStates.BATTERY)

    def __init__(self, charger_id: str):
        topic = sub_controller_topic(MasterChargerGetStates.COMMAND,
                                     charger_id)
        power_state_topic = sub_controller_topic(
            MasterChargerGetStates.POWER_STATE, charger_id)
        battery_state_topic = sub_controller_topic(
            MasterChargerGetStates.BATTERY_STATE, charger_id)

        ack_response = create_ack_response(topic)
        power_state_response = Response(power_state_topic,
                                        MULTIPLE_RESPONSE_TIMEOUT,
                                        EnableDisableResponse(None))
        battery_state_response = Response(battery_state_topic,
                                          MULTIPLE_RESPONSE_TIMEOUT,
                                          BatteryResponse(None))

        responses = [ack_response, power_state_response,
                     battery_state_response]

        super().__init__(call_topic(topic), responses, CommandMessage.GET)
