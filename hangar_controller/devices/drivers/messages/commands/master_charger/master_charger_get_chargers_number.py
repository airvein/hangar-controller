from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class MasterChargerGetChargersNumber(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_MASTER_CHARGER,
                          CommandMessage.GET_CHARGERS_NUMBER)
    STATE_TOPIC = state_change_topic(
        Controllers.STM_MASTER_CHARGER,
        MasterChargerStates.CHARGERS_NUMBER)

    def __init__(self):
        ack_response = create_ack_response(
            MasterChargerGetChargersNumber.TOPIC)
        numbers_response = Response(MasterChargerGetChargersNumber.STATE_TOPIC,
                                    MULTIPLE_RESPONSE_TIMEOUT,
                                    ValueResponse())
        responses = [ack_response, numbers_response]
        super().__init__(call_topic(MasterChargerGetChargersNumber.TOPIC),
                         responses, CommandMessage.GET)
