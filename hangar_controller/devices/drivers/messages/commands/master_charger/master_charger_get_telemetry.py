from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, sub_controller_topic, battery_measurement, \
    create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.list_response import ListResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, SubControllers, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    Measurements


class MasterChargerGetTelemetry(AbstractCommand):
    COMMAND = command_topic(Controllers.STM_MASTER_CHARGER,
                            CommandMessage.GET_TELEMETRY)

    def __init__(self, charger_id: str):
        topic = sub_controller_topic(MasterChargerGetTelemetry.COMMAND,
                                     charger_id)
        batteries = SubControllers.BATTERIES
        chargers = SubControllers.CHARGER
        topics = [
            battery_measurement(charger_id, batteries,
                                Measurements.TEMPERATURE),
            battery_measurement(charger_id, batteries, Measurements.UID),
            battery_measurement(charger_id, batteries, Measurements.VOLTAGE),
            battery_measurement(charger_id, chargers,
                                Measurements.BMS_TEMPERATURE),
            battery_measurement(charger_id, chargers, Measurements.CURRENT),
        ]
        timeouts = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(topics)
        expected_messages = [ValueResponse(),
                             *[ListResponse(), ] * 4]
        responses = create_response_list(topics, timeouts, expected_messages)
        ack_response = create_ack_response(topic)
        responses.append(ack_response)

        super().__init__(call_topic(topic), responses, CommandMessage.GET)
