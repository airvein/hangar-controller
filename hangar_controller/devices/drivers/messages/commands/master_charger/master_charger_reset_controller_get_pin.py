from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, pin_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, PIN_TIMEOUT


class MasterChargerResetControllerGetPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_MASTER_CHARGER,
                          CommandMessage.CTRL_RESET)

    def __init__(self):
        responses = [
            Response(pin_topic(MasterChargerResetControllerGetPin.TOPIC),
                     PIN_TIMEOUT,
                     ValueResponse())]
        super().__init__(call_topic(MasterChargerResetControllerGetPin.TOPIC),
                         responses, CommandMessage.RESET)
