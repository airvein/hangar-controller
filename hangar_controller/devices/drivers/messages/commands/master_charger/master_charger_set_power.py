from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, sub_controller_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_response import \
    BatteryResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerControlCommands, MasterChargerTimeouts


class MasterChargerSetPower(AbstractCommand):
    ENABLED = MasterChargerControlCommands.ENABLE
    DISABLED = MasterChargerControlCommands.DISABLE

    COMMAND = command_topic(Controllers.STM_MASTER_CHARGER,
                            MasterChargerControlCommands.POWER)
    POWER_STATE = state_change_topic(Controllers.STM_MASTER_CHARGER,
                                     MasterChargerControlCommands.POWER)
    BATTERY_STATE = state_change_topic(Controllers.STM_MASTER_CHARGER,
                                       MasterChargerControlCommands.BATTERY)

    def __init__(self, charger_id: str, value: Union[ENABLED, DISABLED]):
        topic = sub_controller_topic(MasterChargerSetPower.COMMAND, charger_id)
        power_state_topic = sub_controller_topic(
            MasterChargerSetPower.POWER_STATE, charger_id)
        battery_state_topic = sub_controller_topic(
            MasterChargerSetPower.BATTERY_STATE, charger_id)

        ack_response = create_ack_response(topic)
        power_state_response = Response(power_state_topic,
                                        MasterChargerTimeouts.POWER,
                                        EnableDisableResponse(value))
        charging_response = Response(battery_state_topic,
                                     MasterChargerTimeouts.CHARGING,
                                     BatteryResponse(BatteryResponse.CHARGING))
        charged_response = Response(battery_state_topic,
                                    MasterChargerTimeouts.CHARGED,
                                    BatteryResponse(BatteryResponse.CHARGED))

        responses = [ack_response, power_state_response, charging_response,
                     charged_response]

        super().__init__(call_topic(topic), responses, value)
