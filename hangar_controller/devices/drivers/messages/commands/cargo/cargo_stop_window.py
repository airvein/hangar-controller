from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    state_change_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts
from settingsd.drivers.stm_communication_settings import Controllers


class StopWindow(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.WINDOW)
    STATE_CHANGE_TOPIC = state_change_topic(TOPIC, CargoControlCommands.WINDOW)

    def __init__(self):
        responses = [
            create_ack_response(StopWindow.TOPIC),
            Response(StopWindow.STATE_CHANGE_TOPIC, CargoTimeouts.WINDOW_STOP,
                     WindowResponse(WindowResponse.STOP))
        ]
        super().__init__(call_topic(StopWindow.TOPIC), responses,
                         CargoControlCommands.STOP)
