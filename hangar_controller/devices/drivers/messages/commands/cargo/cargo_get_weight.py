from hangar_controller.devices.drivers.messages.commands.abstract_command import \
    AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import \
    ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import \
    Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class CargoGetWeight(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO,
                          CargoControlCommands.CARGO_WEIGHT)
    STATE_CHANGE_TOPIC = state_change_topic(Controllers.STM_CARGO,
                                            CargoControlCommands.CARGO_WEIGHT)

    def __init__(self):
        ack_response = create_ack_response(CargoGetWeight.TOPIC)
        weight_response = Response(CargoGetWeight.STATE_CHANGE_TOPIC,
                                   CargoTimeouts.CARGO_WEIGHT, ValueResponse())

        responses = [ack_response, weight_response]
        super().__init__(call_topic(CargoGetWeight.TOPIC), responses,
                         CommandMessage.GET)
