from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, plc_io_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.plc_io_response import \
    PLCIOResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, CommandMessage


class CargoGetPLCIO(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CommandMessage.GET_PLC_IO)

    def __init__(self):
        ack_response = create_ack_response(CargoGetPLCIO.TOPIC)
        plc_inputs_response = Response(
            plc_io_topic(Controllers.STM_CARGO, inputs=True),
            ACK_TIMEOUT, PLCIOResponse())
        plc_outputs_response = Response(
            plc_io_topic(Controllers.STM_CARGO, inputs=False),
            ACK_TIMEOUT, PLCIOResponse())

        responses = [ack_response, plc_inputs_response, plc_outputs_response]
        super().__init__(call_topic(CargoGetPLCIO.TOPIC), responses,
                         CommandMessage.GET)
