from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    state_change_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.shift_response import ShiftResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts, CargoStates
from settingsd.drivers.stm_communication_settings import Controllers


class CargoBaseShift(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.SHIFT)

    def __init__(self):
        ack_response = create_ack_response(CargoBaseShift.TOPIC)
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE,
            ShiftResponse(ShiftResponse.IDLE))
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_BASE,
            ShiftResponse(ShiftResponse.BASE))

        responses = [ack_response, idle_response, shift_response]
        super().__init__(call_topic(CargoBaseShift.TOPIC), responses,
                         CargoControlCommands.BASE)
