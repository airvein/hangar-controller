from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import command_topic, \
    state_change_topic, call_topic, response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands, \
    CargoTimeouts
from settingsd.drivers.stm_communication_settings import Controllers


class CargoCloseWindow(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CargoControlCommands.WINDOW)
    STATE_CHANGE_TOPIC = state_change_topic(Controllers.STM_CARGO,
                                            CargoControlCommands.WINDOW)

    def __init__(self):
        responses = [
            Response(response_topic(CargoCloseWindow.TOPIC),
                     CargoTimeouts.WINDOW_ACK,
                     Acknowledgment()),
            Response(CargoCloseWindow.STATE_CHANGE_TOPIC,
                     CargoTimeouts.WINDOW_CLOSE,
                     WindowResponse(WindowResponse.CLOSE))
        ]
        super().__init__(call_topic(CargoCloseWindow.TOPIC), responses,
                         CargoControlCommands.CLOSE)
