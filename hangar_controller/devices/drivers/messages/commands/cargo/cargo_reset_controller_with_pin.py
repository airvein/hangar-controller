from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class CargoResetControllerWithPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CARGO, CommandMessage.CTRL_RESET)

    def __init__(self, pin: str):
        responses = [create_ack_response(CargoResetControllerWithPin.TOPIC)]
        super().__init__(call_topic(CargoResetControllerWithPin.TOPIC),
                         responses, pin)
