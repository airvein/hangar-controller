from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage


class ClimateResetControllerWithPin(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE, CommandMessage.CTRL_RESET)

    def __init__(self, pin: str):
        responses = [create_ack_response(ClimateResetControllerWithPin.TOPIC)]
        super().__init__(call_topic(ClimateResetControllerWithPin.TOPIC),
                         responses, pin)
