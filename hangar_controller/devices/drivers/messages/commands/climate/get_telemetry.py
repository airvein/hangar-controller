from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_humidity import HangarAverageHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_temperature import \
    HangarAverageTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_humidity import \
    HangarLowerHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_temperature import HangarLowerTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_humidity import HangarUpperHumidity
from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_temperature import HangarUpperTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.roof_left_temperature import RoofLeftTemperature
from hangar_controller.devices.drivers.messages.measurements.climate.roof_right_temperature import \
    RoofRightTemperature
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, command_topic, call_topic, response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT


class ClimateGetTelemetry(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE,
                          CommandMessage.GET_PARAMETERS)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        HangarAverageHumidity.TOPIC,
        HangarAverageTemperature.TOPIC,
        HangarLowerHumidity.TOPIC,
        HangarLowerTemperature.TOPIC,
        HangarUpperHumidity.TOPIC,
        HangarUpperTemperature.TOPIC,
        RoofLeftTemperature.TOPIC,
        RoofRightTemperature.TOPIC
    ]

    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [Acknowledgment(),
                             *[ValueResponse(), ] * (len(
                                 ClimateGetTelemetry.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(ClimateGetTelemetry.RESPONSE_TOPICS,
                                         ClimateGetTelemetry.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(ClimateGetTelemetry.TOPIC), responses,
                         CommandMessage.GET)
