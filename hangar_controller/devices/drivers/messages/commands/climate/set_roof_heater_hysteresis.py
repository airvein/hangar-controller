from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, response_topic, set_topic, parameter_topic, \
    temperature_payload
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, Quantity


class SetRoofHeaterHysteresis(AbstractCommand):
    TOPIC = parameter_topic(Controllers.STM_CLIMATE, Quantity.TEMPERATURE,
                            ClimateControlCommands.ROOF_HYSTERESIS)

    RESPONSE_TOPICS = [response_topic(TOPIC)]
    TIMEOUTS = [ACK_TIMEOUT, ]

    def __init__(self, temperature: float):
        """

        :param temperature: hysteresis temperature set point in K
        """
        payload = temperature_payload(temperature)
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetRoofHeaterHysteresis.RESPONSE_TOPICS,
            SetRoofHeaterHysteresis.TIMEOUTS,
            expected_messages)

        super().__init__(set_topic(SetRoofHeaterHysteresis.TOPIC), responses,
                         payload)
