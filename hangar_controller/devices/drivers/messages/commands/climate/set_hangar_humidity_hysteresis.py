from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    set_topic, parameter_topic, \
    humidity_payload
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    Quantity


class SetHangarHumidityHysteresis(AbstractCommand):
    TOPIC = parameter_topic(Controllers.STM_CLIMATE, Quantity.HUMIDITY,
                            ClimateControlCommands.HANGAR_HYSTERESIS)

    def __init__(self, humidity: float):
        """
        :param humidity: float humidity value in % (0-100)
        """
        payload = humidity_payload(humidity)
        responses = [create_ack_response(SetHangarHumidityHysteresis.TOPIC)]

        super().__init__(set_topic(SetHangarHumidityHysteresis.TOPIC),
                         responses, payload)
