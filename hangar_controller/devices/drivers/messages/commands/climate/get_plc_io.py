from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, command_topic, call_topic, plc_io_topic, \
    response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.plc_io_response import \
    PLCIOResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT


class ClimateGetPLCIO(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE, CommandMessage.GET_PLC_IO)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        plc_io_topic(Controllers.STM_CLIMATE, inputs=True),
        plc_io_topic(Controllers.STM_CLIMATE, inputs=False),
    ]

    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [Acknowledgment(),
                             *[PLCIOResponse(), ] * (len(
                                 ClimateGetPLCIO.RESPONSE_TOPICS) - 1)]
        responses = create_response_list(ClimateGetPLCIO.RESPONSE_TOPICS,
                                         ClimateGetPLCIO.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(ClimateGetPLCIO.TOPIC), responses,
                         CommandMessage.GET)
