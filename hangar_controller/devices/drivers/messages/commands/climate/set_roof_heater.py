from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_climate_settings import ClimateControlCommands, \
    ClimateControlTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    ResponseMessage


class SetRoofHeater(AbstractCommand):
    ENABLE = ResponseMessage.ENABLE
    DISABLE = ResponseMessage.DISABLE

    TOPIC = command_topic(Controllers.STM_CLIMATE,
                          ClimateControlCommands.ROOF_HEATER)

    STATE_CHANGE_TOPIC = state_change_topic(Controllers.STM_CLIMATE,
                                            ClimateControlCommands.ROOF_HEATER)

    def __init__(self, enable: Union[ENABLE, DISABLE]):
        """

        :param enable: enable roof heater if TRUE, disable if FALSE
        """
        ack_response = create_ack_response(SetRoofHeater.TOPIC)
        state_change = Response(SetRoofHeater.STATE_CHANGE_TOPIC,
                                ClimateControlTimeouts.ROOF_HEATER,
                                EnableDisableResponse(enable))
        responses = [ack_response, state_change]

        super().__init__(call_topic(SetRoofHeater.TOPIC), responses, enable)
