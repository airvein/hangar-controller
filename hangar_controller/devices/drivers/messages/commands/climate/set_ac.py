from typing import Union

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, command_topic, call_topic, response_topic, \
    state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_climate_settings import ClimateControlCommands, \
    ClimateControlTimeouts
from settingsd.drivers.stm_communication_settings import Controllers, \
    ACK_TIMEOUT, \
    ResponseMessage


class SetAC(AbstractCommand):
    ENABLE = ResponseMessage.ENABLE
    DISABLE = ResponseMessage.DISABLE

    TOPIC = command_topic(Controllers.STM_CLIMATE, ClimateControlCommands.AC)

    RESPONSE_TOPICS = [response_topic(TOPIC),
                       state_change_topic(Controllers.STM_CLIMATE,
                                          ClimateControlCommands.AC)]
    TIMEOUTS = [ACK_TIMEOUT,
                ClimateControlTimeouts.AC]

    def __init__(self, enable: Union[ENABLE, DISABLE]):
        """
        :param enable: sets AC on if SetAC.TRUE and off if SetAC.FALSE
        """
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(enable)]
        responses = create_response_list(SetAC.RESPONSE_TOPICS, SetAC.TIMEOUTS,
                                         expected_messages)

        super().__init__(call_topic(SetAC.TOPIC), responses, enable)
