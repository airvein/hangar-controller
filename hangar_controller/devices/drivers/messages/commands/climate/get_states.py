from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, command_topic, call_topic, response_topic, \
    state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.climate.auto_manual_response import \
    AutoManualResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from settingsd.drivers.stm_climate_settings import ClimateControlCommands
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT


class ClimateGetStates(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_CLIMATE,
                          CommandMessage.GET_STATES)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        state_change_topic(Controllers.STM_CLIMATE,
                           ClimateControlCommands.TYPE),
        state_change_topic(Controllers.STM_CLIMATE, ClimateControlCommands.AC),
        state_change_topic(Controllers.STM_CLIMATE,
                           ClimateControlCommands.HANGAR_HEATER),
        state_change_topic(Controllers.STM_CLIMATE,
                           ClimateControlCommands.ROOF_HEATER),
        state_change_topic(Controllers.STM_CLIMATE,
                           ClimateControlCommands.FAN),
    ]

    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [
            Acknowledgment(),
            AutoManualResponse(positive_messages=None),
            *[EnableDisableResponse(positive_messages=None), ] * 4
        ]
        responses = create_response_list(ClimateGetStates.RESPONSE_TOPICS,
                                         ClimateGetStates.TIMEOUTS,
                                         expected_messages)
        super().__init__(call_topic(ClimateGetStates.TOPIC), responses,
                         CommandMessage.GET)
