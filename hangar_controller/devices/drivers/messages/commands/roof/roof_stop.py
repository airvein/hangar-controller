from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    command_topic, call_topic, state_change_topic, response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_roof_settings import RoofControlCommands, \
    RoofControlTimeouts


class RoofStop(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_ROOF, RoofControlCommands.ROOF)
    STATE_CHANGE = state_change_topic(Controllers.STM_ROOF,
                                      RoofControlCommands.ROOF)

    def __init__(self):
        responses = [
            Response(response_topic(RoofStop.TOPIC),
                     RoofControlTimeouts.ROOF_ACK, Acknowledgment()),
            Response(RoofStop.STATE_CHANGE,
                     RoofControlTimeouts.ROOF_STOP,
                     RoofStateResponse(RoofStateResponse.IDLE))
        ]
        super().__init__(call_topic(RoofStop.TOPIC), responses,
                         RoofControlCommands.STOP)
