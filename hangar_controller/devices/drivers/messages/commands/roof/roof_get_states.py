from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, command_topic, call_topic, response_topic, \
    state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_roof_settings import RoofControlCommands


class RoofGetStates(AbstractCommand):
    TOPIC = command_topic(Controllers.STM_ROOF, CommandMessage.GET_STATES)

    RESPONSE_TOPICS = [
        response_topic(TOPIC),
        state_change_topic(Controllers.STM_ROOF, RoofControlCommands.ROOF),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.SV_OPEN_LEFT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.SV_CLOSE_LEFT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.SV_OPEN_RIGHT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.SV_CLOSE_RIGHT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.CPS_MIN_LEFT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.CPS_MAX_LEFT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.CPS_MIN_RIGHT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.CPS_MAX_RIGHT),
        state_change_topic(Controllers.STM_ROOF,
                           RoofControlCommands.LIGHT_BUZZER),
    ]

    TIMEOUTS = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(RESPONSE_TOPICS)

    def __init__(self):
        expected_messages = [
            Acknowledgment(),
            RoofStateResponse(positive_messages=None),
            *[EnableDisableResponse(positive_messages=None), ] * 9
        ]
        responses = create_response_list(RoofGetStates.RESPONSE_TOPICS,
                                         RoofGetStates.TIMEOUTS,
                                         expected_messages)

        super().__init__(call_topic(RoofGetStates.TOPIC), responses,
                         CommandMessage.GET)
