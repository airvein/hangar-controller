from typing import Optional


class ResponseEvaluation:
    def __init__(self, evaluation: bool, topic: str, payload: str):
        self.evaluation: bool = evaluation
        self.topic: str = topic
        self.payload: str = payload
        self.evaluation_reason: Optional[str] = None

    def __repr__(self):
        out = f'\nTOPIC: {self.topic}, ' \
              f'PAYLOAD: {self.payload}, EVALUATION: {self.evaluation}'
        if not self.evaluation:
            out += f', EVALUATION_REASON: {self.evaluation_reason}'

        return out

    def update_evaluation(self, evaluation: bool):
        self.evaluation = evaluation

    def update_evaluation_reason(self, evaluation_reason: str):
        self.evaluation_reason = evaluation_reason
