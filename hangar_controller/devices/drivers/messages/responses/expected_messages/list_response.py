from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage


class ListResponse(ExpectedMessage):
    def __init__(self):
        super().__init__(positive_messages=None)

    def evaluate_message(self, message: str):
        self.evaluation = True
        if not self.check_message_expected(message):
            self.evaluation = False
            self.evaluation_reason = ResponseMessage.UNEXPECTED_RESPONSE

    def check_message_expected(self, message: str) -> bool:
        try:
            for element in message.split(','):
                float(element)
                return True
        except ValueError:
            return False
