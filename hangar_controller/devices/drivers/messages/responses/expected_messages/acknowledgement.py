from hangar_controller.devices.drivers.messages.message_utils.message_utils import response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import ExpectedMessage
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import ResponseMessage, \
    ACK_TIMEOUT


class Acknowledgment(ExpectedMessage):
    ACK = ResponseMessage.ACK
    WRONG_VALUE = ResponseMessage.WRONG_VALUE
    REFUSED = ResponseMessage.REFUSED

    def __init__(self):
        super().__init__(expected_messages=[Acknowledgment.ACK,
                                            Acknowledgment.WRONG_VALUE,
                                            Acknowledgment.REFUSED],
                         positive_messages=Acknowledgment.ACK)

    def check_message_expected(self, message: str) -> bool:
        """
        Acknowledgment may be sent with number after ack, e.g. 'ack/24',
        suggesting that it it 24th message, hence we need to update evaluation
        :param message:
        :return:
        """
        expected = [exp_message in message
                    for exp_message in self.expected_messages]

        return any(expected)

    def check_message_positive(self, message: str) -> bool:
        """
        Acknowledgment may be sent with number after ack, e.g. 'ack/24',
        suggesting that it it 24th message, hence we need to update evaluation
        :param message:
        :return:
        """
        positive = self.positive_messages in message

        return positive

    def evaluate_message(self, message: str):
        super().evaluate_message(message)

        if self.evaluation:
            return
        elif Acknowledgment.WRONG_VALUE in message:
            self.evaluation_reason = ResponseMessage.WRONG_VALUE
        elif Acknowledgment.REFUSED in message:
            self.evaluation_reason = ResponseMessage.REFUSED


def create_ack_response(topic: str):
    ack_response = Response(response_topic(topic), ACK_TIMEOUT,
                            Acknowledgment())

    return ack_response
