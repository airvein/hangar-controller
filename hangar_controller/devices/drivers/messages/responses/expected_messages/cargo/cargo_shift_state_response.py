from typing import Optional, Union

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import ExpectedMessage
from settingsd.drivers.stm_cargo_settings import CargoControlCommands


class CargoShiftStateResponse(ExpectedMessage):
    WORK = CargoControlCommands.WORK
    BASE = CargoControlCommands.BASE
    IDLE = CargoControlCommands.IDLE
    ERROR = CargoControlCommands.ERROR

    def __init__(self, positive_messages: Optional[Union[WORK, BASE, IDLE,
                                                         ERROR]]):
        super().__init__(positive_messages=positive_messages)
