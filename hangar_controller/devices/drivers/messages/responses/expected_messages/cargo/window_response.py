from typing import Optional, Union

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_cargo_settings import CargoControlCommands


class WindowResponse(ExpectedMessage):
    OPEN = CargoControlCommands.OPEN
    CLOSE = CargoControlCommands.CLOSE
    STOP = CargoControlCommands.STOP
    IDLE = CargoControlCommands.IDLE

    def __init__(self,
                 positive_messages: Optional[Union[OPEN, CLOSE, STOP, IDLE]]):
        super().__init__(positive_messages=positive_messages)
