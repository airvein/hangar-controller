from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage


class HeartbeatResponse(ExpectedMessage):
    TOCK = ResponseMessage.TOCK

    def __init__(self):
        super().__init__(positive_messages=HeartbeatResponse.TOCK)
