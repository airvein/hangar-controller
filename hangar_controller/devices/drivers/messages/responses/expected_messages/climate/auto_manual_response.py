from typing import Optional, Union

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_climate_settings import ClimateControlCommands


class AutoManualResponse(ExpectedMessage):
    AUTO = ClimateControlCommands.AUTO
    MANUAL = ClimateControlCommands.MANUAL

    def __init__(self, positive_messages: Optional[Union[AUTO, MANUAL]]):
        super().__init__(positive_messages=positive_messages)
