from typing import Optional

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_user_panel_settings import UserPanelViews


class ViewResponse(ExpectedMessage):
    VIEWS = [value for (key, value) in UserPanelViews.__dict__.items() if
             not key.startswith('__')]

    def __init__(self, positive_messages: Optional[UserPanelViews]):
        super().__init__(positive_messages=positive_messages)
