from typing import Optional

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_user_panel_settings import UserPanelViews, \
    UserPanelControlCommands


class UserConfirmation(ExpectedMessage):
    BUTTON_PRESSED = UserPanelControlCommands.BUTTON_PRESS_PAYLOAD

    def __init__(self,
                 positive_messages: Optional[UserPanelViews] = BUTTON_PRESSED):
        super().__init__(positive_messages=positive_messages)
