from typing import Optional, Union

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_roof_settings import RoofControlCommands


class RoofStateResponse(ExpectedMessage):
    OPEN = RoofControlCommands.OPEN
    CLOSE = RoofControlCommands.CLOSE
    IDLE = RoofControlCommands.IDLE

    def __init__(self, positive_messages: Optional[Union[OPEN, CLOSE, IDLE]]):
        super().__init__(positive_messages=positive_messages)
