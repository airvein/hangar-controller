from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerStates


class BatteryResponse(ExpectedMessage):
    CHARGING = MasterChargerStates.CHARGING
    CHARGED = MasterChargerStates.CHARGED
    UNCHARGED = MasterChargerStates.UNCHARGED
    UNPLUGGED = MasterChargerStates.UNPLUGGED
    BATTERY_STATES = [CHARGING, CHARGED, UNCHARGED, UNPLUGGED, None]

    def __init__(self, positive_messages):
        assert (positive_messages in BatteryResponse.BATTERY_STATES)
        super().__init__(positive_messages=positive_messages)
