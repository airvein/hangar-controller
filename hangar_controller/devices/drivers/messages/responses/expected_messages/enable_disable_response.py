from typing import Optional, Union

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_communication_settings import CommandMessage


class EnableDisableResponse(ExpectedMessage):
    ENABLE = CommandMessage.ENABLE
    DISABLE = CommandMessage.DISABLE

    def __init__(self, positive_messages: Optional[Union[ENABLE, DISABLE]]):
        super().__init__(positive_messages=positive_messages)
