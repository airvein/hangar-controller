import logging
from typing import Optional

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage
from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import ExpectedMessage
from hangar_controller.devices.drivers.messages.responses.response_evaluation import ResponseEvaluation
from hangar_controller.devices.drivers.messages.responses.response_timer import ResponseTimer
from settingsd.drivers.stm_communication_settings import ResponseMessage


class Response(AbstractMessage):
    def __init__(self, topic: str, timeout: float,
                 expected_message: ExpectedMessage):
        super().__init__(topic)
        self._timeout = timeout
        self._timer = ResponseTimer(timeout)
        self._timer_status = None
        self._expected_message = expected_message
        self._payload: Optional[str] = expected_message.expected_messages
        self._evaluation: Optional[bool] = True
        self._evaluation_reason: Optional[str] = None

    def __eq__(self, other):
        topic_same = super().__eq__(other)
        expected_message_same = self._expected_message.positive_messages == \
                                other.expected_message.positive_messages

        return topic_same and expected_message_same

    def __repr__(self):
        representation = f'\nTOPIC: {self.topic}, ' \
                         f'TIMEOUT: {self._timer.timeout}, ' \
                         f'EXPECTED_MESSAGE: {self._expected_message}'

        return representation

    @property
    def expected_message(self):
        return self._expected_message

    @property
    def timeout(self):
        return self._timeout

    def update_message_with_timer_status(self, message: ResponseEvaluation):
        if not self._timer_status:
            message.evaluation = False
            message.evaluation_reason = ResponseMessage.TIMEOUT

    def update_message_with_evaluation(self, message: ResponseEvaluation):
        if not self._evaluation:
            message.evaluation = False
            message.evaluation_reason = self._evaluation_reason

    def prepare_response(self) -> ResponseEvaluation:
        evaluation = ResponseEvaluation(self._evaluation, self.topic,
                                        self._payload)

        self.update_message_with_timer_status(evaluation)
        self.update_message_with_evaluation(evaluation)

        return evaluation

    def wait_for_message(self) -> ResponseEvaluation:
        # wait until timer.stop() is called -> on self.receive()
        self._timer_status = self._timer.wait()
        response = self.prepare_response()

        return response

    def decode_message(self, message: mqtt.MQTTMessage):
        self._payload = message.payload.decode('UTF-8')

    def evaluate(self, message: mqtt.MQTTMessage):
        self.decode_message(message)
        self._expected_message.evaluate_message(self._payload)
        self._evaluation = self._expected_message.evaluation
        self._evaluation_reason = self._expected_message.evaluation_reason

    def check_if_payload_expected(self, message: mqtt.MQTTMessage) -> bool:
        # check if response is expected and can be unregistered
        payload = message.payload.decode('UTF-8')
        is_expected = self._expected_message.check_message_expected(payload)

        return is_expected

    def check_evaluation(self, message: mqtt.MQTTMessage) -> bool:
        # checking if message has correct payload in case two or more messages
        # are registered with the same topic
        self.decode_message(message)
        evaluation = self._expected_message.check_evaluation(self._payload)

        return evaluation

    def cancel(self):
        self._evaluation = False
        self._evaluation_reason = ResponseMessage.CANCELLED
        self.stop()

    def stop(self):
        self._timer.stop()

    def receive(self, message: mqtt.MQTTMessage) -> Optional[bool]:
        logging.debug(f'Receiving response on TOPIC: {message.topic}, '
                      f'PAYLOAD: {message.payload}')
        self.evaluate(message)
        self._timer.stop()

        return self._evaluation
