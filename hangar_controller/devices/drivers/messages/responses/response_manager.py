from concurrent.futures.thread import ThreadPoolExecutor

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message_manager import \
    AbstractMessageManager
from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand
from hangar_controller.devices.drivers.messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation
from hangar_controller.devices.drivers.messages.responses.future_response import \
    FutureResponse
from hangar_controller.devices.drivers.messages.responses.response import Response


class ResponseManager(AbstractMessageManager):
    def __init__(self, executor: ThreadPoolExecutor):
        super().__init__()
        self.executor = executor

    def create_future_response(self, command: AbstractCommand) \
            -> FutureResponse:
        future_response = FutureResponse(self.executor, command)

        return future_response

    def unregister_timeout_topics(self, evaluation: CollectiveResponseEvaluation):
        cancelled_topics = [resp.topic for resp in evaluation.responses if
                            resp.evaluation_reason in ['timeout', 'cancelled']]
        [self.unregister_message_by_topic(topic) for topic in cancelled_topics]

    def receive(self, message: mqtt.MQTTMessage) -> None:
        # get all messages that could be registered with the same topic
        # e.g. charging battery will send 'charging' and 'charged' state change
        # to the same topic
        responses = self.get_messages_by_topic(message.topic)
        # check which of the responses is the one received
        for possible_response in responses:
            assert (isinstance(possible_response, Response))
            # if evaluation has passed, received message is response found
            if possible_response.check_if_payload_expected(message):
                self.unregister_message(possible_response)
                possible_response.receive(message)
                break
