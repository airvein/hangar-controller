from hangar_controller.devices.drivers.messages.errors.error_message import ErrorMessage
from hangar_controller.devices.drivers.messages.message_utils.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class PMSErrorMessage(ErrorMessage):
    def __init__(self):
        super().__init__(
            error_topic(Controllers.STM_PMS),
            stm_communication_settings.ControllerErrorCodes.STM_PMS)
