import queue

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.errors.controllers.climate_error import ClimateErrorMessage
from hangar_controller.devices.drivers.messages.errors.controllers.master_charger_error import MasterChargerErrorMessage
from hangar_controller.devices.drivers.messages.errors.controllers.pms_error import PMSErrorMessage
from hangar_controller.devices.drivers.messages.errors.controllers.roof_error import RoofErrorMessage
from hangar_controller.devices.drivers.messages.errors.controllers.user_panel_error import UserPanelErrorMessage
from hangar_controller.devices.drivers.messages.errors.error_message import ErrorMessage


class ErrorManager(AbstractMessageManager):
    def __init__(self, error_queue: queue.Queue,
                 error_warning_log_queue: queue.Queue):
        super().__init__()
        self._error_queue = error_queue
        self._error_warning_log_queue = error_warning_log_queue
        self._init_error_messages()

    def _init_error_messages(self):
        error_list = [ClimateErrorMessage(), ClimateErrorMessage(),
                      MasterChargerErrorMessage(), PMSErrorMessage(),
                      RoofErrorMessage(), UserPanelErrorMessage()]
        self.register_messages(error_list)

    def _decide_on_message(self, error: ErrorMessage, payload: str):
        telemetry = error.is_telemetry(payload)
        peripheral = error.is_peripheral(payload)
        if telemetry or peripheral:
            self._error_queue.put(error)

    def receive(self, message: mqtt.MQTTMessage):
        error = self.get_message_by_topic(message.topic)

        assert (isinstance(error, ErrorMessage))
        self._error_warning_log_queue.put(error)
        self._decide_on_message(error, message.payload)
