from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage
from hangar_controller.devices.drivers.messages.errors.error_code import ErrorCodes


class ErrorMessage(AbstractMessage):
    def __init__(self, topic: str, error_code: ErrorCodes):
        super().__init__(topic)
        self.error_code = error_code

    def is_telemetry(self, payload: str):
        telemetry = self.error_code.telemetry in payload

        return telemetry

    def is_peripheral(self, payload: str):
        peripheral = self.error_code.peripheral in payload

        return peripheral
