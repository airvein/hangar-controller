from typing import Optional


class RefusedError(Exception):
    def __init__(self, message: Optional[str] = None):
        super().__init__(message)
