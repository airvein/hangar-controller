from hangar_controller.devices.drivers.messages.measurements.measurement import Measurement
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements


class BatteryVoltage(Measurement):
    def __init__(self, battery_id: str):
        topic = battery_measurement(battery_id, SubControllers.BATTERY,
                                    Measurements.VOLTAGE)

        super().__init__(topic)
