from hangar_controller.devices.drivers.messages.measurements.measurement import Measurement
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    climate_measurement
from settingsd.drivers.stm_climate_settings import Measurements
from settingsd.drivers.stm_communication_settings import Quantity


class RoofLeftTemperature(Measurement):
    TOPIC = climate_measurement(Quantity.TEMPERATURE, Measurements.ROOF_LEFT)

    def __init__(self):
        super().__init__(RoofLeftTemperature.TOPIC)
