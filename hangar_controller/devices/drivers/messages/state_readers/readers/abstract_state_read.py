import logging
from typing import List, Callable

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage
from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand


class AbstractStateRead(AbstractMessage):
    def __init__(self, topic):
        super().__init__(topic)
        self.log_creation()

    def get_responses(self) -> List[AbstractCommand]:
        raise NotImplementedError

    def publish_responses(self, send_message: Callable):
        responses = self.get_responses()
        for response in responses:
            response.publish(send_message)

    def log_creation(self):
        logging.debug(f'Creating state read with \nTOPIC: {self.topic}, \n')
