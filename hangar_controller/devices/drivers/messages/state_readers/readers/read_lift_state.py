from typing import List

from hangar_controller.devices.drivers.messages.state_readers.readers.abstract_state_read import AbstractStateRead
from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    read_state_topic, call_topic, response_topic
from hangar_controller.devices.drivers.messages.state_readers.responses.state_read_response \
    import StateReadResponse
from settingsd.motors import parameters_settings
from settingsd.drivers.wobit_state_read_settings import MotorStateReads

TOPIC = read_state_topic(MotorStateReads.LIFT_POSITION)


class ReadLiftState(AbstractStateRead):
    def __init__(self, wobit_diagnostics_reader: WobitDiagnosticsReader):
        self.wobit_diagnostics_reader = wobit_diagnostics_reader
        super().__init__(call_topic(TOPIC))

    @staticmethod
    def _get_lift_position(wobit_diagnostics_reader: WobitDiagnosticsReader) \
            -> parameters_settings.RelativePositions:
        """
        Get position of lift motor relative to up and down position specified
        in parameters.yaml.
        :return: string 'up', 'down' or 'middle'
        """
        position = wobit_diagnostics_reader.get_lift_position()

        return position

    def get_responses(self) -> List[StateReadResponse]:
        position = self._get_lift_position(self.wobit_diagnostics_reader)
        responses = [StateReadResponse(response_topic(TOPIC), str(position))]

        return responses
