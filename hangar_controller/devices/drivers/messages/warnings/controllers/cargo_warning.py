from hangar_controller.devices.drivers.messages.message_utils.message_utils import warning_topic
from hangar_controller.devices.drivers.messages.warnings.warning_message import WarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class CargoWarningMessage(WarningMessage):
    def __init__(self):
        super().__init__(
            warning_topic(Controllers.STM_CARGO),
            stm_communication_settings.ControllerWarningCodes.STM_CARGO)
