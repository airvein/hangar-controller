import logging
from typing import Callable

from hangar_controller.devices.drivers.messages.commands.abstract_command import \
    AbstractCommand
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, response_topic, command_topic, \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.heartbeat_response import HeartbeatResponse
from settingsd.drivers.stm_communication_settings import Controllers, \
    CommandMessage, Common, HEARTBEAT_TICK_TIME

TOPICS = [
    command_topic(Controllers.STM_CLIMATE, Common.HEARTBEAT),
    command_topic(Controllers.STM_ROOF, Common.HEARTBEAT),
    command_topic(Controllers.STM_CARGO, Common.HEARTBEAT),
    command_topic(Controllers.STM_USER_PANEL, Common.HEARTBEAT),
    command_topic(Controllers.STM_PMS, Common.HEARTBEAT),
    command_topic(Controllers.STM_MASTER_CHARGER, Common.HEARTBEAT),
]
RESP_TOPICS = [response_topic(topic) for topic in TOPICS]
TIMEOUTS = [HEARTBEAT_TICK_TIME, ] * len(RESP_TOPICS)
EXPECTED_MESSAGES = [HeartbeatResponse(), ] * len(RESP_TOPICS)

RESPONSES = create_response_list(RESP_TOPICS, TIMEOUTS,
                                 EXPECTED_MESSAGES)


class Heartbeat(AbstractCommand):
    TICK = CommandMessage.TICK

    def __init__(self):
        # topic in constructor is needed only for publishing. Since we override
        # that method, we can put here any topic
        super().__init__('Heartbeats', RESPONSES, Heartbeat.TICK)

    def publish(self, send_message: Callable):
        # heartbeats need to be sent at the same time
        for topic in TOPICS:
            send_message(call_topic(topic), self.payload)
            logging.info(f'Publishing Command: TOPIC: {call_topic(topic)}, '
                         f'PAYLOAD: {self.payload}')
