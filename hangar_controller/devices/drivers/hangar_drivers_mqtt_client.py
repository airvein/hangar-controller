import queue

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.message_utils.message_utils import get_topic
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_communication_settings import Quantity
from hangar_controller.mqtt_client import MQTTClient


class HangarDriversMQTTClient(MQTTClient):
    def __init__(self, client_id: str = 'drivers'):
        super().__init__(client_id)
        self.messages_queue = queue.Queue()
        self._subscribe()

    def _subscribe(self):
        controllers_topics = [value for topic, value in
                              Controllers.__dict__.items()
                              if not topic.startswith('__')]

        [self.client.subscribe(f'{topic}/#', 2) for topic in controllers_topics]
        self.client.subscribe(f'{get_topic(Quantity.TIME)}')

    def _on_message_callback(self, client, userdata, message: mqtt.MQTTMessage):
        self.messages_queue.put(message)

    def send_message(self, topic: str, message: str):
        self._publish_message(message, topic)
