from threading import Thread, Lock

from hangar_controller.devices.drivers.messages.responses.future_response import FutureResponse


class FutureResponseObserver(Thread):
    # Observer waits until future response is finished or cancelled
    # Then unregisters itself from FutureResponseManager
    def __init__(self, future_response: FutureResponse, unregister_call):
        super().__init__()
        self.future_response = future_response
        self.future_response.add_done_callback(self.done_callback)
        self.unregister_call = unregister_call
        self.lock = Lock()
        self.lock.acquire()

    def done_callback(self, *args):
        self.lock.release()
        self.unregister_call(self)

    def run(self) -> None:
        self.lock.acquire()
