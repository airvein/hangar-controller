class DriversDiagnostics:
    def __init__(self):
        self.values = {}

    def __repr__(self):
        return str(self.values)

    def update(self, **kwargs):
        self.values.update(kwargs)
