from typing import Optional

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.weather_station import modbus_constants


def device_status_parser(response: Optional[list]) -> Optional[dict]:
    """
    Device state:101110111111111 (from left to right)
    1:UV radiation
    0:Snow thickness
    1:True wind speed
    1:True wind direction
    1:Altitude
    0:Visibility
    1:Luminance
    1:Solar radiation
    1:PM1.0/2.5/10
    1:GPS
    1:Precipitation
    1:Compass
    1:Pressure
    1:Relative wind
    1:Temperature and humidity
    :return: dictionary of name: bool
    """
    if response is None:
        return None

    device_status = response[0]
    binary_status = modbus_utils.decimal_to_binary(device_status, 15)
    response = modbus_constants.DEVICE_STATUS_TEMPLATE

    for name, value in zip(modbus_constants.DEVICE_STATUS_NAMES, binary_status):
        if value == '1':
            response[name] = True

    return response


def angle_parser(response: Optional[list]) -> Optional[int]:
    if response is None:
        return None

    return response[0]
