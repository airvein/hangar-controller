from concurrent.futures import ThreadPoolExecutor

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd.weather_station import modbus_constants
from settingsd.weather_station.modbus_constants import HoldingRegisters, RealRegisters


class ValueReader:
    """
    ValueReader is reading specific registers using ModbusIO and returns
    future with value.
    Before initializing, ModbusIO.connect() should be called to ensure that
    connection is present.
    """

    def __init__(self, modbus_io: ModbusIO,
                 thread_pool_executor: ThreadPoolExecutor):
        """
        Thread pool exeturor is used to ensure no tasks will be stuck for
        unknown amount of time. Every function returns future that will be
        returned in higher instance
        :param modbus_io: initialized and connected Modbus_io
        """
        self.executor = thread_pool_executor
        self.modbus_io = modbus_io

    def _read_holding_register(self, address: HoldingRegisters):
        """
        Submit to executor reading holding register
        :param address: address of register
        :return: future
        """
        future = self.executor.submit(
            self.modbus_io.read_holding_registers,
            address
        )

        return future

    def _read_real_register(self, address: RealRegisters):
        """
        Submit to executor reading real register
        :param address: address of first register
        :return: future
        """
        future = self.executor.submit(
            self.modbus_io.read_real_register,
            address
        )

        return future

    def read_register(self, address: modbus_constants.ADDRESS_TYPE):
        """
        Find out if register is holding (one address - 16 bit) or real (two
        addresses - 32 bit)
        :param address: starting address
        :return: future with reading register
        """
        if address in HoldingRegisters.__dict__.values():
            future = self._read_holding_register(address)
        elif address in RealRegisters.__dict__.values():
            future = self._read_real_register(address)
        else:
            raise NotImplementedError('Address is not part of Real or Holding '
                                      'registers')

        return future
