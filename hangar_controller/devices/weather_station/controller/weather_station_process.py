import logging
import queue
import time
from threading import Thread

from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.weather_station.controller.telemetry import Telemetry
from hangar_controller.devices.weather_station.weather_station_controller import \
    WeatherStationController
from settingsd.devices_modbus_connection import WEATHER_STATION_CONNECTION
from settingsd.weather_station import modbus_settings, modbus_constants


class WeatherStationProcess(Thread):
    """
    Weather station updates telemetry class in the while loop until stop is
    called. All errors that are caused by connection are put on the error queue
    that is initialized before the process is created.
    """
    def __init__(self, error_queue: queue.Queue):
        """
        :param error_queue: queue that all errors should be stored in
        """
        super().__init__()
        self._modbus_io = ModbusIOManager().weather_station
        self._internal_error_queue = queue.Queue()
        self._error_queue = error_queue
        self._controller = WeatherStationController(self._modbus_io,
                                                    self._internal_error_queue)
        self._telemetry = Telemetry()
        self._frequency = modbus_settings.TelemetrySettings.FREQUENCY
        self._stop_update = False

    @property
    def telemetry(self) -> Telemetry:
        """
        Get telemetry class that stores all the values read by the weather
        station
        :return: Telemetry class
        """
        return self._telemetry

    def stop(self) -> None:
        logging.debug('Telemetry process stop')
        self._stop_update = True

    def connect(self) -> bool:
        """
        Create modbus connection and check if it's possible to read a single
        register
        :return: bool signalling if connection is present and response is
        correct
        """
        logging.debug('Telemetry process connect')
        connection_status = self._modbus_io.connect_and_test()
        connected = modbus_utils.check_response_status(connection_status)

        return connected

    def reconnect(self) -> bool:
        """
        Try reconnecting n times before giving up. This method is called
        automatically after failing reading values.
        When reconnection fails, ConnectionError is put onto the error_queue
        :return: True if was able to reconnect, False if not
        """
        logging.info('Reconnecting...')
        for i in range(modbus_settings.TelemetrySettings.RECONNECT_TIMES):
            if self.connect():
                return True
            time.sleep(1)

        self._error_queue.put(ConnectionError)

        return False

    def _update_telemetry_values(self) -> None:
        """
        Updates values in telemetry class. Each read can put error into
        internal error queue
        :return:
        """
        self._telemetry.wind_direction = self._controller.read_wind_direction()
        self._telemetry.wind_speed = self._controller.read_wind_speed()
        self._telemetry.temperature = self._controller.read_temperature()
        self._telemetry.humidity = self._controller.read_humidity()
        self._telemetry.pressure = self._controller.read_pressure()

    def _check_error_queue(self) -> bool:
        """
        Check internal error queue. Return True if empty, False if not
        :return:
        """
        if self._internal_error_queue.empty():
            return True

        return False

    def try_reading_values(self) -> bool:
        """
        Check if after reading all values, there is an error in the error queue.
        If there was an error, reconnect
        :return: boolean indicating if values are correct, or if being able to
        reconnect after loosing connection
        """
        self._update_telemetry_values()
        if self._check_error_queue():
            return True

        connected = self.reconnect()
        return connected

    def run(self) -> None:
        """
        Read values and wait until stop() is called. If weather station
        disconnects, there will be a try to reconnect. It it fails, loop stops.

        If after try, reconnection can't be established, thread stops and needs
        to be reinitialized. This is not recoverable
        :return:
        """
        logging.info('Weather station telemetry started')
        while not self._stop_update:
            if not self.try_reading_values():
                break
            time.sleep(1 / self._frequency)

        self._modbus_io.disconnect()
        logging.info('Weather station telemetry stopped')
