class Telemetry:
    """
    Class holding weather station values
    """
    def __init__(self):
        self.wind_direction = None
        self.wind_speed = None
        self.temperature = None
        self.humidity = None
        self.pressure = None

    def __repr__(self):
        """
        String representation of all the values
        :return:
        """
        wind_direction = f'wind_direction: {self.wind_direction}'
        wind_speed = f'wind_speed: {self.wind_speed}'
        temperature = f'temperature: {self.temperature}'
        humidity = f'humidity: {self.humidity}'
        pressure = f'pressure: {self.pressure}'

        return '\n'.join([wind_direction, wind_speed, temperature,
                          humidity, pressure])
