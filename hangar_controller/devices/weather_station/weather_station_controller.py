import logging
import queue
from concurrent.futures import Future, ThreadPoolExecutor, TimeoutError
from typing import List, Optional

from satella.coding.structures import Singleton

from hangar_controller.devices.error_queues import ErrorQueue
from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import \
    ModbusIO
from hangar_controller.devices.weather_station.controller import \
    weather_station_utils
from hangar_controller.devices.weather_station.controller.value_reader import \
    ValueReader
from settingsd.weather_station import modbus_settings, modbus_constants
from settingsd.weather_station.modbus_constants import HoldingRegisters, \
    RealRegisters


@Singleton
class WeatherStationController:
    def __init__(self, modbus_io: ModbusIO = None,
                 error_queue: queue.Queue = None):
        self._error_queue = error_queue
        if error_queue is None:
            self._error_queue = ErrorQueue()
        if modbus_io is None:
            self.modbus_io = ModbusIOManager().weather_station
        self.modbus_io = modbus_io
        self.thread_pool_executor = ThreadPoolExecutor()
        self.value_reader = ValueReader(modbus_io, self.thread_pool_executor)
        self.timeout = modbus_settings.MessagesSettings.TIMEOUT

    def _wait_for_response_and_handle_errors(self, future: Future) \
            -> Optional[List]:
        """
        Wait for future response and check if response status is OK.
        Put error on the error queue if there is exception coming from
        bad request, or there is a connection error
        :param future: modbus io read register future
        :return: list of registers read by modbus
        """
        response = None

        try:
            result = future.result(timeout=self.timeout)
            response_status, response = result

            if not modbus_utils.check_response_status(response_status):
                self.error_queue.put(Exception(response_status))

        except TimeoutError:
            self.error_queue.put(ConnectionError)

        return response

    def _send_and_wait(self, address: modbus_constants.ADDRESS_TYPE):
        """
        Send modbus message and wait for response. Handle errors by putting them
        on the queue
        :param address: modbus address to read
        :return:
        """
        future = self.value_reader.read_register(address)
        response = self._wait_for_response_and_handle_errors(future)

        return response

    def read_device_state(self) -> dict:
        """
        Read current device state. This function is additional and not included
        in telemetry. Used for debugging
        :return: Dictionary of possible to measure values.
        """
        logging.debug('Reading device state')
        response = self._send_and_wait(HoldingRegisters.DEVICE_STATE)
        device_state = weather_station_utils.device_status_parser(response)

        return device_state

    def read_wind_direction(self) -> int:
        """
        Current wind direction 0-360. North is 0 clockwise (east is 90)
        :return:
        """
        logging.debug('Reading wind direction')
        response = self._send_and_wait(HoldingRegisters.WIND_DIRECTION)
        angle = weather_station_utils.angle_parser(response)

        return angle

    def read_wind_speed(self) -> float:
        """
        Current wind speed in m/s
        :return:
        """
        logging.debug('Reading wind speed')
        response = self._send_and_wait(RealRegisters.WIND_SPEED)
        speed = modbus_utils.registers_to_float(response)

        return speed

    def read_temperature(self) -> float:
        """
        Current temperature in C
        :return: float temperature
        """
        logging.debug('Reading temperature')
        response = self._send_and_wait(RealRegisters.TEMPERATURE)
        temperature = modbus_utils.registers_to_float(response)

        return temperature

    def read_humidity(self) -> float:
        """
        Read current humidity value in percentage
        :return: 0-100
        """
        logging.debug('Reading humidity')
        response = self._send_and_wait(RealRegisters.HUMIDITY)
        humidity = modbus_utils.registers_to_float(response)

        return humidity

    def read_pressure(self) -> float:
        """
        Read current pressure in hPa
        :return: float
        """
        logging.debug('Reading pressure')
        response = self._send_and_wait(RealRegisters.PRESSURE)
        humidity = modbus_utils.registers_to_float(response)

        return humidity
