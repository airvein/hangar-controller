from typing import Optional


class MotorDiagnostics:
    def __init__(self):
        self._limit_switch_in: Optional[bool] = None
        self._limit_switch_out: Optional[bool] = None
        self._position: Optional[float] = None

    def __repr__(self):
        return f'position: {self._position}, limit_in: {self.limit_switch_in}' \
               f', limit out: {self._limit_switch_out}'

    @property
    def limit_switch_in(self):
        return self._limit_switch_in

    @property
    def limit_switch_out(self):
        return self._limit_switch_out

    @property
    def position(self):
        return self._position

    def update_limit_switch_in(self, limit_switch: bool):
        self._limit_switch_in = limit_switch

    def update_limit_switch_out(self, limit_switch: bool):
        self._limit_switch_out = limit_switch

    def update_position(self, position: float):
        self._position = position
