import threading
import time
from typing import Callable

from pymodbus import exceptions


class AlarmChecker(threading.Thread):
    def __init__(self, input_read: Callable, alarm_callback: Callable,
                 error_callback: Callable, frequency: int = 10):
        super().__init__()
        self._input_read = input_read
        self._alarm_callback = alarm_callback
        self._error_callback = error_callback
        self._frequency = frequency
        self._stop_alarm_checking = False

    def stop_alarm_check(self) -> None:
        self._stop_alarm_checking = True

    def read_input(self) -> bool:
        try:
            result = self._input_read()
        except exceptions.ConnectionException as error:
            self._error_callback(error)
            self._stop_alarm_checking = True
            result = False

        return result

    def run(self) -> None:
        while not self._stop_alarm_checking:
            alarm = self.read_input()
            if alarm:
                self._alarm_callback()
                return
            time.sleep(1 / self._frequency)
