from typing import List

from hangar_controller.devices.motors.controller.zones.zone_entry_point import ZoneEntryPositions


class Zone:
    def __init__(self, name, exit_path: List[ZoneEntryPositions]):
        self.name = name
        self.exit_path = exit_path

    def __repr__(self):
        return self.name
