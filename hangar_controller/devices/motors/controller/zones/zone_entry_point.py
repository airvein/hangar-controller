from settingsd.motors import parameters_settings


class ZoneEntryPositions:
    # positions of entry points for different hub parts,
    # and how to read them from global parameters
    CARGO_WINDOW_PICK_UP = parameters_settings.Positions.CARGO_WINDOW_PICK_UP
    MOTOR_Y_BACK = parameters_settings.Positions.MOTOR_Y_BACK
    CARGO_SAFE_HEIGHT = parameters_settings.Positions.CARGO_SAFE_MOVE_HEIGHT
