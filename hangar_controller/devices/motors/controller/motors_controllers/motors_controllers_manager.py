from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from hangar_controller.devices.motors.controller.motors_controllers.grasper_motor_controller import \
    GrasperMotorController
from hangar_controller.devices.motors.controller.motors_controllers.lift_motor_controller import \
    LiftMotorController
from hangar_controller.devices.motors.controller.motors_controllers.manipulator_motor_controller import \
    ManipulatorMotorController
from hangar_controller.devices.motors.controller.motors_controllers.positioning_motors_controller import \
    PositioningMotorController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader


class WobitsControllers:
    def __init__(self, error_handler: ErrorHandler, task_queue: TaskQueue):
        self._modbus_io_manager = ModbusIOManager()
        self._error_handler = error_handler
        self._task_queue = task_queue
        self._parameters_reader = ParametersReader()

        self.manipulator = ManipulatorMotorController(
            self._modbus_io_manager.batteries, self._error_handler,
            self._task_queue
        )
        self.grasper = GrasperMotorController(
            self._modbus_io_manager.batteries, self._error_handler,
            self._task_queue
        )
        self.lift = LiftMotorController(
            self._modbus_io_manager.positioning, self._error_handler,
            self._task_queue
        )
        self.positioning = PositioningMotorController(
            self._modbus_io_manager.positioning, self._error_handler,
            self._task_queue
        )

        self.controllers = [
            self.lift,
            self.positioning,
            self.grasper,
            self.manipulator
        ]
        self.diagnostics_reader = WobitDiagnosticsReader(
            self.controllers, self._parameters_reader
        )

    def connect_wobits(self, positioning: bool = True, batteries: bool = True):
        self._modbus_io_manager.connect_devices(positioning, batteries)

    def update_motor_parameters(self) -> bool:
        for controller in self.controllers:
            if not controller.update_motors_parameters():
                return False

        return True

    def start_diagnostics(self):
        for controller in self.controllers:
            controller.start_diagnostics()

    def stop_diagnostics(self):
        for controller in self.controllers:
            controller.stop_diagnostics()
