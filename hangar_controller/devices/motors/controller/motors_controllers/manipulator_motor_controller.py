import logging
from typing import List

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from hangar_controller.devices.motors.controller.task_queue.task_queue import \
    TaskQueue
from hangar_controller.devices.motors.controller.zones.hub_zones import \
    HubZones
from hangar_controller.devices.motors.controller.zones.zone import Zone
from hangar_controller.devices.motors.controller.zones.zone_entry_point import \
    ZoneEntryPositions
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface.modbus_io import \
    ModbusIO
from hangar_controller.devices.motors.parameters.parameters_reader import \
    ParametersReader
from hangar_controller.devices.motors.utils.motors_position import \
    MotorsPosition
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES
from settingsd.motors.parameters_settings import DrivesNames

MOTOR_X = parameters_settings.DrivesNames.X
MOTOR_Y = parameters_settings.DrivesNames.Y
MOTOR_Z = parameters_settings.DrivesNames.Z


class ManipulatorMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        manipulator_motors = self._init_motors_addresses()
        super().__init__(manipulator_motors, modbus_io, error_handler)
        self.current_zone = HubZones.ENTRY_ZONE
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_manipulator_motors_placement()
        assert (wobit_id[0] == wobit_id[1] == wobit_id[2])
        motors = [MOTOR_X, MOTOR_Y, MOTOR_Z]

        addresses = [ALL_ADDRESSES[i] for i in motor_id]

        return list(zip(motors, addresses))

    def _update_current_zone(self, zone: Zone):
        logging.info(f'{type(self).__name__}: Updating current zone: {zone}')
        self.current_zone = zone

    def _move_by_path(self, movement_path: List[ZoneEntryPositions]):
        logging.info(f'{type(self).__name__}: Moving by path {movement_path}')
        for entry_position in movement_path:
            if entry_position == ZoneEntryPositions.CARGO_WINDOW_PICK_UP:
                self._pick_up_cargo()
            elif entry_position == ZoneEntryPositions.MOTOR_Y_BACK:
                self._move_y_motor_back()
            elif entry_position == ZoneEntryPositions.CARGO_SAFE_HEIGHT:
                self._move_z_motor_safe_height()
            else:
                raise ValueError('Unspecified home location')

    def _exit_current_zone(self):
        logging.info(f'{type(self).__name__}: Exiting current zone '
                     f'{self.current_zone} by path {self.current_zone.exit_path}')
        self._move_by_path(self.current_zone.exit_path)

    def _move_motors_xz_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.Y, False)
        second_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])

    def _move_motors_x_z_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.X)
        second_movement = self.filter_positions(position, DrivesNames.Z)
        third_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])
        self.task_queue.add_task(self.move_motors, [third_movement])

    def _move_motors_z_x_y_position(self, position: List[MotorsPosition]):
        first_movement = self.filter_positions(position, DrivesNames.Z)
        second_movement = self.filter_positions(position, DrivesNames.X)
        third_movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [first_movement])
        self.task_queue.add_task(self.move_motors, [second_movement])
        self.task_queue.add_task(self.move_motors, [third_movement])

    def _move_home(self):
        logging.info(f'{type(self).__name__}: Moving to home position')
        position = self._parameters_reader.read_home_position()
        # By this time, motor y should already be full back, this is to be sure
        self._move_y_motor_back()
        # This command will move only x, then z position, y should be back
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone, [HubZones.HOME])

    def _pick_up_cargo(self):
        logging.info(f'{type(self).__name__}: Picking up cargo')
        position = self._parameters_reader.read_cargo_window_pick_up()
        movement = self.filter_positions(position, DrivesNames.Z)
        self.task_queue.add_task(self.move_motors, [movement])

    def _move_y_motor_back(self):
        logging.info(
            f'{type(self).__name__}: Moving y motor to home y position')
        position = self._parameters_reader.read_home_position()
        movement = self.filter_positions(position, DrivesNames.Y)
        self.task_queue.add_task(self.move_motors, [movement])

    def _move_z_motor_safe_height(self):
        logging.info(f'{type(self).__name__}: Moving z motor to safe height')
        position = self._parameters_reader.read_cargo_safe_move_height()
        self.task_queue.add_task(self.move_motors, [position])

    def calibrate_x(self):
        logging.info(f'{type(self).__name__}: Calibrating motor x')
        motor_x_params_name = parameters_settings.DrivesNames.X
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_X], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()

    def calibrate_y(self):
        logging.info(f'{type(self).__name__}: Calibrating motor y')
        motor_y_params_name = parameters_settings.DrivesNames.Y
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_Y], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()

    def calibrate_z(self):
        logging.info(f'{type(self).__name__}: Calibrating motor z')
        calibration_speed = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Z
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_Z], [calibration_speed]])
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()

    def calibrate_safe_sequence(self):
        self.calibrate_y()
        self.calibrate_z()
        self._move_z_motor_safe_height()
        self.calibrate_x()
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])

    def calibrate_all(self):
        logging.info(f'{type(self).__name__}: Calibrating all motors')
        calibration_speed_x = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.X
        )
        calibration_speed_y = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Y
        )
        calibration_speed_z = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.Z
        )
        self.task_queue.add_task(
            self.calibrate,
            [[MOTOR_X, MOTOR_Y, MOTOR_Z],
             [calibration_speed_x, calibration_speed_y, calibration_speed_z]]
        )
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CALIBRATION])
        self.task_queue.wait_until_done()

    def go_to_battery_slot(self, slot_number: int):
        """
        Go to battery slot, either to pick up battery or leave one.
        If picking up, battery there is no need for safety, as long as
        the manipulator is moving on the back wall. However, if it's holding
        a battery from drone, after it moves back, it has to go straight down,
        then x and then y.
        """
        logging.info(f'{type(self).__name__}: Moving to battery slot '
                     f'{slot_number}')
        self._exit_current_zone()
        position = self._parameters_reader.read_battery_slot_position(
            slot_number)
        self._move_motors_z_x_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.BATTERY_SLOTS])
        self.task_queue.wait_until_done()

    def go_home(self):
        logging.info(f'{type(self).__name__}: Moving home')
        self._exit_current_zone()
        self._move_home()
        self.calibrate_all()
        self._move_home()
        self.task_queue.wait_until_done()

    def go_cargo_window_pick_up(self):
        """
        Move manipulator to position of cargo, assuming cargo is there
        to pick it up. Manipulator stops with grasper in the cargo
        and next command should be to grasp cargo
        """
        logging.info(f'{type(self).__name__}: Moving cargo window')
        self._exit_current_zone()
        position = self._parameters_reader.read_cargo_window()
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_WINDOW_FOR_PICKING_UP])
        self.task_queue.wait_until_done()

    def go_cargo_window_put_down(self):
        """
        Move manipulator to put down cargo. First move above the cargo
        position, then put it down. The next command should be to release cargo
        """
        logging.info(f'{type(self).__name__}: Moving cargo window home')
        self._exit_current_zone()
        # if cargo is taken from drone, first move horizontally not to catch
        # propellers, then go up, then to the front
        position = self._parameters_reader.read_cargo_window_pick_up()
        self._move_motors_x_z_y_position(position)
        # position of cargo window should be directly below current point
        position = self._parameters_reader.read_cargo_window()
        self._move_motors_xz_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_WINDOW_PUT_DOWN])
        self.task_queue.wait_until_done()

    def go_cargo_drone(self):
        logging.info(f'{type(self).__name__}: Moving cargo drone')
        self._exit_current_zone()
        position = self._parameters_reader.read_cargo_drone()
        # if cargo is taken from window, first move it horizontally
        # not to catch the propellers
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.CARGO_DRONE])
        self.task_queue.wait_until_done()

    def go_battery_drone(self):
        """
        Go to battery drone.
        """
        logging.info(f'{type(self).__name__}: Moving battery drone')
        self._exit_current_zone()
        position = self._parameters_reader.read_battery_drone()
        self._move_motors_x_z_y_position(position)
        self.task_queue.add_task(self._update_current_zone,
                                 [HubZones.BATTERY_DRONE])
        self.task_queue.wait_until_done()
