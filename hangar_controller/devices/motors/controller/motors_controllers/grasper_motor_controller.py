import logging

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES

MOTOR_GRASPER = parameters_settings.DrivesNames.GRASPER


class GrasperMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        grasper_motors = self._init_motors_addresses()
        super().__init__(grasper_motors, modbus_io, error_handler)
        self._task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_grasper_motor_placement()
        assert (len(motor_id) == 1)

        address = ALL_ADDRESSES[motor_id[0]]

        return [(MOTOR_GRASPER, address)]

    def calibrate_grasper(self):
        """
        Rotate grasper back to the closest hole and then finish movemet
        by calibrating it in the another direction
        """
        logging.debug(f'{type(self).__name__}: Calibrating grasper')
        motor_lift_params_name = parameters_settings.DrivesNames.GRASPER
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_lift_params_name
        )
        self._task_queue.add_task(self.calibrate,
                                  [[MOTOR_GRASPER], [calibration_speed]])
        # self._position_grasper_after_closest_hole()
        self._task_queue.wait_until_done()

    def _position_grasper_after_closest_hole(self):
        """
        Finish movement or calibration of grasping by moving it
        after closest limit switch. This is done by wobit calibration
        """
        motor_lift_params_name = parameters_settings.DrivesNames.GRASPER
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_lift_params_name
        )
        self._task_queue.add_task(self.calibrate,
                                  [[MOTOR_GRASPER], [calibration_speed]])
        self._task_queue.wait_until_done()

    def grasper_horizontal(self):
        logging.debug(f'{type(self).__name__}: Grasping')
        position = self._parameters_reader.read_grasp_position()
        self._task_queue.add_task(self.move_motors, [position])
        self._task_queue.wait_until_done()

    def release(self):
        logging.debug(f'{type(self).__name__}: Realising')
        position = self._parameters_reader.read_release_position()
        self._task_queue.add_task(self.move_motors, [position])
        self._task_queue.wait_until_done()
