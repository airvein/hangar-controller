import logging

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES
from settingsd.motors.parameters_settings import DrivesNames

X_PLATFORM = parameters_settings.DrivesNames.X_PLATFORM
Y_PLATFORM = parameters_settings.DrivesNames.Y_PLATFORM


class PositioningMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        positioning_motors = self._init_motors_addresses()
        super().__init__(positioning_motors, modbus_io, error_handler)
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_positioning_motors_placement()
        assert (wobit_id[0] == wobit_id[1])
        motors = [X_PLATFORM, Y_PLATFORM]

        addresses = [ALL_ADDRESSES[i] for i in motor_id]

        return list(zip(motors, addresses))

    def calibrate_x(self):
        logging.debug(f'{type(self).__name__}: Calibrating motor x')
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[X_PLATFORM], [calibration_speed]])
        self.task_queue.wait_until_done()

    def calibrate_y(self):
        logging.debug(f'{type(self).__name__}: Calibrating motor y')
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        calibration_speed = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )
        self.task_queue.add_task(self.calibrate,
                                 [[Y_PLATFORM], [calibration_speed]])
        self.task_queue.wait_until_done()

    def calibrate_all(self):
        logging.debug(f'{type(self).__name__}: Calibrating both motors')
        motor_x_params_name = parameters_settings.DrivesNames.X_PLATFORM
        motor_y_params_name = parameters_settings.DrivesNames.Y_PLATFORM
        calibration_speed_x = self._parameters_reader.read_calibration_speed(
            motor_x_params_name
        )
        calibration_speed_y = self._parameters_reader.read_calibration_speed(
            motor_y_params_name
        )

        self.task_queue.add_task(
            self.calibrate,
            [[X_PLATFORM, Y_PLATFORM],
             [calibration_speed_x, calibration_speed_y]])
        self.task_queue.wait_until_done()

    def position_drone(self):
        logging.debug(f'{type(self).__name__}: Positioning drone')
        self.calibrate_all()
        positions = self._parameters_reader.read_platform_middle()
        first_movement = self.filter_positions(positions,
                                               DrivesNames.Y_PLATFORM)
        self.task_queue.add_task(self.move_motors,
                                 [first_movement])
        second_movement = self.filter_positions(positions,
                                                DrivesNames.X_PLATFORM)
        self.task_queue.add_task(self.move_motors,
                                 [second_movement])
        self.task_queue.wait_until_done()

    def release_drone(self):
        logging.debug(f'{type(self).__name__}: Releasing drone')
        positions = self._parameters_reader.read_platform_home()
        self.task_queue.add_task(self.move_motors,
                                 [positions])
        self.task_queue.wait_until_done()
