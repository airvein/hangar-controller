import logging

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller \
    import AbstractMotorsController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import ALL_ADDRESSES

MOTOR_LIFT = parameters_settings.DrivesNames.LIFT


class LiftMotorController(AbstractMotorsController):
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 task_queue: TaskQueue):
        lift_motors = self._init_motors_addresses()
        super().__init__(lift_motors, modbus_io, error_handler)
        self.task_queue = task_queue

    def _init_motors_addresses(self):
        parameters_reader = ParametersReader()
        wobit_id, motor_id = parameters_reader.read_lift_motor_placement()
        assert (len(motor_id) == 1)

        address = ALL_ADDRESSES[motor_id[0]]

        return [(MOTOR_LIFT, address)]

    def calibrate_lift(self):
        logging.debug(f'{type(self).__name__}: Calibrating lift')
        calibration_speed = self._parameters_reader.read_calibration_speed(
            parameters_settings.DrivesNames.LIFT
        )
        self.task_queue.add_task(self.calibrate,
                                 [[MOTOR_LIFT], [calibration_speed]])
        self.task_queue.wait_until_done()

    def go_lift_up(self):
        logging.debug(f'{type(self).__name__}: Moving lift up')
        positions = self._parameters_reader.read_lift_up()
        self.task_queue.add_task(self.move_motors, [positions])
        self.task_queue.wait_until_done()

    def go_lift_down(self):
        logging.debug(f'{type(self).__name__}: Moving lift up')
        positions = self._parameters_reader.read_lift_down()
        self.task_queue.add_task(self.move_motors, [positions])
        self.task_queue.wait_until_done()
