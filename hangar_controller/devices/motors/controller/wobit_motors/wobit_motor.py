from typing import Optional, Tuple, Type, Union

from hangar_controller.devices.motors.controller.observers.motor_position_observer import \
    MotorPositionObserver
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.modbus_interface.device_data_holders.discrete_input_reader \
    import DiscreteInputReader
from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    discrete_output_controller import DiscreteOutputController
from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    holding_register_controller import HoldingRegisterController
from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    real_register_controller import RealRegisterController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd.motors import modbus_constants as mc
from settingsd.motors.modbus_constants import ModbusResponseStatus, Wobit


class WobitMotor:
    def __init__(self, modbus_io: ModbusIO, error_handler: ErrorHandler,
                 motor_name: Union[mc.Wobit.BatteriesMotorsNames,
                                   mc.Wobit.PositioningMotorsNames],
                 registers: Type[mc.WobitMotorAddresses]):
        self.wobit_id = registers.WOBIT_ID
        self.motor_name = motor_name
        self.modbus_io = modbus_io
        self.error_handler = error_handler

        self.discrete_inputs: Optional[DiscreteInputReader] = None
        self.real_registers: Optional[RealRegisterController] = None
        self.holding_registers: Optional[HoldingRegisterController] = None
        self.discrete_outputs: Optional[DiscreteOutputController] = None

        self.update_registers(registers)

        self._position_observer = MotorPositionObserver(
            self.read_position, self.read_motor_status, self.error_handler)

    def __repr__(self):
        return self.motor_name

    def update_registers(self, registers: Type[mc.WobitMotorAddresses]):
        self.discrete_inputs = DiscreteInputReader(
            self.modbus_io, registers.DISCRETE_INPUTS_ADDRESSES
        )
        self.discrete_outputs = DiscreteOutputController(
            self.modbus_io, registers.DISCRETE_OUTPUTS_ADDRESSES
        )
        self.holding_registers = HoldingRegisterController(
            self.modbus_io, registers.HOLDING_REGISTERS
        )
        self.real_registers = RealRegisterController(
            self.modbus_io, registers.REAL_REGISTERS
        )

    @property
    def position_observer(self):
        return self._position_observer

    def power_on_motor(self) -> ModbusResponseStatus:
        status = self.discrete_outputs.write_by_channel_name(
            Wobit.BitRegistersNames.Mx_POWER, True
        )

        return status

    def enable_motor(self) -> ModbusResponseStatus:
        status = self.holding_registers.write_by_channel_name(
            Wobit.HoldingRegistersNames.M_ENABLE, True
        )

        return status

    def read_position(self) -> Tuple[ModbusResponseStatus, Optional[float]]:
        status, position = self.real_registers.read_by_channel_name(
            Wobit.RealRegistersNames.Mx_POS_ACT)

        return status, position

    def read_motor_status(self) -> Tuple[ModbusResponseStatus, Optional[int]]:
        status, position = self.holding_registers.read_by_channel_name(
            Wobit.HoldingRegistersNames.M_STATUS
        )

        return status, position

    def read_limit_switch_in(self) -> Tuple[ModbusResponseStatus,
                                            Optional[bool]]:
        status, limit_switch_in = self.discrete_inputs.read_by_channel_name(
            Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN
        )
        return status, limit_switch_in

    def read_limit_switch_out(self) -> Tuple[ModbusResponseStatus,
                                             Optional[bool]]:
        status, limit_switch_out = self.discrete_inputs.read_by_channel_name(
            Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT
        )

        return status, limit_switch_out

    def write_actual_position(self, position: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_POS_ACT,
            position
        )

        return status

    def write_absolute_position(self, position: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_POS_ABS,
            position
        )

        return status

    def write_max_velocity(self, velocity: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_VMAX,
            velocity)

        return status

    def write_acceleration(self, acceleration: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_ACC,
            acceleration
        )

        return status

    def write_deceleration(self, deceleration: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_DEC,
            deceleration
        )

        return status

    def move_home(self, velocity: float) -> ModbusResponseStatus:
        status = self.real_registers.write_by_channel_name(
            Wobit.RealRegistersNames.Mx_HOME,
            velocity
        )

        return status
