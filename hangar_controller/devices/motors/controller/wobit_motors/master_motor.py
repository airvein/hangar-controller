from typing import List

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    holding_register_controller import HoldingRegisterController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd.motors import modbus_constants


class MasterMotor:
    def __init__(self, modbus_io: ModbusIO):
        self.modbus_io = modbus_io
        self.holding_registers: HoldingRegisterController = \
            HoldingRegisterController(
                self.modbus_io,
                modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS
            )

    def write_axis_control_mode(
            self, axis_control: modbus_constants.Wobit.AxisControlMode):
        status = self.holding_registers.write_by_channel_name(
            channel_name=
            modbus_constants.Wobit.HoldingRegistersNames.AXIS_CTR_MODE,
            value=axis_control.value
        )

        return status

    def write_data_type(self, data_type: modbus_constants.Wobit.DataTypes):
        status = self.holding_registers.write_by_channel_name(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.DATA_TYPE,
            value=data_type.value
        )

        return status

    def trigger_absolute_position(self, motor_ids: List[int]):
        status = self.holding_registers.write_by_channel_name(
            channel_name=
            modbus_constants.Wobit.HoldingRegistersNames.AXIS_POS_ABS_TRIG,
            value=modbus_utils.id_list_to_control_word(motor_ids)
        )

        return status

    def enable_motors(self, motor_ids: List[int]):
        status = self.holding_registers.write_by_channel_name(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_ENABLE,
            value=modbus_utils.id_list_to_control_word(motor_ids)
        )

        return status

    def disable_motors(self, motor_ids: List[int]):
        status = self.holding_registers.write_by_channel_name(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_DISABLE,
            value=modbus_utils.id_list_to_control_word(motor_ids)
        )

        return status

    def stop_motors(self, motor_ids: List[int]):
        status = self.holding_registers.write_by_channel_name(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_STOP,
            value=modbus_utils.id_list_to_control_word(motor_ids)
        )

        return status

    def stop_all_motors(self):
        status = self.holding_registers.write_by_channel_name(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_STOP,
            value=modbus_utils.id_list_to_control_word([1, 2, 3, 4])
        )

        return status
