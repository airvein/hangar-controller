import logging
import queue
from threading import Lock
from typing import Optional, Callable

from satella.coding.structures import Singleton

from hangar_controller.devices.motors.controller.task_queue.empty_task import EmptyTask
from hangar_controller.devices.motors.controller.task_queue.task import Task


@Singleton
class TaskQueue(queue.Queue):
    def __init__(self):
        super().__init__()
        self._lock = Lock()
        self.release_lock()

    @property
    def lock(self) -> Lock:
        return self._lock

    def add_task(self, callback: Callable, args: Optional[list] = None,
                 locking=True) -> None:
        lock_release = None

        if locking:
            lock_release = self.release_lock

        self.put(Task(callback, args, lock_release))

    def add_empty_task(self):
        logging.debug('Putting empty task on queue')
        self.put(EmptyTask())

    def wait_for_unlock(self) -> None:
        # acquire will wait until lock is released.
        self.lock.acquire()
    
    def release_lock(self) -> None:
        logging.debug('Releasing queue lock')
        if self.lock.locked():
            self.lock.release()
    
    def process_task(self) -> None:
        self.wait_for_unlock()
        task: Task = self.get()

        if not task.queue_locking:
            self.release_lock()

        # task should call lock release callback if locking was set
        task.start()

    def wait_until_done(self):  # pragma: no cover
        lock = Lock()
        lock.acquire(True)
        self.add_task(lock.release)
        lock.acquire(True)

    def flush(self) -> None:
        logging.debug('Getting all queued messages')
        while not self.empty():
            self.get()

    def exit_procedure(self):
        self.flush()
        self.release_lock()

        # add some empty task to push the queue if it's waiting to get task
        self.add_empty_task()
