from threading import Thread

from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue


class QueueController(Thread):
    def __init__(self):
        super().__init__()
        self._queue = TaskQueue()
        self._stop_queue = False

    def stop(self):
        self._queue.flush()
        self._stop_queue = True

    def run(self) -> None:
        while not self._stop_queue:
            self._queue.process_task()
