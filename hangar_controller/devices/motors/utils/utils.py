import time


def get_current_time():
    return int(time.time() * 1000)

