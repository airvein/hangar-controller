class MotorsPosition:
    def __init__(self, motor_name, position):
        self.motor_name = motor_name
        self.position = position

    def __repr__(self):
        name = f'name: {self.motor_name}'
        position = f'position: {self.position}'

        return 'position: ' + ', '.join([name, position])
