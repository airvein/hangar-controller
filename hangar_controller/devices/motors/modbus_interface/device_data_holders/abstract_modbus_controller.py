import logging
from abc import abstractmethod
from typing import Type, Union

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_reader \
    import AbstractModbusReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus


class AbstractModbusController(AbstractModbusReader):
    # Controller has a list of io_addresses
    @abstractmethod
    def __init__(self, address_list: modbus_constants.ADDRESS_LIST_TYPE,
                 address_type: Type[AbstractIOAddress],
                 modbus_io: ModbusIO):
        super().__init__(address_list, address_type, modbus_io)

    @staticmethod
    def check_io_address_writable(io_address) -> ModbusResponseStatus:
        if not io_address:
            return ModbusResponseStatus.VALUE_ERROR

        if not isinstance(io_address, WritableIOAddress):
            msg = f'Trying to write value to io_address "{io_address.name}' \
                f', that is read-only'
            logging.error(msg)
            return ModbusResponseStatus.VALUE_ERROR

        return ModbusResponseStatus.STATUS_OK

    def write_to_io_address(self, io_address: WritableIOAddress,
                            value: Union[int, bool]) \
            -> ModbusResponseStatus:

        check_status = self.check_io_address_writable(io_address)
        if not modbus_utils.check_response_status(check_status):
            return check_status

        status = io_address.write(self.modbus_io, value)

        return status

    def write_by_channel_name(self, channel_name: str,
                              value: Union[float, int, bool]) \
            -> ModbusResponseStatus:
        io_address: WritableIOAddress = self.get_io_address_by_name(
            channel_name)

        status = self.write_to_io_address(io_address, value)

        return status

    def write_by_address(self, address: int, value: Union[float, int, bool]) \
            -> ModbusResponseStatus:
        io_address: WritableIOAddress = self.get_io_address_by_address(address)

        status = self.write_to_io_address(io_address, value)

        return status
