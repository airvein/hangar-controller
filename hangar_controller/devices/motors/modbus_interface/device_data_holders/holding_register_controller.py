import logging

from hangar_controller.devices.motors.modbus_interface.device_data_holders. \
    abstract_modbus_controller import AbstractModbusController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd.motors import modbus_constants


class HoldingRegisterController(AbstractModbusController):
    def __init__(self, modbus_io: ModbusIO,
                 address_list: modbus_constants.ADDRESS_LIST_TYPE):

        logging.debug('Initializing Holding Registers values')
        super().__init__(address_list, HoldingRegister, modbus_io)
