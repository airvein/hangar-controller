import logging

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_reader \
    import AbstractModbusReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_input import \
    DiscreteInput
from settingsd.motors import modbus_constants


class DiscreteInputReader(AbstractModbusReader):
    def __init__(self, modbus_io: ModbusIO,
                 address_list: modbus_constants.ADDRESS_LIST_TYPE):
        logging.debug('Initializing Discrete Input values')
        super().__init__(address_list, DiscreteInput, modbus_io)
