import logging

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_reader \
    import AbstractModbusReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.input_register import \
    InputRegister
from settingsd.motors import modbus_constants


class InputRegistersReader(AbstractModbusReader):
    def __init__(self, modbus_io: ModbusIO,
                 address_list: modbus_constants.ADDRESS_LIST_TYPE):

        logging.debug('Initializing Input Registers values')
        super().__init__(address_list, InputRegister, modbus_io)
