import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus


class DiscreteOutput(WritableIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logging.debug('DiscreteOutput %s with address 0x%04X created',
                      self.name, self.address)

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_coils(self._address, count)

    def write_value(self, modbus_io: ModbusIO,
                    value: bool) -> ModbusResponseStatus:
        status, _ = modbus_io.write_coil(self.address, value)

        return status
