import logging
from abc import abstractmethod, ABC
from typing import Optional, Union, Tuple

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd.motors.modbus_constants import ModbusResponseStatus


class AbstractIOAddress(ABC):
    @abstractmethod
    def __init__(self, name: str, address: int):
        self._name: str = name
        self._address: int = address
        self._value: Optional[Union[int, bool]] = None

    @property
    def name(self) -> str:
        return self._name

    @property
    def address(self) -> int:
        return self._address

    @property
    def value(self) -> Union[bool, int, float]:
        return self._value

    @value.setter
    def value(self, new_value: Union[bool, int, float]) -> None:
        self._value = new_value

    @abstractmethod
    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        raise NotImplementedError

    def update(self, modbus_io: ModbusIO) -> None:  # pragma: no cover
        status, values = self.read_values(modbus_io, count=1)

        if not modbus_utils.check_response_status(status):
            logging.error('Error occurred while reading "%s" with '
                          'address 0x%04X', self.name, self.address)
            return status

        self.value = values[0]
        logging.debug('Address %s updated to %s',
                      self.name, self.value)

        return status

    def read_updated_value(self, modbus_io: ModbusIO) \
            -> Tuple[ModbusResponseStatus, Union[bool, int, float]]:
        status = self.update(modbus_io)
        logging.debug('Address %s read, status: %s',
                      self.name, status)

        return status, self.value
