import logging
from abc import abstractmethod
from typing import Union

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus


class WritableIOAddress(AbstractIOAddress):
    @abstractmethod
    def __init__(self, name: str, address: int):
        super().__init__(name, address)

    @abstractmethod
    def write_value(self, modbus_io: ModbusIO, value) \
            -> ModbusResponseStatus:
        raise NotImplementedError

    @abstractmethod
    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        raise NotImplementedError

    def write(self, modbus_io: ModbusIO,
              value: Union[int, float]) -> ModbusResponseStatus:
        logging.debug('Writing value %s to HoldingRegister with address 0x%04X',
                      value, self.address)
        status = self.write_value(modbus_io, value)

        if modbus_utils.check_response_status(status):
            self._value = value

        return status
