import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress


class InputRegister(AbstractIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logging.debug('InputRegister %s with address 0x%04X created',
                      name, address)

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_input_registers(self.address, count)
