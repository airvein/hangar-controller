import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus


class HoldingRegister(WritableIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logging.debug('HoldingRegister %s with address 0x%04X created',
                      name, address)

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_holding_registers(self._address, count)

    def write_value(self, modbus_io: ModbusIO, value: int) \
            -> ModbusResponseStatus:

        status, _ = modbus_io.write_holding_register(self.address, value)

        return status
