import logging

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress


class DiscreteInput(AbstractIOAddress):
    def __init__(self, name: str, address: int):
        super().__init__(name, address)
        logging.debug('DiscreteInput %s with address 0x%04X created',
                      name, address)

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        # read value of this address, or consecutive addresses
        # starting from this one.
        # Returns status and list of values
        return modbus_io.read_discrete_inputs(self._address, count)
