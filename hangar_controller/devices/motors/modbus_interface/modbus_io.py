import logging
from typing import Optional, Callable, Tuple, Any, List

from pymodbus.client.sync import ModbusSerialClient
from pymodbus.exceptions import ConnectionException

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors.modbus_constants import ModbusResponseStatus, Wobit, WobitMotorAddresses


def connection_check(wrapped_function: Callable):
    # decorator checks if there is modbus connection exception thrown
    # on read/write register

    def wrapper(*args, **kwargs) -> \
            Tuple[ModbusResponseStatus, Any]:
        function_name = str(wrapped_function).split()[1]
        logging.debug('Connection check for %s', function_name)
        try:
            response = wrapped_function(*args, **kwargs)
            return ModbusResponseStatus.STATUS_OK, response
        except ConnectionException:
            logging.error('Connection exception when executing function %s',
                          function_name)
            return ModbusResponseStatus.CONNECTION_ERROR, None

    return wrapper


class ModbusIO:
    def __init__(self, connection: modbus_utils.Connection,
                 testing_register: int):
        self._client: Optional[ModbusSerialClient] = None
        self._connected: bool = False
        self.connection = connection
        self._testing_register: int = testing_register

    @property
    def client(self) -> Optional[ModbusSerialClient]:
        return self._client

    @property
    def connected(self) -> bool:
        return self._connected

    def create_client(self):
        logging.info('Creating modbus client')
        port = self.connection.port
        baud = self.connection.baudrate
        stop_bits = self.connection.stop_bits
        parity = self.connection.parity
        timeout = self.connection.timeout

        self._client = ModbusSerialClient(method='rtu', port=port,
                                          baudrate=baud, stopbits=stop_bits,
                                          parity=parity, timeout=timeout)

    def connect_and_test(self) -> ModbusResponseStatus:
        self.create_client()
        connection_status = self.check_connection()

        return connection_status

    def disconnect(self) -> None:
        logging.info('Closing modbus client')
        self._client.close()

    def check_connection(self) -> ModbusResponseStatus:
        # try to read some inputs. If controller is not connected,
        # register response will not have .bits attribute, hence AttributeError
        logging.info('Checking modbus connection')
        connection_success = self._client.connect()

        if not connection_success:
            logging.error("Modbus client couldn't be created")
            return ModbusResponseStatus.PARAMETERS_ERROR
        reading_status, _ = self.test_reading_value()

        return reading_status

    def test_reading_value(self) -> Tuple[ModbusResponseStatus, list]:
        reading_status, value = self.read_holding_registers(
            self._testing_register)
        logging.info('Reading single digital input status: %s',
                     str(reading_status).split('.')[-1])

        return reading_status, value

    @connection_check
    def read_discrete_inputs(self, channel: int, count: int = 1) \
            -> List[bool]:
        logging.debug('Reading %d discrete input(s) starting from address: '
                      '0x%04X', count, channel
                      )

        response = self._client.read_discrete_inputs(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        bits = modbus_utils.try_reading_bits(response)

        return bits

    @connection_check
    def read_coils(self, channel: int, count: int = 1) \
            -> List[bool]:
        logging.debug(f'Reading {count} coil(s) from address 0x%04X',
                      channel)
        response = self.client.read_coils(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        bits = modbus_utils.try_reading_bits(response)

        return bits

    @connection_check
    def write_coil(self, channel: int, value: bool):
        logging.debug(f'Writing coil value {channel} to address 0x%04X',
                      value)
        self._client.write_coil(channel, value,
                                unit=self.connection.device_address)

    @connection_check
    def read_input_registers(self, channel: int, count: int = 1) \
            -> List[int]:
        logging.debug('Reading input register from address 0x%04X',
                      channel)
        response = self.client.read_input_registers(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def read_holding_registers(self, channel: int, count: int = 1) -> List[int]:
        logging.debug('Reading holding register from address 0x%04X',
                      channel)
        response = self.client.read_holding_registers(
            address=channel,
            count=count,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def write_holding_register(self, channel: int, value: int):
        logging.debug('Writing holding register value %d to addressed 0x%04X',
                      value, channel)
        self.client.write_register(
            address=channel,
            value=value,
            unit=self.connection.device_address
        )

    @connection_check
    def read_real_register(self, channel: int, count: int = 1) -> List[int]:
        logging.debug('Reading real register from address 0x%04X',
                      channel)
        response = self.client.read_holding_registers(
            address=channel,
            count=count * 2,
            unit=self.connection.device_address
        )

        registers = modbus_utils.try_reading_registers(response)

        return registers

    @connection_check
    def write_real_register(self, channel: int, value: float):
        logging.debug('Writing real register values %f to addressed 0x%04X',
                      value, channel)
        self.client.write_registers(
            address=channel,
            values=modbus_utils.float_to_2_registers(value),
            unit=self.connection.device_address
        )

    def write_user_registers(self):
        self._client.write_coil(WobitMotorAddresses.USER_REG_SAVE, 1)

    def load_user_registers(self):
        self._client.write_coil(WobitMotorAddresses.USER_REG_READ, 1)
