import logging
import queue
from threading import Thread
from typing import NoReturn

from satella.coding.structures import Singleton

from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import \
    DriversDiagnostics
from hangar_controller.devices.error_queues import ErrorQueue, EventLogQueue
from hangar_controller.devices.motors.controller.motors_controllers.motors_controllers_manager \
    import WobitsControllers
from hangar_controller.devices.motors.controller.task_queue.task_queue import \
    TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler


@Singleton
class Motors(Thread):
    """
    Main thread that controls all hangar functions. Method start() needs to be
    called when running motors.

    If we are using the real motors, we need to update their parameters
    to match the ones saved in parameters.yaml. To do this:
    update_motor_parameters()

    If user wants to observe motors diagnostics (e.g. position) he needs to
    call start_diagnostics() as well.

    To stop diagnostics call stop_diagnostics()

    To stop motors thread call stop()

    :raises:
    MotorError() - when motor hits the position limiter
    ModbusError() - when modbus connection has been broken
    """
    def __init__(self, error_queue: queue.Queue = None,
                 error_warning_log_queue: queue.Queue = None):
        super().__init__()
        self._error_queue = error_queue
        self._error_warning_log_queue = error_warning_log_queue
        if error_queue is None:
            self._error_queue = ErrorQueue()
        if error_warning_log_queue is None:
            self._error_warning_log_queue = EventLogQueue()
        self._stop_controller = False
        self._task_queue = TaskQueue()
        self._error_handler = ErrorHandler()
        self.wobits = WobitsControllers(self._error_handler, self._task_queue)

    @property
    def drivers_diagnostics(self) -> DriversDiagnostics:
        """

        :return: DriversDiagnostics class- dictionary containing all
        the updates sent from STMs
        """
        return self.drivers.diagnostics

    def stop(self) -> None:
        """
        Stops main controller thread.
        Stops diagnostics thread that updates diagnostic messages from stm
        Sets stop flag
        Exit procedure stops queue safely

        :return None
        :raises: look for 'raises' definition in the class header
        """
        self.stop_diagnostics()
        self._stop_controller = True
        self._task_queue.exit_procedure()

    def run(self) -> NoReturn:
        """
        Runs this thread and starts processing tasks from the motors queue
        Runs drivers controller thread

        Stops after executing self.stop()
        :raises: look for 'raises' definition in the class header
        """
        logging.info('Starting controller')
        while not self._stop_controller:
            self._task_queue.process_task()

    def update_motors_parameters(self) -> bool:
        """
        Reads wobit motor parameters (acceleration, deceleration, speed)
        from yaml file and sends modbus message to each motor, with parameters
        set.

        :return: bool status if there was no modbus error during update
        :raises: look for 'raises' definition in the class header
        """
        logging.debug('Updating wobit_motors parameters')
        status = self.wobits.update_motor_parameters()

        return status

    def start_diagnostics(self) -> None:
        """
        Starting wobit diagnostic thread.

        Wobit diagnostics sends modbus message to get response about
        motor position, and other parameters. This is being sent every
        second, or faster if specified otherwise

        :raises: look for 'raises' definition in the class header
        """
        logging.info('Starting diagnostics')
        self.wobits.start_diagnostics()

    def stop_diagnostics(self) -> None:
        """Stops wobit diagnostics thread
        :raises: look for 'raises' definition in the class header
        """
        logging.info('Stopping diagnostics')
        self.wobits.stop_diagnostics()

    def calibrate_battery_motors(self) -> None:
        """
        Sends signal to battery motors to calibrate. I.e. go to home
        position and reset position to 0.

        Lock time: ~ 10s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Calibrating battery wobit_motors')
        self.wobits.manipulator.calibrate_safe_sequence()

    def go_cargo_drone(self) -> None:
        """
        Sends message to wobit controllers to move arm to cargo drone
        either to get the cargo from drone, or to put cargo into drone

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving to cargo drone')
        self.wobits.manipulator.go_cargo_drone()

    def go_battery_drone(self) -> None:
        """
        Sends message to wobit controllers to move arm to drone battery
        either to get the battery from drone, or to put fully charged
        battery into drone

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving to battery drone')
        self.wobits.manipulator.go_battery_drone()

    def go_battery_slot(self, battery_slot_number: int) -> None:
        """
        Sends message to wobit controllers to move arm to battery slot
        specified by number

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info(f'Moving to battery slot number {battery_slot_number}')
        self.wobits.manipulator.go_to_battery_slot(battery_slot_number)

    def go_home(self) -> None:
        """
        Sends message to wobit controllers to move arm to safe position
        called home. Home position can be set in parameters.yaml file
        in main folder

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving home')
        self.wobits.manipulator.go_home()

    def go_cargo_window_pick_up(self) -> None:
        """
        Sends message to wobit controllers to move arm to slide into cargo from
        cargo window position. Next command should be to grasp.
        Exit sequence is to go up to pick up cargo and back

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving arm to pick up cargo')
        self.wobits.manipulator.go_cargo_window_pick_up()

    def go_cargo_window_put_down(self) -> None:
        """
        Sends message to wobit controllers to move arm with cargo above
        the weight and put it back.
        The next command should be to release cargo.
        Exit sequence is to move arm back, without going up.

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving arm to put down cargo')
        self.wobits.manipulator.go_cargo_window_put_down()

    def calibrate_lift(self) -> None:
        """
        Sends signal to lift motor to calibrate. I.e. go to home
        position and reset position to 0.

        Lock time: ~ 10s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Calibrating lift')
        self.wobits.lift.calibrate_lift()

    def go_lift_up(self) -> None:
        """
        Send signal for lift motor to go up

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving lift up')
        self.wobits.lift.go_lift_up()

    def go_lift_down(self) -> None:
        """
        Send signal for lift motor to go down

        Lock time: ~ 5s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Moving lift down')
        self.wobits.lift.go_lift_down()

    def calibrate_positioning(self) -> None:
        """
        Send signal to positioning motors to move to the limits and
        reset positions to 0

        Lock time: ~ 15s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Calibrating positioning wobit_motors')
        self.wobits.positioning.calibrate_all()

    def position_drone(self) -> None:
        """
        Send signal to positioning motors to move in and position drone

        :return: None
        """
        logging.info('Positioning drone')
        self.wobits.positioning.position_drone()

    def release_drone(self) -> None:
        """
        Send signal to positioning motors to move out and release drone

        Lock time: ~ 10s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Releasing drone')
        self.wobits.positioning.release_drone()

    def calibrate_grasper(self) -> None:
        """
        Send signal to grasper motor to calibrate

        :return: None
        """
        logging.info('Calibrating grasper')
        self.wobits.grasper.calibrate_grasper()

    def grasp(self) -> None:
        """
        Send signal to grasper motor to grasp cargo or
        battery that is either on drone or on battery stash.
        Grasper moves to location and than calibrates to move

        Lock time: ~ 2s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Grasping')
        self.wobits.grasper.calibrate_grasper()

    def release(self) -> None:
        """
        Send signal to motors operating on manipulator to
        release battery either on drone or on battery stash

        Lock time: ~ 2s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Releasing battery')
        self.wobits.grasper.grasper_horizontal()

    def grasp_cargo(self) -> None:
        """
        Send signal to positioning motors to move in and grasp cargo
        that is either on drone or cargo window

        Lock time: ~ 2s
        :raises: look for 'raises' definition in the class header
        :return: None
        """
        logging.info('Grasping cargo')
        self.wobits.grasper.grasper_horizontal()

    def release_cargo(self) -> None:
        """
        Send signal to positioning motors to move out and release cargo
        either on drone or cargo window

        Lock time: ~ 2s
        :return: None
        :raises: look for 'raises' definition in the class header
        """
        logging.info('Releasing cargo')
        self.wobits.grasper.release()
