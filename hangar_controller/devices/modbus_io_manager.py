import logging
from copy import copy
from typing import Optional

from satella.coding.structures import Singleton

from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from settingsd import devices_modbus_connection
from settingsd.devices_modbus_connection import WEATHER_STATION_PORT_PAIR, \
    WOBIT_BATTERIES_PORT_PAIR, WOBIT_POSITIONING_PORT_PAIR
from settingsd.motors.modbus_constants import ModbusResponseStatus, Wobit


@Singleton
class ModbusIOManager:
    def __init__(self):
        self._modbus_batteries_io: Optional[ModbusIO] = None
        self._modbus_positioning_io: Optional[ModbusIO] = None
        self._modbus_weather_station: Optional[ModbusIO] = None
        self._port_pairs = devices_modbus_connection.DEVICES_PORT_PAIRS.values()

    @property
    def batteries(self):
        return self._modbus_batteries_io

    @property
    def positioning(self):
        return self._modbus_positioning_io

    @property
    def weather_station(self):
        return self._modbus_weather_station

    def validate_port_pairs(self):
        """
        Check if ports are correctly assigned. No devices has the same port as other device,
        no device has unknown port
        """
        applied_ports = [port_pair.connection.port for port_pair in self._port_pairs]
        if any([port_pair.connection.port == 'UNKNOWN' for port_pair in self._port_pairs]):
            msg = f'Error while applying ports to devices. Some ports were not applied: ' \
                  f'{[(pp.name, pp.connection.port) for pp in self._port_pairs]}'

            raise ConnectionError(msg)

        if not set(applied_ports) == set(devices_modbus_connection.POSSIBLE_PORTS):
            raise ConnectionError(
                f'Error while applying ports to devices, '
                f'{[(pp.name, pp.connection.port) for pp in self._port_pairs]}'
                f' possible ports: {devices_modbus_connection.POSSIBLE_PORTS}')

    def apply_port_pairs(self):
        self._modbus_weather_station = ModbusIO(WEATHER_STATION_PORT_PAIR.connection,
                                                WEATHER_STATION_PORT_PAIR.register_test_address)
        self._modbus_batteries_io = ModbusIO(WOBIT_BATTERIES_PORT_PAIR.connection,
                                             WOBIT_BATTERIES_PORT_PAIR.register_test_address)
        self._modbus_positioning_io = ModbusIO(WOBIT_POSITIONING_PORT_PAIR.connection,
                                               WOBIT_BATTERIES_PORT_PAIR.register_test_address)

    def find_ports(self):
        """
        For each modbus connection check if it's possible to establish connection on port from
        list. Changes are applied to PortPair classes from settings
        :raises: no errors are raised
        """
        for port_pair in self._port_pairs:
            for port in devices_modbus_connection.POSSIBLE_PORTS:
                connection = copy(port_pair.connection)
                connection.port = port
                modbus_io = ModbusIO(connection, port_pair.register_test_address)

                status = modbus_io.connect_and_test()
                if not modbus_utils.check_response_status(status):
                    continue

                if port_pair.name in devices_modbus_connection.DeviceID.WOBITS:
                    modbus_io.load_user_registers()

                status, value = modbus_io.test_reading_value()
                modbus_io.disconnect()

                if value[0] == port_pair.expected_response:
                    port_pair.connection.port = port
                    logging.info(f'Found modbus connection of device {port_pair.name}, '
                                 f'on port: {port_pair.connection.port}')
                    break

    def find_and_apply_ports(self):
        """
        Find ports that are paired with one of devices. Check if ports are valid. Apply connections
        to devices that are attributes to this class
        :raises: ConnectionError if ports assignment is not valid. Non-recoverable
        """
        self.find_ports()
        self.validate_port_pairs()
        self.apply_port_pairs()

    def connect_batteries_wobit(self) -> ModbusResponseStatus:
        status = self._modbus_batteries_io.connect_and_test()

        return status

    def connect_positioning_wobit(self) -> ModbusResponseStatus:
        status = self._modbus_positioning_io.connect_and_test()

        return status

    def connect_weather_station(self) -> ModbusResponseStatus:
        status = self._modbus_weather_station.connect_and_test()

        return status

    def connect_devices(self, positioning: bool = True, batteries: bool = True,
                        weather_station: bool = True):
        if batteries:
            status = self.connect_batteries_wobit()
            if not modbus_utils.check_response_status(status):
                raise ConnectionError('Could not connect to batteries Wobit')

        if positioning:
            status = self.connect_positioning_wobit()
            if not modbus_utils.check_response_status(status):
                raise ConnectionError('Could not connect to positioning Wobit')

        if weather_station:
            status = self.connect_weather_station()
            if not modbus_utils.check_response_status(status):
                raise ConnectionError('Could not connect to weather station')
