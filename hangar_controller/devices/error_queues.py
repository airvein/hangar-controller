import queue

from satella.coding.structures import Singleton


@Singleton
class ErrorQueue(queue.Queue):
    pass


@Singleton
class EventLogQueue(queue.Queue):
    pass
