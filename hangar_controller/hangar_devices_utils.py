import logging
import time

from hangar_controller.devices.error_queues import ErrorQueue, EventLogQueue

from hangar_controller.devices.modbus_io_manager import ModbusIOManager
from hangar_controller.devices.motors.motors import Motors
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.weather_station.weather_station_controller \
    import WeatherStationController

logger = logging.getLogger(__name__)


def set_up_devices():
    logger.info('[LAUNCHING] [START] Set Up devices')

    error_queue = ErrorQueue()
    event_log_queue = EventLogQueue()

    modbus_io_manager = ModbusIOManager()
    modbus_io_manager.find_and_apply_ports()

    motors = Motors(error_queue, event_log_queue)
    modbus_io_manager.connect_devices()
    motors.update_motors_parameters()
    motors.start()

    # modbus_io_manager.connect_devices()

    drivers = DriversController(error_queue, event_log_queue)
    drivers.start()
    time.sleep(1)

    WeatherStationController(modbus_io_manager.weather_station, error_queue)
    time.sleep(2)

    logger.info('[LAUNCHING] [END] Set Up devices')


def power_on_executive_subsystems():
    logger.info('[LAUNCHING] [START] Power ON executive subsystems')

    drivers = DriversController()

    logger.debug(drivers.pms_set_chargers_power(True))
    logger.debug(drivers.pms_set_motors_pos_power(True))
    logger.debug(drivers.pms_set_motors_man_power(True))
    logger.debug(drivers.pms_set_motors_lift_power(True))
    logger.debug(drivers.pms_set_air_compressor(True))
    logger.debug(drivers.pms_set_pressure(True))
    time.sleep(1)

    logger.info('[LAUNCHING] [END] Power ON executive subsystems')


def calibrate_mechanisms():
    logger.info('[LAUNCHING] [START] Calibrate mechanisms')

    motors = Motors()
    drivers = DriversController()

    logger.debug(motors.calibrate_grasper())
    logger.debug(motors.release())
    logger.debug(motors.calibrate_battery_motors())
    logger.debug(motors.calibrate_positioning())
    logger.debug(motors.position_drone())
    logger.debug(drivers.cargo_base_shift())
    logger.debug(motors.calibrate_lift())
    logger.debug(motors.go_lift_down())
    logger.debug(drivers.cargo_close_window())
    logger.debug(drivers.roof_close())
    time.sleep(1)

    logger.info('[LAUNCHING] [END] Calibrate mechanisms')


def tear_down_executive_subsystems():
    logger.info('[DISABLING] [START] Tear Down executive subsystems')

    drivers = DriversController()

    logger.debug(drivers.pms_set_chargers_power(False))
    logger.debug(drivers.pms_set_motors_pos_power(False))
    logger.debug(drivers.pms_set_motors_man_power(False))
    logger.debug(drivers.pms_set_motors_lift_power(False))
    logger.debug(drivers.pms_set_air_compressor(False))
    logger.debug(drivers.pms_set_pressure(False))

    ModbusIOManager().weather_station.disconnect()

    logger.info('[DISABLING] [END] Tear Down executive subsystems')
