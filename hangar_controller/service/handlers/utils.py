import typing as tp
import functools


def must_have_command_name(command_name: str):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(cmd: dict) -> bool:
            if cmd['command'] != command_name:
                return False
            return fun(cmd)

        return inner2

    return inner


def handler_factory(command_name: str, action: type) -> \
        tp.Callable[[dict], bool]:
    def generic_handler(cmd: dict) -> bool:
        if cmd['command'] != command_name:
            return False
        action().schedule()
        return True

    return generic_handler
