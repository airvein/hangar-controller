import logging
import typing as tp

from .crumb_handlers import handlers as crumb_actions_handlers
from .diagnostic_handlers import handlers as diagnostic_actions_handlers
from .sequence_handlers import handlers as sequence_actions_handlers

logger = logging.getLogger(__name__)

_handlers_type = tp.List[tp.Callable[[dict], bool]]
handlers: _handlers_type = crumb_actions_handlers \
                           + diagnostic_actions_handlers \
                           + sequence_actions_handlers
