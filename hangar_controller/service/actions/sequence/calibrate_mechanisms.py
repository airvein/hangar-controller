import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class CalibrateMechanismsAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.motors.calibrate_grasper())
        logger.debug(self.motors.release())
        logger.debug(self.motors.calibrate_battery_motors())
        logger.debug(self.motors.calibrate_positioning())
        logger.debug(self.motors.position_drone())
        logger.debug(self.drivers.cargo_base_shift())
        logger.debug(self.motors.calibrate_lift())
        logger.debug(self.motors.go_lift_down())
        logger.debug(self.drivers.cargo_close_window())
        logger.debug(self.drivers.roof_close())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
