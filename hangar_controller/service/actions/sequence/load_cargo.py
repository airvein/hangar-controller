import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class LoadCargoAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.drivers.cargo_work_shift())
        logger.debug(self.motors.go_cargo_window_pick_up())
        logger.debug(self.motors.grasp())
        logger.debug(self.motors.go_cargo_drone())
        logger.debug(self.motors.release())
        logger.debug(self.drivers.cargo_base_shift())
        logger.debug(self.motors.go_home())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
