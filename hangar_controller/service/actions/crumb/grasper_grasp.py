import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class GrasperGraspAction(BaseCrumbAction):
    def run(self):
        logger.debug(self.motors.grasp())

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
