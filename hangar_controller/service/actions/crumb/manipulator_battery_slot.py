import logging

from hangar_controller.service.actions.base_action import \
    BaseCrumbAction

logger = logging.getLogger(__name__)


class ManipulatorBatterySlotAction(BaseCrumbAction):
    def __init__(self, battery_slot_number: int):
        super(ManipulatorBatterySlotAction, self).__init__()
        self.battery_slot_number = battery_slot_number

    def run(self):
        logger.debug(self.motors.go_battery_slot(self.battery_slot_number))

    def can_run(self) -> bool:
        # TODO: Check that can run
        return True
