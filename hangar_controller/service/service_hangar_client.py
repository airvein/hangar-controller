import json
import logging
import typing as tp

from satella.coding.concurrent import CallableGroup
from satella.coding.structures import Singleton

from hangar_controller.mqtt_client import MQTTClient

logger = logging.getLogger(__name__)


@Singleton
class ServiceHangarClient(MQTTClient):
    def __init__(self):
        super(ServiceHangarClient, self).__init__(f'hangar_service_client')
        self.client.subscribe('serviceman', 2)

        self.message_validators = CallableGroup()
        self.message_handlers = CallableGroup()

    def add_validator(self, validator: tp.Callable[[dict], bool]):
        """
        Add a validator that accepts a message and returns whether it is correct

        All of your validators must return True for handler to be called upon it
        """
        self.message_validators.add(validator)

    def add_handler(self, handler: tp.Callable[[dict], bool]):
        """
        Register a handler to be called when a correct message is received

        Your handler should return True when correct handled message, else False
        """
        self.message_handlers.add(handler)

    def send_message(self, message: dict):
        message_text = json.dumps(message)
        self._publish_message(message_text, 'service_hangar')

    def _on_message_callback(self, client, userdata, message):
        raw_message = message.payload.decode("utf-8")
        command = json.loads(raw_message)

        if all(self.message_validators(command)):
            if not any(self.message_handlers(command)):
                logger.warning('Cannot perform received command from serviceman')
            else:
                logger.debug('Received correct command from serviceman')
        else:
            logger.error('Received invalid command from serviceman')
            logger.debug(f'Serviceman command: {command}')
