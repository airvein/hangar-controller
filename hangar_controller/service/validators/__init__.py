import typing as tp

from . import utils
from .crumb_validators import validators as crumb_actions_validators
from .diagnostic_validators import validators as diagnostic_actions_validators
from .sequence_validators import validators as sequence_actions_validators
from .utils import validators as utils_validators
from .crumb_validators import KNOWN_COMMAND as CRUMB_ACTIONS_KNOWN_COMMAND
from .diagnostic_validators import KNOWN_COMMAND as \
    DIAGNOSTIC_ACTIONS_KNOWN_COMMAND
from .sequence_validators import KNOWN_COMMAND as \
    SEQUENCE_ACTIONS_KNOWN_COMMAND

utils.KNOWN_COMMAND = CRUMB_ACTIONS_KNOWN_COMMAND \
                      + DIAGNOSTIC_ACTIONS_KNOWN_COMMAND \
                      + SEQUENCE_ACTIONS_KNOWN_COMMAND

_validators_type = tp.List[tp.Callable[[dict], bool]]
validators: _validators_type = utils_validators \
                               + crumb_actions_validators \
                               + diagnostic_actions_validators \
                               + sequence_actions_validators
