import typing as tp

from hangar_controller.service.validators.utils import _validate_command_name

KNOWN_COMMAND = ('get_weather',)


@_validate_command_name('get_weather')
def _validate_get_weather_action(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_get_weather_action
]
