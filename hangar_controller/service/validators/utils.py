import functools
import logging
import typing as tp

logger = logging.getLogger(__name__)

KNOWN_COMMAND = None
EXPECTED_COMMAND_SCHEMA = {'command': str, 'parameters': dict}


def _validate_command_name(command_name: str):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(command: dict) -> bool:
            try:
                if command['command'] != command_name:
                    return True
                return fun(command)
            except KeyError:
                return False

        return inner2

    return inner


def _validate_parameters_schema(
        expected_schema: tp.Dict[str, tp.Union[type, tp.Union[tp.Any]]]):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(command: dict) -> bool:
            try:
                parameters = command['parameters']
            except KeyError:
                return False

            for expected_key in tuple(expected_schema.keys()):
                if expected_key not in parameters.keys():
                    return False

                expected_types: tp.Union[tp.Tuple[tp.Any], tp.Any] = \
                    expected_schema[expected_key]

                if isinstance(expected_types, type(tp.Union)):
                    expected_types: type = expected_types.__args__

                # noinspection PyTypeHints
                correct_type = isinstance(parameters[expected_key],
                                          expected_types)
                if not correct_type:
                    return False

            return fun(parameters)

        return inner2

    return inner


def _validate_is_known_command(command: dict) -> bool:
    command_name = command.get('command', None)

    if command_name not in KNOWN_COMMAND:
        return False
    return True


def _validate_command_schema(command: dict) -> bool:
    for expected_key in tuple(EXPECTED_COMMAND_SCHEMA.keys()):
        if expected_key not in command.keys():
            return False

        # noinspection PyTypeHints
        if not isinstance(command[expected_key],
                          EXPECTED_COMMAND_SCHEMA[expected_key]):
            return False

        return True


validators: tp.List[tp.Callable[[dict], bool]] = [
    _validate_is_known_command,
    _validate_command_schema,
]
