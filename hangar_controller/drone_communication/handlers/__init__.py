import typing as tp
from hangar_controller.executor.actions import OnReceivedDroneLaunched, \
    OnReceivedDroneInitialized, OnReceivedDroneReady, OnReceivedCargoReady, \
    OnReceivedFlightEnded, OnReceivedFlightCanceled


def handler_factory(command_name: str, message_class: type) -> \
        tp.Callable[[dict], bool]:
    def inner(cmd: dict) -> bool:
        if cmd['command'] != command_name:
            return False
        message_class().schedule()
        return True

    return inner


handlers: tp.List[tp.Callable[[dict], bool]] = [
    handler_factory('drone_launched', OnReceivedDroneLaunched),
    handler_factory('cargo_ready', OnReceivedCargoReady),
    handler_factory('drone_initialized', OnReceivedDroneInitialized),
    handler_factory('drone_ready', OnReceivedDroneReady),
    handler_factory('flight_ended', OnReceivedFlightEnded),
    handler_factory('flight_canceled', OnReceivedFlightCanceled)
]
