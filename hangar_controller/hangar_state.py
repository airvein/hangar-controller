import enum


class HangarState(enum.Enum):
    HANGAR_FULL = 'hangar_full'
    DRONE_POWERING = 'drone_powering'
    DRONE_INIT = 'drone_init'
    CARGO_PREPARING = 'cargo_preparing'
    DRONE_PREPARING = 'drone_preparing'
    DRONE_READY = 'drone_ready'
    PROTECT_DRONE = 'protect_drone'
    HANGAR_EMPTY = 'hangar_empty'
    RECEIVING_DRONE = 'receiving_drone'
    WAITING_DRONE = 'waiting_drone'
    DRONE_RECEIVED = 'drone_received'
    FLIGHT_FINISHING = 'flight_finishing'
    CANCELLING_FLIGHT = 'canceling_flight'
    SHUTDOWN = 'shutdown'
    BREAKDOWN = 'breakdown'
