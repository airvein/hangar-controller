import inspect
import logging

from satella.coding.structures import Singleton

logger = logging.getLogger(__name__)

__all__ = ['MADRepository', 'register']


@Singleton
class MADRepository:
    def __init__(self):
        self.registry = {}
        self.mocking = {}

    def mock(self, item, new_class):
        """
        Use this in your unit tests to mock certain classes with other objects.

        Eg.
        with MADRepository().mock('rs485_wobbit', mock.Mock()) as mocked_object:
            ...
        """
        class ContextManager:
            def __enter__(slf):
                slf.item = item
                self.mocking[item] = self.registry[item]
                self.registry[item] = new_class
                return self.registry[item]

            def __exit__(slf, exc_type, exc_val, exc_tb):
                self.registry[slf.item] = self.mocking[slf.item]

        return ContextManager()

    def __getattr__(self, item):
        if item not in self.registry:
            raise AttributeError('Item %s not found in the registry' % (item,))

        if inspect.isclass(self.registry[item]):
            args = inspect.getfullargspec(self.registry[item].__init__).args[1:]  # skim off the self in args
            args = [getattr(self, arg) for arg in args]
            self.registry[item] = self.registry[item](*args)

        return self.registry[item]

    def register(self, item, cls):
        self.registry[item] = cls


def register(name):
    def inner(cls):
        MADRepository().register(name, cls)
        return cls

    return inner
