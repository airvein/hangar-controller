import logging
import typing as tp

from satella.coding.structures import Singleton

from hangar_controller.azure_communication.messages import DeviceState
from hangar_controller.hangar_state import HangarState

AzureIoTHubClient = 'hangar_controller.azure_communication.azure_iothub_client.AzureIoTHubClient'

logger = logging.getLogger(__name__)


@Singleton
class StateMachine:
    state: HangarState = HangarState.SHUTDOWN

    def __init__(self, azure_iothub_client: tp.Optional[AzureIoTHubClient] = None):
        azure_iothub_client_class_path = f'{azure_iothub_client.__class__.__module__}' \
                                         f'.{azure_iothub_client.__class__.__name__}'

        is_azure_iothub_client_empty = azure_iothub_client is None
        has_azure_iothub_client_expected_type = AzureIoTHubClient == azure_iothub_client_class_path
        if is_azure_iothub_client_empty or not has_azure_iothub_client_expected_type:
            logger.critical('StateMachine initialized without azure_iothub_client '
                            'or azure_iothub_client is not AzureIoTHubClient instance')
            raise TypeError

        self.azure_iothub_client = azure_iothub_client

    def transition(self, new_state: HangarState):
        self.azure_iothub_client.send_iothub_message(
            DeviceState(self.state, new_state)
        )
        self.state = new_state
