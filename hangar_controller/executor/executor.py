import collections
import concurrent.futures
import logging
import time
import threading
import typing as tp

from satella.coding.structures import Singleton
from satella.coding.concurrent import LockedDataset, TerminableThread

from hangar_controller.executor.state_machine import StateMachine
from hangar_controller.mad_repository import MADRepository

logger = logging.getLogger(__name__)


class CannotRun(Exception):
    """This action will not run, because it's can_run returned False"""


class ExecutorThreadData(LockedDataset):
    def __init__(self):
        from .actions import BaseAction
        super().__init__()
        with self:
            self.todo: tp.List[BaseAction] = collections.deque()
            self.currently_doing: tp.Set[BaseAction] = set()
            self.whens: tp.List[tp.Tuple[tp.Callable[[], bool], BaseAction]] = \
                []


@Singleton
class ExecutorThread(TerminableThread):
    def __init__(self):
        super().__init__()
        self.dataset = ExecutorThreadData()
        self.state_machine: StateMachine = StateMachine()

    def cleanup(self):
        with self.dataset as dataset:
            concurrent.futures.wait(dataset.currently_doing)
            dataset.currently_doing = set()

    def loop(self):
        time.sleep(0.5)
        with self.dataset as dataset:
            if len(dataset.todo) == 0:
                for when, action in dataset.whens:
                    if when():
                        dataset.todo.append(action)
                        dataset.whens.remove((when, action))
            completed, waiting = concurrent.futures.wait(
                dataset.currently_doing, 0.5,
                concurrent.futures.FIRST_COMPLETED)
            for completed_future in completed:
                dataset.currently_doing.remove(completed_future)

            if len(dataset.todo) == 0:
                return

            def class_to_qualname(x: tp.Union[str, type]) -> str:
                if not isinstance(x, str):
                    return x.__qualname__.split('.')[-1]
                return x

            first = dataset.todo[0]

            can_schedule = False
            if len(first.CAN_EXECUTE_CONCURRENTLY_WITH) == 0:
                if dataset.currently_doing:
                    return

            if len(first.CAN_EXECUTE_CONCURRENTLY_WITH) > 0:
                if first.CAN_EXECUTE_CONCURRENTLY_WITH[0] is True:
                    can_schedule = True

            if not can_schedule:
                can_execute_with = set(map(class_to_qualname,
                                           first.CAN_EXECUTE_CONCURRENTLY_WITH))
                currently_doing = set(
                    map(lambda x: class_to_qualname(x.__class__),
                        dataset.currently_doing))
                can_schedule = can_execute_with.issubset(currently_doing)

            # can we schedule another element?
            if can_schedule:
                first = dataset.todo.popleft()
                if first.set_running_or_notify_cancel():

                    if not first.can_run():
                        first.set_exception(CannotRun())
                        return

                    logger.info('Running %s', first)
                    dataset.currently_doing.add(first)
                    threading.Thread(target=run_action, args=(self,
                                                              first,)).start()

    def schedule(self, action: 'BaseAction',
                 when: tp.Optional[tp.Callable[[], bool]] = None) -> bool:
        """
        Schedule an action to execute
        :param action: action to schedule
        :param when: optional condition to start processing the action. Used
           to implement deferred actions.
        :return:
        """
        with self.dataset as dataset:
            action.api = ExecutorAPI(self, action)
            if when is None:
                dataset.todo.append(action)
                return action.can_run()
            else:
                if when():
                    dataset.todo.append(action)
                else:
                    dataset.whens.append((when, action))
                return True


class ExecutorAPI:
    def __init__(self, executor: ExecutorThread, action: 'BaseAction'):
        self.executor = executor
        self.action = action
        self.mad_repository = MADRepository()
        self.state_machine = executor.state_machine


def run_action(executor: ExecutorThread, action: 'BaseAction') -> None:
    try:
        action.api = ExecutorAPI(executor, action)
        a = action.run()
    except BaseException as e:
        action.set_exception(e)
    else:
        action.set_result(a)
