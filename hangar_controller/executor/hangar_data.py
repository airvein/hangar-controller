import random
import typing as tp

from satella.coding.concurrent import LockedDataset
from satella.coding.structures import Singleton

import settings
from hangar_controller.executor.state_machine import StateMachine


@Singleton
class HangarData(LockedDataset):
    def __init__(self):
        super(HangarData, self).__init__()
        with self:
            # Hangar Info
            self.launch_timestamp: tp.Optional[int] = None
            self.flight_id: tp.Optional[str] = None
            self.drone_id: tp.Optional[str] = settings.INIT_DRONE_ID
            self.hangar_type: tp.Optional[str] = None
            self.available_battery: int = 0
            self.init_flight_lock: bool = False

            # Cargo Info
            self.drone_cargo_id: tp.Optional[str] = None
            self.drone_cargo_weight: tp.Optional[int] = 0
            self.hangar_cargo_id: tp.Optional[str] = None
            self.hangar_cargo_weight: tp.Optional[int] = 0
            self.cancel_flight_cargo_id: tp.Optional[str] = None
            self.cancel_flight_cargo_weight: tp.Optional[int] = 0

            # Command Sync Data
            self.sync_is_flight_with_cargo: tp.Optional[bool] = None
            self.sync_route_id: tp.Optional[str] = None
            self.sync_drone_id: tp.Optional[str] = None
            self.sync_put_and_take_locked = False

    @LockedDataset.locked()
    def hangar_type_getter(self):
        hangar_type = self.hangar_type

        return hangar_type

    @LockedDataset.locked()
    def flight_id_getter(self):
        flight_id = self.flight_id

        return flight_id

    @LockedDataset.locked()
    def system_info_getter(self):
        system_info = {
            "drone_id": self.drone_id,
            "cargo_id": self.hangar_cargo_id,
            "cargo_weight": self.hangar_cargo_weight,
            "state": StateMachine().state.value,
            "available_battery": self.available_battery,
        }

        return system_info

    @LockedDataset.locked()
    def weather_info_getter(self):
        weather_info = {
            "wind_speed": float(random.uniform(0, 10)),
            "wind_direction": int(random.randrange(0, 360)),
            "temperature": float(random.uniform(20, 40)),
            "humidity": float(random.uniform(50, 90)),
            "pressure": float(random.uniform(997, 1015)),
        }

        return weather_info

    @LockedDataset.locked()
    def lock_put_and_take_actions(self):
        self.sync_put_and_take_locked = True

    @LockedDataset.locked()
    def unlock_put_and_take_actions(self):
        self.sync_put_and_take_locked = False
