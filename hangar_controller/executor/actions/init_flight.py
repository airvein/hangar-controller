import logging
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import DroneInitializing, \
    DronePrepare
from .cargo import LoadingDrone
from hangar_controller.azure_communication.messages import BaseIoTHubMessage

logger = logging.getLogger(__name__)


class InitFlight(BaseAction):
    """
    Action that is called in response to receiving the init_flight command from
    Azure
    """

    flight_id: str
    route_id: str
    cargo: bool

    def __init__(self, flight_id: str, route_id: str, cargo: bool):
        super().__init__()
        self.flight_id = flight_id
        self.route_id = route_id
        self.cargo = cargo

    def run(self):
        with self.hangar_data:
            self.hangar_data.flight_id = self.flight_id
            self.hangar_data.sync_route_id = self.route_id
            self.hangar_data.sync_is_flight_with_cargo = self.cargo
            self.hangar_data.hangar_type = 'S'
        BaseIoTHubMessage.reset_counter()

        self.api.state_machine.transition(HangarState.DRONE_POWERING)

        # TODO: Check best slot in battery chargers supervisor checker
        best_battery_slot = 1
        logger.debug(self.motors.go_battery_slot(best_battery_slot))
        logger.debug(self.motors.grasp())
        logger.debug(self.motors.go_battery_drone())
        logger.debug(self.motors.release())
        logger.debug(self.motors.go_home())

    def can_run(self) -> bool:
        with self.hangar_data:
            init_flight_locked = self.hangar_data.init_flight_lock
            hangar_has_cargo = self.hangar_data.hangar_cargo_id is not None

        hangar_has_correct_state = \
            self.api.state_machine.state == HangarState.HANGAR_FULL

        if init_flight_locked:
            return False

        if not self.cargo:
            return hangar_has_correct_state

        return hangar_has_correct_state and hangar_has_cargo


class OnReceivedDroneLaunched(BaseAction):

    def run(self):
        self.api.state_machine.transition(HangarState.DRONE_INIT)

        with self.hangar_data:
            route_id = self.hangar_data.sync_route_id
            flight_id = self.hangar_data.flight_id
            is_flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo

        HangarDroneMQTTClient().send_message(
            DroneInitializing(route_id, flight_id, is_flight_with_cargo)
        )

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.DRONE_POWERING


class OnReceivedDroneInitialized(BaseAction):

    def run(self):
        with self.hangar_data:
            is_flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo

        if is_flight_with_cargo:
            self.api.state_machine.transition(HangarState.CARGO_PREPARING)
            LoadingDrone().schedule()
        else:
            self.api.state_machine.transition(HangarState.DRONE_PREPARING)

            logger.debug(self.drivers.roof_open())
            logger.debug(self.motors.go_lift_up())
            logger.debug(self.motors.release_drone())
            logger.debug(self.drivers.pms_set_ir_lock_power(True))

            HangarDroneMQTTClient().send_message(DronePrepare())

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.DRONE_INIT


class OnReceivedCargoReady(BaseAction):

    def run(self):
        self.api.state_machine.transition(HangarState.DRONE_PREPARING)

        logger.debug(self.drivers.roof_open())
        logger.debug(self.motors.go_lift_up())
        logger.debug(self.motors.release_drone())
        logger.debug(self.drivers.pms_set_ir_lock_power(True))

        HangarDroneMQTTClient().send_message(DronePrepare())

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.CARGO_PREPARING


class OnReceivedDroneReady(BaseAction):

    def run(self):
        self.api.state_machine.transition(HangarState.DRONE_READY)

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.DRONE_PREPARING
