import logging
import time
import typing as tp

from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import FinishFlight, \
    ShutdownDrone
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from .cargo import UnloadingDrone

logger = logging.getLogger(__name__)


class DroneLanded(BaseAction):
    """
    Action that is called in response to receiving the drone_landed command
    from Azure
    """

    cargo_id: str
    cargo_weight: int

    def __init__(self, cargo_id: tp.Optional[str],
                 cargo_weight: tp.Optional[int]):
        super().__init__()
        self.cargo_id = cargo_id
        self.cargo_weight = cargo_weight

    def run(self):
        with self.hangar_data:
            self.hangar_data.drone_cargo_id = self.cargo_id
            self.hangar_data.drone_cargo_weight = self.cargo_weight

        self.api.state_machine.transition(HangarState.DRONE_RECEIVED)

        logger.debug(self.motors.position_drone())
        logger.debug(self.drivers.pms_set_ir_lock_power(False))
        logger.debug(self.motors.go_lift_down())
        logger.debug(self.drivers.roof_close())

        self.api.state_machine.transition(HangarState.FLIGHT_FINISHING)
        HangarDroneMQTTClient().send_message(FinishFlight())

        if self.cargo_id is not None:
            # this may tops fail to execute
            with self.hangar_data:
                self.hangar_data.init_flight_lock = True

            UnloadingDrone().schedule(when=UnloadingDrone.can_unload)

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.WAITING_DRONE


class OnReceivedFlightEnded(BaseAction):

    def run(self):
        HangarDroneMQTTClient().send_message(ShutdownDrone())
        time.sleep(10)

        # TODO: Check free slot in battery chargers supervisor checker
        free_battery_slot = 1
        logger.debug(self.motors.go_battery_drone())
        logger.debug(self.motors.grasp())
        logger.debug(self.motors.go_battery_slot(free_battery_slot))
        logger.debug(self.motors.release())
        logger.debug(self.motors.go_home())

        self.api.state_machine.transition(HangarState.HANGAR_FULL)
        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None
            self.hangar_data.drone_id = self.hangar_data.sync_drone_id
            self.hangar_data.sync_drone_id = None

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.FLIGHT_FINISHING
