import logging
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState

logger = logging.getLogger(__name__)


class LeaveDrone(BaseAction):
    """
    Action that is called in response to receiving the leave_drone command from
    Azure
    """

    def run(self):
        logger.debug(self.drivers.pms_set_ir_lock_power(False))
        logger.debug(self.motors.go_lift_down())
        logger.debug(self.drivers.roof_close())

        self.api.state_machine.transition(HangarState.HANGAR_EMPTY)

        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.PROTECT_DRONE
