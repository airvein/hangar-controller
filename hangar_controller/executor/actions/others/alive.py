import logging

from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import HangarHeartBeat
from ..abstract import BaseAction

logger = logging.getLogger(__name__)


class Alive(BaseAction):
    """
    Action that is called in response to receiving the alive command from Azure
    """
    CAN_EXECUTE_CONCURRENTLY_WITH = [True]
    operator_id: str

    def __init__(self, operator_id: str):
        super(Alive, self).__init__()
        self.operator_id = operator_id

    def run(self):
        AzureIoTHubClient().send_iothub_message(HangarHeartBeat(
            operator_id=self.operator_id))

    def can_run(self) -> bool:
        return True
