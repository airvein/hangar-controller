import logging
import time

from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from .cargo import UnloadingDrone
from .. import HangarData, ExecutorThread
from ...drone_communication import HangarDroneMQTTClient
from ...drone_communication.messages import FlightCancel, ShutdownDrone

logger = logging.getLogger(__name__)


class CancelFlight(BaseAction):
    """
    Action that is called in response to receiving the cancel_flight command
    from Azure
    """

    def run(self):
        self.api.state_machine.transition(HangarState.CANCELLING_FLIGHT)

        logger.debug(self.motors.position_drone())
        logger.debug(self.motors.go_lift_down())
        logger.debug(self.drivers.roof_close())

        time.sleep(0.01)

        with self.hangar_data:
            flight_with_cargo = self.hangar_data.sync_is_flight_with_cargo
            self.hangar_data.drone_cargo_id = \
                self.hangar_data.cancel_flight_cargo_id
            self.hangar_data.drone_cargo_weight = \
                self.hangar_data.cancel_flight_cargo_weight

        if flight_with_cargo:
            with self.hangar_data:
                self.hangar_data.init_flight_lock = True

            UnloadingDrone().schedule(when=UnloadingDrone.can_unload)

        HangarDroneMQTTClient().send_message(FlightCancel())

        # TODO: Remove below when drone will send "flight_canceled" message
        self.api.state_machine.transition(HangarState.HANGAR_FULL)

        with self.hangar_data:
            self.hangar_data.flight_id = None
            self.hangar_data.hangar_type = None

    def can_run(self) -> bool:
        with self.hangar_data:
            hangar_has_correct_state = self.api.state_machine.state in (
                HangarState.DRONE_POWERING, HangarState.DRONE_INIT,
                HangarState.CARGO_PREPARING, HangarState.DRONE_READY
            )

        return hangar_has_correct_state

    @staticmethod
    def can_cancel():
        with HangarData():
            hangar_has_correct_state = \
                ExecutorThread().state_machine.state == HangarState.DRONE_READY

        return hangar_has_correct_state


class OnReceivedFlightCanceled(BaseAction):

    def run(self):
        HangarDroneMQTTClient().send_message(ShutdownDrone())
        time.sleep(10)

        # TODO: Check free slot in battery chargers supervisor checker
        free_battery_slot = 1
        logger.debug(self.motors.go_battery_drone())
        logger.debug(self.motors.grasp())
        logger.debug(self.motors.go_battery_slot(free_battery_slot))
        logger.debug(self.motors.release())
        logger.debug(self.motors.go_home())

        self.api.state_machine.transition(HangarState.HANGAR_FULL)
        with self.hangar_data:
            self.hangar_data.hangar_type = None
            self.hangar_data.flight_id = None

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.CANCELLING_FLIGHT
