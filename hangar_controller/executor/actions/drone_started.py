import logging
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState

logger = logging.getLogger(__name__)


class DroneStarted(BaseAction):
    """
    Action that is called in response to receiving the drone_started command
    from Azure
    """

    def run(self):
        self.api.state_machine.transition(HangarState.PROTECT_DRONE)

        with self.hangar_data:
            self.hangar_data.drone_id = None
            self.hangar_data.sync_is_flight_with_cargo = False

            # Temporary data for possibly cancel_flight no longer needed
            self.hangar_data.cancel_flight_cargo_id = None
            self.hangar_data.cancel_flight_cargo_weight = 0

    def can_run(self) -> bool:
        return self.api.state_machine.state == HangarState.DRONE_READY
