import logging
from .abstract import BaseAction
from hangar_controller.hangar_state import HangarState
from hangar_controller.azure_communication.messages import BaseIoTHubMessage

logger = logging.getLogger(__name__)


class ReceiveDrone(BaseAction):
    """
    Action that is called in response to receiving the receive_drone command
    from Azure
    """

    flight_id: str
    drone_id: str

    def __init__(self, flight_id: str, drone_id: str):
        super().__init__()
        self.flight_id = flight_id
        self.drone_id = drone_id

    def run(self):
        with self.hangar_data:
            self.hangar_data.flight_id = self.flight_id
            self.hangar_data.sync_drone_id = self.drone_id
            self.hangar_data.hangar_type = 'E'
        BaseIoTHubMessage.reset_counter()

        self.api.state_machine.transition(HangarState.RECEIVING_DRONE)

        logger.debug(self.drivers.roof_open())
        logger.debug(self.motors.go_lift_up())
        logger.debug(self.drivers.pms_set_ir_lock_power(True))

        self.api.state_machine.transition(HangarState.WAITING_DRONE)

    def can_run(self) -> bool:
        return self.api.state_machine.state in (HangarState.HANGAR_EMPTY,
                                                HangarState.PROTECT_DRONE)
