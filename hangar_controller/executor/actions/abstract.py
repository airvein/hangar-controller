import abc
import concurrent.futures
import logging
import typing as tp
import uuid
from ..executor import ExecutorThread
from ..hangar_data import HangarData
from ...devices.drivers.drivers_controller import DriversController
from ...devices.motors.motors import Motors
from ...devices.weather_station.weather_station_controller import \
    WeatherStationController

logger = logging.getLogger(__name__)


class BaseAction(concurrent.futures.Future, metaclass=abc.ABCMeta):
    """Base class for an executor-schedulable actions"""
    CAN_EXECUTE_CONCURRENTLY_WITH: tp.List[type] = []  # list of subclasses of
    #  BaseAction, alternatively, you can set the first value to True to
    #  always be able co execute concurrently
    action_id: str

    def schedule(self, when=None) -> bool:
        return ExecutorThread().schedule(self, when=when)

    def __init__(self):
        """
        The .api field is filled in before .run() is launched
        """
        from ..executor import ExecutorAPI
        super(BaseAction, self).__init__()
        self.action_id = uuid.uuid4().hex
        self.api: ExecutorAPI = None
        self.hangar_data = HangarData()
        self.motors = Motors()
        self.drivers = DriversController()
        self.weather_station = WeatherStationController()

    @abc.abstractmethod
    def run(self):
        """Perform the action. This is performed in a separate thread."""
        pass

    @abc.abstractmethod
    def can_run(self) -> bool:
        """
        Can this entry, to the best knowing of current system state, be
        ran?
        """
