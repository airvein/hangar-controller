import logging

from ..abstract import BaseAction

from hangar_controller.hangar_state import HangarState
from hangar_controller.drone_communication import HangarDroneMQTTClient
from hangar_controller.drone_communication.messages import CargoPrepare
from hangar_controller.azure_communication import AzureIoTHubClient
from hangar_controller.azure_communication.messages import CargoAction, \
    CargoActionType, CargoActionObjectType

logger = logging.getLogger(__name__)


class LoadingDrone(BaseAction):

    def run(self):
        logger.debug(self.drivers.cargo_work_shift())
        logger.debug(self.motors.go_cargo_window_pick_up())
        logger.debug(self.motors.grasp())
        logger.debug(self.motors.go_cargo_drone())
        logger.debug(self.motors.release())
        logger.debug(self.drivers.cargo_base_shift())
        logger.debug(self.motors.go_home())

        with self.hangar_data:
            self.hangar_data.drone_cargo_id = \
                self.hangar_data.hangar_cargo_id
            self.hangar_data.drone_cargo_weight = \
                self.hangar_data.hangar_cargo_weight

            # Save cargo data for possibly cancel_flight
            self.hangar_data.cancel_flight_cargo_id = \
                self.hangar_data.hangar_cargo_id
            self.hangar_data.cancel_flight_cargo_weight = \
                self.hangar_data.cancel_flight_cargo_weight

            self.hangar_data.hangar_cargo_id = None
            self.hangar_data.hangar_cargo_weight = 0

        HangarDroneMQTTClient().send_message(CargoPrepare())

        with self.hangar_data:
            drone_id = self.hangar_data.drone_id
            cargo_id = self.hangar_data.drone_cargo_id
            cargo_weight = self.hangar_data.drone_cargo_weight

        cargo_action_message = CargoAction(
            CargoActionType.LOADING_DRONE, drone_id,
            CargoActionObjectType.DRONE, True, cargo_id, cargo_weight
        )

        AzureIoTHubClient().send_iothub_message(cargo_action_message)

    def can_run(self) -> bool:
        with self.hangar_data:
            drone_has_no_cargo = self.hangar_data.drone_cargo_id is None
            hangar_has_correct_state = \
                self.api.state_machine.state == HangarState.CARGO_PREPARING

        return drone_has_no_cargo and hangar_has_correct_state
