import logging
import functools
import typing as tp

from hangar_controller.executor import HangarData
from hangar_controller.executor.actions import InitFlight, DroneStarted, \
    LeaveDrone, ReceiveDrone, DroneLanded, ServiceFinishFlight, CancelFlight
from hangar_controller.executor.actions.cargo import UnloadingDrone
from hangar_controller.executor.actions.others.alive import Alive

logger = logging.getLogger(__name__)


def must_have_command_name(command_name: str):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(cmd: dict) -> bool:
            if cmd['name'] != command_name:
                return False
            return fun(cmd)

        return inner2

    return inner


@must_have_command_name('init_flight')
def handle_init_flight(cmd: dict) -> bool:
    return InitFlight(cmd['parameters']['flight_id'],
                      cmd['parameters']['route_id'],
                      cmd['parameters'].get('cargo', False)).schedule()


@must_have_command_name('drone_started')
def handle_drone_started(cmd: dict) -> bool:
    return DroneStarted().schedule()


@must_have_command_name('leave_drone')
def handle_leave_drone(cmd: dict) -> bool:
    return LeaveDrone().schedule()


@must_have_command_name('receive_drone')
def handle_receive_drone(cmd: dict) -> bool:
    return ReceiveDrone(cmd['parameters']['flight_id'],
                        cmd['parameters']['drone_id']).schedule()


@must_have_command_name('drone_landed')
def handle_drone_landed(cmd: dict) -> bool:
    return DroneLanded(cmd['parameters'].get('cargo_id', None),
                       cmd['parameters'].get('cargo_weight', None)).schedule()


@must_have_command_name('cancel_flight')
def handle_cancel_flight(cmd: dict) -> bool:
    return CancelFlight().schedule(when=CancelFlight.can_cancel)


@must_have_command_name('service_finish_flight')
def handle_service_finish_flight(cmd: dict) -> bool:
    return ServiceFinishFlight(cmd['parameters']['drone_id'],
                               cmd['parameters']['flight_id']).schedule()


@must_have_command_name('alive')
def handle_alive(cmd: dict) -> bool:
    return Alive(cmd["operator_id"]).schedule()


@must_have_command_name('unload_drone')
def handle_unload_drone(cmd: dict) -> bool:
    hangar_data = HangarData()

    with hangar_data:
        hangar_data.init_flight_lock = True

    return UnloadingDrone(cmd['parameters']['cargo_id'],
                          cmd['parameters']['cargo_weight']
                          ).schedule(when=UnloadingDrone.can_unload)


handlers: tp.List[tp.Callable[[dict], bool]] = [handle_init_flight,
                                                handle_drone_started,
                                                handle_leave_drone,
                                                handle_receive_drone,
                                                handle_drone_landed,
                                                handle_cancel_flight,
                                                handle_service_finish_flight,
                                                handle_alive,
                                                handle_unload_drone]
