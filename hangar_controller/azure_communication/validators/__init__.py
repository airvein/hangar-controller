import logging
import functools
import typing as tp


logger = logging.getLogger(__name__)

KNOWN_COMMAND = ('init_flight', 'cancel_flight', 'drone_started', 'leave_drone',
                 'receive_drone', 'drone_landed', 'alive', 'service_finish_flight',
                 'unload_drone')

EXPECTED_COMMAND_SCHEMA = {'timestamp': int, 'operator_id': str, 'name': str,
                           'parameters': dict}


def validate_azure_method_name(command: dict, azure_method_name: str):
    """Do not use with validators Callable list (different interface)"""
    command_name = command.get('name', None)

    if azure_method_name != command_name:
        logger.error("Azure method_name not equal to command_name")
        return False

    return True


def _validate_command_name(command_name: str):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(command: dict) -> bool:
            try:
                if command['name'] != command_name:
                    return True
                return fun(command)
            except KeyError:
                return False

        return inner2

    return inner


def _validate_schema(expected_schema: tp.Dict[str, tp.Union[type, tp.Union[tp.Any]]],
                     check_parameters: bool = True):
    def inner(fun):
        @functools.wraps(fun)
        def inner2(command: dict) -> bool:
            try:
                if check_parameters:
                    fields: dict = command['parameters']
                else:
                    fields: dict = command

                for expected_key in tuple(expected_schema.keys()):
                    if expected_key not in fields.keys():
                        return False

                    expected_value_types: tp.Optional[tp.List[type]] = expected_schema[
                        expected_key]

                    if not isinstance(expected_value_types, type(tp.Union)):
                        expected_value_types: type
                        expected_value_types: tp.List[type] = [expected_value_types]
                    else:
                        expected_value_types: type = expected_value_types.__args__

                    correct_type = []
                    for expected_value_type in expected_value_types:
                        # noinspection PyTypeHints
                        if not isinstance(fields[expected_key], expected_value_type):
                            correct_type.append(False)
                        else:
                            correct_type.append(True)

                    if not any(correct_type):
                        return False
                return fun(command)
            except KeyError:
                return False

        return inner2

    return inner


def _validate_is_known_command(command: dict) -> bool:
    command_name = command.get('name', None)

    if command_name not in KNOWN_COMMAND:
        return False
    return True


@_validate_schema(EXPECTED_COMMAND_SCHEMA, False)
def _validate_command_schema(command: dict) -> bool:
    return True


@_validate_command_name('init_flight')
@_validate_schema({'flight_id': str, 'route_id': str, 'cargo': bool})
def _validate_init_flight(command: dict) -> bool:
    return True


@_validate_command_name('receive_drone')
@_validate_schema({'flight_id': str, 'drone_id': str})
def _validate_receive_drone(command: dict) -> bool:
    return True


@_validate_command_name('drone_landed')
@_validate_schema({'cargo_id': tp.Optional[str], 'cargo_weight': tp.Optional[int]})
def _validate_drone_landed(command: dict) -> bool:
    return True


@_validate_command_name('service_finish_flight')
@_validate_schema({'flight_id': str, 'drone_id': str})
def _validate_service_finish_flight(command: object) -> object:
    return True


@_validate_command_name('unload_drone')
@_validate_schema({'cargo_id': str, 'cargo_weight': int})
def _validate_unload_drone(command: dict) -> bool:
    return True


validators: tp.List[tp.Callable[[dict], bool]] = [_validate_is_known_command,
                                                  _validate_command_schema,
                                                  _validate_init_flight,
                                                  _validate_receive_drone,
                                                  _validate_drone_landed,
                                                  _validate_service_finish_flight,
                                                  _validate_unload_drone]
