# Hangar Controller

## Important environment variables
The defaults are set [settings.py](settings.py) for you.

### MQTT:
* **MQTT_BROKER_ADDRESS** - MQTT broker address

### Azure:
* **AZURE_DEVICE_ID** - device identifier
* **AZURE_SHARED_ACCESS_KEY** - IoTHub device primary key
### Hangar:
* **HANGAR_POS_LAT** - latitude of the hangar
* **HANGAR_POS_LON** - longitude of the hangar
* **HANGAR_POS_ALT** - altitude of the hangar
* **INIT_DRONE_ID** - initial identifier of the drone located in the hangar
* **MODBUS_USB_PORT_0** - path to first usb port of one of three modbus devices
* **MODBUS_USB_PORT_1** - path to second usb port of one of three modbus devices
* **MODBUS_USB_PORT_2** - path to third usb port of one of three modbus devices

### Debug Mode
* **PENTACOMP_API_MOCK_ADDRESS** - PCSI API mock address

## Running
Remember to check `--build, force build images` options in _Run/Debug Configuration_ if you run services with PyCharm!
### Running the application
Just use `docker-compose up --build run` to run the app. 

### Running the unittests
Run `docker-compose up --build unittests`

### Running manual tests tools/mocks
Remember to define env *MQTT_BROKER_ADDRESS* and optionally *PYTHONPATH*


## Local setup
Local setup is used mainly for development with autocomplete or eventually for fast unittest running (without other 
set up it can be used only with unittests, it's not suitable for tests that using MQTT or other 
external services, in that case, it's better to use Docker)

### Download project
```
git clone git@git.cervirobotics.com:airvein/hangar/hangar-controller.git
```

### Go to project directory
```
cd hangar-controler
```

### Create venv
```
python3 -m venv .venv
```

### Activate venv
```
source .venv/bin/activate
```

### Install requirements
```
pip install -r requirements.txt
```

### Start programming :)

### Deactivate venv
```
cd hangar-controller
dectivate
```
