from unittest import TestCase, mock
from unittest.mock import call

import settings
from hangar_controller.azure_communication.messages import CargoActionObjectType
from hangar_controller.executor.actions.cargo import PutToHangar
from hangar_controller.put_and_take_to_hangar_backend.api_backend import \
    APIBackend, WrongStatusCode, WrongSchema, UnknownAction, Actions


class TestAPIBackend(TestCase):
    def setUp(self) -> None:
        self.requests_get_patcher = mock.patch('requests.get')
        self.requests_get_mock = self.requests_get_patcher.start()

        self.test_api_backend = APIBackend()

    def tearDown(self) -> None:
        self.requests_get_patcher.stop()

    @mock.patch('time.sleep', return_value=None)
    def test_loop_tick_exception_called(self, time_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (_ for _ in ()).throw(WrongStatusCode)
        self.assertEqual(None, self.test_api_backend.loop_tick())

        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (_ for _ in ()).throw(WrongSchema)
        self.assertEqual(None, self.test_api_backend.loop_tick())

        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (_ for _ in ()).throw(UnknownAction)
        self.assertEqual(None, self.test_api_backend.loop_tick())

    @mock.patch('time.sleep', return_value=None)
    def test_loop_tick_none_action(self, time_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (Actions.NONE, None, None)

        self.test_api_backend.loop_tick()
        time_mock.assert_called_once_with(10)

    @mock.patch('hangar_controller.put_and_take_to_hangar_backend.api_backend'
                '.logger')
    @mock.patch('time.sleep', return_value=None)
    def test_loop_tick_unknown_user_type(self, time_mock, logger_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (Actions.TAKE_FROM_HANGAR, 'UNKNOWN_USER_TYPE', None)

        self.test_api_backend.loop_tick()
        time_mock.assert_called_once_with(10)
        logger_mock.error.assert_called_once()

    @mock.patch('hangar_controller.executor.actions.cargo.take_from_hangar'
                '.TakeFromHangar.__new__')
    @mock.patch('hangar_controller.put_and_take_to_hangar_backend.api_backend'
                '.logger')
    def test_loop_tick_take_from_hangar_action(self, logger_mock,
                                               take_from_hangar_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (Actions.TAKE_FROM_HANGAR, 'client', None)

        self.test_api_backend.loop_tick()
        take_from_hangar_mock.assert_called_once()
        take_from_hangar_mock.return_value.schedule.assert_called_once()

        tfh_mock_call_args = take_from_hangar_mock.call_args[0][1:]
        self.assertEqual(None, tfh_mock_call_args[0])
        self.assertEqual(CargoActionObjectType("client"), tfh_mock_call_args[1])

        take_from_hangar_mock.return_value.schedule.return_value = False
        self.test_api_backend.loop_tick()
        logger_mock.warning.assert_called_once()

    @mock.patch('hangar_controller.executor.actions.cargo.put_to_hangar'
                '.PutToHangar.__new__')
    def test_loop_tick_put_to_hangar_action(self, put_to_hangar_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (Actions.PUT_TO_HANGAR, 'client', "1234CD")

        self.test_api_backend.loop_tick()
        put_to_hangar_mock.assert_called_once()
        put_to_hangar_mock.return_value.schedule.assert_called_once()

        expected_call = call(PutToHangar, None, CargoActionObjectType("client"),
                             pin='1234CD')

        self.assertEqual(expected_call, put_to_hangar_mock.call_args)

    @mock.patch('hangar_controller.put_and_take_to_hangar_backend.api_backend'
                '.logger')
    def test_loop_tick_unknown_action(self, logger_mock):
        self.test_api_backend._get_expected_cargo_user_info = \
            lambda: (Actions, 'client', None)

        self.test_api_backend.loop_tick()
        logger_mock.warning.assert_called_once()

    def test_cargo_user_info_correct_request(self):
        try:
            self.test_api_backend._get_expected_cargo_user_info()
        except (WrongStatusCode, WrongSchema, UnknownAction):
            pass

        self.requests_get_mock.assert_called_once_with(
            'http://airveincrudapi.azurewebsites.net/api/CargoUserInfo',
            {'deviceid': settings.HANGAR_ID}
        )

    def test_cargo_user_info_wrong_status_code(self):
        self.requests_get_mock.return_value.status_code = 400

        tested_method = self.test_api_backend._get_expected_cargo_user_info
        self.assertRaises(WrongStatusCode, tested_method)

    def test_cargo_user_info_schema_validation(self):
        self.requests_get_mock.return_value.status_code = 200
        self.requests_get_mock.return_value.json.return_value = {
            "action": "none", "user_type": None, "pin": None
        }

        tested_method = self.test_api_backend._get_expected_cargo_user_info

        tested_method()

        self.requests_get_mock.return_value.json.return_value = {
            "wrong_key1": None, "wrong_key2": None, "wrong_key3": None
        }
        self.assertRaises(WrongSchema, tested_method)

        self.requests_get_mock.return_value.json.return_value = {
            "action": 'put_to_hangar', "wrong_key2": None, "wrong_key3": None
        }
        self.assertRaises(WrongSchema, tested_method)

    def test_cargo_user_info_action_type(self):
        self.requests_get_mock.return_value.status_code = 200
        self.requests_get_mock.return_value.json.return_value = {
            "action": "WRONG", "user_type": 'client', "pin": "1234CD"
        }

        tested_method = self.test_api_backend._get_expected_cargo_user_info

        self.assertRaises(UnknownAction, tested_method)

        self.requests_get_mock.return_value.json.return_value = {
            "action": "none",
        }
        cargo_user_info = tested_method()
        self.assertEqual((Actions.NONE, None, None), cargo_user_info)

        self.requests_get_mock.return_value.json.return_value = {
            "action": "put_to_hangar", "user_type": 'client', "pin": "1234CD"
        }
        cargo_user_info = tested_method()
        self.assertEqual(
            (Actions.PUT_TO_HANGAR, 'client', '1234CD'), cargo_user_info
        )

        self.requests_get_mock.return_value.json.return_value = {
            "action": "take_from_hangar", "user_type": 'client', "pin": None
        }
        cargo_user_info = tested_method()
        self.assertEqual((Actions.TAKE_FROM_HANGAR, "client", None),
                         cargo_user_info)
