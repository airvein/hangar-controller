from unittest import TestCase

from hangar_controller.devices.weather_station.controller.telemetry import Telemetry


class TestTelemetry(TestCase):
    def setUp(self) -> None:
        self.telemetry = Telemetry()

    def test___init__(self):
        self.assertIsNone(self.telemetry.wind_direction)
        self.assertIsNone(self.telemetry.wind_speed)
        self.assertIsNone(self.telemetry.temperature)
        self.assertIsNone(self.telemetry.humidity)
        self.assertIsNone(self.telemetry.pressure)

    def test___repr__(self):
        # given
        self.telemetry.wind_speed = 1
        self.telemetry.wind_direction = 2
        self.telemetry.temperature = 3
        self.telemetry.humidity = 4
        self.telemetry.pressure = 5

        # when
        result = self.telemetry.__repr__()

        # then
        self.assertEqual('wind_direction: 2\nwind_speed: 1\ntemperature: 3\n'
                         'humidity: 4\npressure: 5', result)
