from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.weather_station.controller.value_reader import ValueReader
from settingsd.weather_station import modbus_constants
from settingsd.weather_station.modbus_constants import HoldingRegisters, RealRegisters


class TestValueReader(TestCase):
    def setUp(self) -> None:
        self.modbus_io = MagicMock()
        self.executor = MagicMock()
        self.value_reader = ValueReader(self.modbus_io, self.executor)

    def test___init__(self):
        self.assertEqual(self.value_reader.executor, self.executor)
        self.assertEqual(self.value_reader.modbus_io, self.modbus_io)

    def test__read_holding_register(self):
        # given
        self.value_reader.executor = MagicMock()
        address = MagicMock(spec=HoldingRegisters)

        # when
        self.value_reader._read_holding_register(address)

        # then
        self.value_reader.executor.submit.assert_called_with(
            self.value_reader.modbus_io.read_holding_registers,
            address
        )

    def test__read_real_register(self):
        # given
        self.value_reader.executor = MagicMock()
        address = MagicMock(spec=RealRegisters)

        # when
        self.value_reader._read_real_register(address)

        # then
        self.value_reader.executor.submit.assert_called_with(
            self.value_reader.modbus_io.read_real_register,
            address
        )

    def test_read_register_holding_register(self):
        # given
        address = modbus_constants.HoldingRegisters.PRECIPITATION_TYPE
        self.value_reader._read_holding_register = MagicMock()

        # when
        self.value_reader.read_register(address)

        # then
        self.value_reader._read_holding_register.assert_called()

    def test_read_register_real_register(self):
        # given
        address = modbus_constants.RealRegisters.PRECIPITATION_INTENSITY
        self.value_reader._read_real_register = MagicMock()

        # when
        self.value_reader.read_register(address)

        # then
        self.value_reader._read_real_register.assert_called()

    def test_read_register_unspecified_register(self):
        # given
        address = MagicMock(spec=RealRegisters)
        self.value_reader._read_real_register = MagicMock()

        # when
        self.assertRaises(NotImplementedError, self.value_reader.read_register,
                          address)
