import queue
from unittest import TestCase
from unittest.mock import MagicMock, patch, call

from hangar_controller.devices.weather_station.controller.telemetry import \
    Telemetry
from hangar_controller.devices.weather_station.controller.weather_station_process import \
    WeatherStationProcess
from hangar_controller.devices.weather_station.weather_station_controller import \
    WeatherStationController
from settingsd.weather_station import modbus_settings


class TestWeatherStationProcess(TestCase):
    def setUp(self) -> None:
        error_queue = MagicMock()
        self.telemetry_process = WeatherStationProcess(error_queue)

    def test___init__(self):
        self.assertIsInstance(self.telemetry_process._controller,
                              WeatherStationController)
        self.assertIsInstance(self.telemetry_process._telemetry,
                              Telemetry)
        self.assertIsInstance(self.telemetry_process._internal_error_queue,
                              queue.Queue)
        self.assertEqual(modbus_settings.TelemetrySettings.FREQUENCY,
                         self.telemetry_process._frequency)
        self.assertFalse(self.telemetry_process._stop_update)

    def test_telemetry_prop(self):
        self.assertEqual(self.telemetry_process._telemetry,
                         self.telemetry_process.telemetry)

    def test_stop(self):
        # given

        # when
        self.telemetry_process.stop()

        # then
        self.assertTrue(self.telemetry_process._stop_update)

    @patch(
    'hangar_controller.devices.motors.modbus_interface.modbus_utils.check_response_status')
    def test_connect(self, check_response_mock):
        # given
        self.telemetry_process._modbus_io = MagicMock()

        # when
        self.telemetry_process.connect()

        # then
        self.telemetry_process._modbus_io.connect_and_test.assert_called()
        check_response_mock.assert_called()

    @patch('time.sleep')
    def test_reconnect_calls_connect_n_times(self, sleep_mock):
        # given
        n = modbus_settings.TelemetrySettings.RECONNECT_TIMES
        self.telemetry_process.connect = MagicMock(return_value=False)
        self.telemetry_process._error_queue = MagicMock()

        # when
        result = self.telemetry_process.reconnect()

        # then
        self.telemetry_process.connect.assert_has_calls([call(), ] * n)
        sleep_mock.assert_has_calls([call(1), ] * n)
        self.telemetry_process._error_queue.put.assert_called_with(
            ConnectionError)
        self.assertFalse(result)

    @patch('time.sleep')
    def test_reconnect_returns_True_if_connected(self, sleep_mock):
        # given
        n = modbus_settings.TelemetrySettings.RECONNECT_TIMES
        self.telemetry_process.connect = MagicMock(return_value=True)

        # when
        result = self.telemetry_process.reconnect()

        # then
        self.telemetry_process.connect.assert_called_once()
        sleep_mock.assert_not_called()
        self.assertTrue(result)

    def test__update_telemetry_values(self):
        # given
        self.telemetry_process._controller = MagicMock()
        self.telemetry_process._controller.read_wind_speed = MagicMock(
            return_value='2')
        self.telemetry_process._controller.read_wind_direction = MagicMock(
            return_value='3')
        self.telemetry_process._controller.read_temperature = MagicMock(
            return_value='4')
        self.telemetry_process._controller.read_humidity = MagicMock(
            return_value='5')
        self.telemetry_process._controller.read_pressure = MagicMock(
            return_value='6')

        # when
        self.telemetry_process._update_telemetry_values()

        # then
        self.assertEqual('2', self.telemetry_process._telemetry.wind_speed)
        self.assertEqual('3', self.telemetry_process._telemetry.wind_direction)
        self.assertEqual('4', self.telemetry_process._telemetry.temperature)
        self.assertEqual('5', self.telemetry_process._telemetry.humidity)
        self.assertEqual('6', self.telemetry_process._telemetry.pressure)

    def test_check_error_queue(self):
        # given
        self.telemetry_process._internal_error_queue = MagicMock()
        self.telemetry_process._internal_error_queue.empty = MagicMock(
            return_value=True)

        # when
        result = self.telemetry_process._check_error_queue()

        # then
        self.telemetry_process._internal_error_queue.empty.assert_called()
        self.assertTrue(result)

    def test_check_error_queue_not_empty(self):
        # given
        self.telemetry_process._internal_error_queue = MagicMock()
        self.telemetry_process._internal_error_queue.empty = MagicMock(
            return_value=False)

        # when
        result = self.telemetry_process._check_error_queue()

        # then
        self.telemetry_process._internal_error_queue.empty.assert_called()
        self.assertFalse(result)

    def test_try_reading_values_returns_True_if_values_read(self):
        # given
        self.telemetry_process._update_telemetry_values = MagicMock()
        self.telemetry_process._check_error_queue = MagicMock(return_value=True)

        # when
        result = self.telemetry_process.try_reading_values()

        # then
        self.telemetry_process._update_telemetry_values.assert_called()
        self.assertTrue(result)

    def test_try_reading_values_returns_False_if_cant_reconnect(self):
        # given
        self.telemetry_process._update_telemetry_values = MagicMock()
        self.telemetry_process._check_error_queue = MagicMock(
            return_value=False)
        self.telemetry_process.reconnect = MagicMock(return_value=False)

        # when
        result = self.telemetry_process.try_reading_values()

        # then
        self.telemetry_process._update_telemetry_values.assert_called()
        self.telemetry_process.reconnect.assert_called()
        self.assertFalse(result)

    def test_try_reading_values_returns_True_if_reconnected(self):
        # given
        self.telemetry_process._update_telemetry_values = MagicMock()
        self.telemetry_process._check_error_queue = MagicMock(
            return_value=False)
        self.telemetry_process.reconnect = MagicMock(return_value=True)

        # when
        result = self.telemetry_process.try_reading_values()

        # then
        self.telemetry_process._update_telemetry_values.assert_called()
        self.telemetry_process.reconnect.assert_called()
        self.assertTrue(result)

    @patch('time.sleep')
    def test_run_tries_reading_values_until_stop(self, sleep_mock):
        # given
        def stop(*args):
            self.telemetry_process._stop_update = True

        sleep_mock.side_effect = stop
        self.telemetry_process.try_reading_values = MagicMock(return_value=True)
        self.telemetry_process._modbus_io = MagicMock()

        # when
        self.telemetry_process.run()

        # then
        self.telemetry_process.try_reading_values.assert_called()
        sleep_mock.assert_called_with(1 / self.telemetry_process._frequency)
        self.telemetry_process._modbus_io.disconnect.assert_called()

    @patch('time.sleep')
    def test_run_disconnects_after_failed_to_reconnect(self, sleep_mock):
        # given
        def stop(*args):
            self.telemetry_process._stop_update = True

        sleep_mock.side_effect = stop
        self.telemetry_process.try_reading_values = MagicMock(
            return_value=False)
        self.telemetry_process._modbus_io = MagicMock()

        # when
        self.telemetry_process.run()

        # then
        self.telemetry_process.try_reading_values.assert_called()
        sleep_mock.assert_not_called()
        self.telemetry_process._modbus_io.disconnect.assert_called()
