from concurrent.futures.thread import ThreadPoolExecutor
from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.weather_station.weather_station_controller import \
    WeatherStationController
from settingsd.weather_station import modbus_settings
from settingsd.weather_station.modbus_constants import HoldingRegisters, \
    RealRegisters


class TestWeatherStationController(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_queue = MagicMock()

    def setUp(self) -> None:
        self.weather = WeatherStationController(
            self.modbus_io, self.error_queue)

    def tearDown(self) -> None:
        WeatherStationController.__it__ = None

    def test___init__(self):
        self.assertIsInstance(self.weather.thread_pool_executor,
                              ThreadPoolExecutor)
        self.assertEqual(modbus_settings.MessagesSettings.TIMEOUT,
                         self.weather.timeout)

    @patch(
        'hangar_controller.devices.motors.modbus_interface.modbus_utils.check_response_status')
    def test__wait_checks_response_value_if_no_timeout2(self,
                                                        check_status_mock):
        # given
        check_status_mock.return_value = True
        future = MagicMock()
        future.result = MagicMock(return_value=('status', 'result'))
        self.weather.error_queue = MagicMock()

        # when
        result = self.weather._wait_for_response_and_handle_errors(future)

        # then
        self.weather.error_queue.put.assert_not_called()
        self.assertEqual('result', result)

    def test__send_and_wait(self):
        # given
        self.weather.value_reader.read_register = MagicMock()
        self.weather._wait_for_response_and_handle_errors = MagicMock(
            return_value='result')
        address = MagicMock(spec=HoldingRegisters)

        # when
        result = self.weather._send_and_wait(address)

        # then
        self.weather.value_reader.read_register.assert_called()
        self.weather._wait_for_response_and_handle_errors.assert_called()
        self.assertEqual('result', result)

    def read_register_test_template(self, weather_callable, expected_address):
        # given
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = weather_callable()

        # then
        self.weather._send_and_wait.assert_called_with(expected_address)

        return result, response
    
    @patch('hangar_controller.devices.weather_station.controller.weather_station_utils.device_status_parser')
    def test_read_device_state(self, status_parser_mock):
        # given
        status_parser_mock.return_value = 'device_state'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_device_state()

        # then
        self.weather._send_and_wait.assert_called_with(
            HoldingRegisters.DEVICE_STATE)
        status_parser_mock.assert_called_with(response)
        self.assertEqual('device_state', result)

    @patch('hangar_controller.devices.weather_station.controller.weather_station_utils.angle_parser')
    def test_read_wind_direction(self, angle_parser_mock):
        # given
        angle_parser_mock.return_value = 'angle'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_wind_direction()

        # then
        self.weather._send_and_wait.assert_called_with(
            HoldingRegisters.WIND_DIRECTION)
        angle_parser_mock.assert_called_with(response)
        self.assertEqual('angle', result)

    @patch('hangar_controller.devices.motors.modbus_interface.modbus_utils.registers_to_float')
    def test_read_wind_speed(self, reg2float_mock):
        # given
        reg2float_mock.return_value = 'wind_speed'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_wind_speed()

        # then
        self.weather._send_and_wait.assert_called_with(RealRegisters.WIND_SPEED)
        reg2float_mock.assert_called_with(response)
        self.assertEqual('wind_speed', result)

    @patch('hangar_controller.devices.motors.modbus_interface.modbus_utils.registers_to_float')
    def test_read_temperature(self, reg2float_mock):
        # given
        reg2float_mock.return_value = 'temp'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_temperature()

        # then
        self.weather._send_and_wait.assert_called_with(
            RealRegisters.TEMPERATURE)
        reg2float_mock.assert_called_with(response)
        self.assertEqual('temp', result)

    @patch('hangar_controller.devices.motors.modbus_interface.modbus_utils.registers_to_float')
    def test_read_humidity(self, reg2float_mock):
        # given
        reg2float_mock.return_value = 'humidity'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_humidity()

        # then
        self.weather._send_and_wait.assert_called_with(RealRegisters.HUMIDITY)
        reg2float_mock.assert_called_with(response)
        self.assertEqual('humidity', result)

    @patch('hangar_controller.devices.motors.modbus_interface.modbus_utils.registers_to_float')
    def test_read_pressure(self, reg2float_mock):
        # given
        reg2float_mock.return_value = 'pressure'
        response = MagicMock()
        self.weather._send_and_wait = MagicMock(return_value=response)

        # when
        result = self.weather.read_pressure()

        # then
        self.weather._send_and_wait.assert_called_with(RealRegisters.PRESSURE)
        reg2float_mock.assert_called_with(response)
        self.assertEqual('pressure', result)
