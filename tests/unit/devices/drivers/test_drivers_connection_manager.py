import queue
from unittest import TestCase
from unittest.mock import MagicMock, patch, call

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.hangar_drivers_mqtt_client import HangarDriversMQTTClient
from hangar_controller.devices.drivers.messages.message_utils.message_utils import get_topic
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_communication_settings import Quantity
from settings import MQTT_BROKER_ADDRESS


class TestMQTTClient(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.mqtt_client = MagicMock()
        cls.address = MQTT_BROKER_ADDRESS

    @patch.object(mqtt.Client, 'connect')
    @patch.object(mqtt.Client, 'loop_start')
    @patch.object(HangarDriversMQTTClient, '_subscribe')
    def setUp(self, subscribe_mock, loop_start_mock, connect_mock) -> None:
        self.client = HangarDriversMQTTClient(self.address)

    @patch.object(mqtt.Client, 'connect')
    @patch.object(mqtt.Client, 'loop_start')
    @patch.object(HangarDriversMQTTClient, '_subscribe')
    def test___init__(self, subscribe_mock, loop_start_mock, connect_mock):
        # when
        self.client = HangarDriversMQTTClient(self.address)

        # then
        self.assertIsInstance(self.client.client, mqtt.Client)
        self.assertIsInstance(self.client.messages_queue, queue.Queue)
        self.assertEqual(self.client._on_message_callback,
                         self.client.client.on_message)
        connect_mock.assert_called()
        loop_start_mock.assert_called()
        subscribe_mock.assert_called()

    def test__subscribe(self):
        self.client.client = MagicMock()
        controllers_topics = [value for topic, value in
                              Controllers.__dict__.items()
                              if not topic.startswith('__')]
        calls = [call(f'{topic}/#', 2) for topic in controllers_topics]
        calls.append(call(f'{get_topic(Quantity.TIME)}'))

        # when
        self.client._subscribe()

        # then
        self.client.client.subscribe.assert_has_calls(calls)

    def test_on_message_callback(self):
        # given
        self.client.messages_queue = MagicMock()
        message = MagicMock()

        # when
        self.client._on_message_callback(MagicMock(), MagicMock(), message)

        # then
        self.client.messages_queue.put.assert_called_with(message)

    def test_send_message(self):
        # given
        self.client.client.publish = MagicMock()

        # when
        self.client.send_message('topic', 'message')

        # then
        self.client.client.publish('topic', 'message')

    def test_close_connection(self):
        # given
        self.client.client = MagicMock()

        # when
        self.client.close_connection()

        # then
        self.client.client.loop_stop.assert_called()
