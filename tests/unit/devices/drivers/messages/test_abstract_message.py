from unittest import TestCase

from hangar_controller.devices.drivers.messages.abstract_message import AbstractMessage


class TestAbstractMessage(TestCase):
    def setUp(self) -> None:
        self.abstract_message = AbstractMessage('topic')

    def test___init__(self):
        self.assertEqual('topic', self.abstract_message.topic)

    def test___eq__(self):
        # given
        another = AbstractMessage('topic')

        # when
        equal = another == self.abstract_message

        # then
        self.assertTrue(equal)

    def test___eq__non_equal(self):
        # given
        another = AbstractMessage('different_topic')
        
        # when
        equal = another == self.abstract_message

        # then
        self.assertFalse(equal)
