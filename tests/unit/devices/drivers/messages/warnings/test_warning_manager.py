from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.warnings.warning_manager import WarningManager
from hangar_controller.devices.drivers.messages.warnings.warning_message import WarningMessage


class TestWarningManager(TestCase):
    def setUp(self) -> None:
        self.warning_manager = WarningManager(MagicMock())

    def test___init__(self):
        self.assertIsInstance(self.warning_manager, AbstractMessageManager)

    def test_init_warning_messages(self):
        # given
        self.warning_manager.register_messages = MagicMock()

        # when
        self.warning_manager._init_warning_messages()

        # then
        self.warning_manager.register_messages.assert_called()

    def test_receive(self):
        # given
        warning_message = MagicMock(spec=WarningMessage)
        self.warning_manager.get_message_by_topic = MagicMock(
            return_value=warning_message
        )
        message = MagicMock()
        self.warning_manager._error_warning_log_queue = MagicMock()

        # when
        self.warning_manager.receive(message)

        # then
        self.warning_manager.get_message_by_topic.assert_called_with(
            message.topic)
        self.warning_manager._error_warning_log_queue.put.assert_called()
