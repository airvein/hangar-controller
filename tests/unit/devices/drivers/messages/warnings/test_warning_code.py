from unittest import TestCase

from hangar_controller.devices.drivers.messages.warnings.warning_code import WarningCodes


class TestWarningCodes(TestCase):
    def setUp(self) -> None:
        self.peripheral = 'PE'
        self.command_control = 'CC'
        self.telemetry = 'TE'
        self.warning_codes = WarningCodes(peripheral=self.peripheral,
                                          command_control=self.command_control,
                                          telemetry=self.telemetry)

    def test___init__(self):
        self.assertEqual(self.warning_codes.peripheral, self.peripheral)
        self.assertEqual(self.warning_codes.command_control,
                         self.command_control)
        self.assertEqual(self.warning_codes.telemetry, self.telemetry)
