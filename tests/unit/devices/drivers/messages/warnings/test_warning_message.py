from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.warnings.warning_message import WarningMessage


class TestWarningMessage(TestCase):
    def setUp(self) -> None:
        self.warning_message = WarningMessage('topic', MagicMock())

    def test___init__(self):
        self.assertEqual('topic', self.warning_message.topic)
