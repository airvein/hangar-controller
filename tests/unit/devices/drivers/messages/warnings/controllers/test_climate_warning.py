from unittest import TestCase

from hangar_controller.devices.drivers.messages.message_utils.message_utils import warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.climate_warning import ClimateWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestClimateWarningMessage(TestCase):
    def setUp(self) -> None:
        self.climate_warning_message = ClimateWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_CLIMATE),
                         self.climate_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_CLIMATE,
            self.climate_warning_message.warning_code)
