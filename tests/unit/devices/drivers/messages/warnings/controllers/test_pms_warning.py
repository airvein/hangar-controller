from unittest import TestCase

from hangar_controller.devices.drivers.messages.message_utils.message_utils import warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.pms_warning import PMSWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestPMSWarningMessage(TestCase):
    def setUp(self) -> None:
        self.pms_warning_message = PMSWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_PMS),
                         self.pms_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_PMS,
            self.pms_warning_message.warning_code)
