from unittest import TestCase

from hangar_controller.devices.drivers.messages.message_utils.message_utils import warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.user_panel_warning import UserPanelWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestUserPanelWarningMessage(TestCase):
    def setUp(self) -> None:
        self.user_panel_warning_message = UserPanelWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_USER_PANEL),
                         self.user_panel_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_USER_PANEL,
            self.user_panel_warning_message.warning_code)
