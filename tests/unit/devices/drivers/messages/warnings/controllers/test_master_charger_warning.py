from unittest import TestCase

from hangar_controller.devices.drivers.messages.message_utils.message_utils import warning_topic
from hangar_controller.devices.drivers.messages.warnings.controllers.master_charger_warning import MasterChargerWarningMessage
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestMasterChargerWarningMessage(TestCase):
    def setUp(self) -> None:
        self.master_charger_warning_message = MasterChargerWarningMessage()

    def test___init__(self):
        self.assertEqual(warning_topic(Controllers.STM_MASTER_CHARGER),
                         self.master_charger_warning_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerWarningCodes.STM_MASTER_CHARGER,
            self.master_charger_warning_message.warning_code)
