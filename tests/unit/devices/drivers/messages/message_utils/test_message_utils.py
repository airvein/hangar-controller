from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.message_utils import message_utils
from hangar_controller.devices.drivers.messages.responses.response import Response


class TestMessageUtils(TestCase):
    def test_create_response_list_non_equal1(self):
        # given
        topics = ['a', 'b']
        timeouts = [1]
        expected_messages = [MagicMock(), MagicMock()]

        # when

        # then
        self.assertRaises(AssertionError,
                          message_utils.create_response_list,
                          topics, timeouts, expected_messages)

    def test_create_response_list_non_equal2(self):
        # given
        topics = ['a', 'b']
        timeouts = [1, 2]
        expected_messages = [MagicMock()]

        # when

        # then
        self.assertRaises(AssertionError,
                          message_utils.create_response_list,
                          topics, timeouts, expected_messages)

    def test_create_response_list_non_equal3(self):
        # given
        topics = ['a']
        timeouts = [1, 2]
        expected_messages = [MagicMock(), MagicMock()]

        # when

        # then
        self.assertRaises(AssertionError,
                          message_utils.create_response_list,
                          topics, timeouts, expected_messages)

    def test_create_response_list(self):
        # given
        topics = ['a/call', 'b/call']
        timeouts = [1, 2]
        message1 = MagicMock()
        message2 = MagicMock()
        expected_messages = [message1, message2]

        # when
        responses = message_utils.create_response_list(topics, timeouts,
                                                       expected_messages)

        # then
        self.assertEqual(2, len(responses))
        self.assertIsInstance(responses[0], Response)
        self.assertIsInstance(responses[1], Response)
        self.assertEqual('a/call', responses[0].topic)
        self.assertEqual(1, responses[0]._timer.timeout)
        self.assertEqual(message1, responses[0]._expected_message)
        self.assertEqual('b/call', responses[1].topic)
        self.assertEqual(2, responses[1]._timer.timeout)
        self.assertEqual(message2, responses[1]._expected_message)
