from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.responses.collective_response_evaluation import CollectiveResponseEvaluation


class TestCollectiveResponseEvaluation(TestCase):
    def setUp(self) -> None:
        self.evaluation = CollectiveResponseEvaluation(
            'topic', 'payload')

    def test___init__(self):
        self.assertEqual([], self.evaluation.responses)

    def test_add_response(self):
        # given

        # when
        self.evaluation.add_response(MagicMock())

        # then
        self.assertEqual(1, len(self.evaluation.responses))

    def test_add_and_evaluate_response_current_eval_False(self):
        # given
        self.evaluation.add_response = MagicMock()
        self.evaluation.evaluation = False
        self.evaluation.update_evaluation = MagicMock()
        self.evaluation.update_evaluation_reason = MagicMock()

        # when
        self.evaluation.add_and_evaluate_response(MagicMock())

        # then
        self.evaluation.add_response.assert_called()
        self.evaluation.update_evaluation.assert_not_called()
        self.evaluation.update_evaluation_reason.assert_not_called()

    def test_add_and_evaluate_response_message_eval_True(self):
        # given
        self.evaluation.add_response = MagicMock()
        self.evaluation.evaluation = True
        self.evaluation.update_evaluation = MagicMock()
        self.evaluation.update_evaluation_reason = MagicMock()
        message = MagicMock()
        message.evaluation = True

        # when
        self.evaluation.add_and_evaluate_response(message)

        # then
        self.evaluation.update_evaluation.assert_not_called()
        self.evaluation.update_evaluation_reason.assert_not_called()

    def test_add_and_evaluate_response(self):
        # given
        self.evaluation.add_response = MagicMock()
        self.evaluation.evaluation = True
        self.evaluation.update_evaluation = MagicMock()
        self.evaluation.update_evaluation_reason = MagicMock()
        message = MagicMock()
        message.evaluation = False
        message.evaluation_reason = 'reason'

        # when
        self.evaluation.add_and_evaluate_response(message)

        # then
        self.evaluation.update_evaluation.assert_called()
        self.evaluation.update_evaluation_reason.assert_called()

    def test_get_response_by_topic(self):
        # given
        response1 = MagicMock(topic='t1')
        response2 = MagicMock(topic='t2')
        responses = [response1, response2]
        self.evaluation.responses = responses

        # when
        result = self.evaluation.get_response_by_topic('t2')

        # then
        self.assertEqual(response2, result)
