from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.responses.response import Response
from hangar_controller.devices.drivers.messages.responses.response_manager import ResponseManager


class TestResponseManager(TestCase):
    def setUp(self) -> None:
        self.client = MagicMock()
        self.executor = MagicMock()
        self.response_manager = ResponseManager(self.executor)

    def test___init__(self):
        self.assertEqual(self.executor, self.response_manager.executor)

    def test_unregister(self):
        # given
        response = MagicMock(spec=Response)
        self.response_manager.registered_messages = [response]

        # when
        self.response_manager.unregister_message(response)

        # then
        self.assertEqual([], self.response_manager.registered_messages)

    def test_receive_not_response(self):
        # given
        self.response_manager.get_messages_by_topic = MagicMock(
            return_value='response'
        )
        
        # when
        
        # then
        self.assertRaises(AssertionError, self.response_manager.receive,
                          MagicMock())

    def test_receive_wrong_message(self):
        # given
        message = MagicMock()
        response = MagicMock(spec=Response)
        response.receive = MagicMock(return_value=False)
        self.response_manager.get_messages_by_topic = MagicMock(
            return_value=[response]
        )
        self.response_manager.unregister_message = MagicMock()
        self.response_manager.stop_all_future_responses = MagicMock()

        # when
        self.response_manager.receive(message)

        # then
        self.response_manager.get_messages_by_topic.assert_called_with(
            message.topic
        )
        response.receive.assert_called_with(message)
        self.response_manager.unregister_message.assert_called_with(response)

    def test_receive(self):
        # given
        message = MagicMock()
        response = MagicMock(spec=Response)
        response.receive = MagicMock(return_value=True)
        self.response_manager.get_messages_by_topic = MagicMock(
            return_value=[response]
        )
        self.response_manager.unregister_message = MagicMock()
        self.response_manager.stop_all_future_responses = MagicMock()

        # when
        self.response_manager.receive(message)

        # then
        self.response_manager.get_messages_by_topic.assert_called_with(
            message.topic
        )
        response.receive.assert_called_with(message)
        self.response_manager.unregister_message.assert_called_with(response)
        self.response_manager.stop_all_future_responses.assert_not_called()
