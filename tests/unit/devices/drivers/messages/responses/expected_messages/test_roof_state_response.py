from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse


class TestRoofStateResponse(TestCase):
    def setUp(self) -> None:
        self.roof_state_response = RoofStateResponse(RoofStateResponse.OPEN)

    def test___init__(self):
        self.assertEqual(RoofStateResponse.OPEN,
                         self.roof_state_response.positive_messages)
