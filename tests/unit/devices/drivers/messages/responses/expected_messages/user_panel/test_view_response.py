from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from settingsd.drivers.stm_user_panel_settings import UserPanelViews


class TestViewResponse(TestCase):
    def setUp(self) -> None:
        self.view_response = ViewResponse(
            UserPanelViews.CARGO_CORRECT)

    def test___init__(self):
        self.assertEqual(UserPanelViews.CARGO_CORRECT,
                         self.view_response.positive_messages)
