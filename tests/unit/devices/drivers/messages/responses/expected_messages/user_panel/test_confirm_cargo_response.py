from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.user_confirmation import \
    UserConfirmation


class TestConfirmCargoResponse(TestCase):
    def setUp(self) -> None:
        self.response = UserConfirmation(
            UserConfirmation.BUTTON_PRESSED)

    def test___init__(self):
        self.assertEqual(UserConfirmation.BUTTON_PRESSED,
                         self.response.positive_messages)
