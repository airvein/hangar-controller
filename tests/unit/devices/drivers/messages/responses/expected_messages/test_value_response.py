from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import ResponseMessage


class TestValueResponse(TestCase):
    def setUp(self) -> None:
        self.value_response = ValueResponse()

    def test___init__(self):
        self.assertIsNone(self.value_response.positive_messages)
        self.assertIsNone(self.value_response.expected_messages)

    def test_evaluate_message_wrong_match(self):
        # given

        # when
        self.value_response.evaluate_message('sdf123')

        # then
        self.assertFalse(self.value_response.evaluation)
        self.assertEqual(ResponseMessage.UNEXPECTED_RESPONSE,
                         self.value_response.evaluation_reason)

    def test_evaluate_message(self):
        # given

        # when
        self.value_response.evaluate_message('123.123')

        # then
        self.assertTrue(self.value_response.evaluation)
