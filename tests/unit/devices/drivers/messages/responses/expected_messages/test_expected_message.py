from unittest import TestCase
from unittest.mock import MagicMock, call

from hangar_controller.devices.drivers.messages.responses.expected_messages.expected_message import \
    ExpectedMessage
from settingsd.drivers.stm_communication_settings import ResponseMessage


class TestExpectedMessage(TestCase):
    def setUp(self) -> None:
        self.expected_messages = ['a', 'b']
        self.positive_massages = ['a']
        self.expected_message = ExpectedMessage(
            self.positive_massages, self.expected_messages, )

    def test___init__(self):
        self.assertEqual(self.positive_massages,
                         self.expected_message.positive_messages)
        self.assertEqual(self.expected_messages,
                         self.expected_message.expected_messages)

    def test_check_message_expected_false(self):
        # given
        message = 'c'

        # when
        result = self.expected_message.check_message_expected(message)

        # then
        self.assertFalse(result)

    def test_check_message_expected(self):
        # given
        message = 'b'

        # when
        result = self.expected_message.check_message_expected(message)

        # then
        self.assertTrue(result)

    def test_check_message_positive_empty(self):
        # given
        self.expected_message.positive_messages = None
        message = 'c'

        # when
        result = self.expected_message.check_message_positive(message)

        # then
        self.assertTrue(result)

    def test_check_message_positive_false(self):
        # given
        message = 'c'

        # when
        result = self.expected_message.check_message_positive(message)

        # then
        self.assertFalse(result)

    def test_check_message_positive(self):
        # given
        message = 'a'

        # when
        result = self.expected_message.check_message_positive(message)

        # then
        self.assertTrue(result)

    def test_check_evaluation(self):
        # given
        self.expected_message.check_message_positive = MagicMock(
            side_effect=[True, False, True, False]
        )
        self.expected_message.check_message_expected = MagicMock(
            side_effect=[True, True, False, False]
        )

        # when
        result = [self.expected_message.check_evaluation('a') for _ in range(4)]

        # then
        self.expected_message.check_message_positive.assert_has_calls(
            [call('a'), ] * 2)
        self.expected_message.check_message_expected.assert_has_calls(
            [call('a'), ] * 2)
        self.assertEqual([True, False, False, False], result)

    def test_evaluate_message_negative(self):
        # given
        message = 'b'

        # when
        self.expected_message.evaluate_message(message)

        # then
        self.assertFalse(self.expected_message.evaluation)
        self.assertEqual(ResponseMessage.NEGATIVE_RESPONSE,
                         self.expected_message.evaluation_reason)

    def test_evaluate_message_unexpected(self):
        # given
        message = 'c'

        # when
        self.expected_message.evaluate_message(message)

        # then
        self.assertFalse(self.expected_message.evaluation)
        self.assertEqual(ResponseMessage.UNEXPECTED_RESPONSE,
                         self.expected_message.evaluation_reason)

    def test_evaluate_message(self):
        # given
        message = 'a'

        # when
        self.expected_message.evaluate_message(message)

        # then
        self.assertTrue(self.expected_message.evaluation)
        self.assertIsNone(self.expected_message.evaluation_reason)
