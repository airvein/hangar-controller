from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse


class TestRoofStateResponse(TestCase):
    def setUp(self) -> None:
        self.roof_state_response = RoofStateResponse(
            RoofStateResponse.IDLE)

    def test___init__(self):
        self.assertEqual(RoofStateResponse.IDLE,
                         self.roof_state_response.positive_messages)
