from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_response import \
    BatteryResponse


class TestBatteryResponse(TestCase):
    def setUp(self) -> None:
        self.battery_response = BatteryResponse(
            BatteryResponse.CHARGED)

    def test___init__(self):
        self.assertEqual(BatteryResponse.CHARGED,
                         self.battery_response.positive_messages)
