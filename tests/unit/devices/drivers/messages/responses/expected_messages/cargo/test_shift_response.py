from unittest import TestCase

from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.shift_response import ShiftResponse


class TestShiftResponse(TestCase):
    def setUp(self) -> None:
        self.shift_response = ShiftResponse([ShiftResponse.WORK])

    def test___init__(self):
        self.assertEqual([ShiftResponse.WORK],
                         self.shift_response.positive_messages)
