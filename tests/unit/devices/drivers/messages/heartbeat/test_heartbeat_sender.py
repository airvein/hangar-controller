import time
from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.messages.heartbeat.heartbeat import Heartbeat
from hangar_controller.devices.drivers.messages.heartbeat.heartbeat_sender import HeartbeatSender
from settingsd.drivers.stm_communication_settings import HEARTBEAT_TICK_TIME


class TestHeartbeatSender(TestCase):
    def setUp(self) -> None:
        self.send_command_and_wait = MagicMock()
        self.heartbeat_sender = HeartbeatSender(self.send_command_and_wait)

    def test___init__(self):
        self.assertEqual(self.send_command_and_wait,
                         self.heartbeat_sender._send_command_and_wait)
        self.assertFalse(self.heartbeat_sender._stop_sender)
        self.assertIsInstance(self.heartbeat_sender.heartbeat, Heartbeat)

    @patch.object(time, 'sleep')
    def test_run(self, sleep_mock):
        # given
        self.heartbeat_sender._send_command_and_wait = MagicMock()
        self.heartbeat_sender._stop_sender = False
        sleep_mock.side_effect = lambda x: self.heartbeat_sender.stop()
        
        # when
        self.heartbeat_sender.run()
        
        # then
        self.heartbeat_sender._send_command_and_wait.assert_called_with(
            self.heartbeat_sender.heartbeat
        )
        sleep_mock.assert_called_with(HEARTBEAT_TICK_TIME)

    def test_stop_heartbeat(self):
        # given
        self.assertFalse(self.heartbeat_sender._stop_sender)
        
        # when
        self.heartbeat_sender.stop()
        
        # then
        self.assertTrue(self.heartbeat_sender._stop_sender)
