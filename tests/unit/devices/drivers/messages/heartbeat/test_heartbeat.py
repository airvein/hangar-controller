from unittest import TestCase
from unittest.mock import MagicMock, call

from hangar_controller.devices.drivers.messages.heartbeat.heartbeat import Heartbeat, RESPONSES, TOPICS
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic


class TestHeartbeat(TestCase):
    def setUp(self) -> None:
        self.heartbeat = Heartbeat()

    def test___init__(self):
        self.assertEqual('Heartbeats', self.heartbeat.topic)
        self.assertEqual(RESPONSES, self.heartbeat.responses)

    def test_publish(self):
        # given
        client = MagicMock()
        calls = [call(call_topic(topic), self.heartbeat.TICK) for topic in
                 TOPICS]

        # when
        self.heartbeat.publish(client.publish)

        # then
        client.publish.assert_has_calls(calls)
