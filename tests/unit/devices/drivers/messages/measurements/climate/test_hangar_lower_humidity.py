from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.climate.hangar_lower_humidity import \
    HangarLowerHumidity


class TestHangarLowerHumidity(TestCase):
    def setUp(self) -> None:
        self.hangar_lower_humidity = HangarLowerHumidity()

    def test___init__(self):
        self.assertEqual(HangarLowerHumidity.TOPIC,
                         self.hangar_lower_humidity.topic)
        self.assertEqual(float, self.hangar_lower_humidity.type_)
