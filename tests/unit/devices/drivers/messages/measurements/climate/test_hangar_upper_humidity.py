from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.climate.hangar_upper_humidity import HangarUpperHumidity


class TestHangarUpperHumidity(TestCase):
    def setUp(self) -> None:
        self.hangar_upper_humidity = HangarUpperHumidity()

    def test___init__(self):
        self.assertEqual(HangarUpperHumidity.TOPIC,
                         self.hangar_upper_humidity.topic)
        self.assertEqual(float, self.hangar_upper_humidity.type_)
