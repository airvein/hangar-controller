from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.climate.roof_left_temperature import RoofLeftTemperature


class TestRoofLeftTemperature(TestCase):
    def setUp(self) -> None:
        self.roof_left_temperature = RoofLeftTemperature()

    def test___init__(self):
        self.assertEqual(RoofLeftTemperature.TOPIC,
                         self.roof_left_temperature.topic)
        self.assertEqual(float, self.roof_left_temperature.type_)
