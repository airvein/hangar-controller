from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.climate.hangar_average_humidity import HangarAverageHumidity


class TestHangarAverageHumidity(TestCase):
    def setUp(self) -> None:
        self.hangar_average_humidity = HangarAverageHumidity()

    def test___init__(self):
        self.assertEqual(HangarAverageHumidity.TOPIC,
                         self.hangar_average_humidity.topic)
        self.assertEqual(float, self.hangar_average_humidity.type_)
