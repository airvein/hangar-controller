from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.climate.roof_right_temperature \
    import RoofRightTemperature


class TestRoofRightTemperature(TestCase):
    def setUp(self) -> None:
        self.roof_right_temperature = RoofRightTemperature()

    def test___init__(self):
        self.assertEqual(RoofRightTemperature.TOPIC,
                         self.roof_right_temperature.topic)
        self.assertEqual(float, self.roof_right_temperature.type_)
