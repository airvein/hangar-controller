from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.measurements.measurement import Measurement
from hangar_controller.devices.drivers.messages.measurements.measurements_manager import MeasurementsManager


class TestMeasurementsManager(TestCase):
    def setUp(self) -> None:
        self.client = MagicMock()
        self.diagnostics = MagicMock()
        self.measurements_manager = MeasurementsManager(self.diagnostics)

    def test___init__(self):
        self.assertEqual(self.diagnostics,
                         self.measurements_manager.diagnostics)

    def test_receive_non_measurement(self):
        # given
        message = MagicMock()
        self.measurements_manager.get_message_by_topic = MagicMock(
            return_value=MagicMock()
        )
        
        # when

        # then
        self.assertRaises(AssertionError,
                          self.measurements_manager.receive, message)

    def test_receive_measurement(self):
        # given
        message = MagicMock()
        message.topic = 'temperature'
        message.payload = 123
        self.measurements_manager.get_message_by_topic = MagicMock(
            return_value=Measurement('temperature')
        )
        self.measurements_manager.diagnostics = MagicMock()

        # when
        self.measurements_manager.receive(message)

        # then
        self.measurements_manager.get_message_by_topic.assert_called_with(
            'temperature'
        )
        self.measurements_manager.diagnostics.update.assert_called_with(
            temperature=123
        )
