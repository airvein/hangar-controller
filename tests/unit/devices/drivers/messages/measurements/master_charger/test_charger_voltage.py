from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.master_charger.charger_voltage import ChargerVoltage
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements


class TestChargerVoltage(TestCase):
    def setUp(self) -> None:
        self.charger_voltage = ChargerVoltage('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.CHARGER,
                                         Measurements.VOLTAGE)

    def test___init__(self):
        self.assertEqual(self.topic, self.charger_voltage.topic)
