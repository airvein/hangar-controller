from unittest import TestCase

from hangar_controller.devices.drivers.messages.measurements.master_charger.battery_uid import BatteryUID
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    battery_measurement
from settingsd.drivers.stm_communication_settings import SubControllers
from settingsd.drivers.stm_master_charger_settings import Measurements


class TestBatteryUID(TestCase):
    def setUp(self) -> None:
        self.battery_uid = BatteryUID('battery_id')
        self.topic = battery_measurement('battery_id', SubControllers.BATTERY,
                                         Measurements.UID)

    def test___init__(self):
        self.assertEqual(self.topic, self.battery_uid.topic)
