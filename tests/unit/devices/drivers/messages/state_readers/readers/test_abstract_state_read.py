from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.messages.state_readers.readers.abstract_state_read import AbstractStateRead


class TestAbstractStateRead(TestCase):
    @patch.object(AbstractStateRead, 'log_creation')
    def setUp(self, log_mock) -> None:
        self.log_creation_mock = log_mock
        self.response = MagicMock()
        self.state_read = AbstractStateRead('topic')

    def test___init__(self):
        self.log_creation_mock.assert_called()

    def test_publish_responses(self):
        # given
        response = MagicMock()
        self.state_read.get_responses = MagicMock(return_value=[response])

        # when
        self.state_read.publish_responses(MagicMock())

        # then
        self.state_read.get_responses.assert_called()
        response.publish.assert_called()

    @patch('logging.debug')
    def test_log_creation(self, debug_mock):
        # given

        # when
        self.state_read.log_creation()

        # then
        debug_mock.assert_called()
