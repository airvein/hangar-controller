from unittest import TestCase
from unittest.mock import patch

from hangar_controller.devices.drivers.messages.message_utils.message_utils import get_topic
from hangar_controller.devices.drivers.messages.state_readers.readers.read_time import ReadTime
from hangar_controller.devices.drivers.messages.state_readers.responses.state_read_response import \
    StateReadResponse


class TestReadTime(TestCase):
    def setUp(self) -> None:
        self.read_time = ReadTime()

    def test___init__(self):
        self.assertEqual(get_topic(ReadTime.TOPIC), self.read_time.topic)

    @patch('time.time')
    def test__get_responses(self, time_mock):
        # given
        time_mock.return_value = 1234

        # when
        result = self.read_time.get_responses()

        # then
        self.assertIsInstance(result, list)
        self.assertEqual(1, len(result))
        self.assertIsInstance(result[0], StateReadResponse)
