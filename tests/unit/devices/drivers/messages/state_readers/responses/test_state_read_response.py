from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.messages.state_readers.responses.state_read_response import \
    StateReadResponse


class TestStateReadResponse(TestCase):
    @patch.object(StateReadResponse, 'log_creation')
    def setUp(self, creation_mock) -> None:
        self.creation_mock = creation_mock
        self.response = StateReadResponse('topic', 'payload')

    def test___init__(self):
        self.creation_mock.assert_called()

    @patch('logging.info')
    def test_publish(self, info_mock):
        # given
        send_mock = MagicMock()
        
        # when
        self.response.publish(send_mock)
        
        # then
        send_mock.assert_called()
        info_mock.assert_called()

    @patch('logging.debug')
    def test_log_creation(self, debug_mock):
        # given
        
        # when
        self.response.log_creation()
        
        # then
        debug_mock.assert_called()

