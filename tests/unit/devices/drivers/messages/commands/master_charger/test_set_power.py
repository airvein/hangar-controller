from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_set_power import \
    MasterChargerSetPower
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, sub_controller_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_response import \
    BatteryResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import CommandMessage
from settingsd.drivers.stm_master_charger_settings import \
    MasterChargerTimeouts


class TestSetPower(TestCase):
    def setUp(self) -> None:
        self.climate_set_power = MasterChargerSetPower(
            'charger_id', MasterChargerSetPower.ENABLED)

    def test___init__(self):
        topic = sub_controller_topic(MasterChargerSetPower.COMMAND,
                                     'charger_id')
        power_state_topic = sub_controller_topic(
            MasterChargerSetPower.POWER_STATE, 'charger_id')
        battery_state_topic = sub_controller_topic(
            MasterChargerSetPower.BATTERY_STATE, 'charger_id')

        ack_response = create_ack_response(topic)
        power_state_response = Response(power_state_topic,
                                        MasterChargerTimeouts.POWER,
                                        EnableDisableResponse(
                                            MasterChargerSetPower.ENABLED))
        charging_response = Response(battery_state_topic,
                                     MasterChargerTimeouts.CHARGING,
                                     BatteryResponse(BatteryResponse.CHARGING))
        charged_response = Response(battery_state_topic,
                                    MasterChargerTimeouts.CHARGED,
                                    BatteryResponse(BatteryResponse.CHARGED))

        responses = [ack_response, power_state_response, charging_response,
                     charged_response]

        self.assertEqual(call_topic(topic), self.climate_set_power.topic)
        self.assertEqual(responses, self.climate_set_power.responses)
        self.assertEqual(CommandMessage.ENABLE, self.climate_set_power.payload)
