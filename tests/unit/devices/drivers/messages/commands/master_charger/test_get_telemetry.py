from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_telemetry import \
    MasterChargerGetTelemetry
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    create_response_list, battery_measurement, sub_controller_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.list_response import ListResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from settingsd.drivers.stm_communication_settings import CommandMessage, \
    SubControllers, MULTIPLE_RESPONSE_TIMEOUT
from settingsd.drivers.stm_master_charger_settings import \
    Measurements


class TestMasterChargerGetTelemetry(TestCase):
    def setUp(self) -> None:
        self.climate_get_telemetry = MasterChargerGetTelemetry('charger_id')

    def test___init__(self):
        topic = sub_controller_topic(MasterChargerGetTelemetry.COMMAND,
                                     'charger_id')
        batteries = SubControllers.BATTERIES
        chargers = SubControllers.CHARGER
        topics = [
            battery_measurement('charger_id', batteries,
                                Measurements.TEMPERATURE),
            battery_measurement('charger_id', batteries, Measurements.UID),
            battery_measurement('charger_id', batteries, Measurements.VOLTAGE),
            battery_measurement('charger_id', chargers,
                                Measurements.BMS_TEMPERATURE),
            battery_measurement('charger_id', chargers, Measurements.CURRENT),
        ]
        timeouts = [MULTIPLE_RESPONSE_TIMEOUT, ] * len(topics)
        expected_messages = [ValueResponse(),
                             *[ListResponse(), ] * 4]
        responses = create_response_list(topics, timeouts, expected_messages)
        ack_response = create_ack_response(topic)
        responses.append(ack_response)

        self.assertEqual(call_topic(topic),
                         self.climate_get_telemetry.topic)
        self.assertEqual(responses, self.climate_get_telemetry.responses)
        self.assertEqual(CommandMessage.GET,
                         self.climate_get_telemetry.payload)
