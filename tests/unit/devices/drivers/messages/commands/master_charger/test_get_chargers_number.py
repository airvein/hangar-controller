from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_chargers_number import \
    MasterChargerGetChargersNumber
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import CommandMessage, \
    MULTIPLE_RESPONSE_TIMEOUT


class TestSetPower(TestCase):
    def setUp(self) -> None:
        self.get_chargers_number = MasterChargerGetChargersNumber()

    def test___init__(self):
        ack_response = create_ack_response(
            MasterChargerGetChargersNumber.TOPIC)
        numbers_response = Response(MasterChargerGetChargersNumber.STATE_TOPIC,
                                    MULTIPLE_RESPONSE_TIMEOUT,
                                    ValueResponse())
        responses = [ack_response, numbers_response]
        self.assertEqual(call_topic(MasterChargerGetChargersNumber.TOPIC),
                         self.get_chargers_number.topic)
        self.assertEqual(responses, self.get_chargers_number.responses)
        self.assertEqual(CommandMessage.GET, self.get_chargers_number.payload)
