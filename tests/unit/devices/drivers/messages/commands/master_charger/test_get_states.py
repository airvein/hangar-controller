from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_states import \
    MasterChargerGetStates
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    sub_controller_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.master_charger.battery_response import \
    BatteryResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import CommandMessage, \
    MULTIPLE_RESPONSE_TIMEOUT


class TestMasterChargerGetStates(TestCase):
    def setUp(self) -> None:
        self.climate_get_states = MasterChargerGetStates('charger_id')

    def test___init__(self):
        topic = sub_controller_topic(MasterChargerGetStates.COMMAND,
                                     'charger_id')
        power_state_topic = sub_controller_topic(
            MasterChargerGetStates.POWER_STATE, 'charger_id')
        battery_state_topic = sub_controller_topic(
            MasterChargerGetStates.BATTERY_STATE, 'charger_id')

        ack_response = create_ack_response(topic)
        power_state_response = Response(power_state_topic,
                                        MULTIPLE_RESPONSE_TIMEOUT,
                                        EnableDisableResponse(None))
        battery_state_response = Response(battery_state_topic,
                                          MULTIPLE_RESPONSE_TIMEOUT,
                                          BatteryResponse(None))

        responses = [ack_response, power_state_response,
                     battery_state_response]

        self.assertEqual(call_topic(topic),
                         self.climate_get_states.topic)
        self.assertEqual(responses, self.climate_get_states.responses)
        self.assertEqual(CommandMessage.GET, self.climate_get_states.payload)
