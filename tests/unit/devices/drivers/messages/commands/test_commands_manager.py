import concurrent.futures
from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.messages.commands.commands_manager import CommandsManager
from hangar_controller.devices.drivers.messages.heartbeat.heartbeat_sender import HeartbeatSender
from hangar_controller.devices.drivers.messages.responses.collective_response_evaluation import CollectiveResponseEvaluation


class TestCommandsManager(TestCase):
    def setUp(self) -> None:
        self.client = MagicMock()
        self.response_manager = MagicMock()
        self.error_handler = MagicMock()
        self.send_message = MagicMock()
        self.commands_manager = CommandsManager(self.response_manager,
                                                self.send_message, MagicMock())

    def test___init__(self):
        self.assertEqual(self.response_manager,
                         self.commands_manager._response_manager)
        self.assertIsInstance(self.commands_manager.heartbeat_sender,
                              HeartbeatSender)
        self.assertEqual(self.send_message,
                         self.commands_manager.send_message_callable)

    def test__send_command(self):
        # given
        self.commands_manager._response_manager.register_messages = MagicMock()
        command = MagicMock()
        self.commands_manager._response_manager.create_future_response = \
            MagicMock(return_value='future_responses')

        # when
        future = self.commands_manager.send_command(command)

        # then
        self.commands_manager._response_manager.register_messages. \
            assert_called_with(command.responses)
        self.commands_manager._response_manager.create_future_response. \
            assert_called_with(command)
        command.publish.assert_called_with(self.send_message)
        self.assertEqual('future_responses', future)

    @patch.object(concurrent.futures, 'as_completed')
    def test_send_command_and_wait(self, as_completed_mock):
        # given
        command = MagicMock()
        evaluation = MagicMock(spec=CollectiveResponseEvaluation)
        response = MagicMock()
        response.result = MagicMock(return_value=evaluation)
        self.commands_manager.send_command = MagicMock(return_value=response)
        self.commands_manager.check_evaluation_and_add_to_error_queue = MagicMock()
        # when
        result_actual = self.commands_manager.send_command_and_wait(command)

        # then
        self.commands_manager.send_command.assert_called_with(command)
        response.result.assert_called()
        self.commands_manager.check_evaluation_and_add_to_error_queue.assert_called()
        self.assertEqual(evaluation, result_actual)

    def test_send_command_no_wait(self):
        # given
        command = MagicMock()
        self.commands_manager.send_command = MagicMock(
            return_value='future_response')
        self.commands_manager.future_response_manager = MagicMock()

        # when
        self.commands_manager.send_command_no_wait(command)

        # then
        self.commands_manager.send_command.assert_called_with(command)
        self.commands_manager.future_response_manager.start_future_observer. \
            assert_called_with('future_response')

    def test_receive(self):
        self.assertRaises(NotImplementedError, self.commands_manager.receive,
                          'message')
