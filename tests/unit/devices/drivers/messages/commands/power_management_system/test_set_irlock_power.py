from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_irlock_power import \
    PMSSetIRLockPower
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSTimeouts, PMSStates


class TestSetIRLockPower(TestCase):
    def setUp(self) -> None:
        self.set_ir_lock_power = PMSSetIRLockPower(PMSSetIRLockPower.ENABLE)

    def test___init__(self):

        ack_response = create_ack_response(PMSSetIRLockPower.TOPIC)
        ir_lock_response = Response(
            state_change_topic(Controllers.STM_PMS, PMSStates.IR_LOCK_POWER),
            PMSTimeouts.IR_LOCK_POWER,
            EnableDisableResponse(PMSSetIRLockPower.ENABLE))

        responses = [ack_response, ir_lock_response]

        self.assertEqual(call_topic(PMSSetIRLockPower.TOPIC),
                         self.set_ir_lock_power.topic)
        self.assertEqual(responses, self.set_ir_lock_power.responses)
