from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_controller_get_pin import \
    PMSResetControllerGetPin
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, pin_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import PIN_TIMEOUT


class TestPMSResetControllerGetPIN(TestCase):
    def setUp(self) -> None:
        self.reset_ctrl = PMSResetControllerGetPin()

    def test___init__(self):
        responses = [
            Response(pin_topic(PMSResetControllerGetPin.TOPIC), PIN_TIMEOUT,
                     ValueResponse())]
        self.assertEqual(call_topic(PMSResetControllerGetPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
