from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_pressure import PMSSetPressure
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSTimeouts, PMSStates


class TestSetPressure(TestCase):
    def setUp(self) -> None:
        self.set_pressure = PMSSetPressure(PMSSetPressure.ENABLE)

    def test___init__(self):
        ack_response = create_ack_response(PMSSetPressure.TOPIC)
        state_response = Response(
            state_change_topic(Controllers.STM_PMS, PMSStates.PRESSURE),
            PMSTimeouts.PRESSURE,
            EnableDisableResponse(PMSSetPressure.ENABLE))

        responses = [ack_response, state_response]

        self.assertEqual(call_topic(PMSSetPressure.TOPIC),
                         self.set_pressure.topic)
        self.assertEqual(responses, self.set_pressure.responses)
