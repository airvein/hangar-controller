from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_air_compressor import \
    PMSSetAirCompressor
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_pms_settings import PMSStates, PMSTimeouts


class TestSetAirCompressor(TestCase):
    def setUp(self) -> None:
        self.set_air_compressor = PMSSetAirCompressor(
            PMSSetAirCompressor.ENABLE)

    def test___init__(self):
        responses = [
            create_ack_response(PMSSetAirCompressor.TOPIC),
            Response(state_change_topic(Controllers.STM_PMS,
                                        PMSStates.AIR_COMPRESSOR_POWER),
                     PMSTimeouts.AIR_COMPRESSOR,
                     EnableDisableResponse(PMSSetAirCompressor.ENABLE))]

        self.assertEqual(call_topic(PMSSetAirCompressor.TOPIC),
                         self.set_air_compressor.topic)
        self.assertEqual(responses, self.set_air_compressor.responses)
