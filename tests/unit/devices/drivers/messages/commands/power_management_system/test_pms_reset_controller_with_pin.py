from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_controller_with_pin import \
    PMSResetControllerWithPin
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestPMSResetControllerGetPIN(TestCase):
    def setUp(self) -> None:
        self.reset_ctrl = PMSResetControllerWithPin('1234')

    def test___init__(self):
        responses = [create_ack_response(PMSResetControllerWithPin.TOPIC)]
        self.assertEqual(call_topic(PMSResetControllerWithPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
