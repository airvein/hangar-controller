from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_reset_plc import PMSResetPLC
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestPMSResetPLC(TestCase):
    def setUp(self) -> None:
        self.reset_plc = PMSResetPLC()

    def test___init__(self):
        responses = [create_ack_response(PMSResetPLC.TOPIC)]

        self.assertEqual(call_topic(PMSResetPLC.TOPIC),
                         self.reset_plc.topic)
        self.assertEqual(responses, self.reset_plc.responses)
