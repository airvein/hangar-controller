from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_reset_plc import CargoResetPLC
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestCargoResetPLC(TestCase):
    def setUp(self) -> None:
        self.reset_plc = CargoResetPLC()

    def test___init__(self):
        responses = [create_ack_response(CargoResetPLC.TOPIC)]

        self.assertEqual(call_topic(CargoResetPLC.TOPIC),
                         self.reset_plc.topic)
        self.assertEqual(responses, self.reset_plc.responses)
