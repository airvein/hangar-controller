from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_plc_io import CargoGetPLCIO
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    plc_io_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.plc_io_response \
    import PLCIOResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT, \
    Controllers


class TestCargoGetPLCIO(TestCase):
    def setUp(self) -> None:
        self.cargo_get_plc_io = CargoGetPLCIO()

    def test___init__(self):
        ack_response = create_ack_response(CargoGetPLCIO.TOPIC)
        plc_inputs_response = Response(
            plc_io_topic(Controllers.STM_CARGO, inputs=True),
            ACK_TIMEOUT, PLCIOResponse())
        plc_outputs_response = Response(
            plc_io_topic(Controllers.STM_CARGO, inputs=False),
            ACK_TIMEOUT, PLCIOResponse())

        responses = [ack_response, plc_inputs_response, plc_outputs_response]
        self.assertEqual(call_topic(CargoGetPLCIO.TOPIC),
                         self.cargo_get_plc_io.topic)
        self.assertEqual(responses, self.cargo_get_plc_io.responses)
