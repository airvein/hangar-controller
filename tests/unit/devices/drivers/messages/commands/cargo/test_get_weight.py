from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_weight import CargoGetWeight
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoControlCommands
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT, \
    Controllers


class TestCargoGetWeight(TestCase):
    def setUp(self) -> None:
        self.cargo_get_weight = CargoGetWeight()

    def test___init__(self):
        ack_response = create_ack_response(CargoGetWeight.TOPIC)
        weight_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoControlCommands.CARGO_WEIGHT),
            ACK_TIMEOUT, ValueResponse())

        responses = [ack_response, weight_response]

        self.assertEqual(call_topic(CargoGetWeight.TOPIC),
                         self.cargo_get_weight.topic)
        self.assertEqual(responses, self.cargo_get_weight.responses)
