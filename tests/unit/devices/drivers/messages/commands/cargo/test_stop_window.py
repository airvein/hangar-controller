from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_stop_window import StopWindow
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT


class TestStopWindow(TestCase):
    def setUp(self) -> None:
        self.stop_window = StopWindow()

    def test___init__(self):
        ack_response = Response(response_topic(StopWindow.TOPIC), ACK_TIMEOUT,
                                Acknowledgment())
        window_response = Response(StopWindow.STATE_CHANGE_TOPIC,
                                   CargoTimeouts.WINDOW_STOP,
                                   WindowResponse(WindowResponse.STOP))
        responses = [ack_response, window_response]
        self.assertEqual(call_topic(StopWindow.TOPIC), self.stop_window.topic)
        self.assertEqual(responses, self.stop_window.responses)
