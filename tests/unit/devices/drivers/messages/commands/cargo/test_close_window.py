from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_close_window import CargoCloseWindow
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts


class TestCloseWindow(TestCase):
    def setUp(self) -> None:
        self.close_window = CargoCloseWindow()

    def test___init__(self):
        responses = [
            Response(response_topic(CargoCloseWindow.TOPIC),
                     CargoTimeouts.WINDOW_ACK,
                     Acknowledgment()),
            Response(CargoCloseWindow.STATE_CHANGE_TOPIC,
                     CargoTimeouts.WINDOW_CLOSE,
                     WindowResponse(WindowResponse.CLOSE))
        ]

        self.assertEqual(call_topic(CargoCloseWindow.TOPIC),
                         self.close_window.topic)
        self.assertEqual(responses, self.close_window.responses)
