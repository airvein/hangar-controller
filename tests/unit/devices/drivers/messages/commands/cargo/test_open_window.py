from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_open_window import CargoOpenWindow
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.window_response import WindowResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts


class TestOpenWindow(TestCase):
    def setUp(self) -> None:
        self.open_window = CargoOpenWindow()

    def test___init__(self):
        responses = [
            Response(response_topic(CargoOpenWindow.TOPIC),
                     CargoTimeouts.WINDOW_ACK,
                     Acknowledgment()),
            Response(CargoOpenWindow.STATE_CHANGE_TOPIC,
                     CargoTimeouts.WINDOW_OPEN,
                     WindowResponse(WindowResponse.OPEN))
        ]

        self.assertEqual(call_topic(CargoOpenWindow.TOPIC),
                         self.open_window.topic)
        self.assertEqual(responses, self.open_window.responses)
