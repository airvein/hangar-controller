from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_reset_controller_get_pin import \
    CargoResetControllerGetPin
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, pin_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import PIN_TIMEOUT


class TestCargoResetControllerGetPIN(TestCase):
    def setUp(self) -> None:
        self.reset_ctrl = CargoResetControllerGetPin()

    def test___init__(self):
        responses = [
            Response(pin_topic(CargoResetControllerGetPin.TOPIC), PIN_TIMEOUT,
                     ValueResponse())]
        self.assertEqual(call_topic(CargoResetControllerGetPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
