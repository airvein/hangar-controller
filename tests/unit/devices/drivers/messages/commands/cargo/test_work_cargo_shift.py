from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.cargo.cargo_work_shift_check_conditions import \
    CargoWorkShiftCheckConditions
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic, \
    response_topic, state_change_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.cargo.shift_response import ShiftResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_cargo_settings import CargoTimeouts, \
    CargoStates
from settingsd.drivers.stm_communication_settings import ACK_TIMEOUT, \
    Controllers


class TestWorkCargoShift(TestCase):
    def setUp(self) -> None:
        self.work_shift = CargoWorkShiftCheckConditions()

    def test___init__(self):
        ack_response = Response(response_topic(CargoWorkShiftCheckConditions.TOPIC),
                                ACK_TIMEOUT,
                                Acknowledgment())
        shift_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK_CHECK_COND,
            ShiftResponse(ShiftResponse.WORK))
        idle_response = Response(
            state_change_topic(Controllers.STM_CARGO,
                               CargoStates.CARGO_SHIFT),
            CargoTimeouts.SHIFT_WORK_CHECK_COND,
            ShiftResponse(ShiftResponse.IDLE))
        responses = [ack_response, idle_response, shift_response]

        self.assertEqual(call_topic(CargoWorkShiftCheckConditions.TOPIC),
                         self.work_shift.topic)
        self.assertEqual(responses, self.work_shift.responses)
