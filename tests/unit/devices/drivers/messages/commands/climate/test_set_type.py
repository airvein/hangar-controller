from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_type_climate_controller import SetTypeClimateController
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.climate.auto_manual_response import \
    AutoManualResponse


class TestSetType(TestCase):
    def setUp(self) -> None:
        self.set_type = SetTypeClimateController(SetTypeClimateController.AUTO)

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             AutoManualResponse(SetTypeClimateController.AUTO)]
        responses = create_response_list(
            SetTypeClimateController.RESPONSE_TOPICS,
            SetTypeClimateController.TIMEOUTS,
            expected_messages)

        self.assertEqual(call_topic(SetTypeClimateController.TOPIC),
                         self.set_type.topic)
        self.assertEqual(responses, self.set_type.responses)
