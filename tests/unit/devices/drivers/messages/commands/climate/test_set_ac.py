from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_ac import SetAC
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse


class TestSetAC(TestCase):
    def setUp(self) -> None:
        self.set_ac = SetAC(SetAC.ENABLE)

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(SetAC.ENABLE)]
        responses = create_response_list(SetAC.RESPONSE_TOPICS, SetAC.TIMEOUTS,
                                         expected_messages)

        self.assertEqual(call_topic(SetAC.TOPIC), self.set_ac.topic)
        self.assertEqual(responses, self.set_ac.responses)
