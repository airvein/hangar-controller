from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater import \
    SetHangarHeater
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse


class TestSetHangarHeater(TestCase):
    def setUp(self) -> None:
        self.set_hangar_heater = SetHangarHeater(SetHangarHeater.ENABLE)

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(SetHangarHeater.ENABLE)]
        responses = create_response_list(SetHangarHeater.RESPONSE_TOPICS,
                                         SetHangarHeater.TIMEOUTS,
                                         expected_messages)

        self.assertEqual(call_topic(SetHangarHeater.TOPIC),
                         self.set_hangar_heater.topic)
        self.assertEqual(responses, self.set_hangar_heater.responses)
