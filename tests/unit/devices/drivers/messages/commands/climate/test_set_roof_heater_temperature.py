from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater_temperature import SetRoofHeaterTemperature
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    temperature_payload, set_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment


class TestSetRoofHeaterTemperature(TestCase):
    def setUp(self) -> None:
        self.set_roof_heater_temperature = SetRoofHeaterTemperature(123)

    def test___init__(self):
        # given
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetRoofHeaterTemperature.RESPONSE_TOPICS,
            SetRoofHeaterTemperature.TIMEOUTS,
            expected_messages)
        payload = temperature_payload(123)
        self.assertEqual(set_topic(SetRoofHeaterTemperature.TOPIC),
                         self.set_roof_heater_temperature.topic)
        self.assertEqual(responses, self.set_roof_heater_temperature.responses)
        self.assertEqual(payload, self.set_roof_heater_temperature.payload)
