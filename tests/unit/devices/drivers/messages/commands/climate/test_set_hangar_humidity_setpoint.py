from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_setpoint \
    import SetHangarHumiditySetPoint
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    humidity_payload, set_topic, create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment


class TestSetHangarHumiditySetPoint(TestCase):
    def setUp(self) -> None:
        self.set_hangar_humidity_set_point = SetHangarHumiditySetPoint(123)

    def test___init__(self):
        # given
        expected_messages = [Acknowledgment()]
        responses = create_response_list(
            SetHangarHumiditySetPoint.RESPONSE_TOPICS,
            SetHangarHumiditySetPoint.TIMEOUTS,
            expected_messages)
        payload = humidity_payload(123)
        self.assertEqual(set_topic(SetHangarHumiditySetPoint.TOPIC),
                         self.set_hangar_humidity_set_point.topic)
        self.assertEqual(responses,
                         self.set_hangar_humidity_set_point.responses)
        self.assertEqual(payload, self.set_hangar_humidity_set_point.payload)
