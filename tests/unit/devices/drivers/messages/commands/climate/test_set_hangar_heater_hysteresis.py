from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater_hysteresis import SetHangarHeaterHysteresis
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    temperature_payload, set_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestSetHangarHeaterHysteresis(TestCase):
    def setUp(self) -> None:
        self.set_hangar_heater_hysteresis = SetHangarHeaterHysteresis(123)

    def test___init__(self):
        # given
        responses = [create_ack_response(SetHangarHeaterHysteresis.TOPIC)]
        payload = temperature_payload(123)
        self.assertEqual(set_topic(SetHangarHeaterHysteresis.TOPIC),
                         self.set_hangar_heater_hysteresis.topic)
        self.assertEqual(responses, self.set_hangar_heater_hysteresis.responses)
        self.assertEqual(payload, self.set_hangar_heater_hysteresis.payload)
