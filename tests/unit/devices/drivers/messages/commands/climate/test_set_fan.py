from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_fan import SetFan
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    create_response_list, call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse


class TestSetFan(TestCase):
    def setUp(self) -> None:
        self.set_fan = SetFan(SetFan.ENABLE)

    def test___init__(self):
        expected_messages = [Acknowledgment(),
                             EnableDisableResponse(SetFan.ENABLE)]
        responses = create_response_list(SetFan.RESPONSE_TOPICS,
                                         SetFan.TIMEOUTS,
                                         expected_messages)

        self.assertEqual(call_topic(SetFan.TOPIC), self.set_fan.topic)
        self.assertEqual(responses, self.set_fan.responses)
