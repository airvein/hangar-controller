from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater import \
    SetRoofHeater
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.enable_disable_response import \
    EnableDisableResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_climate_settings import ClimateControlTimeouts


class TestSetRoofHeater(TestCase):
    def setUp(self) -> None:
        self.set_roof_heater = SetRoofHeater(SetRoofHeater.ENABLE)

    def test___init__(self):
        ack_response = create_ack_response(SetRoofHeater.TOPIC)
        state_change = Response(SetRoofHeater.STATE_CHANGE_TOPIC,
                                ClimateControlTimeouts.ROOF_HEATER,
                                EnableDisableResponse(SetRoofHeater.ENABLE))
        responses = [ack_response, state_change]

        self.assertEqual(call_topic(SetRoofHeater.TOPIC),
                         self.set_roof_heater.topic)
        self.assertEqual(responses, self.set_roof_heater.responses)
