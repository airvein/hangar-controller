from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.reset_plc import ClimateResetPLC
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestClimateResetPLC(TestCase):
    def setUp(self) -> None:
        self.reset_plc = ClimateResetPLC()

    def test___init__(self):
        responses = [create_ack_response(ClimateResetPLC.TOPIC)]

        self.assertEqual(call_topic(ClimateResetPLC.TOPIC),
                         self.reset_plc.topic)
        self.assertEqual(responses, self.reset_plc.responses)
