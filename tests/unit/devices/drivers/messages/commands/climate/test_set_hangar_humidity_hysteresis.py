from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_hysteresis import \
    SetHangarHumidityHysteresis
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    humidity_payload, set_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestSetHangarHumidityHysteresis(TestCase):
    def setUp(self) -> None:
        self.set_hangar_humidity_hysteresis = SetHangarHumidityHysteresis(123)

    def test___init__(self):
        # given
        responses = [create_ack_response(SetHangarHumidityHysteresis.TOPIC)]
        payload = humidity_payload(123)
        self.assertEqual(set_topic(SetHangarHumidityHysteresis.TOPIC),
                         self.set_hangar_humidity_hysteresis.topic)
        self.assertEqual(responses,
                         self.set_hangar_humidity_hysteresis.responses)
        self.assertEqual(payload, self.set_hangar_humidity_hysteresis.payload)
