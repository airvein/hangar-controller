from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_states import \
    UserPanelGetStates
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, \
    create_response_list
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    Acknowledgment
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse


class TestUserPanelGetStates(TestCase):
    def setUp(self) -> None:
        self.user_panel_get_states = UserPanelGetStates()

    def test___init__(self):
        expected_messages = [
            Acknowledgment(),
            ViewResponse(None)
        ]

        responses = create_response_list(UserPanelGetStates.RESPONSE_TOPICS,
                                         UserPanelGetStates.TIMEOUTS,
                                         expected_messages)
        self.assertEqual(call_topic(UserPanelGetStates.TOPIC),
                         self.user_panel_get_states.topic)
        self.assertEqual(responses, self.user_panel_get_states.responses)
