from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_reset_controller_get_pin import \
    UserPanelResetControllerGetPin
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic, pin_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import PIN_TIMEOUT


class TestUserPanelResetControllerGetPIN(TestCase):
    def setUp(self) -> None:
        self.reset_ctrl = UserPanelResetControllerGetPin()

    def test___init__(self):
        responses = [
            Response(pin_topic(UserPanelResetControllerGetPin.TOPIC),
                     PIN_TIMEOUT,
                     ValueResponse())]
        self.assertEqual(call_topic(UserPanelResetControllerGetPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
