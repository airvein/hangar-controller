from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_set_confirmation_view import \
    UserPanelSetConfirmationView
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import \
    create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.user_confirmation import \
    UserConfirmation
from hangar_controller.devices.drivers.messages.responses.expected_messages.user_panel.view_response import \
    ViewResponse
from hangar_controller.devices.drivers.messages.responses.response import \
    Response
from settingsd.drivers.stm_user_panel_settings import UserPanelTimeouts, \
    ConfirmationViews


class TestUserPanelSetConfirmationView(TestCase):
    def setUp(self) -> None:
        self.confirm_cargo = UserPanelSetConfirmationView(
            ConfirmationViews.HOME)

    def test___init__(self):
        ack_response = create_ack_response(UserPanelSetConfirmationView.TOPIC)
        response = Response(UserPanelSetConfirmationView.CONFIRMATION_STATE_CHANGE,
                            UserPanelTimeouts.CONFIRMATION_TIMEOUTS[
                                ConfirmationViews.HOME
                            ],
                            UserConfirmation())
        # view_change = Response(UserPanelSetConfirmationView.VIEW_STATE_CHANGE,
        #                        UserPanelTimeouts.VIEW,
        #                        ViewResponse(ConfirmationViews.HOME))

        responses = [ack_response, response]
        self.assertEqual(call_topic(UserPanelSetConfirmationView.TOPIC),
                         self.confirm_cargo.topic)
        self.assertEqual(responses, self.confirm_cargo.responses)
