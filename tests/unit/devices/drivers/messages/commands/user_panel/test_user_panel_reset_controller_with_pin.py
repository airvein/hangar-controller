from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_reset_controller_with_pin import \
    UserPanelResetControllerWithPin
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestUserPanelResetControllerGetPIN(TestCase):
    def setUp(self) -> None:
        self.reset_ctrl = UserPanelResetControllerWithPin('1234')

    def test___init__(self):
        responses = [create_ack_response(UserPanelResetControllerWithPin.TOPIC)]
        self.assertEqual(call_topic(UserPanelResetControllerWithPin.TOPIC),
                         self.reset_ctrl.topic)
        self.assertEqual(responses, self.reset_ctrl.responses)
