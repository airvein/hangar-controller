from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.roof.roof_reset_plc import RoofResetPLC
from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response


class TestRoofResetPLC(TestCase):
    def setUp(self) -> None:
        self.reset_plc = RoofResetPLC()

    def test___init__(self):
        responses = [create_ack_response(RoofResetPLC.TOPIC)]

        self.assertEqual(call_topic(RoofResetPLC.TOPIC),
                         self.reset_plc.topic)
        self.assertEqual(responses, self.reset_plc.responses)
