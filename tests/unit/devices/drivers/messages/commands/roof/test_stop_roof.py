from unittest import TestCase

from hangar_controller.devices.drivers.messages.commands.roof.roof_stop import RoofStop
from hangar_controller.devices.drivers.messages.message_utils.message_utils import call_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.acknowledgement import create_ack_response
from hangar_controller.devices.drivers.messages.responses.expected_messages.roof.roof_state_response import RoofStateResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_roof_settings import RoofControlTimeouts


class TestStopRoof(TestCase):
    def setUp(self) -> None:
        self.command = RoofStop()

    def test___init__(self):
        self.assertEqual(call_topic(RoofStop.TOPIC), self.command.topic)

        responses = [
            create_ack_response(RoofStop.TOPIC),
            Response(RoofStop.STATE_CHANGE,
                     RoofControlTimeouts.ROOF_STOP,
                     RoofStateResponse(RoofStateResponse.IDLE))
        ]
        self.assertEqual(responses, self.command.responses)
