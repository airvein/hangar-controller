from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.commands.abstract_command import AbstractCommand


class TestAbstractCommand(TestCase):
    def setUp(self) -> None:
        self.responses = [MagicMock()]
        self.abstract_command = AbstractCommand('topic', self.responses,
                                                'payload')

    def test___init__(self):
        self.assertEqual('topic', self.abstract_command.topic)
        self.assertEqual(self.responses, self.abstract_command.responses)

    def test_publish(self):
        # given
        send_message = MagicMock()
        
        # when
        self.abstract_command.publish(send_message)

        # then
        send_message.assert_called_with(self.abstract_command.topic, 'payload')
