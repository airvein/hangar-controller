from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.messages.errors.error_message import ErrorMessage


class TestErrorMessage(TestCase):
    def setUp(self) -> None:
        self.error_message = ErrorMessage('topic', MagicMock())

    def test___init__(self):
        self.assertEqual('topic', self.error_message.topic)

    def test_is_telemetry_return_False_if_error_code_not_in_payload(self):
        # given
        self.error_message.error_code = MagicMock()
        self.error_message.error_code.telemetry = 'foo'

        # when
        result = self.error_message.is_telemetry('bar')

        # then
        self.assertFalse(result)

    def test_is_telemetry_return_True_if_error_code_in_payload(self):
        # given
        self.error_message.error_code = MagicMock()
        self.error_message.error_code.telemetry = 'foo'

        # when
        result = self.error_message.is_telemetry('foobar')

        # then
        self.assertTrue(result)

    def test_is_peripheral_return_False_if_error_code_not_in_payload(self):
        # given
        self.error_message.error_code = MagicMock()
        self.error_message.error_code.peripheral = 'foo'

        # when
        result = self.error_message.is_peripheral('bar')

        # then
        self.assertFalse(result)

    def test_is_peripheral_return_True_if_error_code_in_payload(self):
        # given
        self.error_message.error_code = MagicMock()
        self.error_message.error_code.peripheral = 'foo'

        # when
        result = self.error_message.is_peripheral('foobar')

        # then
        self.assertTrue(result)
