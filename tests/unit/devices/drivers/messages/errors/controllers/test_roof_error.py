from unittest import TestCase

from hangar_controller.devices.drivers.messages.errors.controllers.roof_error import RoofErrorMessage
from hangar_controller.devices.drivers.messages.message_utils.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestRoofErrorMessage(TestCase):
    def setUp(self) -> None:
        self.roof_error_message = RoofErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_ROOF),
                         self.roof_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_ROOF,
            self.roof_error_message.error_code)
