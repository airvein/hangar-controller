from unittest import TestCase

from hangar_controller.devices.drivers.messages.errors.controllers.master_charger_error import MasterChargerErrorMessage
from hangar_controller.devices.drivers.messages.message_utils.message_utils import error_topic
from settingsd.drivers import stm_communication_settings
from settingsd.drivers.stm_communication_settings import Controllers


class TestMasterChargerErrorMessage(TestCase):
    def setUp(self) -> None:
        self.master_charger_error_message = MasterChargerErrorMessage()

    def test___init__(self):
        self.assertEqual(error_topic(Controllers.STM_MASTER_CHARGER),
                         self.master_charger_error_message.topic)
        self.assertEqual(
            stm_communication_settings.ControllerErrorCodes.STM_MASTER_CHARGER,
            self.master_charger_error_message.error_code)
