from unittest import TestCase
from unittest.mock import MagicMock, call

from hangar_controller.devices.drivers.messages.abstract_message_manager import AbstractMessageManager
from hangar_controller.devices.drivers.messages.errors.error_manager import ErrorManager
from hangar_controller.devices.drivers.messages.errors.error_message import ErrorMessage


class TestErrorManager(TestCase):
    def setUp(self) -> None:
        self.error_queue = MagicMock()
        self.error_manager = ErrorManager(self.error_queue, MagicMock())

    def test___init__(self):
        self.assertIsInstance(self.error_manager, AbstractMessageManager)
        self.assertEqual(self.error_queue, self.error_manager._error_queue)

    def test_init_error_messages(self):
        # given
        self.error_manager.register_messages = MagicMock()

        # when
        self.error_manager._init_error_messages()

        # then
        self.error_manager.register_messages.assert_called()

    def test_decide_upon_message_doesnt_put_error_on_error_queue_if_not_telemetry_or_peripheral(
            self):
        # given
        self.error_manager._error_warning_log_queue = MagicMock()
        self.error_manager._error_queue = MagicMock()
        error = MagicMock()
        error.is_telemetry = MagicMock(return_value=False)
        error.is_peripheral = MagicMock(return_value=False)

        # when
        self.error_manager._decide_on_message(error, MagicMock())

        # then
        self.error_manager._error_queue.put.assert_not_called()

    def test_decide_upon_message_puts_error_on_error_queue_if_telemetry_or_peripheral(
            self):
        # given
        self.error_manager._error_warning_log_queue = MagicMock()
        self.error_manager._error_queue = MagicMock()
        error = MagicMock()
        error.is_telemetry = MagicMock(side_effect=[True, False, True])
        error.is_peripheral = MagicMock(side_effect=[False, True, True])

        # when
        for _ in range(3):
            self.error_manager._decide_on_message(error, MagicMock())

        # then
        self.error_manager._error_queue.put.assert_has_calls(
            [call(error), ] * 3)

    def test_receive(self):
        # given
        error_message = MagicMock(spec=ErrorMessage)
        self.error_manager.get_message_by_topic = MagicMock(
            return_value=error_message
        )
        message = MagicMock()
        self.error_manager._decide_on_message = MagicMock()
        self.error_manager._error_warning_log_queue = MagicMock()

        # when
        self.error_manager.receive(message)

        # then
        self.error_manager.get_message_by_topic.assert_called_with(
            message.topic)
        self.error_manager._decide_on_message.assert_called()
        self.error_manager._error_warning_log_queue.put.assert_called()
