from unittest import TestCase

from hangar_controller.devices.drivers.messages.errors.acknowledgment.unexpected_response_error import UnexpectedResponseError


class TestUnexpectedResponseError(TestCase):
    def setUp(self) -> None:
        self.unexpected_response_error = UnexpectedResponseError()

    def test___init__(self):
        self.assertIsInstance(self.unexpected_response_error, Exception)
