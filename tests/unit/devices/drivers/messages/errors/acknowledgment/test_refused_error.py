from unittest import TestCase

from hangar_controller.devices.drivers.messages.errors.acknowledgment.refused_error import RefusedError


class TestRefusedError(TestCase):
    def setUp(self) -> None:
        self.refused_error = RefusedError('description')

    def test___init__(self):
        self.assertIsInstance(self.refused_error, Exception)
