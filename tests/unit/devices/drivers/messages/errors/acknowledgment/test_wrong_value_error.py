from unittest import TestCase

from hangar_controller.devices.drivers.messages.errors.acknowledgment.wrong_value_error import WrongValueError


class TestWrongValueError(TestCase):
    def setUp(self) -> None:
        self.wrong_value_error = WrongValueError()

    def test___init__(self):
        self.assertIsInstance(self.wrong_value_error, Exception)
