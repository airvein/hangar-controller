from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.drivers.future_response.future_response_manager import \
    FutureResponseObserver


class TestFutureResponseObserver(TestCase):
    def setUp(self) -> None:
        self.future_response = MagicMock()
        self.unregister_call = MagicMock()
        self.observer = FutureResponseObserver(self.future_response,
                                               self.unregister_call)

    def test___init__(self):
        self.assertEqual(self.observer.future_response, self.future_response)
        self.assertEqual(self.observer.unregister_call, self.unregister_call)
        self.future_response.add_done_callback.assert_called_with(
            self.observer.done_callback)
        self.assertTrue(self.observer.lock.locked())

    def test_done(self):
        # given
        self.observer.lock = MagicMock()
        self.observer.unregister_call = MagicMock()

        # when
        self.observer.done_callback()

        # then
        self.observer.lock.release.assert_called()
        self.observer.unregister_call.assert_called()

    def test_run(self):
        # given
        self.observer.lock = MagicMock()

        # when
        self.observer.run()

        # then
        self.observer.lock.acquire.assert_called()
