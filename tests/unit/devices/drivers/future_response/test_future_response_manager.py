from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.drivers.future_response.future_response_manager import \
    FutureResponseManager


class TestFutureResponseManager(TestCase):
    def setUp(self) -> None:
        self.manager = FutureResponseManager()

    def test___init__(self):
        self.assertEqual([], self.manager.future_response_observers)

    @patch('hangar_controller.devices.drivers.future_response.future_response_manager.'
           'FutureResponseObserver')
    def test_start_future_observer(self, observer_mock):
        # given
        observer = MagicMock()
        observer_mock.return_value = observer
        
        # when
        self.manager.start_future_observer(MagicMock())
        
        # then
        observer_mock.assert_called()
        observer.start.assert_called()
        self.assertEqual([observer], self.manager.future_response_observers)

    def test_unregister_self(self):
        # given
        observer = MagicMock()
        self.manager.future_response_observers = [observer]
        
        # when
        self.manager.unregister_self(observer)
        
        # then
        self.assertEqual([], self.manager.future_response_observers)

    def test_stop(self):
        # given
        observer1 = MagicMock()
        observer2 = MagicMock()
        self.manager.future_response_observers = [observer1, observer2]

        # when
        self.manager.stop()

        # then
        observer1.future_response.stop.assert_called()
        observer2.future_response.stop.assert_called()
