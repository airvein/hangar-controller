from concurrent.futures import ThreadPoolExecutor
from unittest import TestCase
from unittest.mock import MagicMock, call, patch

from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import \
    DriversDiagnostics
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.drivers.messages.abstract_message_manager import \
    AbstractMessageManager
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_base_shift import \
    CargoBaseShift
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_close_window import \
    CargoCloseWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_plc_io import \
    CargoGetPLCIO
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_states import \
    CargoGetStates
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_open_window import \
    CargoOpenWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_work_shift import \
    CargoWorkShift
from hangar_controller.devices.drivers.messages.commands.commands_manager import \
    CommandsManager
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_chargers_number import \
    MasterChargerGetChargersNumber
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_plc_io import \
    MasterChargerGetPLCIO
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_states import \
    MasterChargerGetStates
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_get_telemetry import \
    MasterChargerGetTelemetry
from hangar_controller.devices.drivers.messages.commands.master_charger.master_charger_set_power import \
    MasterChargerSetPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_plc_io import \
    PMSGetPLCIO
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_states import \
    PMSGetStates
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_chargers_power import \
    PMSSetChargersPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_irlock_power import \
    PMSSetIRLockPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_lift_power import \
    PMSSetMotorsLiftPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_man_power import \
    PMSSetMotorsManPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_pos_power import \
    PMSSetMotorsPosPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_pressure import \
    PMSSetPressure
from hangar_controller.devices.drivers.messages.commands.roof.roof_close import \
    RoofClose
from hangar_controller.devices.drivers.messages.commands.roof.roof_open import \
    RoofOpen
from hangar_controller.devices.drivers.messages.commands.roof.roof_stop import \
    RoofStop
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_pin import \
    UserPanelGetPin
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_set_view import \
    UserPanelSetView
from hangar_controller.devices.drivers.messages.measurements.measurement import \
    Measurement
from hangar_controller.devices.drivers.messages.measurements.measurements_manager import \
    MeasurementsManager
from hangar_controller.devices.drivers.messages.responses.response_manager import \
    ResponseManager
from hangar_controller.devices.drivers.messages.state_readers.state_read_manager import \
    StateReadManager
from settingsd.drivers.stm_user_panel_settings import UserPanelViews


class TestDriversController(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.client = MagicMock()
        cls.error_handler = MagicMock()
        cls.task_queue = MagicMock()
        cls.diagnostics = MagicMock()

    # @patch.object(StateReadManager, '_init_state_reads')
    @patch(
        'hangar_controller.devices.drivers.hangar_drivers_mqtt_client.HangarDriversMQTTClient.__new__',
        return_value=MagicMock())
    def setUp(self, mqtt_mock) -> None:
        self.controller = DriversController(MagicMock(), MagicMock())
        self.controller._task_queue = MagicMock()
        self.mqtt_mock = mqtt_mock

    def tearDown(self):
        DriversController.__it__ = None

    def test___init__(self):
        self.assertIsInstance(self.controller._diagnostics, DriversDiagnostics)
        self.assertIsInstance(self.controller.executor, ThreadPoolExecutor)
        self.assertIsInstance(self.controller._measurements_manager,
                              MeasurementsManager)
        self.assertIsInstance(self.controller._response_manager,
                              ResponseManager)
        self.assertIsInstance(self.controller._commands_manager,
                              CommandsManager)
        self.assertIsInstance(self.controller._state_read_manager,
                              StateReadManager)

    def test_diagnostics(self):
        self.assertEqual(self.controller._diagnostics,
                         self.controller.diagnostics)

    def test_receive_message_response(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        message = MagicMock()
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_called_with(
            message
        )
        self.controller._measurements_manager.receive.assert_not_called()

    def test_receive_message_measurement(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_called_with(
            message
        )

    def test_receive_message_error(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._error_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_not_called()
        self.controller._error_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._error_manager.receive.assert_called()

    def test_receive_message_state_read(self):
        # given
        self.controller._response_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._response_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._measurements_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._error_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._measurements_manager.is_topic_registered = MagicMock(
            return_value=False
        )
        self.controller._state_read_manager = MagicMock(
            spec=AbstractMessageManager)
        self.controller._state_read_manager.is_topic_registered = MagicMock(
            return_value=True
        )
        message = MagicMock(spec=Measurement)
        message.topic = MagicMock()

        # when
        self.controller._receive_message(message)

        # then
        self.controller._response_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._response_manager.receive.assert_not_called()
        self.controller._measurements_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._measurements_manager.receive.assert_not_called()
        self.controller._state_read_manager.is_topic_registered. \
            assert_called_with(message.topic)
        self.controller._state_read_manager.receive.assert_called()

    def test_run_empty_message(self):
        # given
        def stop_controller(*args):
            self.controller.stop()

        self.controller.message_queue = MagicMock()
        self.controller.message_queue.get = MagicMock(
            side_effect=[None, 'msg'])
        self.controller._receive_message = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.run()

        # then
        self.controller.message_queue.get.assert_has_calls([call(), call()])
        self.controller._receive_message.assert_called_once_with('msg')

    def test_run(self):
        # given
        def stop_controller(*args):
            self.controller.stop()

        self.controller.message_queue = MagicMock()
        self.controller.message_queue.get = MagicMock(return_value='msg')
        self.controller._receive_message = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.run()

        # then
        self.controller.message_queue.get.assert_called()
        self.controller._receive_message.assert_called_with('msg')

    def test_start_heartbeat(self):
        # given
        self.controller._commands_manager.heartbeat_sender = MagicMock()

        # when
        self.controller.start_heartbeat()

        # then
        self.controller._commands_manager.heartbeat_sender.start.assert_called()

    def test_stop_heartbeat(self):
        # given
        self.controller._commands_manager.heartbeat_sender = MagicMock()

        # when
        self.controller.stop_heartbeat()

        # then
        self.controller._commands_manager.heartbeat_sender.stop.assert_called()

    def test_stop_roof(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.roof_stop()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(RoofStop())

    def test_open_roof(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.roof_open()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(RoofOpen())

    def test_close_roof(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.roof_close()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(RoofClose())

    def test_mc_get_chargers_number(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_get_chargers_number()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetChargersNumber())

    def test_set_charger_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_set_charger_power('1', True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(
            MasterChargerSetPower('1', MasterChargerSetPower.ENABLED))

    def test_disable_charger_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_set_charger_power('1', False)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(
            MasterChargerSetPower('1', MasterChargerSetPower.DISABLED))

    def test_mc_get_states(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_get_states('1')

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetStates('1'))

    def test_mc_get_telemetry(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_get_telemetry('1')

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetTelemetry('1'))

    def test_mc_get_plc_io(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.master_charger_get_plc_io()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(MasterChargerGetPLCIO())

    def test_open_window(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_open_window()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoOpenWindow())

    def test_close_window(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_close_window()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoCloseWindow())

    def test_base_shift(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_base_shift()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoBaseShift())

    def test_work_shift(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_work_shift()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoWorkShift())

    def test_cargo_get_states(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_get_states()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoGetStates())

    def test_cargo_get_plc_io(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.cargo_get_plc_io()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(CargoGetPLCIO())

    def test_set_chargers_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_chargers_power(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(PMSSetChargersPower(PMSSetChargersPower.ENABLE))

    def test_set_ir_lock_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_ir_lock_power(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(PMSSetIRLockPower(PMSSetIRLockPower.ENABLE))

    def test_set_motors_lift_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_motors_lift_power(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(
            PMSSetMotorsLiftPower(PMSSetMotorsLiftPower.ENABLE))

    def test_set_motors_man_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_motors_man_power(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(
            PMSSetMotorsManPower(PMSSetMotorsManPower.ENABLE))

    def test_set_motors_pos_power(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_motors_pos_power(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(
            PMSSetMotorsPosPower(PMSSetMotorsPosPower.ENABLE))

    def test_set_pressure(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.pms_set_pressure(True)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(PMSSetPressure(PMSSetPressure.ENABLE))

    def test_pms_get_states(self):
        # given
        self.controller._commands_manager = MagicMock()
        self.controller._commands_manager.send_command_and_wait = MagicMock(
            return_value='states'
        )

        # when
        states = self.controller.pms_get_states()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(PMSGetStates())
        self.assertEqual('states', states)

    def test_pms_get_plc_io(self):
        # given
        self.controller._commands_manager = MagicMock()
        self.controller._commands_manager.send_command_and_wait = MagicMock(
            return_value='plc_io'
        )

        # when
        plc_io = self.controller.pms_get_plc_io()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(PMSGetPLCIO())
        self.assertEqual('plc_io', plc_io)

    def test_get_pin(self):
        # given
        self.controller._commands_manager = MagicMock()
        expected_response = MagicMock()
        expected_pin_response = MagicMock()
        expected_response.get_response_by_topic = MagicMock(
            return_value=expected_pin_response)
        expected_pin_response.payload = 'pin'
        self.controller._commands_manager.send_command_and_wait = MagicMock(
            return_value=expected_response
        )

        # when
        resp, pin = self.controller.user_panel_get_pin()

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(UserPanelGetPin())
        self.assertEqual(expected_response, resp)
        self.assertEqual('pin', pin)

    def test_set_view(self):
        # given
        self.controller._commands_manager = MagicMock()

        # when
        self.controller.user_panel_set_view(UserPanelViews.CARGO_CORRECT)

        # then
        self.controller._commands_manager.send_command_and_wait. \
            assert_called_with(UserPanelSetView(UserPanelViews.CARGO_CORRECT))
