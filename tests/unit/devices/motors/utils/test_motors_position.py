from unittest import TestCase

from hangar_controller.devices.motors.utils.motors_position import MotorsPosition


class TestMotorsPosition(TestCase):
    def setUp(self) -> None:
        self.motors_position = MotorsPosition('name', 'position')

    def test___init__(self):
        self.assertEqual('name', self.motors_position.motor_name)
        self.assertEqual('position', self.motors_position.position)

    def test___repr__(self):
        self.assertEqual('position: name: name, position: position',
                         self.motors_position.__repr__())
