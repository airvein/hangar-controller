import time
from unittest import TestCase

from hangar_controller.devices.motors.utils import utils


class TestUtils(TestCase):
    def test_get_current_time(self):
        # given
        curr_time = int(time.time() * 1000)

        # when
        time_got = utils.get_current_time()

        # then
        self.assertAlmostEqual(time_got / 1000, curr_time / 1000, places=3)
