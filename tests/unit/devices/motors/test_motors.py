import time
from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.motors_controllers.motors_controllers_manager import \
    WobitsControllers
from hangar_controller.devices.motors.controller.task_queue.task_queue import \
    TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler
from hangar_controller.devices.motors.motors import Motors
from hangar_controller.devices.motors.parameters.parameters_reader import \
    ParametersReader


class TestMotors(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.parameters_reader = ParametersReader()

    def setUp(self) -> None:
        self.controller = Motors(MagicMock(), MagicMock())
        self.controller._task_queue.wait_until_done = MagicMock()

    def test___init__(self):
        self.assertFalse(self.controller._stop_controller)
        self.assertIsInstance(self.controller._task_queue, TaskQueue)
        self.assertIsInstance(self.controller._error_handler, ErrorHandler)
        self.assertIsInstance(self.controller.wobits, WobitsControllers)

    def test_stop(self):
        # given
        self.controller._task_queue = MagicMock()

        # when
        self.controller.stop()

        # then
        self.assertTrue(self.controller._stop_controller)
        self.controller._task_queue.exit_procedure.assert_called()

    def test_run(self):
        # given
        def stop_controller():
            self.controller.stop()

        self.controller._task_queue.exit_procedure = MagicMock()
        self.controller._task_queue.process_task = MagicMock(
            side_effect=stop_controller)

        # when
        self.controller.start()
        time.sleep(0.1)

        # then
        self.controller._task_queue.process_task.assert_called_once()

    def test_start_diagnostics(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.start_diagnostics()

        # then
        self.controller.wobits.start_diagnostics.assert_called()

    def test_stop_diagnostics(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.stop_diagnostics()

        # then
        self.controller.wobits.stop_diagnostics.assert_called()

    def test_calibrate_battery_motors(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.calibrate_battery_motors()

        # then
        self.controller.wobits.manipulator.calibrate_safe_sequence.assert_called()

    def test_go_cargo_window_pick_up(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_cargo_window_pick_up()

        # then
        self.controller.wobits.manipulator.go_cargo_window_pick_up.assert_called()

    def test_go_cargo_window_put_down(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_cargo_window_put_down()

        # then
        self.controller.wobits.manipulator.go_cargo_window_put_down.assert_called()

    def test_go_cargo_drone(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_cargo_drone()

        # then
        self.controller.wobits.manipulator.go_cargo_drone.assert_called()

    def test_go_battery_drone(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_battery_drone()

        # then
        self.controller.wobits.manipulator.go_battery_drone.assert_called()

    def test_go_battery_slot(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_battery_slot(3)

        # then
        self.controller.wobits.manipulator.go_to_battery_slot.assert_called_with(
            3)

    def test_go_home(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_home()

        # then
        self.controller.wobits.manipulator.go_home.assert_called()

    def test_calibrate_lift(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.calibrate_lift()

        # then
        self.controller.wobits.lift.calibrate_lift.assert_called()

    def test_go_lift_up(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_lift_up()

        # then
        self.controller.wobits.lift.go_lift_up.assert_called()

    def test_go_lift_down(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.go_lift_down()

        # then
        self.controller.wobits.lift.go_lift_down.assert_called()

    def test_calibrate_positioning(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.calibrate_positioning()

        # then
        self.controller.wobits.positioning.calibrate_all.assert_called()

    def test_position_drone(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.position_drone()

        # then
        self.controller.wobits.positioning.position_drone.assert_called()

    def test_release_drone(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.release_drone()

        # then
        self.controller.wobits.positioning.release_drone.assert_called()

    def test_grasp(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.grasp()

        # then
        self.controller.wobits.grasper.calibrate_grasper.assert_called()

    def test_release(self):
        # given
        self.controller.wobits = MagicMock()

        # when
        self.controller.release()

        # then
        self.controller.wobits.grasper.grasper_horizontal.assert_called()
