from unittest import TestCase
from unittest.mock import MagicMock, patch, call

from hangar_controller.devices.motors.controller.motors_controllers.abstract_motor_controller import \
    AbstractMotorsController
from hangar_controller.devices.motors.controller.wobit_motors import motors_utils
from hangar_controller.devices.motors.controller.wobit_motors.wobit_motor import WobitMotor
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from settingsd.motors import modbus_constants, parameters_settings
from settingsd.motors.modbus_constants import ModbusResponseStatus


class TestAbstractMotorsController(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock()

    def setUp(self) -> None:
        motors_list = [(modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_LIFT,
                        modbus_constants.WobitMotor1Addresses),
                       (modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_X,
                        modbus_constants.WobitMotor2Addresses),
                       (modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_Y,
                        modbus_constants.WobitMotor3Addresses),
                       (modbus_constants.Wobit.BatteriesMotorsNames.MOTOR_Z,
                        modbus_constants.WobitMotor4Addresses)]

        self.controller = AbstractMotorsController(motors_list, self.modbus_io,
                                                   self.error_handler)

    def test___init__(self):
        self.assertEqual(self.error_handler, self.controller._error_handler)
        self.assertIsInstance(self.controller.motors[0], WobitMotor)
        self.assertIsInstance(self.controller.motors[1], WobitMotor)
        self.assertIsInstance(self.controller.motors[2], WobitMotor)
        self.assertIsInstance(self.controller.motors[3], WobitMotor)
        self.assertEqual(4, len(self.controller._motors))

    def test_motor_property(self):
        # given
        self.controller._motors = 'motors'

        # when
        result = self.controller.motors

        # then
        self.assertEqual('motors', result)

    def test_start_diagnostics(self):
        # given
        self.controller._diagnostics = MagicMock()

        # when
        self.controller.start_diagnostics()

        # then
        self.controller._diagnostics.start.assert_called()

    def test_stop_diagnostics(self):
        # given
        self.controller._diagnostics = MagicMock()

        # when
        self.controller.stop_diagnostics()

        # then
        self.controller._diagnostics.stop.assert_called()

    def test_diagnostics_property(self):
        self.assertEqual(self.controller.diagnostics,
                         self.controller._diagnostics.diagnostics)

    def test_get_diagnostics_by_motor_name_wrong_name(self):
        # given
        diag_mock = MagicMock()
        self.controller._diagnostics = diag_mock
        diag_mock.diagnostics = {'motor1': MagicMock()}

        # then
        self.assertRaises(ValueError,
                          self.controller.get_diagnostics_by_motor_name, 'm2')

    def test_get_diagnostics_by_motor_name(self):
        # given
        diag_mock = MagicMock()
        self.controller._diagnostics = diag_mock
        motor1_diag = MagicMock()
        diag_mock.diagnostics = {'motor1': motor1_diag}

        # when
        result = self.controller.get_diagnostics_by_motor_name('motor1')

        # then
        self.assertEqual(motor1_diag, result)

    def test_init_motors(self):
        # given
        motor1 = MagicMock()
        registers1 = MagicMock()
        motor2 = MagicMock()
        registers2 = MagicMock()
        motors_list = [(motor1, registers1), (motor2, registers2)]

        # when
        motors = self.controller.init_motors(motors_list)

        # then
        self.assertEqual(2, len(motors))
        self.assertIsInstance(motors[0], WobitMotor)
        self.assertIsInstance(motors[1], WobitMotor)
        self.assertEqual(self.controller._modbus_io, motors[0].modbus_io)
        self.assertEqual(self.controller._modbus_io, motors[1].modbus_io)
        self.assertEqual(self.controller._error_handler,
                         motors[0].error_handler)
        self.assertEqual(self.controller._error_handler,
                         motors[1].error_handler)

    def test_check_modbus_response_status_modbus_error(self):
        # given
        self.controller._error_handler = MagicMock()

        # when
        result = self.controller._check_modbus_response_status(
            modbus_utils.ModbusResponseStatus.CONNECTION_ERROR)

        # then
        self.controller._error_handler.modbus_error.assert_called_with(
            modbus_utils.ModbusResponseStatus.CONNECTION_ERROR
        )
        self.assertFalse(result)

    def test_check_modbus_response_status(self):
        # given
        self.controller._error_handler = MagicMock()

        # when
        result = self.controller._check_modbus_response_status(
            modbus_utils.ModbusResponseStatus.STATUS_OK)

        # then
        self.controller._error_handler.modbus_error.assert_not_called()
        self.assertTrue(result)

    def test_update_motor_parameters(self):
        # given
        motor = MagicMock()
        motor.motor_name = parameters_settings.DrivesNames.X
        self.controller._check_modbus_response_status = MagicMock(
            return_value=True
        )
        self.controller._parameters_reader = MagicMock()

        # when
        self.controller._update_motor_parameters(motor)

        # then
        self.controller._parameters_reader.read_max_velocity.assert_called_with(
            motor.motor_name
        )
        motor.write_max_velocity.assert_called()
        self.controller._parameters_reader.read_acceleration.assert_called_with(
            motor.motor_name
        )
        motor.write_deceleration.assert_called()
        self.controller._parameters_reader.read_deceleration.assert_called_with(
            motor.motor_name
        )
        motor.write_deceleration.assert_called()

    def test_update_motors_parameters(self):
        # given
        motor1 = MagicMock()
        motor2 = MagicMock()
        motors_list = [motor1, motor2]
        self.controller._motors = motors_list
        calls = [call(motor1), call(motor2)]
        self.controller._update_motor_parameters = MagicMock(
            return_value=True
        )

        # when
        self.controller.update_motors_parameters()

        # then
        self.controller._update_motor_parameters.assert_has_calls(calls)

    def test_set_direct_mode(self):
        # given
        self.controller._motor_master = MagicMock()
        self.controller._check_modbus_response_status = MagicMock()

        # when
        self.controller._set_direct_mode()

        # then
        self.controller._motor_master.write_axis_control_mode. \
            assert_called_with(
            modbus_constants.Wobit.AxisControlMode.DIRECT
        )
        self.controller._check_modbus_response_status.assert_called()

    def test_set_trigger_mode(self):
        # given
        self.controller._motor_master = MagicMock()
        self.controller._check_modbus_response_status = MagicMock()

        # when
        self.controller._set_trigger_mode()

        # then
        self.controller._motor_master.write_axis_control_mode. \
            assert_called_with(
            modbus_constants.Wobit.AxisControlMode.TRIGGER
        )
        self.controller._check_modbus_response_status.assert_called()

    def test_set_motor_targets_modbus_error(self):
        # given
        motor_1 = MagicMock()
        motor_1.write_absolute_position = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor_2 = MagicMock()
        motors = [motor_1, motor_2]
        positions = [1.1, 1.2]
        self.controller._check_modbus_response_status = MagicMock(
            return_value=False
        )

        # when
        result = self.controller._set_motors_targets(motors, positions)

        # then
        motor_1.write_absolute_position.assert_called_with(1.1)
        motor_2.write_absolute_position.assert_not_called()
        self.assertFalse(result)
        self.controller._check_modbus_response_status.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )

    def test_set_motor_targets(self):
        # given
        motor_1 = MagicMock()
        motor_1.write_absolute_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        motor_2 = MagicMock()
        motor_2.write_absolute_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        motors = [motor_1, motor_2]
        positions = [1.1, 1.2]
        self.controller._check_modbus_response_status = MagicMock(
            return_value=True
        )
        calls = [call(ModbusResponseStatus.STATUS_OK),
                 call(ModbusResponseStatus.STATUS_OK)]

        # when
        result = self.controller._set_motors_targets(motors, positions)

        # then
        motor_1.write_absolute_position.assert_called_with(1.1)
        motor_2.write_absolute_position.assert_called_with(1.2)
        self.assertTrue(result)
        self.controller._check_modbus_response_status.assert_has_calls(calls)

    def test_trigger_movements_modbus_error(self):
        # given
        motor_1 = MagicMock()
        motor_1.wobit_id = 1
        motor_2 = MagicMock()
        motor_2.wobit_id = 3
        motors = [motor_1, motor_2]

        self.controller._motor_master = MagicMock()
        self.controller._motor_master.trigger_absolute_position = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        self.controller._check_modbus_response_status = MagicMock(
            return_value=False
        )

        # when
        result = self.controller._trigger_movements(motors)

        # then
        self.assertFalse(result)
        self.controller._motor_master.trigger_absolute_position. \
            assert_called_with([1, 3])
        self.controller._check_modbus_response_status.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )

    def test_trigger_movements(self):
        # given
        motor_1 = MagicMock()
        motor_1.wobit_id = 1
        motor_2 = MagicMock()
        motor_2.wobit_id = 3
        motors = [motor_1, motor_2]

        self.controller._motor_master = MagicMock()
        self.controller._motor_master.trigger_absolute_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        self.controller._check_modbus_response_status = MagicMock(
            return_value=True
        )

        # when
        result = self.controller._trigger_movements(motors)

        # then
        self.assertTrue(result)
        self.controller._motor_master.trigger_absolute_position. \
            assert_called_with([1, 3])
        self.controller._check_modbus_response_status.assert_called_with(
            ModbusResponseStatus.STATUS_OK
        )

    def test_start_position_observers_modbus_error(self):
        # given
        motor_1 = MagicMock()
        motor_1.position_observer.observe = MagicMock(return_value=False)
        motor_2 = MagicMock()
        motors = [motor_1, motor_2]
        positions = [1.1, 1.2]

        # when
        result = self.controller._start_position_observers(motors, positions)

        # then
        motor_1.position_observer.observe.assert_called_with(1.1)
        motor_2.position_observer.observe.assert_not_called()
        self.assertFalse(result)

    def test_start_position_observers(self):
        # given
        motor_1 = MagicMock()
        motor_1.position_observer.observe = MagicMock(return_value=True)
        motor_2 = MagicMock()
        motor_2.position_observer.observe = MagicMock(return_value=True)
        motors = [motor_1, motor_2]
        positions = [1.1, 1.2]

        # when
        result = self.controller._start_position_observers(motors, positions)

        # then
        motor_1.position_observer.observe.assert_called_with(1.1)
        motor_2.position_observer.observe.assert_called_with(1.2)
        self.assertTrue(result)

    def test_move_motors_trigger_mode_modbus_error(self):
        # given
        self.controller._set_trigger_mode = MagicMock(return_value=False)
        self.controller._set_motors_targets = MagicMock(return_value=False)
        self.controller._trigger_movements = MagicMock(return_value=False)
        self.controller._start_position_observers = MagicMock(
            return_value=False)

        # when
        result = self.controller.move_motors([])

        # then
        self.controller._set_trigger_mode.assert_called()
        self.assertFalse(result)

    @patch('hangar_controller.devices.motors.controller.wobit_motors.motors_utils.get_motors_by_names')
    def test_move_motors_set_motors_targets_modbus_error(self,
                                                         motors_names_mock):
        # given
        self.controller._set_trigger_mode = MagicMock(return_value=True)
        self.controller._set_motors_targets = MagicMock(return_value=False)
        self.controller._trigger_movements = MagicMock(return_value=False)
        self.controller._start_position_observers = MagicMock(
            return_value=False)
        motors_names_mock.return_value = 'motors'
        position = MagicMock()
        position.motor_name = 'MOTOR_X'
        position.position = 123

        # when
        result = self.controller.move_motors([position])

        # then
        self.controller._set_motors_targets.assert_called()
        self.controller._trigger_movements.assert_not_called()
        self.assertFalse(result)

    @patch('hangar_controller.devices.motors.controller.wobit_motors.motors_utils.get_motors_by_names')
    def test_move_motors_trigger_movements_modbus_error(self,
                                                        motors_names_mock):
        # given
        self.controller._set_trigger_mode = MagicMock(return_value=True)
        self.controller._set_motors_targets = MagicMock(return_value=True)
        self.controller._trigger_movements = MagicMock(return_value=False)
        self.controller._start_position_observers = MagicMock(
            return_value=False)
        motors_names_mock.return_value = 'motors'
        position = MagicMock()
        position.motor_name = 'MOTOR_X'
        position.position = 123

        # when
        result = self.controller.move_motors([position])

        # then
        self.controller._set_trigger_mode.assert_called()
        self.controller._trigger_movements.assert_called_with('motors')
        self.controller._start_position_observers.assert_not_called()
        self.assertFalse(result)

    @patch('hangar_controller.devices.motors.controller.wobit_motors.motors_utils.get_motors_by_names')
    def test_move_motors_start_position_observers_modbus_error(self,
                                                               motors_names_mock):
        # given
        self.controller._set_trigger_mode = MagicMock(return_value=True)
        self.controller._set_motors_targets = MagicMock(return_value=True)
        self.controller._trigger_movements = MagicMock(return_value=True)
        self.controller._start_position_observers = MagicMock(
            return_value=False)
        motors_names_mock.return_value = 'motors'
        position = MagicMock()
        position.motor_name = 'MOTOR_X'
        position.position = 123

        # when
        result = self.controller.move_motors([position])

        # then
        self.controller._set_trigger_mode.assert_called()
        self.controller._trigger_movements.assert_called_with('motors')
        self.controller._start_position_observers. \
            assert_called_with('motors', [123])

        self.assertFalse(result)

    @patch('hangar_controller.devices.motors.controller.wobit_motors.motors_utils.get_motors_by_names')
    def test_move_motors(self, motors_names_mock):
        # given
        self.controller._set_trigger_mode = MagicMock(return_value=True)
        self.controller._set_motors_targets = MagicMock(return_value=True)
        self.controller._trigger_movements = MagicMock(return_value=True)
        self.controller._start_position_observers = MagicMock(return_value=True)
        motors_names_mock.return_value = 'motors'
        position = MagicMock()
        position.motor_name = 'MOTOR_X'
        position.position = 123

        # when
        result = self.controller.move_motors([position])

        # then
        self.controller._set_trigger_mode.assert_called()
        self.controller._trigger_movements.assert_called_with('motors')
        self.controller._start_position_observers.assert_called_with(
            'motors', [123])

        self.assertTrue(result)

    def test_calibrate_set_direct_mode_modbus_error(self):
        # given
        motor = MagicMock()
        motor.write_actual_position = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor.move_home = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor.position_observer.observe = MagicMock(return_value=False)
        motors_utils.get_motors_by_names = MagicMock(return_value=[motor])

        self.controller._set_direct_mode = MagicMock(return_value=False)
        self.controller._set_motors_targets = MagicMock(return_value=False)
        self.controller._trigger_movements = MagicMock(return_value=False)
        self.controller._start_position_observers = MagicMock(
            return_value=False)
        motors_names = MagicMock()

        # when
        result = self.controller.calibrate(motors_names, [12])

        # then
        motors_utils.get_motors_by_names.assert_called_with(
            self.controller._motors,
            motors_names)
        self.controller._set_direct_mode.assert_called()
        motor.write_actual_position.assert_not_called()

        self.assertFalse(result)

    def test_calibrate_write_actual_position_modbus_error(self):
        # given
        motor = MagicMock()
        self.controller._set_direct_mode = MagicMock(return_value=True)
        motor.write_actual_position = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor.move_home = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor.position_observer.observe = MagicMock(return_value=False)
        motors_utils.get_motors_by_names = MagicMock(return_value=[motor])

        motors_names = MagicMock()

        # when
        result = self.controller.calibrate(motors_names, [12])

        # then
        motors_utils.get_motors_by_names.assert_called_with(
            self.controller._motors,
            motors_names)
        self.controller._set_direct_mode.assert_called()
        motor.write_actual_position.assert_called_with(1)
        motor.move_home.assert_not_called()

        self.assertFalse(result)

    def test_calibrate_move_home_modbus_error(self):
        # given
        motor = MagicMock()
        self.controller._set_direct_mode = MagicMock(return_value=True)
        motor.write_actual_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        motor.move_home = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR)
        motor.position_observer.observe = MagicMock(return_value=False)
        motors_utils.get_motors_by_names = MagicMock(return_value=[motor])

        motors_names = MagicMock()

        # when
        result = self.controller.calibrate(motors_names, [12])

        # then
        motors_utils.get_motors_by_names.assert_called_with(
            self.controller._motors,
            motors_names)
        self.controller._set_direct_mode.assert_called()
        motor.write_actual_position.assert_called_with(1)
        motor.move_home.assert_called()
        motor.position_observer.observe.assert_not_called()
        self.assertFalse(result)

    def test_calibrate_position_observer_modbus_error(self):
        # given
        motor = MagicMock()
        self.controller._set_direct_mode = MagicMock(return_value=True)
        motor.write_actual_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        motor.move_home = MagicMock(return_value=ModbusResponseStatus.STATUS_OK)
        motor.position_observer.observe = MagicMock(return_value=False)
        motors_utils.get_motors_by_names = MagicMock(return_value=[motor])

        motors_names = MagicMock()

        # when
        result = self.controller.calibrate(motors_names, [12])

        # then
        motors_utils.get_motors_by_names.assert_called_with(
            self.controller._motors,
            motors_names)
        self.controller._set_direct_mode.assert_called()
        motor.write_actual_position.assert_called_with(1)
        motor.move_home.assert_called()
        motor.position_observer.observe.assert_called_with(end_position=0)

        self.assertFalse(result)

    def test_calibrate(self):
        # given
        motor = MagicMock()
        self.controller._set_direct_mode = MagicMock(return_value=True)
        motor.write_actual_position = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK)
        motor.move_home = MagicMock(return_value=ModbusResponseStatus.STATUS_OK)
        motor.position_observer.observe = MagicMock(return_value=True)
        motors_utils.get_motors_by_names = MagicMock(return_value=[motor])

        motors_names = MagicMock()

        # when
        result = self.controller.calibrate(motors_names, [12])

        # then
        motors_utils.get_motors_by_names.assert_called_with(
            self.controller._motors,
            motors_names)
        self.controller._set_direct_mode.assert_called()
        motor.write_actual_position.assert_called_with(1)
        motor.move_home.assert_called()
        motor.position_observer.observe.assert_called_with(end_position=0)

        self.assertTrue(result)

    def test_stop_all(self):
        # given
        self.controller._motor_master = MagicMock()
        self.controller._check_modbus_response_status = MagicMock()

        # when
        self.controller.stop_all()

        # then
        self.controller._motor_master.stop_all_motors.assert_called()
        self.controller._check_modbus_response_status.assert_called()

    def test_filter_position_equal(self):
        # given
        position1 = MagicMock()
        position1.motor_name = 'MOTOR_X'
        position1.position = 123
        position2 = MagicMock()
        position2.motor_name = 'MOTOR_Y'
        position2.position = 123

        # when
        result = self.controller.filter_positions([position1, position2],
                                                  'MOTOR_X')

        # then
        self.assertEqual([position1], result)

    def test_filter_position_non_equal(self):
        # given
        position1 = MagicMock()
        position1.motor_name = 'MOTOR_X'
        position1.position = 123
        position2 = MagicMock()
        position2.motor_name = 'MOTOR_Y'
        position2.position = 123

        # when
        result = self.controller.filter_positions([position1, position2],
                                                  'MOTOR_X', equal=False)

        # then
        self.assertEqual([position2], result)
