from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from hangar_controller.devices.motors.controller.motors_controllers.grasper_motor_controller import \
    GrasperMotorController
from hangar_controller.devices.motors.controller.motors_controllers.lift_motor_controller import \
    LiftMotorController
from hangar_controller.devices.motors.controller.motors_controllers.manipulator_motor_controller import \
    ManipulatorMotorController
from hangar_controller.devices.motors.controller.motors_controllers.motors_controllers_manager import \
    WobitsControllers
from hangar_controller.devices.motors.controller.motors_controllers.positioning_motors_controller import \
    PositioningMotorController
from hangar_controller.devices.motors.parameters.parameters_reader import ParametersReader


class TestMotorsControllers(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock(spec=ParametersReader)
        cls.task_queue = MagicMock()
        cls.parameters_reader.read_grasper_motor_placement = MagicMock(
            return_value=[[0], [1]]
        )
        cls.parameters_reader.read_positioning_motors_placement = MagicMock(
            return_value=[[0, 0], [1, 2]]
        )
        cls.parameters_reader.read_lift_motor_placement = MagicMock(
            return_value=[[0], [1]]
        )
        cls.parameters_reader.read_manipulator_motors_placement = MagicMock(
            return_value=[[1, 1, 1], [0, 1, 2]]
        )

    def setUp(self) -> None:
        self.controller = WobitsControllers(
            self.error_handler, self.task_queue,
        )

    def test___init__(self):
        self.assertIsInstance(self.controller._parameters_reader,
                              ParametersReader)
        self.assertIsInstance(self.controller.manipulator,
                              ManipulatorMotorController)
        self.assertIsInstance(self.controller.lift, LiftMotorController)
        self.assertIsInstance(self.controller.positioning,
                              PositioningMotorController)
        self.assertIsInstance(self.controller.grasper, GrasperMotorController)
        self.assertIsInstance(self.controller.diagnostics_reader,
                              WobitDiagnosticsReader)

    def test_start_diagnostics(self):
        # given
        self.controller.manipulator = MagicMock()
        self.controller.lift = MagicMock()
        self.controller.positioning = MagicMock()
        self.controller.grasper = MagicMock()
        self.controller.controllers = [self.controller.manipulator,
                                       self.controller.lift,
                                       self.controller.positioning,
                                       self.controller.grasper]

        # when
        self.controller.start_diagnostics()

        # then
        self.controller.manipulator.start_diagnostics.assert_called()
        self.controller.lift.start_diagnostics.assert_called()
        self.controller.positioning.start_diagnostics.assert_called()
        self.controller.grasper.start_diagnostics.assert_called()

    def test_stop_diagnostics(self):
        # given
        self.controller.manipulator = MagicMock()
        self.controller.lift = MagicMock()
        self.controller.positioning = MagicMock()
        self.controller.grasper = MagicMock()
        self.controller.controllers = [self.controller.manipulator,
                                       self.controller.lift,
                                       self.controller.positioning,
                                       self.controller.grasper]

        # when
        self.controller.stop_diagnostics()

        # then
        self.controller.manipulator.stop_diagnostics.assert_called()
        self.controller.lift.stop_diagnostics.assert_called()
        self.controller.positioning.stop_diagnostics.assert_called()
        self.controller.grasper.stop_diagnostics.assert_called()
