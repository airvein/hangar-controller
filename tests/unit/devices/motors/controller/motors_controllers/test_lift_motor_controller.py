from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.motors_controllers.lift_motor_controller import \
    LiftMotorController, MOTOR_LIFT
from settingsd.motors import parameters_settings


class TestLiftMotorController(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.modbus_io = MagicMock()
        cls.error_handler = MagicMock()
        cls.parameters_reader = MagicMock()
        cls.task_queue = MagicMock()
        cls.parameters_reader.read_lift_motor_placement = MagicMock(
            return_value=[[0], [1]]
        )

    def setUp(self) -> None:
        self.controller = LiftMotorController(
            self.modbus_io, self.error_handler,
            self.task_queue
        )

    def test___init__(self):
        self.assertEqual(1, len(self.controller.motors))
        self.assertEqual(MOTOR_LIFT, self.controller.motors[0].motor_name)

    def test_calibrate_lift(self):
        # given
        motor_lift_params_name = parameters_settings.DrivesNames.LIFT
        self.controller._parameters_reader.read_calibration_speed = MagicMock(
            return_value=12.12
        )
        self.task_queue.add_task = MagicMock()

        # when
        self.controller.calibrate_lift()

        # then
        self.controller._parameters_reader.read_calibration_speed. \
            assert_called_with(motor_lift_params_name)
        self.task_queue.add_task.assert_called_with(
            self.controller.calibrate, [[MOTOR_LIFT], [12.12]])
        self.task_queue.wait_until_done.assert_called()

    def test_go_lift_up(self):
        # given
        self.controller._parameters_reader.read_lift_up = MagicMock()
        self.task_queue.add_task = MagicMock()

        # when
        self.controller.go_lift_up()

        # then
        self.controller._parameters_reader.read_lift_up.assert_called()
        self.task_queue.add_task.assert_called()
        self.task_queue.wait_until_done.assert_called()

    def test_go_lift_down(self):
        # given
        self.controller._parameters_reader.read_lift_down = MagicMock()
        self.task_queue.add_task = MagicMock()

        # when
        self.controller.go_lift_down()

        # then
        self.controller._parameters_reader.read_lift_down.assert_called()
        self.task_queue.add_task.assert_called()
        self.task_queue.wait_until_done.assert_called()
