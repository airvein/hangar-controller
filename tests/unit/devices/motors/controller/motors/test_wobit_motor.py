import unittest
from unittest.mock import MagicMock, patch

from hangar_controller.devices.motors.controller.wobit_motors.wobit_motor import WobitMotor
from hangar_controller.devices.motors.modbus_interface.device_data_holders.discrete_input_reader import \
    DiscreteInputReader
from hangar_controller.devices.motors.modbus_interface.device_data_holders.discrete_output_controller \
    import DiscreteOutputController
from hangar_controller.devices.motors.modbus_interface.device_data_holders.holding_register_controller \
    import HoldingRegisterController
from hangar_controller.devices.motors.modbus_interface.device_data_holders.real_register_controller \
    import RealRegisterController
from settingsd.motors import modbus_constants as mc


class TestWobitMotor(unittest.TestCase):
    @patch.object(WobitMotor, 'update_registers')
    def setUp(self, _) -> None:
        self.modbus_io = MagicMock()
        self.error_handler = MagicMock()
        self.motor = WobitMotor(
            self.modbus_io, self.error_handler,
            mc.Wobit.BatteriesMotorsNames.MOTOR_X,
            mc.WobitMotor1Addresses
        )
        self.motor.discrete_outputs = MagicMock()
        self.motor.discrete_outputs.write_by_channel_name = MagicMock(
            return_value='status')
        self.motor.holding_registers = MagicMock()
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')
        self.motor.holding_registers.read_by_channel_name = MagicMock(
            return_value=('status', 'value')
        )
        self.motor.real_registers = MagicMock()
        self.motor.real_registers.write_by_channel_name = MagicMock(
            return_value='status')
        self.motor.real_registers.read_by_channel_name = MagicMock(
            return_value=('status', 'value')
        )

    def test_update_registers(self):
        # given

        self.motor.update_registers(mc.WobitMotor3Addresses)

        # then
        self.assertIsInstance(self.motor.discrete_inputs, DiscreteInputReader)
        self.assertEqual(
            len(self.motor.discrete_inputs.data_list),
            len(mc.WobitMotor3Addresses.DISCRETE_INPUTS_ADDRESSES))
        self.assertIsInstance(self.motor.discrete_outputs,
                              DiscreteOutputController)
        self.assertEqual(
            len(self.motor.discrete_outputs.data_list),
            len(mc.WobitMotor3Addresses.DISCRETE_OUTPUTS_ADDRESSES)
        )
        self.assertIsInstance(self.motor.holding_registers,
                              HoldingRegisterController)
        self.assertEqual(len(self.motor.holding_registers.data_list),
                         len(
                             mc.WobitMotor3Addresses.HOLDING_REGISTERS))
        self.assertIsInstance(self.motor.real_registers, RealRegisterController)
        self.assertEqual(len(self.motor.real_registers.data_list),
                         len(
                             mc.WobitMotor3Addresses.REAL_REGISTERS))

    def test_power_on_motor(self):
        # given

        # when
        status = self.motor.power_on_motor()

        # then
        self.assertEqual('status', status)
        self.motor.discrete_outputs.write_by_channel_name.assert_called_with(
            mc.Wobit.BitRegistersNames.Mx_POWER,
            True
        )

    def test_enable_motor(self):
        # given

        # when
        status = self.motor.power_on_motor()

        # then
        self.assertEqual('status', status)
        self.motor.discrete_outputs.write_by_channel_name.assert_called_with(
            mc.Wobit.BitRegistersNames.Mx_POWER,
            True
        )

    def test_read_position(self):
        # given

        # when
        status, value = self.motor.read_position()

        # then
        self.assertEqual('status', status)
        self.assertEqual('value', value)
        self.motor.real_registers.read_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_POS_ACT,
        )

    def test_read_motor_status(self):
        # given

        # when
        status, value = self.motor.read_motor_status()

        # then
        self.assertEqual('value', value)
        self.assertEqual('status', status)
        self.motor.holding_registers.read_by_channel_name.assert_called_with(
            mc.Wobit.HoldingRegistersNames.M_STATUS
        )

    def test_read_limit_switch_in(self):
        # given
        self.motor.discrete_inputs = MagicMock()
        self.motor.discrete_inputs.read_by_channel_name = MagicMock(
            return_value=('status', 'value')
        )

        # when
        status, value = self.motor.read_limit_switch_in()

        # then
        self.assertEqual('value', value)
        self.assertEqual('status', status)
        self.motor.discrete_inputs.read_by_channel_name.assert_called_with(
            mc.Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_IN
        )

    def test_read_limit_switch_out(self):
        # given
        self.motor.discrete_inputs = MagicMock()
        self.motor.discrete_inputs.read_by_channel_name = MagicMock(
            return_value=('status', 'value')
        )

        # when
        status, value = self.motor.read_limit_switch_out()

        # then
        self.assertEqual('value', value)
        self.assertEqual('status', status)
        self.motor.discrete_inputs.read_by_channel_name.assert_called_with(
            mc.Wobit.BitRegistersNames.Mx_LIMIT_SWITCH_OUT
        )

    def test_write_actual_position(self):
        # given

        # when
        status = self.motor.write_actual_position(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_POS_ACT,
            123
        )

    def test_write_absolute_position(self):
        # given

        # when
        status = self.motor.write_absolute_position(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_POS_ABS,
            123
        )

    def test_write_max_velocity(self):
        # given

        # when
        status = self.motor.write_max_velocity(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_VMAX,
            123
        )

    def test_write_acceleration(self):
        # given

        # when
        status = self.motor.write_acceleration(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_ACC,
            123
        )

    def test_write_deceleration(self):
        # given

        # when
        status = self.motor.write_deceleration(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_DEC,
            123
        )

    def test_move_home(self):
        # given

        # when
        status = self.motor.move_home(123)

        # then
        self.assertEqual('status', status)
        self.motor.real_registers.write_by_channel_name.assert_called_with(
            mc.Wobit.RealRegistersNames.Mx_HOME,
            123
        )


if __name__ == '__main__':
    unittest.main()
