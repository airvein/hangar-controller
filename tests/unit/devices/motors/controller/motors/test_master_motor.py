from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.wobit_motors.master_motor import MasterMotor
from hangar_controller.devices.motors.modbus_interface import modbus_utils
from hangar_controller.devices.motors.modbus_interface.device_data_holders.holding_register_controller \
    import HoldingRegisterController
from settingsd.motors import modbus_constants


class TestMasterMotor(TestCase):
    def setUp(self) -> None:
        self.modbus_io = MagicMock()
        self.motor: MasterMotor = MasterMotor(self.modbus_io)

    def test_init(self):
        self.assertIsInstance(self.motor.holding_registers,
                              HoldingRegisterController)
        self.assertEqual(
            len(self.motor.holding_registers.data_list),
            len(modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS)
        )

    def test_write_axis_control_mode(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.write_axis_control_mode(
            modbus_constants.Wobit.AxisControlMode.DIRECT)

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=
            modbus_constants.Wobit.HoldingRegistersNames.AXIS_CTR_MODE,
            value=modbus_constants.Wobit.AxisControlMode.DIRECT.value
        )

    def test_write_data_type(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.write_data_type(modbus_constants.Wobit.DataTypes.REAL)

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.DATA_TYPE,
            value=modbus_constants.Wobit.DataTypes.REAL.value
        )

    def test_trigger_absolute_position(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.trigger_absolute_position([1, 3, 4])

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=
            modbus_constants.Wobit.HoldingRegistersNames.AXIS_POS_ABS_TRIG,
            value=modbus_utils.id_list_to_control_word([1, 3, 4])
        )

    def test_enable_motors(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.enable_motors([1, 3, 4])

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_ENABLE,
            value=modbus_utils.id_list_to_control_word([1, 3, 4])
        )

    def test_disable_motors(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.disable_motors([1, 3, 4])

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_DISABLE,
            value=modbus_utils.id_list_to_control_word([1, 3, 4])
        )

    def test_stop_motors(self):
        # given
        self.motor.holding_registers.write_by_channel_name = MagicMock(
            return_value='status')

        # when
        self.motor.stop_motors([1, 3, 4])

        # then
        self.motor.holding_registers.write_by_channel_name.assert_called_with(
            channel_name=modbus_constants.Wobit.HoldingRegistersNames.M_STOP,
            value=modbus_utils.id_list_to_control_word([1, 3, 4])
        )
