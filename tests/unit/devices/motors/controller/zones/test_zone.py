from unittest import TestCase

from hangar_controller.devices.motors.controller.zones.zone import Zone
from hangar_controller.devices.motors.controller.zones.zone_entry_point import ZoneEntryPositions


class TestZone(TestCase):
    def setUp(self) -> None:
        self.zone: Zone = Zone('name', [])

    def test_init_with_no_paths(self):
        # given
        exit_path = []

        # when
        zone = Zone('zone', exit_path)

        # then
        self.assertEqual('zone', zone.name)
        self.assertEqual([], zone.exit_path)

    def test_init_with_exit_path(self):
        # given
        exit_path = [ZoneEntryPositions.CARGO_WINDOW_PICK_UP]

        # when
        zone = Zone('name', exit_path)

        # then
        self.assertEqual(
            [ZoneEntryPositions.CARGO_WINDOW_PICK_UP],
            zone.exit_path)
