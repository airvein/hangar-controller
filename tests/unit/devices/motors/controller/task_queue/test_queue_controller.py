from unittest import TestCase
from unittest.mock import MagicMock, patch

from hangar_controller.devices.motors.controller.task_queue.queue_controller import QueueController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue


class TestQueueController(TestCase):
    def setUp(self) -> None:
        self.queue_controller: QueueController = QueueController()

    def test___init__(self):
        self.assertIsInstance(self.queue_controller._queue, TaskQueue)
        self.assertFalse(self.queue_controller._stop_queue)

    def test_stop(self):
        # given
        self.queue_controller._queue = MagicMock()

        # when
        self.queue_controller.stop()

        # then
        self.queue_controller._queue.flush.assert_called()
        self.assertTrue(self.queue_controller._stop_queue)

    def test_run(self):
        # given
        def stop_queue():
            self.queue_controller._stop_queue = True

        self.queue_controller._queue = MagicMock()
        self.queue_controller._queue.process_task = MagicMock(side_effect=stop_queue)

        # when
        self.queue_controller.start()

        # then
        self.queue_controller._queue.process_task.assert_called_once()
