from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from settingsd.motors import parameters_settings
from settingsd.motors.modbus_constants import Wobit


class TestWobitDiagnosticsReader(TestCase):
    def setUp(self) -> None:
        self.motor_controllers = MagicMock()
        self.parameter_reader = MagicMock()
        self.diagnostics = WobitDiagnosticsReader(
            self.motor_controllers, self.parameter_reader
        )

    def test___init__(self):
        self.assertEqual(self.diagnostics.motor_controllers,
                         self.motor_controllers)
        self.assertEqual(self.diagnostics.parameters_reader,
                         self.parameter_reader)

    def test_get_motor_diagnostics_by_name_no_name(self):
        # given
        motor_controller_1 = MagicMock()
        motor_controller_2 = MagicMock()
        motor_name = 'motor_name'
        self.diagnostics.motor_controllers = [motor_controller_1,
                                              motor_controller_2]
        motor_controller_1.diagnostics = {'motor1': 1}
        motor_controller_2.diagnostics = {'motor2': 1}

        # when

        # then
        self.assertRaises(
            ValueError,
            self.diagnostics.get_motor_diagnostics_by_name, motor_name
        )

    def test_get_motor_diagnostics_by_name(self):
        # given
        motor_controller_1 = MagicMock()
        motor_controller_2 = MagicMock()
        motor_controller_2.get_diagnostics_by_motor_name = MagicMock(
            return_value='diagnostics'
        )
        self.diagnostics.motor_controllers = [motor_controller_1,
                                              motor_controller_2]
        motor_name = Wobit.BatteriesMotorsNames.MOTOR_LIFT
        motor_controller_1.diagnostics = {'motor_name1': 1}
        motor_controller_2.diagnostics = {motor_name: 1}

        # when
        result = self.diagnostics.get_motor_diagnostics_by_name(motor_name)

        # then
        self.assertEqual('diagnostics', result)

    def test_compare_lift_position_down(self):
        # given
        position = 5.005
        up = 7
        down = 5

        # when
        result = self.diagnostics.compare_lift_position(position, up, down)

        # then
        self.assertEqual(parameters_settings.RelativePositions.DOWN, result)

    def test_compare_lift_position_up(self):
        # given
        position = 5.005
        up = 5
        down = 7

        # when
        result = self.diagnostics.compare_lift_position(position, up, down)

        # then
        self.assertEqual(parameters_settings.RelativePositions.UP, result)

    def test_compare_lift_position_middle(self):
        # given
        position = 6
        up = 7
        down = 5

        # when
        result = self.diagnostics.compare_lift_position(position, up, down)

        # then
        self.assertEqual(parameters_settings.RelativePositions.MIDDLE, result)

    def test_get_lift_position(self):
        # given
        lift_diagnostics = MagicMock()
        self.diagnostics.get_motor_diagnostics_by_name = MagicMock(
            return_value=lift_diagnostics)
        lift_diagnostics.position = 'pos'
        self.diagnostics.parameters_reader = MagicMock()
        self.diagnostics.parameters_reader.read_lift_up = MagicMock(
            return_value='up')
        self.diagnostics.parameters_reader.read_lift_down = MagicMock(
            return_value='down')
        self.diagnostics.compare_lift_position = MagicMock(
            return_value='position')

        # when
        result = self.diagnostics.get_lift_position()

        # then
        self.diagnostics.get_motor_diagnostics_by_name.assert_called()
        self.diagnostics.parameters_reader.read_lift_up.assert_called()
        self.diagnostics.parameters_reader.read_lift_down.assert_called()
        self.diagnostics.compare_lift_position.assert_called()
        self.assertEqual('position', result)
