from unittest import TestCase

from hangar_controller.devices.motors.controller.observers.motor_diagnostics import \
    MotorDiagnostics


class TestMotorDiagnostics(TestCase):
    def setUp(self) -> None:
        self.diagnostics = MotorDiagnostics()

    def test___init__(self):
        self.assertIsNone(self.diagnostics._limit_switch_in)
        self.assertIsNone(self.diagnostics._limit_switch_out)
        self.assertIsNone(self.diagnostics._position)

    def test_limit_switch_in_property(self):
        self.assertEqual(self.diagnostics.limit_switch_in,
                         self.diagnostics._limit_switch_in)

    def test_limit_switch_out_property(self):
        self.assertEqual(self.diagnostics.limit_switch_out,
                         self.diagnostics._limit_switch_out)

    def test_position_property(self):
        self.assertEqual(self.diagnostics.position,
                         self.diagnostics._position)

    def test_update_limit_switch_in(self):
        # given

        # when
        self.diagnostics.update_limit_switch_in(True)

        # then
        self.assertTrue(self.diagnostics._limit_switch_in)

    def test_update_limit_switch_out(self):
        # given

        # when
        self.diagnostics.update_limit_switch_in(True)

        # then
        self.assertTrue(self.diagnostics._limit_switch_in)

    def test_update_position(self):
        # given

        # when
        self.diagnostics.update_position(12.12)

        # then
        self.assertEqual(12.12, self.diagnostics._position)
