from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.controller.observers.motor_position_observer import \
    MotorPositionObserver
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus, MotorErrors
from settingsd.motors.modbus_constants import Wobit


class TestMotorPositionObserver(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.motor_position_check = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, 123.45)
        )
        cls.motor_status_check = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK,
                          modbus_constants.Wobit.MotorStatus)
        )
        cls.error_handler = MagicMock()

    def setUp(self) -> None:
        self.observer: MotorPositionObserver = \
            MotorPositionObserver(
                self.motor_position_check, self.motor_status_check,
                self.error_handler)

    def test___init__(self):
        self.assertFalse(self.observer._stop_observer)
        self.assertIsNone(self.observer._end_position)

    def test_update_end_position(self):
        # given

        # when
        self.observer._update_end_position(13)

        # then
        self.assertEqual(13, self.observer._end_position)

    def test_stop(self):
        # given

        # when
        self.observer._stop()

        # then
        self.assertTrue(self.observer._stop_observer)

    def test_connection_error(self):
        # given
        self.observer._stop = MagicMock()

        # when
        self.observer._connection_error()

        # then
        self.observer._error_handler.modbus_error.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )
        self.observer._stop.assert_called()

    def test_get_motor_position_connection_exception(self):
        # given
        self.observer._motor_position_check = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )
        self.observer._connection_error = MagicMock()

        # when
        result = self.observer._get_motor_position()

        # then
        self.assertIsNone(result)
        self.observer._connection_error.assert_called()

    def test_get_motor_position(self):
        # given
        self.observer._motor_position_check = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, 123.12)
        )
        self.observer._connection_error = MagicMock()

        # when
        result = self.observer._get_motor_position()

        # then
        self.assertEqual(123.12, result)
        self.observer._connection_error.assert_not_called()

    def test_get_motor_status_connection_exception(self):
        # given
        self.observer._motor_status_check = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )
        self.observer._connection_error = MagicMock()

        # when
        result = self.observer._get_motor_status()

        # then
        self.assertIsNone(result)
        self.observer._connection_error.assert_called()

    def test_get_motor_status(self):
        # given
        self.observer._motor_status_check = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK,
                          Wobit.MotorStatus.AT_POSITION)
        )
        self.observer._connection_error = MagicMock()

        # when
        result = self.observer._get_motor_status()

        # then
        self.assertEqual(Wobit.MotorStatus.AT_POSITION,
                         result)
        self.observer._connection_error.assert_not_called()

    def test_check_end_position_connection_exception(self):
        # given
        self.observer._get_motor_position = MagicMock(
            return_value=None
        )

        # when
        result = self.observer._check_position_reached()

        # then
        self.assertFalse(result)

    def test_check_position_reached_far_from_end(self):
        # given
        self.observer._update_end_position(12)
        self.observer._get_motor_position = MagicMock(
            return_value=self.observer._end_position + 100
        )

        # when
        result = self.observer._check_position_reached()

        # then
        self.assertFalse(result)

    def test_check_position_reached_at_the_end(self):
        # given
        self.observer._get_motor_position = MagicMock(
            return_value=self.observer._end_position
        )

        # when
        result = self.observer._check_position_reached() + 0.0005

        # then
        self.assertTrue(result)

    def test_check_motor_stopped(self):
        # given

        # when
        position_mode = self.observer._check_motor_stopped(
            Wobit.MotorStatus.POSITION_MODE
        )
        velocity_mode = self.observer._check_motor_stopped(
            Wobit.MotorStatus.VELOCITY_MODE
        )
        at_position_mode = self.observer._check_motor_stopped(
            Wobit.MotorStatus.AT_POSITION
        )

        # then
        self.assertFalse(position_mode)
        self.assertFalse(velocity_mode)
        self.assertTrue(at_position_mode)

    def test_check_motor_limit(self):
        # given

        # when
        right_limit = self.observer._check_motor_limit(
            Wobit.MotorStatus.R_LIMIT
        )
        left_limit = self.observer._check_motor_limit(
            Wobit.MotorStatus.L_LIMIT
        )
        at_position = self.observer._check_motor_limit(
            Wobit.MotorStatus.AT_POSITION
        )

        # then
        self.assertTrue(right_limit)
        self.assertTrue(left_limit)
        self.assertFalse(at_position)

    def test_check_status_reached_connection_error(self):
        # given
        self.observer._get_motor_status = MagicMock(
            return_value=None
        )

        # when
        result = self.observer._check_stop_status_reached()

        # then
        self.assertFalse(result)

    def test_stop_status_reached(self):
        # given
        self.observer._get_motor_status = MagicMock(
            return_value=Wobit.MotorStatus.AT_POSITION
        )
        self.observer._check_motor_stopped = MagicMock(
            return_value='1'
        )

        # when
        result = self.observer._check_stop_status_reached()

        self.assertEqual('1', result)

    def test_stop_condition(self):
        # given
        self.observer._check_position_reached = MagicMock(
            return_value=True
        )
        self.observer._check_stop_status_reached = MagicMock(
            return_value=True
        )

        # when
        result = self.observer._check_stop_condition()

        # then
        self.assertTrue(result)

    def test_run_arrived(self):
        # given
        self.observer._check_stop_condition = MagicMock(
            return_value=True
        )

        # when
        arrived = self.observer.observe(13)

        # then
        self.assertTrue(arrived)

    def test_run_stop_signal(self):
        # given
        def stop_observer():
            self.observer._stop()

        self.observer._check_stop_condition = MagicMock(
            return_value=False,
            side_effect=stop_observer
        )

        # when
        arrived = self.observer.observe(13)

        # then thread stops
        self.observer._check_stop_condition.assert_called()
        self.assertFalse(arrived)

    def test_observation_ended_with_modbus_error_position(self):
        # given
        self.observer._motor_position_check = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None))

        # when
        arrived = self.observer.observe(13)

        # then
        self.assertFalse(arrived)
        self.observer._error_handler.modbus_error.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )

    def test_observation_ended_with_modbus_error_status(self):
        # given
        self.observer._motor_status_check = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None))

        # when
        arrived = self.observer.observe(13)

        # then
        self.assertFalse(arrived)
        self.observer._error_handler.modbus_error.assert_called_with(
            ModbusResponseStatus.CONNECTION_ERROR
        )

    def test_observation_ended_with_motor_error_limit_reached(self):
        # given
        self.observer._motor_status_check = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK,
                          Wobit.MotorStatus.R_LIMIT))

        # when
        arrived = self.observer.observe(13)

        # then
        self.assertFalse(arrived)
        self.observer._error_handler.motor_error.assert_called_with(
            MotorErrors.MOTOR_LIMIT_REACHED
        )
