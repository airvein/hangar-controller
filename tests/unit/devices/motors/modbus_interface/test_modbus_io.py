from unittest import TestCase
from unittest.mock import MagicMock, patch

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.exceptions import ConnectionException

from hangar_controller.devices.motors.modbus_interface.modbus_io import connection_check, ModbusIO
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus


class TestModbusConnectionCheck(TestCase):
    def setUp(self) -> None:
        # modbus_read is some tests function, that connection is tested on
        self.wrapped_function = MagicMock()
        self.wrapped_function.return_value = 4

        # modbus_comm is a wrapper around self.modbus_read
        # we have to call self.modbus_comm() to call wrapper and function
        self.modbus_connection_check = connection_check(self.wrapped_function)

    def raise_modbus_exception(self):
        raise ConnectionException

    def test_call(self):
        # given

        # when
        self.modbus_connection_check()

        # then
        self.wrapped_function.assert_called()

    def test_call_args(self):
        # given

        # when
        status, value = self.modbus_connection_check(1, 2, 3)

        # then
        self.wrapped_function.assert_called_with(1, 2, 3)
        self.assertEqual(
            status, ModbusResponseStatus.STATUS_OK
        )
        self.assertEqual(value, 4)

    def test_call_error(self):
        # given
        wrapped_function_error = MagicMock()
        wrapped_function_error.side_effect = ConnectionException
        modbus_comm_error = connection_check(wrapped_function_error)

        # when
        status, value = modbus_comm_error()

        # then
        wrapped_function_error.assert_called()
        self.assertEqual(status, ModbusResponseStatus.CONNECTION_ERROR)
        self.assertIsNone(value)


class TestModbusIO(TestCase):
    def setUp(self) -> None:
        self.modbus_io = ModbusIO(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START
        )

    def test_init(self):
        self.assertIsNone(self.modbus_io._client)
        self.assertFalse(self.modbus_io._connected)
        self.assertEqual(devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
                         self.modbus_io.connection)
        self.assertEqual(modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
                         self.modbus_io._testing_register)

    def test_client(self):
        self.assertEqual(self.modbus_io._client, self.modbus_io.client)

    def test_connected(self):
        self.assertEqual(self.modbus_io._connected, self.modbus_io.connected)

    def test_connect(self):
        # given
        self.modbus_io.check_connection = MagicMock(
            return_value=
            ModbusResponseStatus.STATUS_OK)

        # when
        status = self.modbus_io.connect_and_test()

        # then
        self.assertIsInstance(self.modbus_io.client, ModbusClient)
        self.modbus_io.check_connection.assert_called()
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)

    def test_disconnect(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io._client.close = MagicMock()

        # when
        self.modbus_io.disconnect()

        # then
        self.modbus_io._client.close.assert_called()

    def test_check_connection(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io._client.connect = MagicMock(return_value=True)
        self.modbus_io.test_reading_value = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, 1))

        # when
        connection = self.modbus_io.check_connection()

        # then
        self.modbus_io.client.connect.assert_called()
        self.modbus_io.test_reading_value.assert_called()
        self.assertEqual(connection, ModbusResponseStatus.STATUS_OK)

    def test_check_connection_error(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io._client.connect = MagicMock(return_value=False)
        self.modbus_io.read_discrete_inputs = MagicMock()

        # when
        connection_status = self.modbus_io.check_connection()

        # then
        self.modbus_io.client.connect.assert_called()
        self.assertEqual(connection_status,
                         ModbusResponseStatus.PARAMETERS_ERROR)

    @patch('logging.debug')
    def test_read_discrete_input_single(self, log_mock):
        # given
        response = MagicMock()
        response.bits = [True] * 6
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_discrete_inputs = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_discrete_inputs(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START)

        # then
        self.modbus_io.client.read_discrete_inputs.assert_called_with(
            address=modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=1,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertTrue(value)

    @patch('logging.debug')
    def test_read_discrete_multiple(self, log_mock):
        # given
        response = MagicMock()
        response.bits = [True] * 6
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_discrete_inputs = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_discrete_inputs(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=6
        )

        # then
        self.modbus_io.client.read_discrete_inputs.assert_called_with(
            address=modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=6,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual([True] * 6, value)

    @patch('logging.debug')
    def test_read_coils_single(self, log_mock):
        # given
        response = MagicMock()
        response.bits = [True] * 6
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_coils = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_coils(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START)

        # then
        self.modbus_io.client.read_coils.assert_called_with(
            address=modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=1,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertTrue(value)

    @patch('logging.debug')
    def test_read_coils_multiple(self, log_mock):
        # given
        response = MagicMock()
        response.bits = [True] * 6
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_coils = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_coils(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=6
        )

        # then
        self.modbus_io.client.read_coils.assert_called_with(
            address=modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            count=6,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(value, [True] * 6)

    def test_write_coil(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.write_coil = MagicMock()

        # when
        status, _ = self.modbus_io.write_coil(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START, True)

        # then
        self.modbus_io.client.write_coil.assert_called_with(
            modbus_constants.Wobit.BitRegistersNames.OUTPUTS_START,
            True,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)

    @patch('logging.debug')
    def test_read_input_register(self, log_mock):
        # given
        response = MagicMock()
        response.registers = [3]
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_input_registers = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_input_registers(
            modbus_constants.Wobit.RealRegistersNames.Mx_VMAX
        )

        # then
        self.modbus_io.client.read_input_registers.assert_called_with(
            address=modbus_constants.Wobit.RealRegistersNames.Mx_VMAX,
            count=1,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(value, [3])

    def test_read_holding_register(self):
        # given
        response = MagicMock()
        response.registers = [3]
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.read_holding_registers = \
            MagicMock(return_value=response)

        # when
        status, value = self.modbus_io.read_holding_registers(
            1
        )

        # then
        self.modbus_io.client.read_holding_registers.assert_called_with(
            address=1,
            count=1,
            unit=self.modbus_io.connection.device_address
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(value, [3])

    def test_write_holding_register(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.write_registers = MagicMock()

        # when
        status, _ = self.modbus_io.write_holding_register(
            1,
            2
        )

        # then
        self.modbus_io.client.write_register.assert_called_with(
            address=1,
            unit=self.modbus_io.connection.device_address,
            value=2
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)

    def test_write_real_register(self):
        # given
        self.modbus_io._client = MagicMock()
        self.modbus_io.client.write_registers = MagicMock()

        # when
        status, _ = self.modbus_io.write_real_register(
            1,
            2.4
        )

        # then
        self.modbus_io.client.write_registers.assert_called_with(
            address=1,
            unit=self.modbus_io.connection.device_address,
            values=[39322, 16409]
        )
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
