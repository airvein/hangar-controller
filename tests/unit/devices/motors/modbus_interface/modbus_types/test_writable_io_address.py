from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.writable_io_address import \
    WritableIOAddress
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus


class WritableIOAddressImplementation(WritableIOAddress):
    def __init__(self, name, address):
        super().__init__(name, address)

    def write_value(self, modbus_io: ModbusIO, value) \
            -> ModbusResponseStatus:
        pass

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        pass


class TestWritableIOAddress(TestCase):
    def setUp(self) -> None:
        self.writable_io_address: WritableIOAddressImplementation = \
            WritableIOAddressImplementation(
                *
                modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[
                    0]
            )

    def test___init__(self):
        name, address = \
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0]
        self.assertEqual(name, self.writable_io_address.name)
        self.assertEqual(address, self.writable_io_address.address)

    def test_write_error(self):
        # given
        self.writable_io_address.write_value = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR
        )
        modbus_io = MagicMock()

        # when
        status = self.writable_io_address.write(modbus_io, 123)

        # then
        self.writable_io_address.write_value.assert_called_with(modbus_io, 123)
        self.assertEqual(ModbusResponseStatus.CONNECTION_ERROR, status)
        self.assertNotEqual(123, self.writable_io_address.value)

    def test_write(self):
        # given
        self.writable_io_address.write_value = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK
        )
        modbus_io = MagicMock()

        # when
        status = self.writable_io_address.write(modbus_io, 123)

        # then
        self.writable_io_address.write_value.assert_called_with(modbus_io, 123)
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(123, self.writable_io_address.value)
