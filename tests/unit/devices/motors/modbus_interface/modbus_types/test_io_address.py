from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from settingsd.motors.modbus_constants import ModbusResponseStatus


class IOAddressImplementation(AbstractIOAddress):  # pragma: no cover
    def __init__(self, name: str, address: int):
        super().__init__(name, address)

    def read_values(self, modbus_io: ModbusIO, count: int = 1):
        return modbus_io.read_discrete_inputs(self.address, count=count)


class TestIOAddress(TestCase):
    def setUp(self) -> None:
        self.io_address = IOAddressImplementation('roof_opening', 0x0002)
        self.modbus_io = MagicMock()

    def test_init(self):
        self.assertEqual(self.io_address._name, 'roof_opening')
        self.assertEqual(self.io_address._address, 0x0002)

    def test_name(self):
        self.assertEqual(self.io_address._name, self.io_address.name)

    def test_address(self):
        self.assertEqual(self.io_address._address, self.io_address.address)

    def test_update_connection_error(self):
        # given
        self.io_address.read_values = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )

        # when
        status = self.io_address.update(self.modbus_io)

        # then
        self.assertEqual(ModbusResponseStatus.CONNECTION_ERROR, status)

    def test_update(self):
        # given
        self.io_address.read_values = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [1])
        )

        # when
        status = self.io_address.update(self.modbus_io)

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(1, self.io_address.value)

    def test_read_updated_value(self):
        # given
        self.modbus_io.read_discrete_inputs = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [3])
        )

        # when
        status, value = self.io_address.read_updated_value(self.modbus_io)

        # then
        self.assertEqual(3, value)
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
