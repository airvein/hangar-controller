from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_output import \
    DiscreteOutput
from settingsd.motors.modbus_constants import ModbusResponseStatus


class TestDiscreteOutput(TestCase):
    def setUp(self) -> None:
        self.discrete_output = DiscreteOutput('roof_opening', 0x0002)

    def test_init(self):
        self.assertEqual(self.discrete_output._name, 'roof_opening')
        self.assertEqual(self.discrete_output._address, 0x0002)
        self.assertIsNone(self.discrete_output.value)

    def test_update(self):
        # given
        modbus_io = MagicMock()
        modbus_io.read_coils = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [True])
        )

        # when
        status = self.discrete_output.update(modbus_io)

        # then
        modbus_io.read_coils.assert_called_with(
            self.discrete_output._address, 1)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.discrete_output._value, True)

    def test_write(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_coil = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, None)
        )

        # when
        status = self.discrete_output.write(modbus_io, True)

        # then
        modbus_io.write_coil.assert_called_with(
            self.discrete_output._address, True)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.discrete_output._value, True)

    def test_write_error(self):
        # given
        modbus_io = MagicMock()
        modbus_io.write_coil = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None)
        )

        # when
        status = self.discrete_output.write(modbus_io, True)

        # then
        modbus_io.write_coil.assert_called_with(
            self.discrete_output._address, True)
        self.assertEqual(status, ModbusResponseStatus.CONNECTION_ERROR)
        self.assertIsNone(self.discrete_output._value)
