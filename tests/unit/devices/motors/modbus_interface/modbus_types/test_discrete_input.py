from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_input import \
    DiscreteInput
from settingsd.motors.modbus_constants import ModbusResponseStatus


class TestDiscreteInput(TestCase):
    def setUp(self) -> None:
        self.discrete_input = DiscreteInput('roof_ls', 0x0002)

    def test_init(self):
        self.assertEqual(self.discrete_input._name, 'roof_ls')
        self.assertEqual(self.discrete_input._address, 0x0002)
        self.assertIsNone(self.discrete_input._value)

    def test_update(self):
        # given
        modbus_io = MagicMock()
        modbus_io.read_discrete_inputs = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [True])
        )

        # when
        status = self.discrete_input.update(modbus_io)

        # then
        modbus_io.read_discrete_inputs.assert_called_with(
            self.discrete_input.address, 1)
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(self.discrete_input._value, True)
