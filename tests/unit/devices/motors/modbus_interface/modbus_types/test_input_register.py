from unittest import TestCase

from hangar_controller.devices.motors.modbus_interface.modbus_types.input_register import \
    InputRegister


class TestInputRegister(TestCase):
    def setUp(self) -> None:
        self.input_register = InputRegister('temperature', 0)

    def test_init(self):
        self.assertEqual(self.input_register._name, 'temperature')
        self.assertEqual(self.input_register._address, 0)
        self.assertIsNone(self.input_register._value)
