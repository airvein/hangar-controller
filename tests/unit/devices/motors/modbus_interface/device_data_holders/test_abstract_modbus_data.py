from typing import Type
from unittest import TestCase

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_data import \
    AbstractModbusData
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd.motors import modbus_constants


class AbstractModbusDataImplementation(AbstractModbusData):
    def __init__(self, address_list: list,
                 address_type: Type[AbstractIOAddress]):
        super().__init__(address_list, address_type)


class TestAbstractModbusData(TestCase):
    def setUp(self) -> None:
        self.modbus_data = AbstractModbusDataImplementation(
            modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS,
            HoldingRegister)

    def test_init(self):
        self.assertIsInstance(self.modbus_data.data_list, list)

    def test_init_addresses(self):
        # given
        addresses = modbus_constants.WobitMotorAddresses.\
            COMMON_HOLDING_REGISTERS
        address_type = HoldingRegister

        # when
        data_list = self.modbus_data.init_addresses(addresses, address_type)

        # then
        self.assertEqual(
            len(data_list),
            len(modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS)
        )
        for data_el in data_list:
            self.assertIsInstance(data_el, HoldingRegister)

    def test_group_addresses_list_empty(self):
        # given
        self.modbus_data.data_list = []

        # when
        groups = self.modbus_data.group_addresses()

        # then
        self.assertEqual([[]], groups)

    def test_group_addresses_list_one_el(self):
        # given
        io_address = 'io_address'
        self.modbus_data.data_list = [io_address]

        # when
        groups = self.modbus_data.group_addresses()

        # then
        self.assertEqual([[0]], groups)

    def test_group_addresses_list_2_separate(self):
        # given
        io_1 = ('input1', 0x0001)
        io_2 = ('input2', 0x0004)
        self.modbus_data.data_list = self.modbus_data.init_addresses(
            [io_1, io_2], HoldingRegister
        )

        # when
        groups = self.modbus_data.group_addresses()

        # then
        self.assertEqual([[0], [1]], groups)

    def test_group_addresses_list_2_consecutive(self):
        # given
        io_1 = ('input1', 0x0001)
        io_2 = ('input2', 0x0002)
        self.modbus_data.data_list = self.modbus_data.init_addresses(
            [io_1, io_2], HoldingRegister
        )

        # when
        groups = self.modbus_data.group_addresses()

        # then
        self.assertEqual([[0, 1]], groups)

    def test_group_addresses_list_3_sep_3_cons(self):
        # given
        io_1 = ('input1', 0x0001)
        io_2 = ('input2', 0x0003)
        io_3 = ('input3', 0x0006)
        io_4 = ('input4', 0x0007)
        io_5 = ('input5', 0x0008)
        io_6 = ('input6', 0x0012)
        self.modbus_data.data_list = self.modbus_data.init_addresses(
            [io_1, io_2, io_3, io_4, io_5, io_6], HoldingRegister
        )

        # when
        groups = self.modbus_data.group_addresses()

        # then
        self.assertEqual([[0], [1], [2, 3, 4], [5]], groups)

    def test_get_io_address_by_name(self):
        # given
        name = modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS[0][
            0]

        # when
        io_address = self.modbus_data.get_io_address_by_name(name)

        # then
        self.assertEqual(self.modbus_data.data_list[0], io_address)

    def test_get_io_address_by_name_wrong_name(self):
        # given
        name = modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS[0][
                   0] + 'test'

        # when
        self.assertRaises(ValueError, self.modbus_data.get_io_address_by_name,
                          name)

    def test_get_io_address_by_address(self):
        # given
        address = \
            modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS[0][1]

        # when
        io_address = self.modbus_data.get_io_address_by_address(address)

        # then
        self.assertEqual(self.modbus_data.data_list[0], io_address)

    def test_get_io_address_by_address_wrong_address(self):
        # given
        address = modbus_constants.WobitMotorAddresses.\
            COMMON_HOLDING_REGISTERS[0][1] + 99999

        # when
        self.assertRaises(ValueError,
                          self.modbus_data.get_io_address_by_address,
                          address)

    def test_get_type(self):
        self.assertEqual(self.modbus_data.get_type(), 'HoldingRegister')
