from typing import Type
from unittest import TestCase
from unittest.mock import MagicMock

from hangar_controller.devices.motors.modbus_interface.device_data_holders.abstract_modbus_reader import \
    AbstractModbusReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.abstract_io_address import \
    AbstractIOAddress
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_input import \
    DiscreteInput
from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd.motors import modbus_constants
from settingsd.motors.modbus_constants import ModbusResponseStatus


class AbstractModbusReaderImplementation(
        AbstractModbusReader):  # pragma: no cover

    def __init__(self, address_list: list,
                 address_type: Type[AbstractIOAddress],
                 modbus_io: ModbusIO):
        super().__init__(address_list, address_type, modbus_io)


class TestAbstractModbusReader(TestCase):
    def setUp(self) -> None:
        self.modbus_io = MagicMock()
        self.modbus_reader = AbstractModbusReaderImplementation(
            modbus_constants.WobitMotorAddresses.COMMON_HOLDING_REGISTERS,
            HoldingRegister,
            self.modbus_io)

    def test_init(self):
        self.assertIsInstance(self.modbus_reader.data_list, list)

    def test_update_values_connection_error(self):
        # give
        self.modbus_io.read_holding_registers = MagicMock(
            return_value=(ModbusResponseStatus.CONNECTION_ERROR, None))

        # when
        status = self.modbus_reader.update_values()

        # then
        self.modbus_io.read_holding_registers.assert_called_once()
        self.assertEqual(status,
                         ModbusResponseStatus.CONNECTION_ERROR)

    def test_update_values_1_address(self):
        # given
        io_1 = ('input1', 0x0001)
        self.modbus_reader.data_list = self.modbus_reader.init_addresses(
            [io_1], DiscreteInput
        )
        self.modbus_reader.address_groups = \
            self.modbus_reader.group_addresses()
        self.modbus_io.read_discrete_inputs = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [1])
        )

        # when
        status = self.modbus_reader.update_values()

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(1, self.modbus_reader.data_list[0].value)

    def test_update_values_separate_address(self):
        # given
        io_1 = ('input1', 0x0001)
        io_2 = ('input2', 0x0005)
        self.modbus_reader.data_list = self.modbus_reader.init_addresses(
            [io_1, io_2], DiscreteInput
        )
        self.modbus_reader.address_groups = \
            self.modbus_reader.group_addresses()
        self.modbus_io.read_discrete_inputs = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [1])
        )

        # when
        status = self.modbus_reader.update_values()

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(1, self.modbus_reader.data_list[0].value)
        self.assertEqual(1, self.modbus_reader.data_list[1].value)

    def test_update_value_grouped_address(self):
        # given
        io_1 = ('input1', 0x0003)
        io_2 = ('input2', 0x0004)
        io_3 = ('input3', 0x0005)
        io_4 = ('input4', 0x0010)
        io_5 = ('input5', 0x000A)
        io_6 = ('input6', 0x000B)
        self.modbus_reader.data_list = self.modbus_reader.init_addresses(
            [io_1, io_2, io_3, io_4, io_5, io_6], DiscreteInput
        )
        self.modbus_reader.address_groups = \
            self.modbus_reader.group_addresses()
        self.modbus_io.read_discrete_inputs = MagicMock(
            return_value=(ModbusResponseStatus.STATUS_OK, [1, 2, 3, 4, 5, 6])
        )

        # when
        status = self.modbus_reader.update_values()

        # then
        self.assertEqual(ModbusResponseStatus.STATUS_OK, status)
        self.assertEqual(1, self.modbus_reader.data_list[0].value)
        self.assertEqual(2, self.modbus_reader.data_list[1].value)
        self.assertEqual(3, self.modbus_reader.data_list[2].value)
        self.assertEqual(1, self.modbus_reader.data_list[3].value)
        self.assertEqual(1, self.modbus_reader.data_list[4].value)
        self.assertEqual(2, self.modbus_reader.data_list[5].value)

    def test_read_value(self):
        # given
        self.modbus_reader.update_values = MagicMock(
            return_value=ModbusResponseStatus.STATUS_OK
        )

        values = {}
        for io_address in self.modbus_reader.data_list:
            values[io_address.name] = io_address.value

        # when
        status, data = self.modbus_reader.read_values()

        # then
        self.modbus_reader.update_values.assert_called()
        self.assertEqual(status, ModbusResponseStatus.STATUS_OK)
        self.assertEqual(data, values)

    def test_read_value_connection_error(self):
        # given
        self.modbus_reader.update_values = MagicMock(
            return_value=ModbusResponseStatus.CONNECTION_ERROR
        )

        # when
        status, data = self.modbus_reader.read_values()

        # then
        self.modbus_reader.update_values.assert_called()
        self.assertEqual(status, ModbusResponseStatus.CONNECTION_ERROR)
        self.assertIsNone(data)
