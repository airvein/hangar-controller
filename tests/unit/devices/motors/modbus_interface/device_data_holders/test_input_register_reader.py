from unittest import TestCase

from hangar_controller.devices.motors.modbus_interface.device_data_holders.input_registers_reader import \
    InputRegistersReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.input_register import \
    InputRegister
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants


class TestInputRegisterReader(TestCase):
    def setUp(self) -> None:
        self.modbus_io = ModbusIO(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0][
                1]
        )
        self.modbus_data = InputRegistersReader(
            self.modbus_io,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES)

    def test_init(self):
        self.assertIsInstance(self.modbus_data.data_list, list)
        self.assertIsInstance(self.modbus_data.data_list[0], InputRegister)
        self.assertIsInstance(self.modbus_data.modbus_io, ModbusIO)
