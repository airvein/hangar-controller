from unittest import TestCase

from hangar_controller.devices.motors.modbus_interface.device_data_holders.holding_register_controller \
    import HoldingRegisterController
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.holding_register import \
    HoldingRegister
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants


class TestHoldingRegisterController(TestCase):
    def setUp(self) -> None:
        self.modbus_io = ModbusIO(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0][
                1]
        )
        self.modbus_data = HoldingRegisterController(
            self.modbus_io,
            modbus_constants.WobitMotor1Addresses.COMMON_HOLDING_REGISTERS)

    def test_init(self):
        self.assertIsInstance(self.modbus_data.data_list, list)
        self.assertIsInstance(self.modbus_data.data_list[0], HoldingRegister)
        self.assertIsInstance(self.modbus_data.modbus_io, ModbusIO)
