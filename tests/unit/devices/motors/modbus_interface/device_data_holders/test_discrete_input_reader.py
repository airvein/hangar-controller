from unittest import TestCase

from hangar_controller.devices.motors.modbus_interface.device_data_holders.discrete_input_reader import \
    DiscreteInputReader
from hangar_controller.devices.motors.modbus_interface.modbus_io import ModbusIO
from hangar_controller.devices.motors.modbus_interface.modbus_types.discrete_input import \
    DiscreteInput
from settingsd import devices_modbus_connection
from settingsd.motors import modbus_constants


class TestDiscreteInputReader(TestCase):
    def setUp(self) -> None:
        self.modbus_io = ModbusIO(
            devices_modbus_connection.WOBIT_BATTERIES_CONNECTION,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES[0][
                1]
        )
        self.modbus_data = DiscreteInputReader(
            self.modbus_io,
            modbus_constants.WobitMotor1Addresses.DISCRETE_INPUTS_ADDRESSES)

    def test_init(self):
        self.assertIsInstance(self.modbus_data.data_list, list)
        self.assertIsInstance(self.modbus_data.data_list[0], DiscreteInput)
        self.assertIsInstance(self.modbus_data.modbus_io, ModbusIO)
