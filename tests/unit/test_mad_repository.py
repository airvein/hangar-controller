import logging
import typing as tp
import unittest

from hangar_controller.mad_repository import MADRepository, register

logger = logging.getLogger(__name__)

@register('rs485')
class RS485:
    def add(self, a, b):
        return a+b

class TestMADRepository(unittest.TestCase):
    def test_rs485_inits(self):
        self.assertEqual(MADRepository().rs485.add(2, 3), 5)
