import json
from typing import Callable
from unittest import TestCase, mock
from unittest.mock import MagicMock

from iothub_client import IoTHubClient, IoTHubMessage
from satella.coding.concurrent import CallableGroup

from hangar_controller.azure_communication.azure_iothub_client import AzureIoTHubClient
from hangar_controller.azure_communication.messages.base_iot_hub_message import BaseIoTHubMessage
from hangar_controller.azure_communication.response_status import ResponseStatus


class TestAzureIoTHubClient(TestCase):
    def setUp(self) -> None:
        self.iothub_client_patcher = mock.patch('iothub_client.IoTHubClient.__new__')
        self.iothub_client_mock = self.iothub_client_patcher.start()
        self.iothub_client_mock.return_value = MagicMock(spec=IoTHubClient)

        self.message_validators_patcher = mock.patch(
            'satella.coding.concurrent.CallableGroup.__new__')
        self.message_validators_mock = self.message_validators_patcher.start()
        self.message_validators_mock.return_value = MagicMock(spec=CallableGroup)

        self.message_handler_patcher = mock.patch('satella.coding.concurrent.CallableGroup.__new__')
        self.message_handler_mock = self.message_handler_patcher.start()
        self.message_handler_mock.return_value = MagicMock(spec=CallableGroup)

        self.azure_iothub_client = AzureIoTHubClient()

    def tearDown(self) -> None:
        self.iothub_client_patcher.stop()
        self.message_validators_patcher.stop()
        self.message_handler_patcher.stop()

    @mock.patch('iothub_client.IoTHubMessage.__new__')
    def test_send_iothub_message_without_properties(self, iothub_path):
        # given
        message = MagicMock(spec=BaseIoTHubMessage)

        messages_data = ({"payload": {}},
                         {"payload": {'test': 'x'}, "properties": {"test_key": "test_value"}})
        for message_data in messages_data:
            with self.subTest(message_data=message_data):
                message.get_data.return_value = message_data

                # when
                self.azure_iothub_client.send_iothub_message(message)

                # then
                self.azure_iothub_client.client.send_event_async.assert_called_with(
                    IoTHubMessage(json.dumps(message_data)),
                    self.azure_iothub_client._send_message_callback,
                    None
                )

    def test_add_validator(self):
        # given
        mock_validator = MagicMock(spec=Callable)

        # when
        self.azure_iothub_client.add_validator(mock_validator)

        # then
        self.azure_iothub_client.message_validators.add.assert_called_with(mock_validator)

    def test_add_handler(self):
        # given
        mock_handler = MagicMock(spec=Callable)

        # when
        self.azure_iothub_client.add_handler(mock_handler)

        # then
        self.azure_iothub_client.message_handlers.add.assert_called_with(mock_handler)

    def test_direct_method_callback_when_incorrect_json(self):
        result = self.azure_iothub_client._direct_method_callback('init_flight', '{}{}',
                                                                  'test_context')
        self.assertEqual(ResponseStatus.INVALID_COMMAND.value, result.status)
