from unittest import TestCase

from hangar_controller.azure_communication.messages.cargo_action_message import CargoAction, \
    CargoActionType, CargoActionObjectType


class TestCargoAction(TestCase):
    def setUp(self) -> None:
        self.test_cargo_id = 'test-cargo_id'
        self.test_cargo_weight = 1000

        self.cargo_action = CargoAction(CargoActionType.LOADING_DRONE, 'cr-drone-1',
                                        CargoActionObjectType.DRONE, True,
                                        self.test_cargo_id, self.test_cargo_weight)

    def test_prepared_payload_should_have_correct_structure(self):
        payload = self.cargo_action._prepare_payload()

        self.assertIn('cargo_action', payload)

        cargo_action_payload = payload['cargo_action']
        self.assertIn('action', cargo_action_payload)
        self.assertIn('action_object', cargo_action_payload)
        self.assertIn('action_object_type', cargo_action_payload)
        self.assertIn('cargo_id', cargo_action_payload)
        self.assertIn('cargo_weight', cargo_action_payload)
        self.assertIn('action_result', cargo_action_payload)

    def test_prepared_properties_should_have_correct_structure(self):
        properties = self.cargo_action._prepare_properties()

        self.assertIn('message_type', properties)
        self.assertIn('cargo_id', properties)

        self.assertEqual('cargo_action', properties['message_type'])
        self.assertEqual(self.test_cargo_id, properties['cargo_id'])
