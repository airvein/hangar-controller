import logging
import time
import typing as tp

from unittest import mock
from unittest.mock import Mock, MagicMock

import requests

import settings
from hangar_controller.devices.drivers.diagnostics.drivers_diagnostics import \
    DriversDiagnostics
from hangar_controller.devices.drivers.drivers_controller import \
    DriversController
from hangar_controller.devices.drivers.messages.responses.collective_response_evaluation import \
    CollectiveResponseEvaluation
from settingsd.drivers.stm_communication_settings import ResponseMessage

logger = logging.getLogger(__name__)


class DriversControllerMock:
    def __new__(cls, *args, **kwargs):
        origin_attributes_dict = cls.__dict__.copy()

        mocked_class_attributes = DriversController.__dict__

        mocked_return_values = cls._specific_return_value_for_some_attributes()
        excluded_attributes = cls._excluded_attributes()

        for name in mocked_class_attributes:
            if name.startswith("__"):
                continue

            if name in excluded_attributes:
                continue

            try:
                if name in mocked_return_values:
                    mocked_attribute = mocked_return_values[name]
                else:
                    mocked_attribute = mock.create_autospec(
                        mocked_class_attributes[name]
                    )

                setattr(cls, name, cls._log_it(mocked_attribute, name))
            except (TypeError, AttributeError):
                pass
        for name in origin_attributes_dict:
            try:
                setattr(cls, name, origin_attributes_dict[name])
            except (TypeError, AttributeError):
                pass

        return super(DriversControllerMock, cls).__new__(cls, *args, **kwargs)

    @classmethod
    def _log_it(cls, attribute, name):
        def logged_callable_attribute(*args, **kwargs):
            logger.debug(f"Calling driver mock method: {name}")
            time.sleep(1)
            return attribute(*args, **kwargs)

        if not callable(attribute):
            return attribute

        return logged_callable_attribute

    @classmethod
    def _specific_return_value_for_some_attributes(cls):
        CollectiveResponseEvaluationMock = MagicMock(
            spec=CollectiveResponseEvaluation
        )
        CollectiveResponseEvaluationMock.evaluation = True

        mocked_return_values: tp.Dict[str, Mock] = {}

        for attribute in DriversController.__dict__:
            mocked_return_values[attribute] = Mock(
                spec=DriversController.__dict__[attribute],
                return_value=CollectiveResponseEvaluationMock
            )

        non_generic_return_values = {
            'master_charger_get_chargers_number': Mock(
                return_value=(CollectiveResponseEvaluationMock, 3)
            ),
            'user_panel_get_pin': Mock(
                return_value=(
                    CollectiveResponseEvaluationMock, '1,2,3,4,5,6')
            ),
            'cargo_get_weight': Mock(
                return_value=(CollectiveResponseEvaluationMock, 12.34)
            ),
        }
        mocked_return_values.update(non_generic_return_values)

        mocked_return_values.update(
            {
                'diagnostics': Mock(return_value=MagicMock(DriversDiagnostics)),
                'stop': Mock(return_value=None),
                'run': Mock(return_value=None),
                'start_heartbeat': Mock(return_value=None),
                'stop_heartbeat': Mock(return_value=None),
            }
        )

        return mocked_return_values

    @classmethod
    def _excluded_attributes(cls):
        excluded_attributes = []

        return excluded_attributes

    @staticmethod
    def start() -> None:
        return None

    @staticmethod
    def user_panel_set_confirmation_view(view):
        logger.debug(f'Showing confirmation view: {view}')

        response = Mock(spec=CollectiveResponseEvaluation)
        response.evaluation = True

        return response

    @staticmethod
    def user_panel_get_pin():
        logger.debug('Showing get PIN view')

        url = settings.PENTACOMP_API_CARGO_USER_INFO
        payload = {'deviceid': settings.HANGAR_ID}

        r = requests.get(url, payload)

        result = r.json()

        pin = result.get('pin', None)

        response = Mock(spec=CollectiveResponseEvaluation)

        if pin is not None:
            response.evaluation = True
        else:
            response.evaluation = False
            response.evaluation_reason = ResponseMessage.TIMEOUT
            time.sleep(10)

        return response, pin

    @staticmethod
    def user_panel_set_view(view):
        logger.debug(f'Showing view: {view}')

        response = Mock(spec=CollectiveResponseEvaluation)

        return response

    @staticmethod
    def cargo_get_weight():
        response = Mock(spec=CollectiveResponseEvaluation)
        weight = 100.01

        return response, weight
