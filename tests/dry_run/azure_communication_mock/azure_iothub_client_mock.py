import json
import logging
import threading
import time
import typing as tp

from satella.coding.concurrent import CallableGroup

from hangar_controller.azure_communication.messages import BaseIoTHubMessage, \
    CommandResponse
from hangar_controller.azure_communication.response_status import ResponseStatus
from hangar_controller.mqtt_client import MQTTClient

logger = logging.getLogger(__name__)


class AzureIoTHubClientMock(MQTTClient):
    def __init__(self):
        super().__init__('azure_mock')
        self.client.subscribe('azure_operator', 2)

        self.message_validators = CallableGroup()
        self.message_handlers = CallableGroup()

        self.__class__.__module__ = 'hangar_controller.azure_communication.azure_iothub_client'
        self.__class__.__name__ = 'AzureIoTHubClient'

        logger.debug('Crated AzureIoTHubClientMock instead AzureIoTHubClient')

    def send_iothub_message(self, message: BaseIoTHubMessage):
        message_data = message.get_data()

        properties = {}
        for k, v in message_data.get("properties", {}).items():
            if v is None:
                continue
            properties[k] = str(v)

        message_data["properties"] = properties
        message = json.dumps(message_data)

        self._publish_message(message, 'azure_hangar')
        logging.debug('Correctly send IoTHub Message')

    def register_azure_method_name_validator(
            self, azure_method_name_validator: tp.Callable):
        """Just stub"""
        pass

    def add_validator(self, validator: tp.Callable[[dict], bool]):
        self.message_validators.add(validator)

    def add_handler(self, handler: tp.Callable[[dict], bool]):
        self.message_handlers.add(handler)

    def _on_message_callback(self, client, userdata, message):
        status = ResponseStatus.INVALID_COMMAND

        command = self._get_command(message)
        if not bool(command):  # check that command is empty, bool({}) == False
            result = self._prepare_command_response(command, status)
            self._send_iothub_response(result)
            return

        status = self._handle_command(command)
        result = self._prepare_command_response(command, status)
        threading.Thread(
            target=self._send_iothub_response, args=(result,)
        ).start()

    def _send_iothub_response(self, message: dict):
        time.sleep(0.01)

        result = json.dumps(message)

        self._publish_message(result, 'azure_hangar_response')

    @staticmethod
    def _get_command(message) -> dict:
        raw_message = "Empty Messages"
        try:
            raw_message = message.payload.decode("utf-8")
            command: dict = json.loads(raw_message)
        except UnicodeDecodeError:
            logger.error('Cannot decode message')
        except (TypeError, json.decoder.JSONDecodeError):
            logger.error('Received command payload is incorrect JSON')
            logger.debug(f'Command payload: {raw_message}')
            return {}

        return command

    def _handle_command(self, command: dict) -> ResponseStatus:
        status = ResponseStatus.INVALID_COMMAND

        if all(self.message_validators(command)):
            if not any(self.message_handlers(command)):
                logger.warning(
                    f'Cannot perform received command. Command: {command}')
                status = ResponseStatus.REFUSED
            else:
                logger.debug(f'Received correct command. Command: {command}')
                status = ResponseStatus.ACKNOWLEDGED
        else:
            logger.error('Received command has invalid command')
            logger.debug(f'Command: {command}')

        return status

    @staticmethod
    def _prepare_command_response(command: dict,
                                  status: ResponseStatus) -> dict:
        result = {'status': None, 'response': None}
        command_response = CommandResponse(command, status)

        result['status'] = status.value
        result['response'] = json.dumps(command_response.get_data()["payload"])
        return result
