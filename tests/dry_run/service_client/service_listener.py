import json
import time

from hangar_controller.mqtt_client import MQTTClient


class ServiceListener(MQTTClient):
    def __init__(self):
        super().__init__('hangar_service_listener')
        self.client.subscribe('service_hangar', 2)

    def _on_message_callback(self, client, userdata, message):
        raw_message = message.payload.decode("utf-8")
        message = json.loads(raw_message)
        print(f'{message}\n')

    def run(self):
        work = True
        while work:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                self.close_connection()
                break


if __name__ == '__main__':
    service_listener = ServiceListener()
    service_listener.run()
