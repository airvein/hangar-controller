command_parameters_templates = {
    "init_flight": {"flight_id": ["1", str], "route_id": ["123", str],
                    "cargo": [True, bool]},
    "cancel_flight": None,
    "drone_started": None,
    "leave_drone": None,
    "receive_drone": {"flight_id": ["1", str], "drone_id": ["cr-drone-1", str]},
    "drone_landed": {"cargo_id": ["cr-cargo-1", str],
                     "cargo_weight": [100, int]},
    "alive": None,
    "service_finish_flight": {"flight_id": ["1", str],
                              "drone_id": ["cr-drone-1", str]},
    "unload_drone": {"cargo_id": ["cr-cargo-1", str],
                     "cargo_weight": [100, int]}
}

command_names_aliases = {
    "if": "init_flight",
    "cf": "cancel_flight",
    "ds": "drone_started",
    "ld": "leave_drone",
    "rd": "receive_drone",
    "dl": "drone_landed",
    "al": "alive",
    "sff": "service_finish_flight",
    "ud": "unload_drone",
}
