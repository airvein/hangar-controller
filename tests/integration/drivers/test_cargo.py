from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_cargo_mock import STMCargoMock


class TestSTMCargo(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMCargoMock

    def test_base_shift_normal_call(self):
        self.normal_call(self.drivers.cargo_base_shift)

    def test_base_shift_timeout(self):
        self.timeout(self.drivers.cargo_base_shift)

    def test_base_shift_refused(self):
        self.refused(self.drivers.cargo_base_shift)

    def test_base_shift_wrong_value(self):
        self.wrong_value(self.drivers.cargo_base_shift)

    def test_base_shift_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_base_shift)

    def test_close_window_normal_call(self):
        self.normal_call(self.drivers.cargo_close_window)

    def test_close_window_timeout(self):
        self.timeout(self.drivers.cargo_close_window)

    def test_close_window_refused(self):
        self.refused(self.drivers.cargo_close_window)

    def test_close_window_wrong_value(self):
        self.wrong_value(self.drivers.cargo_close_window)

    def test_close_window_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_close_window)

    def test_cargo_get_plc_io_normal_call(self):
        self.normal_call(self.drivers.cargo_get_plc_io)

    def test_cargo_get_plc_io_timeout(self):
        self.timeout(self.drivers.cargo_get_plc_io)

    def test_cargo_get_plc_io_refused(self):
        self.refused(self.drivers.cargo_get_plc_io)

    def test_cargo_get_plc_io_wrong_value(self):
        self.wrong_value(self.drivers.cargo_get_plc_io)

    def test_cargo_get_plc_io_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_get_plc_io)

    def test_cargo_get_states_normal_call(self):
        self.normal_call(self.drivers.cargo_get_states)

    def test_cargo_get_states_timeout(self):
        self.timeout(self.drivers.cargo_get_states)

    def test_cargo_get_states_refused(self):
        self.refused(self.drivers.cargo_get_states)

    def test_cargo_get_states_wrong_value(self):
        self.wrong_value(self.drivers.cargo_get_states)

    def test_cargo_get_states_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_get_states)

    def test_open_window_normal_call(self):
        self.normal_call(self.drivers.cargo_open_window)

    def test_open_window_timeout(self):
        self.timeout(self.drivers.cargo_open_window)

    def test_open_window_refused(self):
        self.refused(self.drivers.cargo_open_window)

    def test_open_window_wrong_value(self):
        self.wrong_value(self.drivers.cargo_open_window)

    def test_open_window_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_open_window)

    def test_work_shift_normal_call(self):
        self.normal_call(self.drivers.cargo_work_shift)

    def test_work_shift_timeout(self):
        self.timeout(self.drivers.cargo_work_shift)

    def test_work_shift_refused(self):
        self.refused(self.drivers.cargo_work_shift)

    def test_work_shift_wrong_value(self):
        self.wrong_value(self.drivers.cargo_work_shift)

    def test_work_shift_unexpected_value(self):
        self.unexpected_response(self.drivers.cargo_work_shift)
