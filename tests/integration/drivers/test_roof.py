from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_roof_mock import STMRoofMock


class TestRoof(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMRoofMock

    def test_open_roof_normal_call(self):
        self.normal_call(self.drivers.roof_open)

    def test_open_roof_timeout(self):
        self.timeout(self.drivers.roof_open)

    def test_open_roof_refused(self):
        self.refused(self.drivers.roof_open)

    def test_open_roof_wrong_value(self):
        self.wrong_value(self.drivers.roof_open)

    def test_open_roof_unexpected_value(self):
        self.unexpected_response(self.drivers.roof_open)

    def test_close_roof_normal_call(self):
        self.normal_call(self.drivers.roof_close)

    def test_close_roof_timeout(self):
        self.timeout(self.drivers.roof_close)

    def test_close_roof_refused(self):
        self.refused(self.drivers.roof_close)

    def test_close_roof_wrong_value(self):
        self.wrong_value(self.drivers.roof_close)

    def test_close_roof_unexpected_response(self):
        self.unexpected_response(self.drivers.roof_close)

    def test_stop_roof_normal_call(self):
        self.normal_call(self.drivers.roof_stop)

    def test_stop_roof_timeout(self):
        self.timeout(self.drivers.roof_stop)

    def test_stop_roof_refused(self):
        self.refused(self.drivers.roof_stop)

    def test_stop_roof_wrong_value(self):
        self.wrong_value(self.drivers.roof_stop)

    def test_stop_roof_unexpected_response(self):
        self.unexpected_response(self.drivers.roof_stop)

    def test_get_states_normal_call(self):
        self.normal_call(self.drivers.roof_get_states)

    def test_get_states_timeout(self):
        self.timeout(self.drivers.roof_get_states)

    def test_get_states_refused(self):
        self.refused(self.drivers.roof_get_states)

    def test_get_states_wrong_value(self):
        self.wrong_value(self.drivers.roof_get_states)

    def test_get_states_unexpected_value(self):
        self.unexpected_response(self.drivers.roof_get_states)

    def test_get_plc_io_normal_call(self):
        self.normal_call(self.drivers.roof_get_plc_io)

    def test_get_plc_io_timeout(self):
        self.timeout(self.drivers.roof_get_plc_io)

    def test_get_plc_io_refused(self):
        self.refused(self.drivers.roof_get_plc_io)

    def test_get_plc_io_wrong_value(self):
        self.wrong_value(self.drivers.roof_get_plc_io)

    def test_get_plc_io_unexpected_value(self):
        self.unexpected_response(self.drivers.roof_get_plc_io)
