from collections import Callable
from threading import Thread
from typing import Any, List, Optional

from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_roof_mock import STMRoofMock


class CommandSender(Thread):
    def __init__(self, callable: Callable, args: Optional[List[Any]] = None):
        self.callable = callable
        self.args = args
        super().__init__()

    def run(self):
        if self.args:
            ret = self.callable(*self.args)
        else:
            ret = self.callable()

        print(ret)


class TestRoof(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMRoofMock

    def test_open_roof_normal_call(self):
        open_sender = CommandSender(self.drivers.roof_open)
        close_sender = CommandSender(self.drivers.roof_close)
        get_states = CommandSender(self.drivers.roof_get_states)
        get_plc_io = CommandSender(self.drivers.roof_get_plc_io)
        stop_sender = CommandSender(self.drivers.roof_stop)

        self.normal_call(open_sender.run)
        self.normal_call(close_sender.run)
        self.normal_call(stop_sender.run)
        self.normal_call(get_states.run)
        self.normal_call(get_plc_io.run)
