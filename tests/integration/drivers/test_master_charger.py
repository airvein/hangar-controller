from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_master_charger_mock import STMMasterChargerMock


class TestSTMMasterCharger(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMMasterChargerMock

    def test_master_charger_get_plc_io_normal_call(self):
        self.normal_call(self.drivers.master_charger_get_plc_io)

    def test_master_charger_get_plc_io_timeout(self):
        self.timeout(self.drivers.master_charger_get_plc_io)

    def test_master_charger_get_plc_io_refused(self):
        self.refused(self.drivers.master_charger_get_plc_io)

    def test_master_charger_get_plc_io_wrong_value(self):
        self.wrong_value(self.drivers.master_charger_get_plc_io)

    def test_master_charger_get_plc_io_unexpected_value(self):
        self.unexpected_response(
            self.drivers.master_charger_get_plc_io)

    def test_master_charger_get_states_normal_call(self):
        self.normal_call(self.drivers.master_charger_get_states, ['1'])

    def test_master_charger_get_states_timeout(self):
        self.timeout(self.drivers.master_charger_get_states, ['1'])

    def test_master_charger_get_states_refused(self):
        self.refused(self.drivers.master_charger_get_states, ['1'])

    def test_master_charger_get_states_wrong_value(self):
        self.wrong_value(self.drivers.master_charger_get_states, ['1'])

    def test_master_charger_get_states_unexpected_value(self):
        self.unexpected_response(
            self.drivers.master_charger_get_states, ['1'])

    def test_master_charger_get_telemetry_normal_call(self):
        self.normal_call(self.drivers.master_charger_get_telemetry, ['1'])

    def test_master_charger_get_telemetry_timeout(self):
        self.timeout(self.drivers.master_charger_get_telemetry, ['1'])

    def test_master_charger_get_telemetry_refused(self):
        self.refused(self.drivers.master_charger_get_telemetry, ['1'])

    def test_master_charger_get_telemetry_wrong_value(self):
        self.wrong_value(self.drivers.master_charger_get_telemetry, ['1'])

    def test_master_charger_get_telemetry_unexpected_value(self):
        self.unexpected_response(
            self.drivers.master_charger_get_telemetry, ['1'])

    def test_set_power_normal_call(self):
        self.normal_call(
            self.drivers.master_charger_set_charger_power,
            ['1', True])

    def test_set_power_timeout(self):
        self.timeout(self.drivers.master_charger_set_charger_power,
                     ['1', True])

    def test_set_power_refused(self):
        self.refused(self.drivers.master_charger_set_charger_power,
                     ['1', True])

    def test_set_power_wrong_value(self):
        self.wrong_value(
            self.drivers.master_charger_set_charger_power,
            ['1', True])

    def test_set_power_unexpected_value(self):
        self.unexpected_response(
            self.drivers.master_charger_set_charger_power,
            ['1', True])
