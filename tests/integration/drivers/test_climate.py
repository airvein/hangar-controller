from hangar_controller.devices.drivers.messages.commands.climate.set_ac import SetAC
from hangar_controller.devices.drivers.messages.commands.climate.set_fan import SetFan
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater import \
    SetHangarHeater
from tests.integration.drivers.test_commands import TestCommands
from tests.mocks.stm_climate_mock import STMClimateMock


class TestSTMClimate(TestCommands):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.stm_mock = STMClimateMock

    def test_climate_get_parameters_normal_call(self):
        self.normal_call(self.drivers.climate_get_parameters)

    def test_climate_get_parameters_timeout(self):
        self.timeout(self.drivers.climate_get_parameters)

    def test_climate_get_parameters_refused(self):
        self.refused(self.drivers.climate_get_parameters)

    def test_climate_get_parameters_wrong_value(self):
        self.wrong_value(self.drivers.climate_get_parameters)

    def test_climate_get_parameters_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_get_parameters)

    def test_climate_get_states_normal_call(self):
        self.normal_call(self.drivers.climate_get_states)

    def test_climate_get_states_timeout(self):
        self.timeout(self.drivers.climate_get_states)

    def test_climate_get_states_refused(self):
        self.refused(self.drivers.climate_get_states)

    def test_climate_get_states_wrong_value(self):
        self.wrong_value(self.drivers.climate_get_states)

    def test_climate_get_states_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_get_states)

    def test_climate_get_plc_io_normal_call(self):
        self.normal_call(self.drivers.climate_get_plc_io)

    def test_climate_get_plc_io_timeout(self):
        self.timeout(self.drivers.climate_get_plc_io)

    def test_climate_get_plc_io_refused(self):
        self.refused(self.drivers.climate_get_plc_io)

    def test_climate_get_plc_io_wrong_value(self):
        self.wrong_value(self.drivers.climate_get_plc_io)

    def test_climate_get_plc_io_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_get_plc_io)

    def test_climate_get_telemetry_normal_call(self):
        self.normal_call(self.drivers.climate_get_telemetry)

    def test_climate_get_telemetry_timeout(self):
        self.timeout(self.drivers.climate_get_telemetry)

    def test_climate_get_telemetry_refused(self):
        self.refused(self.drivers.climate_get_telemetry)

    def test_climate_get_telemetry_wrong_value(self):
        self.wrong_value(self.drivers.climate_get_telemetry)

    def test_climate_get_telemetry_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_get_telemetry)

    def test_set_ac_normal_call(self):
        self.normal_call(self.drivers.climate_set_ac, [SetAC.ENABLE])

    def test_set_ac_timeout(self):
        self.timeout(self.drivers.climate_set_ac, [SetAC.ENABLE])

    def test_set_ac_refused(self):
        self.refused(self.drivers.climate_set_ac, [SetAC.ENABLE])

    def test_set_ac_wrong_value(self):
        self.wrong_value(self.drivers.climate_set_ac, [SetAC.ENABLE])

    def test_set_ac_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_set_ac,
                                 [SetAC.ENABLE])

    def test_set_fan_normal_call(self):
        self.normal_call(self.drivers.climate_set_fan,
                         [SetFan.ENABLE])

    def test_set_fan_timeout(self):
        self.timeout(self.drivers.climate_set_fan, [SetFan.ENABLE])

    def test_set_fan_refused(self):
        self.refused(self.drivers.climate_set_fan, [SetFan.ENABLE])

    def test_set_fan_wrong_value(self):
        self.wrong_value(self.drivers.climate_set_fan,
                         [SetFan.ENABLE])

    def test_set_fan_unexpected_value(self):
        self.unexpected_response(self.drivers.climate_set_fan,
                                 [SetFan.ENABLE])

    def test_set_hangar_heater_normal_call(self):
        self.normal_call(self.drivers.climate_set_hangar_heater,
                         [SetHangarHeater.ENABLE])

    def test_set_hangar_heater_timeout(self):
        self.timeout(self.drivers.climate_set_hangar_heater,
                     [SetHangarHeater.ENABLE])

    def test_set_hangar_heater_refused(self):
        self.refused(self.drivers.climate_set_hangar_heater,
                     [SetHangarHeater.ENABLE])

    def test_set_hangar_heater_wrong_value(self):
        self.wrong_value(self.drivers.climate_set_hangar_heater,
                         [SetHangarHeater.ENABLE])

    def test_set_hangar_heater_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_hangar_heater,
            [SetHangarHeater.ENABLE])

    def test_set_hangar_heater_hysteresis_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_hangar_heater_hysteresis,
            [15])

    def test_set_hangar_heater_hysteresis_timeout(self):
        self.timeout(
            self.drivers.climate_set_hangar_heater_hysteresis, [15])

    def test_set_hangar_heater_hysteresis_refused(self):
        self.refused(
            self.drivers.climate_set_hangar_heater_hysteresis, [15])

    def test_set_hangar_heater_hysteresis_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_hangar_heater_hysteresis,
            [15])

    def test_set_hangar_heater_hysteresis_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_hangar_heater_hysteresis, [15])

    def test_set_hangar_heater_temperature_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_hangar_heater_temperature,
            [15])

    def test_set_hangar_heater_temperature_timeout(self):
        self.timeout(
            self.drivers.climate_set_hangar_heater_temperature,
            [15])

    def test_set_hangar_heater_temperature_refused(self):
        self.refused(
            self.drivers.climate_set_hangar_heater_temperature,
            [15])

    def test_set_hangar_heater_temperature_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_hangar_heater_temperature,
            [15])

    def test_set_hangar_heater_temperature_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_hangar_heater_temperature, [15])

    def test_set_hangar_humidity_hysteresis_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_hangar_humidity_hysteresis,
            [15])

    def test_set_hangar_humidity_hysteresis_timeout(self):
        self.timeout(
            self.drivers.climate_set_hangar_humidity_hysteresis,
            [15])

    def test_set_hangar_humidity_hysteresis_refused(self):
        self.refused(
            self.drivers.climate_set_hangar_humidity_hysteresis,
            [15])

    def test_set_hangar_humidity_hysteresis_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_hangar_humidity_hysteresis,
            [15])

    def test_set_hangar_humidity_hysteresis_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_hangar_humidity_hysteresis,
            [15])

    def test_set_hangar_humidity_set_point_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_hangar_humidity_set_point,
            [15])

    def test_set_hangar_humidity_set_point_timeout(self):
        self.timeout(
            self.drivers.climate_set_hangar_humidity_set_point,
            [15])

    def test_set_hangar_humidity_set_point_refused(self):
        self.refused(
            self.drivers.climate_set_hangar_humidity_set_point,
            [15])

    def test_set_hangar_humidity_set_point_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_hangar_humidity_set_point,
            [15])

    def test_set_hangar_humidity_set_point_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_hangar_humidity_set_point, [15])

    def test_set_roof_heater_normal_call(self):
        self.normal_call(self.drivers.climate_set_roof_heater,
                         [True])

    def test_set_roof_heater_timeout(self):
        self.timeout(self.drivers.climate_set_roof_heater, [True])

    def test_set_roof_heater_refused(self):
        self.refused(self.drivers.climate_set_roof_heater, [True])

    def test_set_roof_heater_wrong_value(self):
        self.wrong_value(self.drivers.climate_set_roof_heater,
                         [True])

    def test_set_roof_heater_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_roof_heater,
            [True])

    def test_set_roof_heater_hysteresis_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_roof_heater_hysteresis,
            [15])

    def test_set_roof_heater_hysteresis_timeout(self):
        self.timeout(self.drivers.climate_set_roof_heater_hysteresis,
                     [15])

    def test_set_roof_heater_hysteresis_refused(self):
        self.refused(self.drivers.climate_set_roof_heater_hysteresis,
                     [15])

    def test_set_roof_heater_hysteresis_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_roof_heater_hysteresis,
            [15])

    def test_set_roof_heater_hysteresis_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_roof_heater_hysteresis, [15])

    def test_set_roof_heater_temperature_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_roof_heater_temperature,
            [15])

    def test_set_roof_heater_temperature_timeout(self):
        self.timeout(
            self.drivers.climate_set_roof_heater_temperature, [15])

    def test_set_roof_heater_temperature_refused(self):
        self.refused(
            self.drivers.climate_set_roof_heater_temperature, [15])

    def test_set_roof_heater_temperature_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_roof_heater_temperature,
            [15])

    def test_set_roof_heater_temperature_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_roof_heater_temperature, [15])

    def test_set_type_climate_controller_normal_call(self):
        self.normal_call(
            self.drivers.climate_set_type_climate_controller,
            [True])

    def test_set_type_climate_controller_timeout(self):
        self.timeout(
            self.drivers.climate_set_type_climate_controller,
            [True])

    def test_set_type_climate_controller_refused(self):
        self.refused(
            self.drivers.climate_set_type_climate_controller,
            [True])

    def test_set_type_climate_controller_wrong_value(self):
        self.wrong_value(
            self.drivers.climate_set_type_climate_controller, [True])

    def test_set_type_climate_controller_unexpected_value(self):
        self.unexpected_response(
            self.drivers.climate_set_type_climate_controller, [True])
