from hangar_controller.devices.drivers.messages.commands.climate.get_parameters import \
    ClimateGetParameters
from hangar_controller.devices.drivers.messages.commands.climate.get_plc_io import ClimateGetPLCIO
from hangar_controller.devices.drivers.messages.commands.climate.get_states import ClimateGetStates
from hangar_controller.devices.drivers.messages.commands.climate.get_telemetry import ClimateGetTelemetry
from hangar_controller.devices.drivers.messages.commands.climate.set_ac import SetAC
from hangar_controller.devices.drivers.messages.commands.climate.set_fan import SetFan
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater import SetHangarHeater
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater_hysteresis import SetHangarHeaterHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_heater_temperature import \
    SetHangarHeaterTemperature
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_hysteresis import \
    SetHangarHumidityHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_hangar_humidity_setpoint import SetHangarHumiditySetPoint
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater import SetRoofHeater
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater_hysteresis import SetRoofHeaterHysteresis
from hangar_controller.devices.drivers.messages.commands.climate.set_roof_heater_temperature import SetRoofHeaterTemperature
from hangar_controller.devices.drivers.messages.commands.climate.set_type_climate_controller import SetTypeClimateController
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMClimateMock(DriversMock):
    def __init__(self):
        super().__init__('STMClimateMock')
        self.commands = [
            ClimateGetParameters(), ClimateGetPLCIO(), ClimateGetStates(),
            ClimateGetTelemetry(), SetAC(SetAC.ENABLE), SetFan(SetFan.ENABLE),
            SetHangarHeater(SetHangarHeater.ENABLE),
            SetHangarHeaterHysteresis(15), SetHangarHeaterTemperature(15),
            SetHangarHumidityHysteresis(15), SetHangarHumiditySetPoint(15),
            SetRoofHeater(SetRoofHeater.ENABLE), SetRoofHeaterHysteresis(15),
            SetRoofHeaterTemperature(15),
            SetTypeClimateController(SetTypeClimateController.AUTO),
        ]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_CLIMATE + '/#')
