import queue
from unittest.mock import patch

from hangar_controller.devices.motors.motors import Motors
from tests.mocks.single_board.wobits_mock_single_board import WobitsMockSingleBoard


class MotorsMockWobitTest(Motors):
    """
    Mock for motors for testing only Wobits without STMs.
    Warning: Extreme memory consumption. After starting, observe memory left
    on device.
    """

    @patch('motors.controller.motors_controllers.motors_controllers_manager.WobitsControllers')
    def __init__(self, error_queue: queue.Queue,
                 error_warning_log_queue: queue.Queue,
                 mock):
        super().__init__(error_queue, error_warning_log_queue)
        self.wobits = None

    def init_wobits(self, positioning, batteries):
        self.wobits = WobitsMockSingleBoard(self._error_handler,
                                            self._task_queue,
                                            positioning,
                                            batteries)
