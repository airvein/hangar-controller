import queue
from unittest.mock import patch

from hangar_controller.devices.drivers.hangar_drivers_mqtt_client import HangarDriversMQTTClient
from hangar_controller.devices.motors.motors import Motors


class MotorsMockSTMTest(Motors):
    """
    Mock for motors for testing only stms without wobits
    """
    @patch('motors.controller.motors_controllers.motors_controllers_manager.WobitsControllers')
    def __init__(self, mqtt_client: HangarDriversMQTTClient,
                 error_queue: queue.Queue,
                 error_warning_log_queue: queue.Queue,
                 mock):
        super().__init__(error_queue, error_warning_log_queue)
        self.wobits = mock
