from unittest.mock import patch

from hangar_controller.devices.motors.controller.diagnostics.wobit_diagnostics_reader import \
    WobitDiagnosticsReader
from hangar_controller.devices.motors.controller.motors_controllers.grasper_motor_controller import \
    GrasperMotorController
from hangar_controller.devices.motors.controller.motors_controllers.lift_motor_controller import \
    LiftMotorController
from hangar_controller.devices.motors.controller.motors_controllers.manipulator_motor_controller import \
    ManipulatorMotorController
from hangar_controller.devices.motors.controller.motors_controllers.motors_controllers_manager import \
    WobitsControllers
from hangar_controller.devices.motors.controller.motors_controllers.positioning_motors_controller import \
    PositioningMotorController
from hangar_controller.devices.motors.controller.task_queue.task_queue import TaskQueue
from hangar_controller.devices.motors.errors.error_handler import ErrorHandler


class WobitsMockSingleBoard(WobitsControllers):
    """
    Mock for Wobits that start only one wobit controller.
    Warning: Extreme memory consumption. After starting, observe memory left
    on device.
    """

    @patch(
        'hangar_controller.devices.motors.controller.motors_controllers.lift_motor_controller.LiftMotorController')
    @patch(
        'hangar_controller.devices.motors.controller.motors_controllers.grasper_motor_controller.GrasperMotorController')
    @patch(
        'hangar_controller.devices.motors.controller.motors_controllers.positioning_motors_controller.PositioningMotorController')
    @patch(
        'hangar_controller.devices.motors.controller.motors_controllers.manipulator_motor_controller.ManipulatorMotorController')
    def __init__(self, error_handler: ErrorHandler, task_queue: TaskQueue,
                 positioning, batteries,
                 man_mock, pos_mock, grasp_mock, lift_mock):
        super().__init__(error_handler, task_queue)

        self.controllers = []
        if positioning:
            self.lift = LiftMotorController(
                self._modbus_io_manager.positioning, self._error_handler,
                self._task_queue
            )
            self.positioning = PositioningMotorController(
                self._modbus_io_manager.positioning, self._error_handler,
                self._task_queue
            )
            self.controllers.append(self.lift)
            self.controllers.append(self.positioning)

        if batteries:
            self.manipulator = ManipulatorMotorController(
                self._modbus_io_manager.batteries, self._error_handler,
                self._task_queue
            )
            self.grasper = GrasperMotorController(
                self._modbus_io_manager.batteries, self._error_handler,
                self._task_queue
            )
            self.controllers.append(self.manipulator)
            self.controllers.append(self.grasper)

        self.diagnostics_reader = WobitDiagnosticsReader(
            self.controllers, self._parameters_reader
        )
