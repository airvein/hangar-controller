import queue
import time
from threading import Thread
from typing import List, Callable

import paho.mqtt.client as mqtt

from hangar_controller.devices.drivers.messages.message_utils.message_utils import \
    response_topic
from hangar_controller.devices.drivers.messages.responses.expected_messages.list_response import \
    ListResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.plc_io_response import \
    PLCIOResponse
from hangar_controller.devices.drivers.messages.responses.expected_messages.value_response import \
    ValueResponse
from hangar_controller.devices.drivers.messages.responses.response import Response
from settingsd.drivers.stm_communication_settings import \
    ResponseMessage, \
    Common
from hangar_controller.mqtt_client import MQTTClient


class DriversMockMQTTClient(MQTTClient):
    def __init__(self, client_id: str = 'drivers_mock'):
        super().__init__(client_id)
        self.messages_queue = queue.Queue()

    def send_message(self, topic: str, message: str):
        self._publish_message(message, topic)

    def _on_message_callback(self, client, userdata, message: mqtt.MQTTMessage):
        self.messages_queue.put(message)

    def subscribe(self, topic):
        self.client.subscribe(topic)

    def disconnect(self):
        self.close_connection()

    def replace_on_message(self, on_message: Callable):
        self.client.on_message = on_message


class ResponseSender(Thread):
    def __init__(self, publish_method, args, send_time=0):
        super().__init__()
        self.send_time = send_time
        self.response = publish_method
        self.args = args
        self.start()

    def run(self) -> None:
        print('---> Driver mock waiting to send response')
        time.sleep(self.send_time)
        print('---> Driver mock publishing response', self.args)
        time.sleep(0.01)
        self.response(*self.args)


class DriversMock(Thread):
    def __init__(self, client_name: str):
        super().__init__()
        self.client = DriversMockMQTTClient(client_name)
        self.client.replace_on_message(self._on_message)
        self.sending_positive_responses = True
        self.sending_wrong_value_response = False
        self.sending_refused_response = False
        self.sending_unexpected_responses = False
        self.sending_timeout_response = False
        self.commands = []

    def subscribe(self):
        raise NotImplementedError

    @staticmethod
    def get_positive_and_expected(response: Response):
        positive_m = response.expected_message.positive_messages
        expected_m = response.expected_message.expected_messages

        if not expected_m:
            if isinstance(response.expected_message, ValueResponse):
                positive, expected = 15, 15
            elif isinstance(response.expected_message, PLCIOResponse):
                positive, expected = [' | '.join(['1'] * 8)] * 2
            elif isinstance(response.expected_message, ListResponse):
                positive, expected = ['12, 14, 15', ] * 2
            else:
                positive, expected = ['some_message', ] * 2

        elif not positive_m:
            positive = expected_m[0]
            expected = expected_m[-1]
        else:
            positive = positive_m
            expected = expected_m[-1]

        return positive, expected

    def _on_message(self, client: mqtt.Client, userdata,
                    message: mqtt.MQTTMessage):

        print('++ STM mock message got', message.topic, message.payload)
        self.check_heartbeat(message)

        for command in self.commands:
            if message.topic == command.topic \
                    and message.payload.decode('UTF-8') == command.payload:
                self.send_response(message, command.responses)

    def heartbeat_response(self, message: mqtt.MQTTMessage):
        topic = response_topic(message.topic)
        ResponseSender(self.client.send_message, [topic, 'tack'])

    def send_ack_response(self, message: mqtt.MQTTMessage):
        response = message.topic.rsplit('/', 1)[0] + f'/{Common.RESP}'
        self.client.send_message(response, ResponseMessage.ACK)

    def send_wrong_value_response(self, message: mqtt.MQTTMessage):
        response = message.topic.rsplit('/', 1)[0] + f'/{Common.RESP}'
        self.client.send_message(response, ResponseMessage.WRONG_VALUE)

    def send_refused_response(self, message: mqtt.MQTTMessage):
        response = message.topic.rsplit('/', 1)[0] + f'/{Common.RESP}'
        self.client.send_message(response, ResponseMessage.REFUSED)

    def send_unexpected_response(self, response: Response):
        ResponseSender(self.client.send_message,
                       [response.topic, 'unexpected_response'])

    def send_timeout_response(self, response: Response):
        positive, expected = self.get_positive_and_expected(response)
        ResponseSender(self.client.send_message,
                       [response.topic, positive], response.timeout + 0.01)

    def _send_response(self, response: Response, positive_resp: bool):
        positive, expected = self.get_positive_and_expected(response)

        message = positive if positive_resp else expected

        ResponseSender(self.client.send_message, [response.topic, message])

    def send_response(self, message: mqtt.MQTTMessage,
                      responses: List[Response]):
        if self.sending_wrong_value_response:
            self.send_wrong_value_response(message)
            return

        if self.sending_refused_response:
            self.send_refused_response(message)
            return

        if self.sending_timeout_response:
            self.send_timeout_response(responses[0])
            return

        for response in responses:
            if self.sending_unexpected_responses:
                self.send_unexpected_response(response)
            else:
                self._send_response(response,
                                    self.sending_positive_responses)
            time.sleep(0.02)

    def check_heartbeat(self, message: mqtt.MQTTMessage):
        if 'heartbeat' in message.topic and 'heartbeat/' not in message.topic:
            self.heartbeat_response(message)

    def connect(self, host):
        self.client.connect(host, port=1883, keepalive=60, bind_address="")

    def stop_drivers(self):
        self.client.disconnect()

    def run(self) -> None:
        self.client_loop.start()
