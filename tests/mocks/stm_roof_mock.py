from hangar_controller.devices.drivers.messages.commands.roof.roof_close import RoofClose
from hangar_controller.devices.drivers.messages.commands.roof.roof_get_plc_io import RoofGetPLCIO
from hangar_controller.devices.drivers.messages.commands.roof.roof_get_states import RoofGetStates
from hangar_controller.devices.drivers.messages.commands.roof.roof_open import RoofOpen
from hangar_controller.devices.drivers.messages.commands.roof.roof_stop import RoofStop
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMRoofMock(DriversMock):
    def __init__(self):
        super().__init__('STMRoofMock')
        self.commands = [RoofOpen(), RoofClose(), RoofStop(), RoofGetStates(),
                         RoofGetPLCIO()]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_ROOF + '/#')
