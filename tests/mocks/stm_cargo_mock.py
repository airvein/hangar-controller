from hangar_controller.devices.drivers.messages.commands.cargo.cargo_base_shift import CargoBaseShift
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_close_window import CargoCloseWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_plc_io import CargoGetPLCIO
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_get_states import CargoGetStates
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_open_window import CargoOpenWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_stop_window import StopWindow
from hangar_controller.devices.drivers.messages.commands.cargo.cargo_work_shift import CargoWorkShift
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMCargoMock(DriversMock):
    def __init__(self):
        super().__init__(Controllers.STM_CARGO)
        self.commands = [CargoBaseShift(), CargoCloseWindow(), CargoGetPLCIO(),
                         CargoGetStates(), CargoOpenWindow(), StopWindow(),
                         CargoWorkShift()]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_CARGO + '/#')
