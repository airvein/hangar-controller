from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_plc_io import \
    PMSGetPLCIO
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_get_states import \
    PMSGetStates
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_chargers_power import \
    PMSSetChargersPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_irlock_power import PMSSetIRLockPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_lift_power import \
    PMSSetMotorsLiftPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_man_power import \
    PMSSetMotorsManPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_motors_pos_power import \
    PMSSetMotorsPosPower
from hangar_controller.devices.drivers.messages.commands.power_management_system.pms_set_pressure import PMSSetPressure
from settingsd.drivers.stm_communication_settings import Controllers
from tests.mocks.drives_mock import DriversMock


class STMPMSMock(DriversMock):
    def __init__(self):
        super().__init__('STMPMSMock')
        self.commands = [PMSGetPLCIO(), PMSGetStates(),
                         PMSSetChargersPower(PMSSetChargersPower.ENABLE),
                         PMSSetIRLockPower(PMSSetIRLockPower.ENABLE),
                         PMSSetMotorsLiftPower(PMSSetMotorsLiftPower.ENABLE),
                         PMSSetMotorsManPower(PMSSetMotorsManPower.ENABLE),
                         PMSSetMotorsPosPower(PMSSetMotorsPosPower.ENABLE),
                         PMSSetPressure(PMSSetPressure.ENABLE)]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_PMS + '/#')
