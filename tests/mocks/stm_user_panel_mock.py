from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_pin import \
    UserPanelGetPin
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_get_states import UserPanelGetStates
from hangar_controller.devices.drivers.messages.commands.user_panel.user_panel_set_view import UserPanelSetView
from settingsd.drivers.stm_communication_settings import Controllers
from settingsd.drivers.stm_user_panel_settings import UserPanelViews
from tests.mocks.drives_mock import DriversMock


class STMUserPanelMock(DriversMock):
    def __init__(self):
        super().__init__('STMUserPanelMock')
        self.commands = [UserPanelGetPin(), UserPanelGetStates(),
                         UserPanelSetView(UserPanelViews.CARGO_CORRECT)]

    def subscribe(self):
        self.client.subscribe(Controllers.STM_USER_PANEL + '/#')
