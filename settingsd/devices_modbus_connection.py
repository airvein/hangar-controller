import os

from hangar_controller.devices.motors.modbus_interface.modbus_utils import Connection
from settingsd.motors.modbus_constants import Wobit
from settingsd.weather_station import modbus_constants as weather_station_modbus_constants

WEATHER_STATION_CONNECTION = Connection(
    baudrate=9600, stop_bits=1, parity='E', timeout=1, device_address=1,
    port='UNKNOWN')

WOBIT_BATTERIES_CONNECTION = Connection(
    baudrate=38400, stop_bits=1, parity='N', timeout=1, device_address=1,
    port='UNKNOWN')

WOBIT_POSITIONING_CONNECTION = Connection(
    baudrate=38400, stop_bits=1, parity='N', timeout=1, device_address=1,
    port='UNKNOWN')

PORT_0 = str(os.environ.get('MODBUS_USB_PORT_0', '/dev/ttyUSB0'))
PORT_1 = str(os.environ.get('MODBUS_USB_PORT_1', '/dev/ttyUSB1'))
PORT_2 = str(os.environ.get('MODBUS_USB_PORT_2', '/dev/ttyUSB2'))

POSSIBLE_PORTS = [PORT_0, PORT_1, PORT_2]
WOBIT_BATTERIES_PORT_CHECK_NUMBER = Wobit.UserRegisters.WOBIT_ID
WOBIT_BATTERIES_EXPECTED_RESPONSE = int('1111111100000000', 2)
WOBIT_POSITIONING_PORT_CHECK_NUMBER = Wobit.UserRegisters.WOBIT_ID
WOBIT_POSITIONING_EXPECTED_RESPONSE = int('11111111', 2)
WEATHER_STATION_PORT_CHECK_NUMBER = weather_station_modbus_constants.HoldingRegisters.DEVICE_STATE
WEATHER_STATION_EXPECTED_RESPONSE = 23


class DeviceID:
    WOBIT_POSITIONING = 'wobit_positioning'
    WOBIT_BATTERY = 'wobit_battery'
    WEATHER_STATION = 'weather_station'
    WOBITS = [WOBIT_POSITIONING, WOBIT_BATTERY]


class PortPair:
    def __init__(self, connection: Connection, register_number: int, expected_response: int,
                 name: str):
        self.connection = connection
        self.register_test_address = register_number
        self.expected_response = expected_response
        self.name = name


WOBIT_BATTERIES_PORT_PAIR = PortPair(WOBIT_BATTERIES_CONNECTION, WOBIT_BATTERIES_PORT_CHECK_NUMBER,
                                     WOBIT_BATTERIES_EXPECTED_RESPONSE, DeviceID.WOBIT_BATTERY)
WOBIT_POSITIONING_PORT_PAIR = PortPair(WOBIT_POSITIONING_CONNECTION,
                                       WOBIT_POSITIONING_PORT_CHECK_NUMBER,
                                       WOBIT_POSITIONING_EXPECTED_RESPONSE, DeviceID.WOBIT_POSITIONING)
WEATHER_STATION_PORT_PAIR = PortPair(WEATHER_STATION_CONNECTION, WEATHER_STATION_PORT_CHECK_NUMBER,
                                     WEATHER_STATION_EXPECTED_RESPONSE, DeviceID.WEATHER_STATION)

DEVICES_PORT_PAIRS = {
    DeviceID.WOBIT_BATTERY: WOBIT_BATTERIES_PORT_PAIR,
    DeviceID.WOBIT_POSITIONING: WOBIT_POSITIONING_PORT_PAIR,
    DeviceID.WEATHER_STATION: WEATHER_STATION_PORT_PAIR
}
