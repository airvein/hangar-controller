class UserPanelControlCommands:
    CARGO_PIN = 'cargo_pin'
    PIN_TIMEOUT = 'timeout'
    CONFIRMATION_VIEW = 'confirmation_view'
    VIEW = 'view'
    BUTTON_PRESS_PAYLOAD = 'true'
    BUTTON_PRESS_TIMEOUT = 'timeout'
    SET_INTERACTIVE_VIEW = 'set'


class UserPanelViews:
    CARGO_TAKEOUT = 'cargo_take_out'
    WINDOW_OPENING = 'window_opening'
    WINDOW_CLOSING = 'window_closing'
    CARGO_PIN_CHECK = 'cargo_pin_check'
    CARGO_PIN_CORRECT = 'cargo_pic_correct'
    CARGO_PIN_INCORRECT = 'cargo_pin_incorrect'
    CARGO_PIN_TIMEOUT = 'cargo_pin_timeout'
    CARGO_CORRECT = 'cargo_correct'
    CARGO_INCORRECT = 'cargo_incorrect'
    PREPARING_MISSION = 'preparing_mission'
    HANGAR_BREAKDOWN = 'hangar_breakdown'
    SERVICE_MODE = 'service_mode'
    CHECKING_CARGO = 'checking_cargo'
    THANK_YOU = 'thank_you'


class ConfirmationViews(UserPanelViews):
    CARGO_PIN = 'enter_cargo_pin'
    CARGO_PUT = 'conf_cargo_put'
    CARGO_TAKE_OUT = 'conf_cargo_take_out'
    HOME = 'conf_home'


class UserPanelStates:
    CARGO_PIN = UserPanelControlCommands.CARGO_PIN
    VIEW = UserPanelControlCommands.VIEW
    CONFIRMATION_VIEW = 'confirmation_view'
    USER_CONFIRMATION = 'user_confirmation'


class UserPanelTimeouts:
    CONFIRMATION_TIMEOUTS = {
        ConfirmationViews.CARGO_PUT: 80,
        ConfirmationViews.CARGO_TAKE_OUT: 80,
        ConfirmationViews.HOME: 99999
    }
    CARGO_PIN = 65
    VIEW = 20


def get_confirmation_timeout(view: ConfirmationViews):
    try:
        return UserPanelTimeouts.CONFIRMATION_TIMEOUTS[view]
    except KeyError:
        raise NotImplementedError(f'View {view} does not have specified '
                                  f'timeout')
