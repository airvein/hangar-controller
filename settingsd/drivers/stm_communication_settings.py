from hangar_controller.devices.drivers.messages.errors.error_code import ErrorCodes
from hangar_controller.devices.drivers.messages.warnings.warning_code import WarningCodes

ACK_TIMEOUT = 10
MULTIPLE_RESPONSE_TIMEOUT = 10
HEARTBEAT_TICK_TIME = 10
PIN_LENGTH = 5
PIN_TIMEOUT = 10
PLC_RESET_TIMEOUT = 35


class ClientNames:
    DRIVERS_CONTROLLER = 'DriversController'


class ControllerErrorCodes:
    STM_ROOF = ErrorCodes(peripheral='RFEPE', command_control='RFWCC')
    STM_CLIMATE = ErrorCodes(peripheral='CLEPE', command_control='CLECC',
                             telemetry='CLETE')
    STM_CARGO = ErrorCodes(peripheral='CGEPE', command_control='CGECC')
    STM_PMS = ErrorCodes(peripheral='PMEPE', command_control='PMEPE',
                         telemetry='PMETE')
    STM_USER_PANEL = ErrorCodes(peripheral='UPEPE', command_control='UPECC')
    STM_MASTER_CHARGER = ErrorCodes()


class ControllerWarningCodes:
    STM_ROOF = WarningCodes(command_control='RFWCC')
    STM_CLIMATE = WarningCodes(peripheral='CLWPE', command_control='CLWCC',
                               telemetry='CLWTE')
    STM_CARGO = WarningCodes(telemetry='CGWTE', command_control='CGWCC')
    STM_PMS = WarningCodes(command_control='PMWPE')
    STM_USER_PANEL = WarningCodes()
    STM_MASTER_CHARGER = WarningCodes()


class Controllers:
    STM_ROOF = 'roof_ctrl'
    STM_CLIMATE = 'climate_ctrl'
    STM_CARGO = 'cargo_ctrl'
    STM_PMS = 'pms_ctrl'
    STM_USER_PANEL = 'user_panel_ctrl'
    STM_MASTER_CHARGER = 'master_charger_ctrl'
    WOBIT = 'wobit'


class SubControllers:
    CLIMATE = 'climate'
    PLC = 'plc'
    BATTERY = 'battery'
    CHARGER = 'charger'
    BATTERIES = ['1', '2', '3']


class Common:
    CALL = 'call'
    COMMAND = 'cmd'
    STATE = 'state'
    RESP = 'resp'
    VAL = 'val'
    SET = 'set'
    DIAGNOSTICS = 'diagnostics'
    ERRORS = 'errors'
    WARNINGS = 'warnings'
    HEARTBEAT = 'heartbeat'
    TELEMETRY = 'telemetry'
    PARAMETERS = 'parameters'
    READ = 'read'
    INPUT = 'input'
    OUTPUT = 'output'
    PIN = 'pin'


class CommandMessage:
    GET = 'get'
    SET = 'set'
    TICK = 'tick'
    GET_STATES = 'get_states'
    GET_PARAMETERS = 'get_parameters'
    GET_PLC_IO = 'get_plc_io'
    GET_CHARGERS_NUMBER = 'get_chargers_num'
    GET_TELEMETRY = 'get_telemetry'
    ENABLE = 'enable'
    DISABLE = 'disable'
    PLC_RESET = 'plc_reset'
    CTRL_RESET = 'ctrl_reset'
    RESET = 'reset'
    CONFIRM = 'confirm'


class ResponseMessage:
    ACK = 'ack'
    REFUSED = 'refuse'
    WRONG_VALUE = 'wrong_value'
    ENABLE = 'enable'
    DISABLE = 'disable'
    TOCK = 'tock'
    EVALUATION = 'evaluation'
    EVALUATION_REASON = 'evaluation_reason'
    RESPONSES = 'responses'
    TOPIC = 'topic'
    PAYLOAD = 'payload'
    TIMEOUT = 'timeout'
    CANCELLED = 'cancelled'
    UNEXPECTED_RESPONSE = 'unexpected_response'
    NEGATIVE_RESPONSE = 'negative_response'
    VALUE_RESPONSE_NOT_FLOAT = 'value_response_not_float'
    LIST_RESPONSE_WRONG_VALUES = 'list_response_wrong_values'
    PCIO_WRONG_VALUES = 'pcio_wrong_values'
    PCIO_WRONG_LENGTH = 'pcio_wrong_length'


class Quantity:
    TEMPERATURE = 'temperature'
    HUMIDITY = 'humidity'
    TIME = 'time'
