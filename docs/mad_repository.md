# Motors and Drivers Repository

It's a repository of lazily-initialized objects that can call upon each other
during construction.

Sample Motor should look like this:

```python
from hangar_controller.mad_repository import register
@register('rs232_wobbit')
class MyClass:
    def __init__(self, rs485_wobbit):
        pass
```

This will also initialize an class registered as rs485_wobbit and pass
it's only instance to this class. So, a constructor is examined for 
it's arguments and every one is passed as a MAD class.

## Unit tests

It is possible to mock given classes. To do this, do the following:

```python
from hangar_controller.mad_repository import MADRepository

def testCase(self):
    with MADRepository.mock('rs485_wobbit', RS485WobbitMockClass) as rs485_wobbitmockclass:
        pass
```
